var amqp = require('amqplib');
var moment = require('moment-timezone');
var mongoose = require('mongoose');
var crypto = require('crypto');
var uuid = require('node-uuid');

var MACADDR = "80:3f:5d:00:00:11";
var WLANMACADDR = "80:3f:5d:00:00:12";
var UID = "WLINKFW_803f5d000011";
var GID = "WLINK_A0000";
var system_UID = "SYSTEM_WLINK_A0001_DAEMON";
var system_rpc_key = "rpc_" + GID;
var waviot_version = "1.0";
var waviot_hashkey = "ea15b9b61f8947c8d2004e0c6033d8bc";

var device_cid = "gwdevice1";
var device_key = "2a4e373bf192f8345f34";
var device_secret = "68261a0c35b13969ef82177381598f98b38d64d9";

var bwCheck_enabled = false;
var bwCheck_duration = 30;
var bwCheck_counter = 0;
var heartbeat_duration = 60*1; // 10 min

if(process.argv[2]!=null)
{
    var MACADDR = process.argv[2].match(/.{1,2}/g).join(':');
    var newmac = parseInt(process.argv[2], 16)+1; 
    WLANMACADDR = newmac.toString(16).match(/.{1,2}/g).join(':');
    UID = "WLINKGW_" + process.argv[2];
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

function make_waviot_msg(msgID, groupID, deviceID, ipAddress, cid, clientKey) {
  var waviot_msg = {waviot: waviot_version};
  var waviot_header = {msgID: msgID, msgTime: moment().format('YYYYMMDDHHmmssZZ') , groupID: groupID, deviceID: deviceID}; 
  if(ipAddress!=null && ipAddress!="")
      waviot_header.ipAddress = ipAddress;
 
  if(cid!=null && cid!="")
      waviot_header.cid = cid;

  if(clientKey!=null && clientKey!="")
      waviot_header.clientKey = clientKey;

  waviot_msg.header = waviot_header;
  return waviot_msg
}

function make_body_string(body)
{
    var bodystr = "";
    for (var i in body) {
        if (body[i] !== null && typeof(body[i])=="object") {
            bodystr += make_body_string(body[i]);
        }
        else
            bodystr += body[i].toString();
    }

    return bodystr;
}

function make_waviot_hashcode(body, encoding, hash_key) {
    var bodystr = make_body_string(body);
    var hmac = crypto.createHmac("md5", hash_key);
    hmac.update(bodystr);
    return hmac.digest(encoding);
} 

function make_error_body(errno) {
  var error_msg = {errno: errno};
  return error_msg
}

function make_error_msg(errno) {
  var newmsg = make_waviot_msg("error", GID, UID, null);
  newmsg.body  = make_error_body(errno);
  newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64", waviot_hashkey);
}

function make_resHeartbeat_body(targetUID, response)
{
  var resHeartbeat_msg = {targetUID: targetUID, systemTime: moment().format('YYYYMMDDHHmmssZZ')  };
  if(response!=null && response!="")
    resHeartbeat_msg.response = response;
  return resHeartbeat_msg
}

function make_reqHeartbeat_body(targetUID, challenge)
{
    var reqHeartbeat_msg = {targetUID: targetUID, systemTime: moment().format('YYYYMMDDHHmmssZZ')  };
    reqHeartbeat_msg.challenge = challenge;
    return reqHeartbeat_msg
}

// system daemon과는 service hash key로 통신
function msg_req_heartbeat(target_uid, device_uid, challenge)
{
    var newmsg = make_waviot_msg("reqHeartbeat", GID, device_uid, null, device_cid, device_key)
    newmsg.body = make_reqHeartbeat_body(target_uid, challenge);    
    newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64", waviot_hashkey);

    return newmsg;
}

var heartbeat_challenge = 1;

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
var attrWanSetting = {
    "type":"1", // 1: dhcp, 2:pppoe, 3:static
    "username":"hello",
    "password":"12345678",
    "ipAddress":"9.9.10.11",
    "subnetMask":"255.0.0.0",
    "gateway":"9.9.10.1",
    "dnsPrimary":"8.8.8.8",
    "dnsSecondary":"9.9.9.9",
    "macAddress":MACADDR,
    "mtu": "1500"
};

var attrWanState = 2;   // 0 : no cable, 1 : not connected, 2: connected
var attrWanDlSpeed = 0;
var attrWanUpSpeed = 0;
var attrWanDlbytes = 0;
var attrWanUpbytes = 0;

var attrWlanSetting24g = {
    "enabled":"1", 
    "mode":"bgn",
    "bssid":WLANMACADDR, 
    "ssid":"WLINK24g",
    "ssidBroadcast":"1"
};
 
var attrWlanSecurity24g = {
    "enabled":"1", 
    "type": "WPA",
    "encryption":"AES", 
    "passphrase":"12345678" 
};

////////////////////////////////////////////////////////////////////////////////////////////////////
// message handler
function invalid_attr_msg(msgobj)
{
    var resBody = {resCode:-1001,attrName:"none", result:{resString:"invalid message format"}};

    if(msgobj.attrName!=null && msgobj.attrName)
        resBody.attrName = msgobj.attrName;

    return resBody;    
}


function msg_set_attr_handler(msgobj)
{
    var newmsg = make_waviot_msg("setAttr", GID, UID, null, device_cid, device_key)
    newmsg.body = {attrName: "msg_set_attr_handler", systemTime: moment().format('YYYYMMDDHHmmssZZ')  };    
    newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64", device_secret);
    
    return newmsg;
}

function get_attr_wan_setting(msgobj)
{
    var wanSetting = {value:attrWanSetting, when:moment().format('YYYYMMDDHHmmssZZ')};

    //테스트를 위해서 실시간 갱신이 필요하면 여기서 

    var resBody = {resCode:0,attrName:msgobj.attrName, result:{wanSetting:wanSetting}};

    return resBody;
}

function get_attr_wan_state(msgobj)
{
    var wanState = {value:attrWanState, when:moment().format('YYYYMMDDHHmmssZZ')};

    //테스트를 위해서 실시간 갱신이 필요하면 여기서 
    
    var resBody = {resCode:0,attrName:msgobj.attrName, result:{wanState:wanState}};

    return resBody;
}

function get_attr_wan_dl_speed(msgobj)
{
    var wanDlSpeed = {value:attrWanDlSpeed, when:moment().format('YYYYMMDDHHmmssZZ')};

    //테스트를 위해서 실시간 갱신이 필요하면 여기서 
    
    var resBody = {resCode:0,attrName:msgobj.attrName, result:{wanDlSpeed:wanDlSpeed}};

    return resBody;
}

function get_attr_wan_up_speed(msgobj)
{
    var wanUpSpeed = {value:attrWanUpSpeed, when:moment().format('YYYYMMDDHHmmssZZ')};

    //테스트를 위해서 실시간 갱신이 필요하면 여기서 
    
    var resBody = {resCode:0,attrName:msgobj.attrName, result:{wanUpSpeed:wanUpSpeed}};

    return resBody;
}

function get_attr_wan_dl_bytes(msgobj)
{
    var wanDlbytes = {value:attrWanDlbytes, when:moment().format('YYYYMMDDHHmmssZZ')};

    //테스트를 위해서 실시간 갱신이 필요하면 여기서 
    
    var resBody = {resCode:0,attrName:msgobj.attrName, result:{wanDlbytes:wanDlbytes}};

    return resBody;
}

function get_attr_wan_up_bytes(msgobj)
{
    var wanUpbytes = {value:attrWanUpbytes, when:moment().format('YYYYMMDDHHmmssZZ')};

    //테스트를 위해서 실시간 갱신이 필요하면 여기서 
    
    var resBody = {resCode:0,attrName:msgobj.attrName, result:{wanUpbytes:wanUpbytes}};

    return resBody;
}

function get_attr_wlan_setting_24g(msgobj)
{
    var wlanSetting24g = {value:attrWlanSetting24g, when:moment().format('YYYYMMDDHHmmssZZ')};

    //테스트를 위해서 실시간 갱신이 필요하면 여기서 
    
    var resBody = {resCode:0,attrName:msgobj.attrName, result:{wlanSetting24g:wlanSetting24g}};

    return resBody;
}

function get_attr_wlan_security_24g(msgobj)
{
    var wlanSecurity24g = {value:attrWlanSecurity24g, when:moment().format('YYYYMMDDHHmmssZZ')};

    //테스트를 위해서 실시간 갱신이 필요하면 여기서 
    
    var resBody = {resCode:0,attrName:msgobj.attrName, result:{wlanSecurity24g:wlanSecurity24g}};

    return resBody;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
function msg_get_attr_handler(msgobj)
{
    var newmsg = make_waviot_msg("getAttr", GID, UID, null, device_cid, device_key);
    try {
        if(msgobj.attrName==null)
        {
            newmsg.body = invalid_attr_msg(msgobj);   
        }
        else
        switch(msgobj.attrName.toLowerCase())
        {
            case "wansetting":
                newmsg.body = get_attr_wan_setting(msgobj);
                break;

            case "wanstate":
                newmsg.body = get_attr_wan_state(msgobj);
                break;

            case "wandlspeed":
                newmsg.body = get_attr_wan_dl_speed(msgobj);
                break;

            case "wanupspeed":
                newmsg.body = get_attr_wan_up_speed(msgobj);
                break;

            case "wandlbytes":
                newmsg.body = get_attr_wan_dl_bytes(msgobj);
                break;

            case "wanupbytes":
                newmsg.body = get_attr_wan_up_bytes(msgobj);
                break;

            case "wlansetting24g":
                newmsg.body = get_attr_wlan_setting_24g(msgobj);
                break;

            case "wlansecurity24g":
                newmsg.body = get_attr_wlan_security_24g(msgobj);
                break;
                
            default:
                newmsg.body = invalid_attr_msg(msgobj);
                break;
        }
        
    } catch (error) {
        console.log(">>>> msg_get_attr_handler error!! " + error);
        var errmsg = make_error_msg(100);
        return errmsg;
    }

    newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64", device_secret);
    
    return newmsg;
}
function msg_add_attr_handler(msgobj)
{
    var newmsg = make_waviot_msg("addAttr", GID, UID, null, device_cid, device_key)
    newmsg.body = {attrName: "msg_add_attr_handler", systemTime: moment().format('YYYYMMDDHHmmssZZ')  };    
    newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64", device_secret);
    
    return newmsg;
}

function msg_del_attr_handler(msgobj)
{
    var newmsg = make_waviot_msg("delAttr", GID, UID, null, device_cid, device_key)
    newmsg.body = {attrName: "msg_del_attr_handler", systemTime: moment().format('YYYYMMDDHHmmssZZ')  };    
    newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64", device_secret);
    
    return newmsg;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// predefined service 
var demo_id = "admin";
var demo_password = "12345678";
var demo_session_key = uuid.v1();
function authDevice(args)
{
    var resBody = {resCode:0,serviceName:"authDevice", result:""};
    if(args.id!=demo_id || args.password!=demo_password)
    {
        resBody.resCode = -1;  
    }
    else
    {
        resBody.result = {session_key:demo_session_key};
    }

    if(args.transNo)
        resBody.transNo = args.transNo;

    return resBody;
}

function bwCheck(args)
{
    var resBody = {resCode:0,serviceName:"bwCheck", result:""};
    try {
        bwCheck_enabled = parseInt(args.enabled);
        bwCheck_duration = parseInt(args.duration);
    } catch (error) {
        resBody.resCode = -1;          
    }
    return resBody;
}

function getSubdeviceList(args)
{
    var resBody = {resCode:0,serviceName:"getSubdeviceList", result:""};
    try {
        var result = { when: moment().format('YYYYMMDDHHmmssZZ')}
        var subdevices = [{name:"PC", mac :"80:3F:5D:11:22:33", ip:"192.168.0.1" }];
        result.subdevices = subdevices;
        resBody.result = result;``
    } catch (error) {
        resBody.resCode = -1;
    }
    return resBody;
}

function service_not_defined(msgobj)
{
    var resBody = {resCode:-1000,serviceName:msgobj.serviceName, result:{resString:"cannot find the service"}};
    if(msgobj.args.transNo!=null && msgobj.args.transNo)
        resBody.transNo = msgobj.args.transNo;
    return resBody;    
}

function service_invalid_message(msgobj)
{
    var resBody = {resCode:-1001,serviceName:"none", result:{resString:"invalid message format"}};

    if(msgobj.serviceName!=null && msgobj.serviceName)
        resBody.serviceName = msgobj.serviceName;

    if(msgobj.args.transNo!=null && msgobj.args.transNo)
        resBody.transNo = msgobj.args.transNo;
    return resBody;    
}

function msg_call_service_handler(msgobj)
{
    var newmsg = make_waviot_msg("callService", GID, UID, null, device_cid, device_key);
    var hash_key = device_secret;
    try {
        if(msgobj.serviceName==null || msgobj.request==null || msgobj.request.args==null)
        {

            newmsg.body = service_invalid_message(msgobj);   
        }
        else
        switch(msgobj.serviceName.toLowerCase())
        {
            case "authdevice":
                // authdevice는 아직 로그인 전으로 serviec hash key를 사용
                newmsg.body = authDevice(msgobj.request.args);
                hash_key = waviot_hashkey;
                break;
            case "bwcheck":
                newmsg.body = bwCheck(msgobj.request.args);
                break;
            case "getsubdevicelist":
                newmsg.body = getSubdeviceList(msgobj.request.args);
                break;
            default:
                newmsg.body = service_not_defined(msgobj);
                break;
        }
        
    } catch (error) {
        console.log(">>>> msg_call_service_handler error!! " + error);
        var errmsg = make_error_msg(100);
        return errmsg;
    }

    newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64", hash_key);
    
    return newmsg;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//amqp.connect('amqps://wlink:wlink%4020161022@121.199.26.216/wlink.wavlink').then(function(conn) {
//amqp.connect('amqps://wlink:wlink%4020180731@wsocktest.wavlink.org/wlink.wavlink').then(function(conn) {
amqp.connect('amqps://wlink:wlink%4020180731@13.115.67.32/wlink.wavlink').then(function(conn) {
  return conn.createChannel().then(function(ch) {
    var queue = 'queue_'+UID;
    var ok = ch.assertQueue(queue, {autoDelete:true, durable: false});
    var ok = ch.bindQueue(queue, "wlink.direct", UID);
    var ok = ok.then(function() {
      setInterval(heatbeat_handler, 1000*heartbeat_duration);
      setInterval(bandwidth_handler, 1000);
      //ch.prefetch(1);
      return ch.consume(queue, device_consumer, {noAck: true});
    });
    return ok.then(function() {
      console.log('>>>>> DEVICE ID :' + UID  + '. Wait the requests');      
      console.log('>>>>> MACADDR [%s] WLANMAC[%s]', MACADDR, WLANMACADDR);      
      console.log('>>>>> SESSION Key :' + demo_session_key);      
    });

    function device_consumer(msg) {
        try {
            var msgobj = JSON.parse(msg.content.toString());

            //console.log("device_consumer : ["+JSON.stringify(msgobj)+"]" );
            if(msgobj.header!=null && msgobj.body!=null){
                // check msg validation
                var newmsg_hash = make_waviot_hashcode(msgobj.body, "base64", waviot_hashkey);
                //if(newmsg_hash==msgobj.header.hashcode)
                {   
                    var res = null;
                    switch(msgobj.header.msgID)
                    {
                        case "resHeartbeat":
                            console.log(">>> RES.heartbeat at %s: response:%d",msgobj.header.msgTime, msgobj.body.response);
                            break;
                        case "setAttr":
                            console.log(">>> setAttr at %s", msgobj.header.msgTime);
                            res = msg_set_attr_handler(msgobj.body);
                            break;
                        case "getAttr":
                            console.log(">>> getAttr at %s", msgobj.header.msgTime);
                            res = msg_get_attr_handler(msgobj.body);
                            break;
                        case "addAttr":
                            console.log(">>> addAttr at %s", msgobj.header.msgTime);
                            res = msg_add_attr_handler(msgobj.body);
                            break;
                        case "delAttr":
                            console.log(">>> delAttr at %s", msgobj.header.msgTime);
                            res = msg_del_attr_handler(msgobj.body);
                            break;
                        case "callService":
                            console.log(">>> callService at %s", msgobj.header.msgTime);
                            res = msg_call_service_handler(msgobj.body);
                            console.log("   msg_call_service_handler:" + JSON.stringify(res));
                            break;
                        default:
                            break;
                    }

                    if(res!=null)
                    {
                        console.log("   RES:" + JSON.stringify(res));
                        ch.publish("wlink.direct", msg.properties.replyTo, new Buffer(JSON.stringify(res)), 
                            {contentType: "application/json", deliveryMode:2, correlationId: msg.properties.correlationId});
                    }
                    else
                        console.log(">>>>> device_consumer/DUMMY NO RESPONSE");

                }    
                //else
                //{
                //    console.log("HASH CODE ERROR!!" + "["+newmsg_hash+"]" + "["+msgobj.header.hash+"]");
                //}

            }
            else
            {
                console.log("MSG FORMAT ERROR!!");
            }

            /*
        var n = parseInt(msg.content.toString());
        console.log(' [.] fib(%d)', n);
        var response = fib(n);
        ch.sendToQueue(msg.properties.replyTo,
                        new Buffer(response.toString()),
                        {correlationId: msg.properties.correlationId});
        ch.ack(msg);
        */
            


        } catch (error) {
            console.log("device_consumer:" + error);
        }
    }

    function toFixed(x)
    {
        return Math.round (x*100) / 100; 
    }

    function heatbeat_handler() {
        var reqHeartbeat = msg_req_heartbeat(system_UID, UID, heartbeat_challenge);
        heartbeat_challenge += 1;
        var res = ch.publish("wlink.direct", system_rpc_key, new Buffer(JSON.stringify(reqHeartbeat)), 
                  {contentType: "application/json", deliveryMode:2, correlationId: "corrId", replyTo: UID});
        console.log("================================================================================");
        console.log("heartbeat: ["+ JSON.stringify(reqHeartbeat) + "]");

/*
        var newmsg = make_waviot_msg("reportAttr", GID, UID, null)
        newmsg.body = {msg: "reportAttr", systemTime: moment().format('YYYYMMDDHHmmssZZ')  };    
        newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64");

        ch.publish("wlink.direct", demo_session_key, new Buffer(JSON.stringify(newmsg)), 
                  {contentType: "application/json", deliveryMode:2});

        console.log("--------------------------------------------------------------------------------");
        console.log("reportAttr: ["+ JSON.stringify(newmsg) + "]");
        console.log("DL: %sKbps, UP: %sKbps  / Total DL: %sMbps UP: %sMbps", 
                (attrWanDlSpeed/1024).toFixed(2), (attrWanUpSpeed/1024).toFixed(2), 
                (attrWanDlbytes/1024/1024).toFixed(2), (attrWanUpbytes/1024/1024).toFixed(2));
                */
    }

    function randomInt (low, high) {
        return Math.floor(Math.random() * (high - low) + low);
    }
    function bandwidth_handler()
    {
        unit = randomInt(0, 100)%3;  // 0:byte, 1:kb
        randomnUp = randomInt(0, 100)*unit;
        if(unit==2) randomnUp *=1024;
        unit = randomInt(0, 100)%3;  // 0:byte, 1:kb
        randomnDown = randomInt(0, 100)*unit;
        if(unit==2) randomnDown *=1024;
        
        attrWanDlSpeed = randomnDown;
        attrWanUpSpeed = randomnUp;
        attrWanDlbytes += randomnDown;
        attrWanUpbytes += randomnUp;
        if(bwCheck_enabled==true)
        {
            bwCheck_counter++;
            if(bwCheck_counter%bwCheck_duration==0)
            {
                bwCheck_counter=0;
                var newmsg = make_waviot_msg("reportAttr", GID, UID, null, device_cid, device_key);
                newmsg.body = {msg: "reportAttr", when: moment().format('YYYYMMDDHHmmssZZ')  };   
                newmsg.body.attributes = {
                    wanDlSpeed:attrWanDlSpeed, wanUpSpeed:attrWanUpSpeed,
                    wanDlbytes:attrWanDlbytes, wanUpbytes:attrWanUpbytes   
                };
                newmsg.header.hashcode = make_waviot_hashcode(newmsg.body, "base64", device_secret);
                ch.publish("wlink.direct", demo_session_key, new Buffer(JSON.stringify(newmsg)), 
                        {contentType: "application/json", deliveryMode:2});

                console.log("--------------------------------------------------------------------------------");
                console.log("reportAttr: ["+ JSON.stringify(newmsg) + "]");
            }

        }
    }
       
  });
}).catch(console.warn);

