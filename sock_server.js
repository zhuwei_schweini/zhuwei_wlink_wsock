// server.js
// alex@win-star.com
// Date : 2016.11.12

var express = require('express');
var mongoose = require('mongoose');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var Redis = require('ioredis');
var cluster = require('cluster');
var os = require('os');
var sticky = require('sticky-session');
var https  = require('https');
var fs = require('fs');
var path = require('path');
//var config = require(path.join(__dirname,'config'));
var bodyParser = require('body-parser');
var methodOverridde = require('method-override');
var configSchema = require('./models/config.js');

var log_level;
var io_port;
var name;
var io_service;

// mongoose.Promise = global.Promise;
///////////////////////////////////////////////////////////////////////////////
// load configuration
var service_type = process.env.WLINK_SVC;
//global.config = config.real;

if(service_type!=null && service_type!="")
{
    console.log("sock_server >>> WLINK SYSTEM :", service_type);
    //global.config = config[service_type];
}
else 
    console.log(">>> use REAL configuration!!!");

///////////////////////////////////////////////////////////////////////////////

//global.config.path = path.join(__dirname);

var redisClient = new Redis.Cluster(process.env.REDIS);
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

process.on('uncaughtException', function (err) {
	logger.info('uncaughtException 발생 : ' + err);
    //f(err.toString().indexOf("ETIMEDOUT")!=-1)
    // 그냥 재시작

    if(err.toString().indexOf("TypeError")==-1)
    process.exit();
});

var Logger = require('./modules/logger');

var logger = new Logger('socket-io');


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////


async function connect_mongodb() {
    // DB Connection
    try {
        var db = mongoose.connection;
        mongoose.Promise = global.Promise;

        db.once("open", function() {
            logger.info("sock server DB is connected");
        });

        await mongoose.connect(process.env.DB, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false });

        if (process.env.NODE_ENV == "development")
            configName = "localtest";
        else if (process.env.NODE_ENV == "testing")
            configName = "sandbox";
        else if (process.env.NODE_ENV == "production")
            configName = "real";

        await configSchema.findOne({"name":'sandbox'}, function(err, result) {
            if (err) {
                console.log("findOne err : " + err);
                return;
            }
            console.log("findOne log_level : " + result.log_level);
            console.log("findOne io_service.length : " + result.io_service.length);
            console.log("findOne process.env.REDIS : " + process.env.REDIS);
            log_level = result.log_level;
            io_port = result.io_port;
            name = result.name;
            io_service = result.io_service;

            logger.level = log_level; //'debug';
            global.logger = logger;
        });

        await start_amqp_service(io_service, 0, io_service.length);
    } catch (error) {
        logger.info("DB Error: " + error)
    }
    
}

/////////////////////////////////////////////////////////////////////////////
// SERVICE SITE
var app_service = express();
var ssl_options = {
    key: fs.readFileSync('/etc/letsencrypt/live/wsocktest.wavlink.org-0001/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/wsocktest.wavlink.org-0001/cert.pem'),
    requestCert: false,
    rejectUnauthorized: false    
};
var http_service = https.createServer(ssl_options, app_service);
app_service.set('view engine', 'ejs');
app_service.use(express.static(path.join(__dirname, 'public')));
app_service.set('views', path.join(__dirname, 'views'));
app_service.use(session({
    secret: 'ilovewinstarswavlinkwlink-sock',
    store: new RedisStore({client: redisClient}),
    resave: false,
    saveUninitialized: true,
    ttl : 10*60 // 10 mins
}));
 
idx = 0;
var amqp_conns = [];

async function start_amqp_service(io_service, idx, length)
{
    var new_amqp = new require('amqplib');

    logger.info('>>>>> CONFIGURATION: ' + name);

    //logger.info("AMQP["+idx+"] : " + io_service[idx].amqp);
    new_amqp.connect(io_service[idx].amqp).then(function(conn) {
        logger.info("connected["+idx+"] with [%s] [%s]",  io_service[idx].ns, io_service[idx].exchange );
        var io_amqp = { conn:conn, ns:io_service[idx].ns, exhg:io_service[idx].exchange};
        amqp_conns.push(io_amqp);

        if(length==amqp_conns.length){

            sticky.listen(http_service, io_port, function()
            {
                logger.info('Server listening at port %d', io_port);
            });    
            var io = require('./sockets/io.session.js').listen(http_service, amqp_conns);
                        
        }
        else
            start_amqp_service(io_service, idx+1, length);
    }).catch (error => {
        logger.error("Failed to connected["+idx+"] with [%s] [%s]",  io_service[idx].ns, io_service[idx].exchange );
        logger.error("Failed to connected["+idx+"] error [%s]",  error );
    });
}

connect_mongodb();

