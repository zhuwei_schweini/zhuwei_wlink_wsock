var mongoose = require('mongoose');
var moment = require('moment-timezone');

var waviotUtil = require("../modules/waviotUtil");
var petExtUtil = require("./petExtUtil");
var petStore = require('../models/petStore');

var logger = global.logger;
var log_module = "ExtPET";

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Add new pet store or update the store informaiton
//   sample =   {
//       "service_code" : "WLINK_XXXXX",
//       "api_key":"admin_api_key",
//       "time":"20181123074943+0800",
//       "ext":"ExtPetsV1",
//       "command" : "putpetstore",
//       "auth": "5638822878d5369485b5ae72675026f6",

//       "source" : "baidu",
//       "source_uuid" : "baidu_uuid1",
//       "name" : "Wavlink Petshop",
//       "geo_longitude" : 36.098948,
//       "geo_latitude" : -112.110492,
//       "petshop" : true,
//       "pethotel" : true,
//       "pethospital" : false,
//       "petgrooming" : true,
//       "petbreeding" : false,


//       "country" : "China",
//       "province" : "GuangDong",    
//       "city" : "ShenZhen",
//       "address" : "Gongye Rd, LongHuaZhen, Baoan Qu, Shenzhen, Guangdong, China",
//       "zipcode" : "518109",
//       "contact_no" : "+862-2470-9068",  
//       "business_hours": "09:00-22:00",
//       "homepage" : "http://www.wavlink.com",
//       "parking" : true,
//       "reservation" : false
// }；

async function putNewStore (req, res) 
{
  try {
    var uuid = req.params.uuid;
    var service_code = req.body.service_code;
    var apikey = req.body.api_key;
    var ext_name = req.body.ext;
    var command = req.body.command;
    var auth = req.body.auth;
    var msgtime = req.body.time;
    var msgvalidate = false;
    var source = req.body.source==null ? "" : req.body.source;
    var source_uuid = req.body.source_uuid==null ? "" : req.body.source_uuid;
    var store_name = req.body.name==null ? "" : req.body.name;

    var errormsg = { error:0, status: 0, message:""};
    var resmsg = { retCode:0, message:"OK"};

    if(uuid==null || uuid=="" ||
       service_code==null || service_code=="" || 
       apikey==null || apikey=="" || 
       ext_name==null || ext_name=="" || 
       command==null || command=="")
    {
        errormsg.error = 100;  // input parameter error
        errormsg.message = "Bad Request";
        res.status(400).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, store_name:%s, error:[%s]",
          log_module, command, msgtime, uuid, apikey, source, source_uuid, store_name, JSON.stringify(errormsg));         
        return;
    }

    [ msgvalidate, errormsg ] = await petExtUtil.check_ext_msg_validation(msgtime, uuid, apikey, service_code, ext_name, command, false, auth);

    if(msgvalidate==false)
    {
      res.status(errormsg.status).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, store_name:%s, error:[%s]",
        log_module, command, msgtime, uuid, apikey, source, source_uuid, store_name, JSON.stringify(errormsg)); 
      return;
    }

    // is it admin?
    let is_admin = await waviotUtil.is_admin_account(apicli, service_code, uuid);
    if(is_admin!=true)
    {
      errormsg.error = 110;  // permission error
      errormsg.message = "Unauthorized.";
      errormsg.status = 401;
      res.status(401).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s source:%s, source_uuid:%s, store_name:%s, error:[%s]",
        log_module, command, msgtime, uuid, apikey, source, source_uuid, store_name, JSON.stringify(errormsg));       
      return;
    }

    // do something
    // message validation
    let geo_longitude = req.body.geo_longitude==null ? 0 : req.body.geo_longitude;
    let geo_latitude = req.body.geo_latitude==null ? 0 : req.body.geo_latitude;
    let is_petshop = req.body.petshop==null ? false : req.body.petshop;
    let is_pethotel = req.body.pethotel==null ? false : req.body.pethotel;
    let is_pethospital = req.body.pethospital==null ? false : req.body.pethospital;
    let is_petgrooming = req.body.petgrooming==null ? false : req.body.petgrooming;
    let is_petbreeding = req.body.petbreeding==null ? false : req.body.petbreeding;

    // get optionals
    let description = req.body.description==null ? "" : req.body.description;
    let source_keyword1 = req.body.source_keyword1==null ? "" : req.body.source_keyword1;
    let source_keyword2 = req.body.source_keyword2==null ? "" : req.body.source_keyword2;
    let source_keyword3 = req.body.source_keyword3==null ? "" : req.body.source_keyword3;
    let country = req.body.country==null ? "" : req.body.country;
    let province = req.body.province==null ? "" : req.body.province;
    let city = req.body.city==null ? "" : req.body.city;
    let address = req.body.address==null ? "" : req.body.address;
    let zipcode = req.body.zipcode==null ? "" : req.body.zipcode;
    let contact_no = req.body.contact_no==null ? "" : req.body.contact_no;
    let business_hours = req.body.business_hours==null ? "" : req.body.business_hours;
    let homepage = req.body.homepage==null ? "" : req.body.homepage;
    let parking = req.body.parking==null ? false : req.body.parking;
    let reservation = req.body.reservation==null ? false : req.body.reservation;
    let image_url = req.body.image_url==null ? "" : req.body.image_url;

    logger.info("PUT REQ:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s,\
source:%s, source_uuid:%s, source_keyword1:%s. source_keyword2:%s, source_keyword3:%s, store_name:%s, geo_longitude:%d, geo_latitude:%d, \
petshop:%s, pethotel:%s, pethospital:%s, petgrooming:%s, petbreeding:%s \
country:%s, province:%s, city:%s, address:%s, zipcode:%s, \
contact_no:%s, business_hours:%s, homepage:%s, parking:%s, reservation:%s, image_url:%s", 
                log_module, command, msgtime,  uuid, apikey,
                source, source_uuid, source_keyword1, source_keyword2, source_keyword3, 
                store_name, geo_longitude, geo_latitude, 
                is_petshop.toString(), is_pethotel.toString(), is_pethospital.toString(), is_petgrooming.toString(), is_petbreeding.toString(), 
                country, province, city, address, zipcode, 
                contact_no, business_hours, homepage, parking.toString(), reservation.toString(), image_url); 

    if (source=="" || source_uuid=="" || store_name=="" || geo_longitude==0 || geo_latitude==0 || command!="putpetstore")
    {
      errormsg.error = 100;
      errormsg.message = "Bad Request";
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, store_name:%s, error:[%s]",
          log_module, command, msgtime,  uuid, apikey, source, source_uuid, store_name, JSON.stringify(errormsg));       
      return;
    }

    // find the old information with source information
    let query = {service_code: service_code, "source":source, "source_uuid":source_uuid};
    let storeinfo = await petStore.findOne(query);

    if(storeinfo==null)
    {
      // new store
      var pic_random_id = "storepic_"+waviotUtil.gen_session_key(uuid+apikey);
      var newPetStore = new petStore(
        {
            service_code: service_code, 
            source: source,
            source_uuid: source_uuid,
            source_keyword1 : source_keyword1,
            source_keyword2 : source_keyword2,
            source_keyword3 : source_keyword3,
            
            client_uuid: uuid,  
            client_apikey: apikey,
            last_client_uuid: uuid,  
            last_client_apikey: apikey,
            name : store_name,
            region : {
              country : country, 
              province : province,  
              city : city  
            },
            store_type : [], 
            geo_location : {
              type: "Point",
              coordinates: [geo_longitude, geo_latitude]
            }, 
            description : description, 
            pic_id : pic_random_id,
            pic_key : "",
            image_url : image_url,
            address : address, 
            zipcode : zipcode, 
            contact_no : contact_no, 
            business_hours: business_hours, 
            homepage : homepage,
            service_option : []
        }
      );

      // update store_type
      if(is_petshop==true) newPetStore.store_type.push("petshop");
      if(is_pethotel==true) newPetStore.store_type.push("pethotel");
      if(is_pethospital==true) newPetStore.store_type.push("pethospital");
      if(is_petgrooming==true) newPetStore.store_type.push("petgrooming");
      if(is_petbreeding==true) newPetStore.store_type.push("petbreeding");

      // update service_option
      if(parking==true) newPetStore.service_option.push("parking");
      if(reservation==true) newPetStore.service_option.push("reservation");

      let  update_pet_store = await petStore.findOneAndUpdate(
          query, 
          newPetStore, 
          {upsert: true, new: true, runValidators: true});

      if(update_pet_store==null)
      {
        errormsg.error = 900;
        errormsg.message = "System Error";
        errormsg.status = 500;
        res.status(500).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, store_name:%s, error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, store_name, JSON.stringify(errormsg)); 
        return;
      }

      resmsg.petstore_key=update_pet_store._id.toHexString();
      resmsg.is_new = true;
    }
    else
    {
      // old store information
      storeinfo.name = store_name;
      storeinfo.last_client_uuid = uuid;
      storeinfo.last_client_apikey = apikey;

      storeinfo.source_keyword1  = source_keyword1;
      storeinfo.source_keyword2  = source_keyword2;
      storeinfo.source_keyword3  = source_keyword3;
      storeinfo.region.country = country;
      storeinfo.region.province = province;
      storeinfo.region.city = city;
      storeinfo.geo_location.coordinates = [geo_longitude, geo_latitude];
      
      storeinfo.image_url = image_url;
      storeinfo.description = description;
      storeinfo.address = address;
      storeinfo.zipcode = zipcode;
      storeinfo.contact_no = contact_no;
      storeinfo.business_hours = business_hours;
      storeinfo.homepage = homepage;
      storeinfo.last_updated_date = moment();

      // update store_type
      if(is_petshop==true && storeinfo.store_type.indexOf("petshop") < 0) storeinfo.store_type.push("petshop");
      if(is_pethotel==true && storeinfo.store_type.indexOf("pethotel") < 0) storeinfo.store_type.push("pethotel");
      if(is_pethospital==true && storeinfo.store_type.indexOf("pethospital") < 0) storeinfo.store_type.push("pethospital");
      if(is_petgrooming==true && storeinfo.store_type.indexOf("petgrooming") < 0) storeinfo.store_type.push("petgrooming");
      if(is_petbreeding==true && storeinfo.store_type.indexOf("petbreeding") < 0) storeinfo.store_type.push("petbreeding");

      // update service_option
      if(parking==true && storeinfo.service_option.indexOf("parking") < 0) storeinfo.service_option.push("parking");
      if(reservation==true && storeinfo.service_option.indexOf("reservation") < 0) storeinfo.service_option.push("reservation");

      let update_pet_store = await storeinfo.save();

      if(update_pet_store==null)
      {
        errormsg.error = 900;
        errormsg.message = "System Error";
        errormsg.status = 500;
        res.status(500).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, store_name:%s, error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, store_name, JSON.stringify(errormsg)); 
        return;
      }      

      resmsg.petstore_key=update_pet_store._id.toHexString();
      resmsg.is_new = false;
    }

    res.send(JSON.stringify(resmsg,null,"\t"));
    logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, store_name:%s, response:[%s]",
                log_module, command, msgtime, uuid, apikey, source, source_uuid, store_name, JSON.stringify(resmsg)); 

  } catch (error) {
      errormsg.error = 901;
      errormsg.message = "Bad Request:"+error;
      errormsg.status = 400;
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, store_name:%s, error:[%s]",
          log_module, command, msgtime, uuid, apikey, source, source_uuid, store_name, JSON.stringify(errormsg)); 
  }     
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// get the store informaiton with key
//   sample =   {
//       "service_code" : "WLINK_XXXXX",
//       "api_key":"client_api_key",
//       "time":"20181123074943+0800",
//       "ext":"ExtPetsV1",
//       "command" : "getpetstore",
//       "auth": "5638822878d5369485b5ae72675026f6",
//       "petstore_key" : "key1",
// }；

async function getPetStore (req, res, service_code, uuid, apikey, msgtime, command) 
{
  var errormsg = { error:0, status: 0, message:""}; 
  var resmsg = { retCode:0, message:"OK"};
  var petstore_key = req.body.petstore_key==null ? "" : req.body.petstore_key;

  try {
    // message validation

    logger.info("POST REQ:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s", 
                log_module, command, msgtime, service_code, uuid, apikey, petstore_key); 

    if (petstore_key=="")
    {
      errormsg.error = 100;
      errormsg.message = "Bad Request";
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s, error:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey,  petstore_key, JSON.stringify(errormsg));       
      return;
    }

    // find petstore information
    let query = {service_code: service_code, _id:mongoose.Types.ObjectId(petstore_key) };
    let fields= {name : 1, store_type: 1, image_url:1, ref_image_url:1, pic_id:1, geo_location:1, region: 1, address:1, zipcode:1, business_hours:1, contact_no:1, description:1, service_option:1, source_keyword1:1, source_keyword2:1, source_keyword3:1};
    let storeinfo = await petStore.findOne(query, fields);

    if(storeinfo==null)
    {
      resmsg.retCode = 404;
      resmsg.message = "Not Found";
      res.status(404).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s, response:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey, petstore_key, JSON.stringify(resmsg));       
        return;
    }
    else
    {
      resmsg.retCode = 0;
      resmsg.message = "Success";
      resmsg.data = storeinfo;
      res.status(200).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s, response:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey, petstore_key, resmsg.message);       
      return;
    }

  } catch (error) {
      errormsg.error = -901;
      errormsg.message = "Bad Request:"+error;
      errormsg.status = 400;
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s, error:[%s]",
          log_module, command, msgtime, service_code, uuid, apikey, petstore_key, JSON.stringify(errormsg)); 
  }     
}



////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Find some store informaiton
// {
// 	"service_code" : "[valid service code]",
// 	"api_key":"[valid api key]",
// 	"time":"[valid message time]",
// 	"ext":"ExtPetsV1",
// 	"command" : "find",
// 	"auth": "[message auth]",
// 	"geo_longitude" : [geo longitude],
// 	"geo_latitude" : [geo location],
// 	"max_distance" : [maximum distance],
// 	"country" : "[country name]",
// 	"province" : "[province name]",
// 	"city" : "[city name]",
// 	"search_store" : ["[service option]"]
// 	"search_keyword" : "[search keyword]"

// }

async function findPetStore(req, res, service_code, uuid, apikey, msgtime, command) 
{
  var errormsg = { error:0, status: 0, message:""};
  var resmsg = { retCode:0, message:"OK"};

  // message validation
  var geo_longitude = req.body.geo_longitude==null ? "" : req.body.geo_longitude;
  var geo_latitude = req.body.geo_latitude==null ? "" : req.body.geo_latitude;
  var max_distance = req.body.max_distance==null ? "" : req.body.max_distance;
  var country = req.body.country==null ? "" : req.body.country;
  var province = req.body.province==null ? "" : req.body.province;
  var city = req.body.city==null ? "" : req.body.city;
  var search_store = req.body.search_store==null ? [] : req.body.search_store;
  var search_keyword = req.body.search_keyword==null ? [] : req.body.search_keyword;
  var use_geo_location = true;

  try {
    logger.info("POST REQ:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, geo_longitude:%s, geo_latitude:%s, max_distance:%s, country:%s, province:%s, city:%s, :%s, search_store:"+search_store, 
                log_module, command, msgtime, service_code, uuid, apikey, 
                geo_longitude.toString(), geo_latitude.toString(), max_distance.toString(), country.toString(), province.toString(), city.toString(), search_keyword.toString()
    ); 

    if(geo_longitude=="" || geo_latitude=="" || max_distance=="")
    {
        if(country=="" && province=="" && city=="" && search_keyword=="")
        {
            errormsg.error = 100;
            errormsg.message = "No Search Keyword!";
            res.status(400).send(JSON.stringify(errormsg,null,"\t"));
            logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]", 
                log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg));       
            return;
        }
        else
            use_geo_location = false; // just search keywoard
    }

    // find petstore information
    let query = {service_code: service_code };

    if(use_geo_location==true)
    {
        query.geo_location = {
            $near: {
             $maxDistance: max_distance,
             $geometry: { type: "Point",
              coordinates: [ geo_longitude,  geo_latitude ]        
             }
            }
        };
    }

    if(country!="")
        query.country = country; 
    if(province!="")
        query.province = province; 
    if(city!="")
        query.city = city; 

    if(search_store.length>0)
        query.store_type  = { $all : search_store };
    
    if(search_keyword.length)
    {
        let i;
        query.$or = [];
        for(i=0; i<search_keyword.length; i++)
        {
            query.$or.push({"name":{"$regex":".*"+search_keyword[i]+".*", "$options" : "i"}});
            query.$or.push({"region.city":{"$regex":".*"+search_keyword[i]+".*", "$options" : "i"}});
            query.$or.push({"region.province":{"$regex":".*"+search_keyword[i]+".*", "$options" : "i"}});
            query.$or.push({"region.country":{"$regex":".*"+search_keyword[i]+".*", "$options" : "i"}});
        }
    }

    //console.log(JSON.stringify(query,null,"\t"));
    let fields= {name : 1, store_type: 1, image_url:1, ref_image_url:1, pic_id:1, geo_location:1, region: 1, address:1, zipcode:1, business_hours:1, contact_no:1, description:1, service_option:1, source_keyword1:1, source_keyword2:1, source_keyword3:1};
    let storeinfo = await petStore.find(query, fields);

    if(storeinfo==null || storeinfo.length==0)
    {
      resmsg.retCode = 404;
      resmsg.message = "Not Found";
      res.status(404).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, response:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(resmsg));       
        return;
    }
    else
    {
      resmsg.retCode = 0;
      resmsg.message = "Success";
      resmsg.data = storeinfo;
      res.status(200).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, response:[%s, count:%d]", 
          log_module, command, msgtime, service_code, uuid, apikey, resmsg.message, storeinfo.length);       
      return;
    }

  } catch (error) {
      errormsg.error = -901;
      errormsg.message = "Bad Request:"+error;
      errormsg.status = 400;
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]",
          log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg)); 
  }     
}

 
async function do_pic_file_upload(req, res, service_code, uuid, apikey, msgtime, command)
{
  try {
    var errormsg = { error:0, status: 0, message:""};
    var resmsg = { retCode:0, message:"OK"};

    // message validation
    var petstore_key = req.body.petstore_key==null ? "" : req.body.petstore_key;

    logger.info("POST REQ:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s", 
                log_module, command, msgtime, service_code, uuid, apikey, petstore_key); 

    if (petstore_key=="")
    {
      errormsg.error = 100;
      errormsg.message = "Bad Request";
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s, error:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey,  petstore_key, JSON.stringify(errormsg));       
      return;
    }

    // find petstore information
    let query = {service_code: service_code, _id:mongoose.Types.ObjectId(petstore_key) };
    let storeinfo = await petStore.findOne(query);

    if(storeinfo==null)
    {
      resmsg.retCode = 404;
      resmsg.message = "Not Found";
      res.status(404).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s, response:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey, petstore_key, JSON.stringify(resmsg));       
        return;
    }
    else
    {
      storeinfo.file_session_key = waviotUtil.gen_session_key(uuid);
      let file_session_key_expired = waviotUtil.gen_file_session_key_time();
      storeinfo.file_session_key_expired = file_session_key_expired;

      if(storeinfo.pic_id==null || storeinfo.pic_id=="")
      {
          var pic_random_id = "storepic_"+waviotUtil.gen_session_key(uuid+apikey);
          storeinfo.pic_id = pic_random_id;
          storeinfo.pic_key = "";
      }    

      var updated_petstore = await storeinfo.save();
      resmsg.retCode = 0;
      resmsg.message = "Success";
      resmsg.service_code = service_code
      resmsg.api_key = apikey;
      resmsg.pic_id = updated_petstore.pic_id;
      resmsg.file_session_key = updated_petstore.file_session_key;
      resmsg.file_session_key_expired = file_session_key_expired.format('YYYYMMDDHHmmssZZ');

      res.status(200).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s, response:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey, petstore_key, JSON.stringify(resmsg));       
      return;
    }
  } catch (error) {
    errormsg.error = -901;
    errormsg.message = "Bad Request:";
    errormsg.status = 400;
    res.status(400).send(JSON.stringify(errormsg,null,"\t"));
    logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petstore_key:%s, error:[%s]:%s",
        log_module, command, msgtime, service_code, uuid, apikey, petstore_key, JSON.stringify(errormsg), error); 
  }     
}

async function postPetStore (req, res) 
{
  try {
    var uuid = req.params.uuid;
    var service_code = req.body.service_code;
    var apikey = req.body.api_key;
    var ext_name = req.body.ext;
    var command = req.body.command;
    var auth = req.body.auth;
    var msgtime = req.body.time;
    var msgvalidate = false;
    var proxy = req.body.proxy==null ? false : req.body.proxy;

    var errormsg = { error:0, status: 0, message:""};

    if(uuid==null || uuid=="" ||
       service_code==null || service_code=="" || 
       apikey==null || apikey=="" || 
       ext_name==null || ext_name=="" || 
       command==null || command=="")
    {
        errormsg.error = 100;
        errormsg.message = "Bad Request";
        res.status(400).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]", log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg));       
        return;
    }

    [ msgvalidate, errormsg ] = await petExtUtil.check_ext_msg_validation(msgtime, uuid, apikey, service_code, ext_name, command, proxy, auth);

    if(msgvalidate==false)
    {
      res.status(errormsg.status).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]", log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg));       
      return;
    }

    switch (command) {
      case "create": 
          break;
      case "read": // 특정 petstore 조회
          getPetStore(req, res, service_code, uuid, apikey, msgtime, "petstore read");
          break;
      case "fileupload": 
          do_pic_file_upload(req, res, service_code, uuid, apikey, msgtime, "petstore fileupload");
          break;
      case "find":
          findPetStore(req, res, service_code, uuid, apikey, msgtime, "petstore find");
          break;
      case "update": 
      case "delete": 
      default:
          errormsg.error = 101;
          errormsg.message = "Bad Request";
          res.status(400).send(JSON.stringify(errormsg,null,"\t"));
          logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]", log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg));       
          return;
        break;
    }
  } catch (error) {
      errormsg.error = 901;
      errormsg.message = "Bad Request:";
      errormsg.status = 400;
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]:%s", log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg), error);       
  }
}




///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

var fs = require('fs');
var Grid = require("gridfs-stream");
Grid.mongo = mongoose.mongo;

async function postUploadPetStoreImage(req, res) 
{
    try {
        var picuuid = req.params.picuuid;
        var msg = picuuid.toString().split("#");
        var pic_id = msg[0]==null ? "" : msg[0];
        var file_session_key = msg[1]==null ? "" : msg[1];
        var conn = mongoose.connection;
        var gfs = Grid(conn.db);
        var command = "upload_store_image";
        var resmsg = {retCode: "0000", msg: "success"};

        logger.info("REQ:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Upload User Picture File!", 
                log_module, command, pic_id, file_session_key);     
        if(pic_id=="" || file_session_key=="")
        {
            res.status(400).send("Bad Request.");
            return;
        }

        req.pipe(req.busboy);
        req.busboy.on('field', function(fieldname, val) 
        {
            //console.log(fieldname, val);
        });

        req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) 
        {
            console.log("Uploading: ", filename,encoding, mimetype);
            file_ext = filename.toString().toLowerCase().substr(filename.toString().length - 4);

            // png 파일명만 처리 한다. 
            if(file_ext!=".png")
            {
                console.log("not PNG!!")
                res.status(400).send("Bad Request.")
                logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, File is not a PNG File!", 
                    log_module, command, pic_id, file_session_key); 
                return;                
            }
            
            // db조회 부분을 busboy와 통합할 수 없음. 
            // 결국 파일을 업로드 완료 후 처리해야 함 
            let writeStream = gfs.createWriteStream({
                filename:  pic_id,
                mode: 'w',
                content_type: mimetype
            });

            if(writeStream==null)
            {
                res.status(400).send("Bad Request.")
                logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Bad Request", 
                    log_module, command, pic_id, file_session_key, error); 
                return;
            }

            file.pipe(writeStream);
            
            writeStream.on('close', (file) => {
                //console.log("written to DB:")

                petStore.findOne({pic_id: pic_id, file_session_key:file_session_key})
                .exec(function(err, item) 
                {
                    upload_success = true;
                    if(err!=null || item==null)
                    {
                        upload_success = false;
                        console.log("Invald file session key!!");                    
                    }
        
                    if(upload_success==true &&  check_file_session_key_expired(item.file_session_key_expired)==true)
                    {
                        upload_success = false;
                        console.log("Invald file session key time!!");                    
                    }
                    
                    if(upload_success==false)
                    {
                        // 업로드된 파일 삭제
                        gfs.remove({ _id: file._id.toHexString()}, 
                            function (err) {
                            console.log("DELETE UPLOAD  "+file._id.toHexString());
                            }   
                        );
                        res.status(400).send("Bad Request.")
                        return;                        
                    }
                    
                    // 이전 화일 삭제
                    var old_pic_key = "";
                    if(item.pic_key!=null)
                        old_pic_key=item.pic_key;
                    else
                        old_pic_key = "";
        
                    item.pic_key = file._id.toHexString();
                    item.save(function(err, upateitem)
                    {
                        if(err!=null || upateitem==null)
                        {
                            gfs.remove({ _id: file._id.toHexString()}, 
                                function (err) {
                                    console.log("DELETE "+file._id.toHexString());
                                }
                            );

                            res.status(400).send("Bad Request.")
                            return;
                        }

                        // 정상 업로드 후 이전에 등록 이미지 삭제
                        if(old_pic_key!="")
                        {
                            gfs.remove({ _id: old_pic_key}, 
                                function (err) {
                                    console.log("DELETE OLD FILE "+old_pic_key);
                                }
                            );
                        }
                        logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Success!!!", 
                            log_module, command, pic_id, file_session_key); 
            
                        return res.status(200).send({
                            message: JSON.stringify(resmsg),
                            file: file
                        });
                    });
                });              
            });
        });

        req.busboy.on('finish', function()
        {
            logger.info("INFO:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Succeed to upload file!", 
                log_module, command, pic_id, file_session_key); 
            //console.log("finish");
        });       
        //writeStream.end();
    } catch (error) {
        res.status(400).send("Bad Request.")
        logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Bad Request:%s", 
            log_module, "upload_user_pic", pic_id, file_session_key, error); 
    }
}

async function getPetStoreImage(req, res) 
{
    var conn = mongoose.connection;
    var gfs = Grid(conn.db);
    
    gfs.files.find({
        filename: req.params.picuuid
    }).toArray((err, files) => {
        if (files.length === 0) {
            return res.status(400).send("Bad Request.");
        }
        let data = [];
        let readstream = gfs.createReadStream({
            filename: files[0].filename
        });

        readstream.on('data', (chunk) => {
            data.push(chunk);
        });

        readstream.on('end', () => {
            data = Buffer.concat(data);
            let img = 'data:image/png;base64,' + Buffer(data).toString('base64');
            res.end(img);
            console.log("sent", files[0].filename);
        });

        readstream.on('error', (err) => {
            console.log('An error occurred!', err);
            return res.status(400).send("Bad Request.");
            //throw err;
        });
    });

}

module.exports = {
  putNewStore,
  postPetStore,
  postUploadPetStoreImage,
  getPetStoreImage
}
