var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');
var crypto = require('crypto');

var waviotUtil = require("../modules/waviotUtil");
var apiClients = require('../models/apiClients');
var adminUsers = require('../models/adminUsers');

var timeCheck = true;
var timeLimit = 15; // 15 minutes
var extName = "ExtPetsV1"

async function check_ext_msg_validation(msgtime, uuid, apikey, service_code, ext_name, command, proxy, auth)
{
  let errormsg = { error:0, status:0, message:""};

  // message 시간 검사
  if(timeCheck==true)
  {
      if(msgtime!=null)
      {
          if(waviotUtil.check_msg_expired(msgtime, timeLimit)>0)
          {
              errormsg.error = 101;
              errormsg.message = "Unauthorized";
              errormsg.status = 401;
              return [ false, errormsg ];
            }
          
      }
      else
      {
          errormsg.error = 100;
          errormsg.message = "Bad Request";
          errormsg.status = 400;
          return [ false, errormsg ];
        }
  }

  if(ext_name!=extName)
  {
      errormsg.error = 101;
      errormsg.message = "Bad Request";
      errormsg.status = 400;
      return [ false, errormsg ];
  }

  // client key 유효성 검사 
  apicli = await apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey });
  if(apicli==null)
  {
      errormsg.error = 111;
      errormsg.message = "Not Found";
      errormsg.status = 404;
      return [ false, errormsg ];
    }

  let secret = apicli.secret;
  if(proxy==true)
    secret = apicli.session_key;    

  // message auth 검사 
  let pauth = waviotUtil.get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret);
  if(auth!=pauth)
  {
      errormsg.error = 102;
      errormsg.message = "Unauthorized.";
      errormsg.status = 401;
      return [ false, errormsg ];
  }  

  return [ true, errormsg ];
}

module.exports = {
    check_ext_msg_validation
}
