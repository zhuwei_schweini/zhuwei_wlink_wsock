var mongoose = require('mongoose');
var moment = require('moment-timezone');

var waviotUtil = require("../modules/waviotUtil");
var petExtUtil = require("./petExtUtil");
var pets = require('../models/pets');
var petMedia = require('../models/petMedia');

var logger = global.logger;
var log_module = "ExtPET";

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Add new pet media contents or update the media informaiton
//   sample =   {
//       "service_code" : "WLINK_XXXXX",
//       "api_key":"admin_api_key",
//       "time":"20181123074943+0800",
//       "ext":"ExtPetsV1",
//       "command" : "putpetmedia",
//       "auth": "5638822878d5369485b5ae72675026f6",
//       "title" : { "en" : "", "cn" : ""},
//       "source" : "youtube",
//       "source_channel" : "",
//       "media_type" : "tv",
//       "pet_type" : "cat",
//       "media_url" : "http://..",

//       "image_url" : "http://..",
//       "source_keyword1" : "",
//       "source_keyword2" : "",
//       "source_keyword3" : "",
//       "description" : ""
// }；

async function putNewMedia (req, res) 
{
  try {
    var uuid = req.params.uuid;
    var service_code = req.body.service_code;
    var apikey = req.body.api_key;
    var ext_name = req.body.ext;
    var command = req.body.command;
    var auth = req.body.auth;
    var msgtime = req.body.time;
    var msgvalidate = false;
    var source = req.body.source==null ? "" : req.body.source;
    var source_uuid = req.body.source_uuid==null ? "" : req.body.source_uuid;
    var source_channel = req.body.source_uuid==null ? "" : req.body.source_channel;
    var media_type = req.body.media_type==null ? "" : req.body.media_type;
    var pet_type = req.body.pet_type==null ? "" : req.body.pet_type;
    var media_url = req.body.media_url==null ? "" : req.body.media_url;
    var title = req.body.title==null ? {"en" : "Untitled", "cn" : "未命名", "ko" : "무제" } : req.body.title;

    var errormsg = { error:0, status: 0, message:""};
    var resmsg = { retCode:0, message:"OK"};

    if(uuid==null || uuid=="" ||
       service_code==null || service_code=="" || 
       apikey==null || apikey=="" || 
       ext_name==null || ext_name=="" || 
       command==null || command=="")
    {
        errormsg.error = 100;  // input parameter error
        errormsg.message = "Bad Request";
        res.status(400).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title), JSON.stringify(errormsg));         
        return;
    }

    [ msgvalidate, errormsg ] = await petExtUtil.check_ext_msg_validation(msgtime, uuid, apikey, service_code, ext_name, command, false, auth);

    if(msgvalidate==false)
    {
        res.status(errormsg.status).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title), JSON.stringify(errormsg));         
        return;
    }

    // is it admin?
    let is_admin = await waviotUtil.is_admin_account(apicli, service_code, uuid);
    if(is_admin!=true)
    {
        errormsg.error = 110;  // permission error
        errormsg.message = "Unauthorized.";
        errormsg.status = 401;
        res.status(401).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title), JSON.stringify(errormsg));         
        return;
    }

    // do something
    // get optionals
    let description = req.body.description==null ? "" : req.body.description;
    let source_keyword1 = req.body.source_keyword1==null ? "" : req.body.source_keyword1;
    let source_keyword2 = req.body.source_keyword2==null ? "" : req.body.source_keyword2;
    let source_keyword3 = req.body.source_keyword3==null ? "" : req.body.source_keyword3;
    let image_url = req.body.image_url==null ? "" : req.body.image_url;
    let source_id = req.body.source_id==null ? {
        "kind": "",
        "videoId": "",
        "channelId": "",
        "playlistId": ""
      } : req.body.source_id;
      let source_snippet = req.body.source_snippet==null ? {
        "title": ""
      } : req.body.source_snippet;      

    logger.info("PUT REQ:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s,\
source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], \
source_keyword1:%s. source_keyword2:%s, source_keyword3:%s, image_url:%s, source_id:[%s], source_snippet:[%s]",
                log_module, command, msgtime,  uuid, apikey,
                source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title),
                source_keyword1, source_keyword2, source_keyword3, image_url, JSON.stringify(source_id), JSON.stringify(source_snippet)); 

    if (source=="" || source_uuid=="" || media_type=="" || pet_type=="" || media_url=="" || command!="putpetmedia")
    {
        errormsg.error = 100;
        errormsg.status = 1;
        errormsg.message = "Bad Request";
        res.status(400).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title), JSON.stringify(errormsg));         
        return;
    }

    // find the old information with source information
    let query = {service_code: service_code, "source":source, "source_uuid":source_uuid};
    let mediaInfo = await petMedia.findOne(query);

    if(mediaInfo==null)
    {
      // new Media
      var pic_random_id = "mediapic_"+waviotUtil.gen_session_key(uuid+apikey);
      var newPetMedia = new petMedia(
        {
            service_code: service_code, 
            source: source,
            source_uuid: source_uuid,
            source_channel : source_channel,
            source_id : source_id,
            source_snippet : source_snippet,

            client_uuid: uuid,
            client_apikey: apikey,
            last_client_uuid: uuid,  
            last_client_apikey: apikey,
            title : title,

            media_type : media_type,
            pet_type : pet_type,
            media_url : media_url,

            source_keyword1 : source_keyword1,
            source_keyword2 : source_keyword2,
            source_keyword3 : source_keyword3,
            description : description,
            pic_id : pic_random_id,
            pic_key : "",
            image_url : image_url
        }
      );

      let  update_pet_media = await petMedia.findOneAndUpdate(
          query, 
          newPetMedia, 
          {upsert: true, new: true, runValidators: true});

      if(update_pet_media==null)
      {
        errormsg.error = 900;
        errormsg.message = "System Error";
        errormsg.status = 500;
        res.status(500).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title), JSON.stringify(errormsg));         
        return;
      }

      resmsg.petmedia_key=update_pet_media._id.toHexString();
      resmsg.is_new = true;
    }
    else
    {
      // old media information
      mediaInfo.title = title;
      mediaInfo.source_channel = source_channel;
      mediaInfo.last_client_uuid = uuid;
      mediaInfo.last_client_apikey = apikey;

      mediaInfo.source_keyword1  = source_keyword1;
      mediaInfo.source_keyword2  = source_keyword2;
      mediaInfo.source_keyword3  = source_keyword3;

      mediaInfo.image_url = image_url;
      mediaInfo.description = description;

      mediaInfo.media_url = media_url;
      mediaInfo.media_type = media_type;
      mediaInfo.pet_type = pet_type;
      mediaInfo.source_id = source_id;
      mediaInfo.source_snippet = source_snippet;

      mediaInfo.last_updated_date = moment();

      let update_pet_media = await mediaInfo.save();

      if(update_pet_media==null)
      {
        errormsg.error = 900;
        errormsg.message = "System Error";
        errormsg.status = 500;
        res.status(500).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title), JSON.stringify(errormsg));
        return;
      }      

      resmsg.petmedia_key=update_pet_media._id.toHexString();
      resmsg.is_new = false;
    }

    res.send(JSON.stringify(resmsg,null,"\t"));
    logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], response:[%s]",
        log_module, command, msgtime, uuid, apikey, source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title), JSON.stringify(resmsg));         

  } catch (error) {
        errormsg.error = 901;
        errormsg.message = "Bad Request:"+error;
        errormsg.status = 400;
        res.status(400).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("PUT RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, source:%s, source_uuid:%s, media_type:%s, pet_type:%s, media_url:%s, title:[%s], error:[%s]",
            log_module, command, msgtime, uuid, apikey, source, source_uuid, media_type, pet_type, media_url, JSON.stringify(title), JSON.stringify(errormsg));         
  }     
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Find media informaiton
// {
// 	"service_code" : "[valid service code]",
// 	"api_key":"[valid api key]",
// 	"time":"[valid message time]",
// 	"ext":"ExtPetsV1",
// 	"command" : "find",
// 	"auth": "[message auth]",
// 	"media_type" : ["media_type"],
// 	"pet_type" : "[pet_type]"
// 	"search_keyword" : "[search_keyword]"

// }

async function findPetMedia(req, res, service_code, uuid, apikey, msgtime, command) 
{
  var errormsg = { error:0, status: 0, message:""};
  var resmsg = { retCode:0, message:"OK"};

  // message validation
  var media_type = req.body.media_type==null ? "" : req.body.media_type;
  var pet_type = req.body.pet_type==null ? "" : req.body.pet_type;
  var search_keyword = req.body.search_keyword==null ? [] : req.body.search_keyword;

  try {
    logger.info("POST REQ:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, media_type:%s, pet_type:%s, search_keyword:%s", 
                log_module, command, msgtime, service_code, uuid, apikey, 
                media_type, pet_type, search_keyword
    ); 

    if(pet_type=="")
    {
        errormsg.error = 100;
        errormsg.message = "No Search Keyword!";
        res.status(400).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]", 
            log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg));       
        return;
    }

    // find petmedia information
    let query = {service_code: service_code, status:"0" };

    if(media_type!="")
    {
        query.media_type = media_type; 
    }
    if(pet_type!="")
    {
        if(pet_type=="dog" || pet_type=="cat")
            query.pet_type = pet_type; 
    }

    /*
    if(search_keyword.length)
    {
        let i;
        query.$or = [];
        for(i=0; i<search_keyword.length; i++)
        {
            query.$or.push({"name":{"$regex":".*"+search_keyword[i]+".*", "$options" : "i"}});
            query.$or.push({"region.city":{"$regex":".*"+search_keyword[i]+".*", "$options" : "i"}});
            query.$or.push({"region.province":{"$regex":".*"+search_keyword[i]+".*", "$options" : "i"}});
            query.$or.push({"region.country":{"$regex":".*"+search_keyword[i]+".*", "$options" : "i"}});
        }
    }
*/
    //console.log(JSON.stringify(query,null,"\t"));
    let fields= {name : 1, title: 1, source:1, source_uuid:1, source_id:1, source_snippet:1, media_type: 1, pet_type:1, media_url:1, description:1, image_url:1};
    let mediatinfo = await petMedia.find(query, fields);

    if(mediatinfo==null || mediatinfo.length==0)
    {
      resmsg.retCode = 404;
      resmsg.message = "Not Found";
      res.status(404).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, response:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(resmsg));       
        return;
    }
    else
    {
      resmsg.retCode = 0;
      resmsg.message = "Success";
      resmsg.data = mediatinfo;
      res.status(200).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, response:[%s, count:%d]", 
          log_module, command, msgtime, service_code, uuid, apikey, resmsg.message, mediatinfo.length);       
      return;
    }

  } catch (error) {
      errormsg.error = -901;
      errormsg.message = "Bad Request:"+error;
      errormsg.status = 400;
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]",
          log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg)); 
  }     
}

 
async function do_pic_file_upload(req, res, service_code, uuid, apikey, msgtime, command)
{
  try {
    var errormsg = { error:0, status: 0, message:""};
    var resmsg = { retCode:0, message:"OK"};

    // message validation
    var petmedia_key = req.body.petmedia_key==null ? "" : req.body.petmedia_key;

    logger.info("POST REQ:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petmedia_key:%s", 
                log_module, command, msgtime, service_code, uuid, apikey, petmedia_key); 

    if (petmedia_key=="")
    {
      errormsg.error = 100;
      errormsg.message = "Bad Request";
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petmedia_key:%s, error:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey,  petmedia_key, JSON.stringify(errormsg));       
      return;
    }

    // find petMedia information
    let query = {service_code: service_code, _id:mongoose.Types.ObjectId(petmedia_key) };
    let mediainfo = await petMedia.findOne(query);

    if(mediainfo==null)
    {
      resmsg.retCode = 404;
      resmsg.message = "Not Found";
      res.status(404).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petmedia_key:%s, response:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey, petmedia_key, JSON.stringify(resmsg));       
        return;
    }
    else
    {
        mediainfo.file_session_key = waviotUtil.gen_session_key(uuid);
        let file_session_key_expired = waviotUtil.gen_file_session_key_time();
        mediainfo.file_session_key_expired = file_session_key_expired;

      if(mediainfo.pic_id==null || mediainfo.pic_id=="")
      {
          var pic_random_id = "mediapic_"+waviotUtil.gen_session_key(uuid+apikey);
          mediainfo.pic_id = pic_random_id;
          mediainfo.pic_key = "";
      }    

      var updated_petmedia = await mediainfo.save();
      resmsg.retCode = 0;
      resmsg.message = "Success";
      resmsg.service_code = service_code
      resmsg.api_key = apikey;
      resmsg.pic_id = updated_petmedia.pic_id;
      resmsg.file_session_key = updated_petmedia.file_session_key;
      resmsg.file_session_key_expired = file_session_key_expired.format('YYYYMMDDHHmmssZZ');

      res.status(200).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petmedia_key:%s, response:[%s]", 
          log_module, command, msgtime, service_code, uuid, apikey, petmedia_key, JSON.stringify(resmsg));       
      return;
    }
  } catch (error) {
    errormsg.error = -901;
    errormsg.message = "Bad Request:";
    errormsg.status = 400;
    res.status(400).send(JSON.stringify(errormsg,null,"\t"));
    logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, petmedia_key:%s, error:[%s]:%s",
        log_module, command, msgtime, service_code, uuid, apikey, petmedia_key, JSON.stringify(errormsg), error); 
  }     
}

async function postPetMedia (req, res) 
{
  try {
    var uuid = req.params.uuid;
    var service_code = req.body.service_code;
    var apikey = req.body.api_key;
    var ext_name = req.body.ext;
    var command = req.body.command;
    var auth = req.body.auth;
    var msgtime = req.body.time;
    var msgvalidate = false;
    var proxy = req.body.proxy==null ? false : req.body.proxy;

    var errormsg = { error:0, status: 0, message:""};

    if(uuid==null || uuid=="" ||
       service_code==null || service_code=="" || 
       apikey==null || apikey=="" || 
       ext_name==null || ext_name=="" || 
       command==null || command=="")
    {
        errormsg.error = 100;
        errormsg.message = "Bad Request";
        res.status(400).send(JSON.stringify(errormsg,null,"\t"));
        logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]", log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg));       
        return;
    }

    [ msgvalidate, errormsg ] = await petExtUtil.check_ext_msg_validation(msgtime, uuid, apikey, service_code, ext_name, command, proxy, auth);

    if(msgvalidate==false)
    {
      res.status(errormsg.status).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]", log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg));       
      return;
    }

    switch (command) {
      case "create": 
          break;
      case "read": // 특정 pet media 조회
          //getPetMedia(req, res, service_code, uuid, apikey, msgtime, "petmedia read");
          break;
      case "fileupload": 
          do_pic_file_upload(req, res, service_code, uuid, apikey, msgtime, "petmedia fileupload");
          break;
      case "find":
          findPetMedia(req, res, service_code, uuid, apikey, msgtime, "petmedia find");
          break;
      case "update": 
      case "delete": 
      default:
          errormsg.error = 101;
          errormsg.message = "Bad Request";
          res.status(400).send(JSON.stringify(errormsg,null,"\t"));
          logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]", log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg));       
          return;
        break;
    }
  } catch (error) {
      errormsg.error = 901;
      errormsg.message = "Bad Request:";
      errormsg.status = 400;
      res.status(400).send(JSON.stringify(errormsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, error:[%s]:%s", log_module, command, msgtime, service_code, uuid, apikey, JSON.stringify(errormsg), error);       
  }
}




module.exports = {
  putNewMedia,
  postPetMedia
}
