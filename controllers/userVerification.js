var AWS = require("aws-sdk");
var waviotUtil = require("../modules/waviotUtil");
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');
var crypto = require('crypto');

var apiClients = require('../models/apiClients');
var users = require('../models/users');
var userDevices = require('../models/userDevices');
var busboy = require('connect-busboy');

var logger = global.logger;
var log_module = "ctrlUser";

async function aws_send_template_email(service_info, uuid, apikey, account_type, account_id, template, receiver, template_data) 
{
  var command = "aws_send_template_email";

  try {
    var ses = new AWS.SES(service_info.aws_config);
    var sendParams =
    {
      "Source": "Do Not Reply <"+service_info.smtp.sender+">",
      "Template": template,
      "Destination": {
        "ToAddresses": [ receiver ]
      },
      "TemplateData": JSON.stringify(template_data)
    }

    ses.sendTemplatedEmail(sendParams, function(err, data) {
      if (err)
      {
        console.log(err, err.stack); 
        logger.info("AWS SES ERROR : module:%s, command:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]",
          log_module, command, uuid, apikey, account_type, account_id, err);         
      }
      else
      {
        console.log(data);           // successful response
        logger.info("AWS SES SUCCSS : module:%s, command:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, data:[%s]",
          log_module, command, uuid, apikey, account_type, account_id, data);         
      } 
    });
  } catch (error) {
     console.log("aws_send_template_email:"+error)   
     logger.info("AWS SES ERROR CATCH : module:%s, command:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]",
       log_module, command, uuid, apikey, account_type, account_id, error);         
}
}

async function send_verification_email(service_code, uuid, apikey, account_type, account_id, receiver, mail_type, language, verification_code)
{
  var service_info = waviotUtil.get_global_service(service_code);
  var template = "verificationCode_"+language;
  var TemplateData  = { 
    service_name: service_info.smtp.service_name, 
    email: receiver,
    type : mail_type, 
    verification_code: verification_code, 
    expired_time: "10"  
  };

  if(language=="en")
  {
    if(mail_type=="signup") TemplateData.type="Sign Up";
    else  
    if(mail_type=="signin") TemplateData.type="Sign In";
  }
  else if(language=="cn")
  {
    if(mail_type=="signup") TemplateData.type="注册";
    else  
    if(mail_type=="signin") TemplateData.type="登录";
  }
  else if(language=="ko")
  {
    if(mail_type=="signup") TemplateData.type="회원가입";
    else  
    if(mail_type=="signin") TemplateData.type="로그인";
  }

  aws_send_template_email(service_info, uuid, apikey, account_type, account_id, template, receiver, TemplateData);
}
async function post_send_email_verification_code(req, res, apikey_record, service_code, uuid, apikey, account_type, account_id)
{
  let resmsg = { retCode : 0, service_code: service_code, key: apikey, verification_key: "",  verification_key_expired: ""};
  let errmsg = { error:0, status: 0, message:""};

  let command = "post_send_email_verification_code";

  let msgtime = req.body.time;
  let verification_type = req.body.verification_type==null ? "" : req.body.verification_type;;
  let language = req.body.language==null ? "en" : req.body.language;
  let req_validation = true;

  try {

    if(apikey_record==null || verification_type=="" || account_type!="email")
    {
      req_validation = false;
      errmsg.status = 1;
    }

    if(verification_type!="signin" && verification_type!="signup")
    {
      req_validation = false;
      errmsg.status = 2;
    }

    if(req_validation==false)
    {
      errmsg.error = 100; // invalid account type;
      errmsg.message = "Bad Request.";
      res.status(400).send(JSON.stringify(errmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]",
        log_module, command, msgtime, uuid, apikey, account_type, account_id, JSON.stringify(errmsg));         
      return;    
    }

    logger.info("POST REQ:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, verification_type:%s, language:[%s]",
      log_module, command, msgtime, uuid, apikey, account_type, account_id, verification_type, language);         

    // 기존에 존재하는 verification 정보가 존재하면, 2분내에 1번만호출 가능하다. 
    if(apikey_record.verification_session_key!=null && apikey_record.verification_session_key!="")
    {
      if(waviotUtil.check_verification_code_timelimit(apikey_record.verification_updated))
      {
        resmsg.retCode = 100; // invalid account type;
        resmsg.message = "can call it again after 2 minutes";
        res.status(200).send(JSON.stringify(resmsg,null,"\t"));
        logger.info("POST RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, resmsg:[%s]",
        log_module, command, msgtime, uuid, apikey, account_type, account_id, JSON.stringify(resmsg));         
        return;
      }
    }

    apikey_record.verification_method = "email";
    apikey_record.verification_type = verification_type;
    apikey_record.verification_code = waviotUtil.gen_verification_code();
    apikey_record.verification_updated = Date.now();
    apikey_record.verification_session_key = waviotUtil.gen_session_key(uuid);
    let verification_session_key_expired = waviotUtil.gen_verification_session_key_time();
    apikey_record.verification_session_key_expired = verification_session_key_expired;
    apikey_record.verification_code_used = false;

    // save the verification code to DB
    apikey_record.save();
    // sned email
    send_verification_email(service_code, uuid, apikey, account_type, account_id, account_id, verification_type, language, apikey_record.verification_code);

    resmsg.retCode = 0; 
    resmsg.message = "success";
    resmsg.verification_key = apikey_record.verification_session_key;
    resmsg.verification_key_expired = verification_session_key_expired.format('YYYYMMDDHHmmssZZ');

    res.status(200).send(JSON.stringify(resmsg,null,"\t"));
    logger.info("POST RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, resmsg:[%s]",
        log_module, command, msgtime, uuid, apikey, account_type, account_id, JSON.stringify(resmsg));

  } catch (error) {
    errmsg.error = 900;
    errmsg.message = "System Error.";
    errmsg.status = 500;
    res.status(500).send(JSON.stringify(errmsg,null,"\t"));
    logger.info("POST RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]:%s",
        log_module, command, msgtime, uuid, apikey, account_type, account_id, JSON.stringify(errmsg), error)
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
async function post_check_email_verification_code(req, res, apikey_record, service_code, uuid, apikey, account_type, account_id)
{
    let resmsg = { retCode : 0, service_code: service_code, key: apikey};
    let errmsg = { error:0, status: 0, message:""};
  
    let command = "post_check_email_verification_code";
  
    var msgtime = req.body.time;
    let verification_key = req.body.verification_key==null ? "" : req.body.verification_key;;
    let verification_code = req.body.verification_code==null ? "" : req.body.verification_code;;
    let req_validation = true;
  
    try {
  
      if(apikey_record==null || verification_code=="" || verification_key=="")
      {
        req_validation = false;
        errmsg.status = 1;
      }

      if(req_validation==false)
      {
        errmsg.error = 100; // invalid account type;
        errmsg.message = "Bad Request.";
        res.status(400).send(JSON.stringify(errmsg,null,"\t"));
        logger.info("POST RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]",
          log_module, command, msgtime, uuid, apikey, account_type, account_id, JSON.stringify(errmsg));         
        return;    
      }
  
      logger.info("POST REQ:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, verification_key:%s, verification_code:[%s]",
        log_module, command, msgtime, uuid, apikey, account_type, account_id, verification_key, verification_code);         
  
      let res_validation = true
      if(apikey_record.verification_session_key==null || apikey_record.verification_session_key!=verification_key)
      {
        resmsg.retCode = 100;
        resmsg.message = "Invalid Verification Key";
        res_validation = false;
      }

      if(waviotUtil.check_verification_session_key_expired(apikey_record.verification_session_key_expired))
      {
        resmsg.retCode = 101;
        resmsg.message = "Expired Verification Key";
        res_validation = false;
      }
  
      if(apikey_record.verification_code==null || apikey_record.verification_code!=verification_code)
      {
        resmsg.retCode = 1;;
        resmsg.message = "Invalid Verification Code";
        res_validation = false;
      }

      if(apikey_record.verification_code_used == true)
      {
        resmsg.retCode = 2;
        resmsg.message = "Already used Verification Code";
        res_validation = false;
      }

      if(res_validation == false) 
      {
        res.status(200).send(JSON.stringify(resmsg,null,"\t"));
        logger.info("POST RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, resmsg:[%s]",
        log_module, command, msgtime, uuid, apikey, account_type, account_id, JSON.stringify(resmsg));         
        return;
      }

      apikey_record.verification_timed = Date.now();
      apikey_record.verification_code_used = true;
      apikey_record.save();
  
      resmsg.retCode = 0; 
      resmsg.message = "success";
  
      res.status(200).send(JSON.stringify(resmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, resmsg:[%s]",
          log_module, command, msgtime, uuid, apikey, account_type, account_id, JSON.stringify(resmsg));
  
    } catch (error) {
      errmsg.error = 900;
      errmsg.message = "System Error.";
      errmsg.status = 500;
      res.status(500).send(JSON.stringify(errmsg,null,"\t"));
      logger.info("POST RES:: module:%s, command:%s, msgtime:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]:%s",
          log_module, command, msgtime, uuid, apikey, account_type, account_id, JSON.stringify(errmsg), error)
    }
}

module.exports = {
  post_send_email_verification_code,
  post_check_email_verification_code
}
