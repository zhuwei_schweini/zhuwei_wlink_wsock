var sessionio = require('socket.io');
var waviotmsg = require("../modules/waviotmsg");
var amqp = require('amqplib');
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var apiClients = require('../models/apiClients');
var serviceCode = require('../models/serviceCode');
var users = require('../models/users');
var crypto = require('crypto');
var logger = global.logger;

// today - expired
// under 0  : remain time 
function check_session_expired(session_expired)
{
    var today = moment();    
    var expired = moment(session_expired);
    return  today.diff(expired, 'minutes');
}

/////////////////////////////////////////////////////////////////////////////////////////
// check device session id
// 클라이언트가 로그인한 디바이스 정보를 검ㅅ해서 로그인 유무를 검사
function check_device_session (socket, device_id)
{
    for(var idx in socket.device_sessions)
    {
        if(socket.device_sessions[idx].uuid==device_id)
        return idx;
    }  
    return -1;
}

function unbind_device_allsession (socket, amqp_ch, amqp_exhg, queue)
{
    try
    {
        for(var idx in socket.device_sessions)
        {
            amqp_ch.unbindQueue(queue, amqp_exhg, socket.device_sessions[idx].session_key);
        }  
    } catch (error) {
        logger.info(">>>>> unbind_device_allsession : " + error);
    }
}


// 디바이스로 받은 메시지중 세션 오류에려인 401에 대해서 검사
// 만일 401이 오면 세션값이 변경된것으로 재로그인 필요 
function check_device_response_session(msgobj)
{
  // invalide session
  var resCode;

  resCode = waviotmsg.FindIgnoreKeyValue(msgobj.body, "rescode");
  if(resCode==null) resCode = waviotmsg.FindIgnoreKeyValue(msgobj.body, "res_code");

  if(resCode==null)
  {
    //logger.info(" false MSGID [%s] resCode is null", msgobj.header.msgID);
    //logger.info(">>>> [%s]", JSON.stringify(msgobj));
    return false;    
  }
  if(resCode=="0401" || resCode==401)
  {
    //logger.info(" false MSGID [%s] resCode[%s]", msgobj.header.msgID, msgobj.body.resCode);
    return false;
  }
    //logger.info(" true MSGID [%s] resCode[%s]", msgobj.header.msgID, msgobj.body.resCode);
  return true;
}

// AMQP Message generate handler
function call_service_handler(GID, hashkey, session_key, msgobj)
{
  var devicemsg =  waviotmsg.make_waviot_msg("callService", GID, "DEVICE_WEBSOCK", msgobj.header.clientKey, "", hashkey, msgobj.body); 
  if(session_key)
    devicemsg.header.sessionKey = session_key;
  return devicemsg;
}

function get_attr_handler(GID, hashkey, session_key, msgobj)
{
  var devicemsg =  waviotmsg.make_waviot_msg("getAttr", GID, "DEVICE_WEBSOCK", msgobj.header.clientKey, "", hashkey, msgobj.body); 
  if(session_key)
    devicemsg.header.sessionKey = session_key;
  return devicemsg;
}

function set_attr_handler(GID, hashkey, session_key, msgobj)
{
  var devicemsg =  waviotmsg.make_waviot_msg("setAttr", GID, "DEVICE_WEBSOCK", msgobj.header.clientKey, "", hashkey, msgobj.body); 
  if(session_key)
    devicemsg.header.sessionKey = session_key;
  return devicemsg;
}

function get_subdev_attr_handler(GID, hashkey, session_key, msgobj)
{
  var devicemsg =  waviotmsg.make_waviot_msg("getSubdevAttr", GID, "DEVICE_WEBSOCK", msgobj.header.clientKey, "", hashkey, msgobj.body); 
  if(session_key)
    devicemsg.header.sessionKey = session_key;
  return devicemsg;
}

function set_subdev_attr_handler(GID, hashkey, session_key, msgobj)
{
  var devicemsg =  waviotmsg.make_waviot_msg("setSubdevAttr", GID, "DEVICE_WEBSOCK", msgobj.header.clientKey, "", hashkey, msgobj.body); 
  if(session_key)
    devicemsg.header.sessionKey = session_key;
  return devicemsg;
}

function add_attr_handler(GID, hashkey, session_key, msgobj)
{
  var devicemsg =  waviotmsg.make_waviot_msg("addAttr", GID, "DEVICE_WEBSOCK", msgobj.header.clientKey, "", hashkey, msgobj.body); 
  if(session_key)
    devicemsg.header.sessionKey = session_key;
  return devicemsg;
}

function del_attr_handler(GID, hashkey, session_key, msgobj)
{
  var devicemsg =  waviotmsg.make_waviot_msg("delAttr", GID, "DEVICE_WEBSOCK", msgobj.header.clientKey, "", hashkey, msgobj.body); 
  if(session_key)
    devicemsg.header.sessionKey = session_key;
  return devicemsg;
}

function login_handler(GID, hashkey, session_key, msgobj)
{
  var devicemsg =  waviotmsg.make_waviot_msg("login", GID, "CLIENT_WEBSOCK", msgobj.header.clientKey, "", hashkey, msgobj.body); 
  if(session_key)
    devicemsg.header.sessionKey = session_key;
  return devicemsg;
}

// call service handler
function call_service_process(socket, amqp_ch, queue, amqp_exhg, service, device_id, cid, client_key,  rescode, result)
{
  try {
    switch(service)
    {
      case "authDevice" : 
        if(rescode==0 || rescode=="0000")
        {
          var new_session_time;
          var new_session_key = "";

          new_session_key = waviotmsg.FindIgnoreKeyValue(result, "session_key");
          if(new_session_key==null) new_session_key = waviotmsg.FindIgnoreKeyValue(result, "sessionkey");
          new_session_time = waviotmsg.FindIgnoreKeyValue(result, "session_time");
          if(new_session_time==null) new_session_time = waviotmsg.FindIgnoreKeyValue(result, "sessiontime");

          // 2016.12.26 
          // 중요: socket.io로 접속하는 디바이스 세션은 device id와  clientkey를 사용함
          // 일반 amqp로 접속하는 디바이스 세션은 cid와 clientkey를 사용함 cid / clientkey 조합은 장치 하나 하나가 아닌 
          // 모델 정보등 그룹 단위로 유니크한 값을 사용한다. 
          new_device_id = device_id;
          if(cid!=null) new_device_id = cid;
          
          // 2016.12.26 
          // 현재 amqp로 부터 메시지는 service의 hash key로 해쉬키가 생성됨.. 
          // 이후에는 장치별로 secret을 사용해서 전달될 것임. 이를 위해서 클라이언트가 로그인 성공시에 
          // 그 정보를 조회해 세션 정보에 저장함. 또한 db에서 해당 정보가 실제로 존재하는지 판단을 추가함. 
          // 없으면 정상적인 메시지 응답이 아니므로 무시.
          apiClients.findOne({service_code:socket.service_code, uuid:new_device_id, key:client_key })
          .exec(function(err, item) 
          {
            if(err || item==null)
            {
                logger.info(">>>>> call_service_process : ", "Invalid device info!",socket.service_code, new_device_id, client_key );
                return;
            }

            var device_info = {uuid:device_id, key:item.key, secret:item.secret, session_key:new_session_key, sessin_time: Date.now(), use_session_expire: false, update_time:Date.now()};
            var idx = check_device_session(socket, device_id);
            if(idx<0)
            {
                if(new_session_time)
                {
                device_info.session_time = moment(new_session_time, "YYYYMMDDHHmmssZZ") ;
                device_info.use_session_expire = true; 
                }
                socket.device_sessions.push(device_info);
                logger.info("call_service_process.authDevice = " + new_session_key);
                amqp_ch.bindQueue(queue, amqp_exhg, new_session_key);
            }
            else
            {
                if(new_session_key!=socket.device_sessions[idx].session_key)
                {
                    amqp_ch.unbindQueue(queue, amqp_exhg, socket.device_sessions[idx].session_key);

                    if(new_session_time)
                    {
                        socket.device_sessions[idx].session_time = moment(new_session_time, "YYYYMMDDHHmmssZZ") ;
                        socket.device_sessions[idx].use_session_expire = true;
                    }
                    else
                        socket.device_sessions[idx].use_session_expire = false;
                    
                    socket.device_sessions[idx].session_key = new_session_key;
                    amqp_ch.bindQueue(queue, amqp_exhg, new_session_key);
                }
            }
          });
        }
        break;    
    }            
  } catch (error) {
    logger.info(">>>>> call_service_process : " + error);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
// AMQP msg from device. forward it client
function amqp_on_call_service(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_call_service: [%s]', JSON.stringify(msgobj.body));

    call_service_process(socket, amqp_ch, queue, amqp_exhg, msgobj.body.serviceName, msgobj.header.deviceID, msgobj.header.cid, msgobj.header.clientKey,  msgobj.body.resCode, msgobj.body.result);
    //logger.info("callService ["+msg.content.toString()+"]");
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('callService',{'msg':JSON.stringify(msgobj.body)});
}

function amqp_on_login(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_login: [%s]', JSON.stringify(msgobj.body));

    if(msgobj.body.login_uuid!=socket.uuid)
    {
        sessions.in(socket.id).emit('login',{'msg':JSON.stringify(msgobj.body)});
    }
}

function amqp_on_get_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_get_attr: [%s]', JSON.stringify(msgobj.body));
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('getAttr',{'msg':JSON.stringify(msgobj.body)});
}

function amqp_on_set_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_set_attr: [%s]', JSON.stringify(msgobj.body));
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('setAttr',{'msg':JSON.stringify(msgobj.body)});
}

function amqp_on_get_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_get_subdev_attr: [%s]', JSON.stringify(msgobj.body));
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('getSubdevAttr',{'msg':JSON.stringify(msgobj.body)});
}

function amqp_on_set_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_set_subdev_attr: [%s]', JSON.stringify(msgobj.body));
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('setSubdevAttr',{'msg':JSON.stringify(msgobj.body)});
}

function amqp_on_add_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_add_attr: [%s]', JSON.stringify(msgobj.body));
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('addAttr',{'msg':JSON.stringify(msgobj.body)});
}


function amqp_on_del_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_del_attr: [%s]', JSON.stringify(msgobj.body));
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('delAttr',{'msg':JSON.stringify(msgobj.body)});
}


function amqp_on_report_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    //logger.info('amqp_on_report_attr: [%s]', JSON.stringify(msgobj.body));
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('reportAttr',{'msg':JSON.stringify(msgobj.body)});
}


function amqp_on_report_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    logger.info('amqp_on_report_subdev_attr: [%s]', JSON.stringify(msgobj.body));
    msgobj.body.deviceID = msgobj.header.deviceID;
    sessions.in(socket.id).emit('reportSubdevAttr',{'msg':JSON.stringify(msgobj.body)});
}


/////////////////////////////////////////////////////////////////////////////////////////
// SOCKET.IO from cleint. forward to device (AMQP)
// from cleint: "callService"
function socket_on_call_service(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    //authDevice의 경우 디바이스 세션키를 전달받는데. 이겂을 꼭 현재 queue에 바인딩 해야한다. 
    //디바이스 세션키는 report데이터를 받는데 필요

    // session information check
    var idx = check_device_session(socket, msgobj.header.deviceUID);
    var device_session_key = "";
    var hashkey = socket.service_hashkey;
    if(idx<0)
    {
        if(!msgobj.body.serviceName || msgobj.body.serviceName !="authDevice")
        {
            logger.info("before login with [%s].", msgobj.header.deviceUID);
            var errormsg = {code:"401", uuid:msgobj.header.deviceUID, msgID: "callService", message:"unauthorized"};
            sessions.in(socket.id).emit('error',{'msg':JSON.stringify(errormsg)});
            return;
        } 
    }
    else
    {
        device_session_key = socket.device_sessions[idx].session_key;
    }

    if(msgobj.body.serviceName !="authDevice")
        hashkey = socket.device_sessions[idx].secret;

    // sock.io로 부터의 메시지를 AMQP용 메시지로 변경
    var resamqpobj = call_service_handler(socket.service_code, hashkey, device_session_key, msgobj);
    // resobj를 amqp로 장치로 전송한다. 
    var targetDeviceID = msgobj.header.deviceUID;
    var res = amqp_ch.publish(amqp_exhg, targetDeviceID, new Buffer(JSON.stringify(resamqpobj)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("callService: push to [%s] [%s]", targetDeviceID, JSON.stringify(resamqpobj));
}

/////////////////////////////////////////////////////////////////////////////////////////
// from cleint: "login"
// 스마트폰에서 로그인 상태에서 socket.io 세선을 새로 만든 경우 login event를 전송한다.
// 서버는 해당 소켓 핸들에 사용하는 amqp에 userkey_xxxxx 를 등록한다. 
// 
function socket_on_login(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    // check login request validations
    var service_code = msgobj.body.service_code==null ? "" : msgobj.body.service_code;
    var user_key = msgobj.body.user_key==null ? "" : msgobj.body.user_key;
    var login_token = msgobj.body.login_token==null ? "" : msgobj.body.login_token;
    var msgbody = msgobj.body;
    var login_uuid =  msgobj.header.deviceUID==null ? "" : msgobj.header.deviceUID;

    if(service_code=="" || user_key=="" || user_key.length!=24 || login_token=="" || login_uuid=="" || (login_uuid!=socket.uuid))
    {
        sessions.in(socket.id).emit('logout',{'msg':JSON.stringify(msgbody)});
        logger.info("login: invalid login event uuid:[%s] msg[%s]", login_uuid, JSON.stringify(msgbody));
        return;
    }
    users.findOne(
       {
           "$and": [
               {"service_code": service_code}, 
               {"_id":mongoose.Types.ObjectId(user_key)}
          ]        
       }
    )
    .exec(function(err, user) 
    {
        if(err!=null || user==null)
        {
            sessions.in(socket.id).emit('logout',{'msg':JSON.stringify(msgbody)});
            logger.info("login: invalid login event uuid:[%s] user_key:[%s] msg[%s]", login_uuid, user_key, JSON.stringify(msgbody));
            return;
        }

        if(user.login_token!=login_token)
        {
            sessions.in(socket.id).emit('logout',{'msg':JSON.stringify(msgbody)});
            logger.info("login: invalid login event uuid:[%s] login_token:[%s] msg[%s]", login_uuid, login_token, JSON.stringify(msgbody));
            return;
        }

        msgobj.body.login_uuid = login_uuid;
        var resamqpobj = login_handler(socket.service_code, socket.service_hashkey, "", msgobj);
        var user_key = msgobj.body.user_key;
        var bind_user_key = "userkey_" + user_key;
    
        // user key를 등록한다. 
        amqp_ch.bindQueue(queue, amqp_exhg, bind_user_key);
    
        var res = amqp_ch.publish(amqp_exhg, bind_user_key, new Buffer(JSON.stringify(resamqpobj)), 
                {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
                
        logger.info("login: multicast to [%s] [%s]", bind_user_key, JSON.stringify(resamqpobj));
    
    });
}

/////////////////////////////////////////////////////////////////////////////////////////
// from cleint: "getAttr"
function socket_on_get_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    var resamqpobj = get_attr_handler(socket.service_code, socket.device_sessions[idx].secret, socket.device_sessions[idx].session_key, msgobj);
    // resobj를 amqp로 장치로 전송한다. 
    var targetDeviceID = msgobj.header.deviceUID;
    var res = amqp_ch.publish(amqp_exhg, targetDeviceID, new Buffer(JSON.stringify(resamqpobj)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("getAttr: push to [%s] [%s]", targetDeviceID, JSON.stringify(resamqpobj));
}

/////////////////////////////////////////////////////////////////////////////////////////
// from cleint: "setAttr"
function socket_on_set_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    var resamqpobj = set_attr_handler(socket.service_code, socket.device_sessions[idx].secret, socket.device_sessions[idx].session_key, msgobj);
    // resobj를 amqp로 장치로 전송한다. 
    var targetDeviceID = msgobj.header.deviceUID;
    var res = amqp_ch.publish(amqp_exhg, targetDeviceID, new Buffer(JSON.stringify(resamqpobj)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("setAttr: push to [%s] [%s]", targetDeviceID, JSON.stringify(resamqpobj));
}

// from cleint: "getSebdevAttr"
function socket_on_get_subdev_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    var resamqpobj = get_subdev_attr_handler(socket.service_code, socket.device_sessions[idx].secret, socket.device_sessions[idx].session_key, msgobj);
    // resobj를 amqp로 장치로 전송한다. 
    var targetDeviceID = msgobj.header.deviceUID;
    var res = amqp_ch.publish(amqp_exhg, targetDeviceID, new Buffer(JSON.stringify(resamqpobj)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("getSuddevAttr: push to [%s] [%s]", targetDeviceID, JSON.stringify(resamqpobj));
}

/////////////////////////////////////////////////////////////////////////////////////////
// from cleint: "setSebdevAttr"
function socket_on_set_subdev_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    var resamqpobj = set_subdev_attr_handler(socket.service_code, socket.device_sessions[idx].secret, socket.device_sessions[idx].session_key, msgobj);
    // resobj를 amqp로 장치로 전송한다. 
    var targetDeviceID = msgobj.header.deviceUID;
    var res = amqp_ch.publish(amqp_exhg, targetDeviceID, new Buffer(JSON.stringify(resamqpobj)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("setSuddevAttr: push to [%s] [%s]", targetDeviceID, JSON.stringify(resamqpobj));
}

/////////////////////////////////////////////////////////////////////////////////////////
// from cleint: "addAttr"
function socket_on_add_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    var resamqpobj = add_attr_handler(socket.service_code, socket.device_sessions[idx].secret, socket.device_sessions[idx].session_key, msgobj);
    // resobj를 amqp로 장치로 전송한다. 
    var targetDeviceID = msgobj.header.deviceUID;
    var res = amqp_ch.publish(amqp_exhg, targetDeviceID, new Buffer(JSON.stringify(resamqpobj)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("addAttr: push to [%s] [%s]", targetDeviceID, JSON.stringify(resamqpobj));
}

/////////////////////////////////////////////////////////////////////////////////////////
// from cleint: "delAttr"
function socket_on_del_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj )
{
    var resamqpobj = del_attr_handler(socket.service_code, socket.device_sessions[idx].secret, socket.device_sessions[idx].session_key, msgobj);
    // resobj를 amqp로 장치로 전송한다. 
    var targetDeviceID = msgobj.header.deviceUID;
    var res = amqp_ch.publish(amqp_exhg, targetDeviceID, new Buffer(JSON.stringify(resamqpobj)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("delAttr: push to [%s] [%s]", targetDeviceID, JSON.stringify(resamqpobj));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
// external functions
var socket_client = {
    // RECV MSG from AMQP. it is from DEVICEs
    ampq_consumer: function(msgID, sessions, socket, amqp_ch, queue, amqp_exhg, msgobj) {
        if(socket.clientkey!=msgobj.header.clientKey)
        {
          logger.debug("socket_client.ampq_consumer msg id[%s] socket.clientkey[%s] msg.header.clientKey[%s]", 
          msgID, socket.clientkey, msgobj.header.clientKey )
        }

        // check hash_key
        var idx = check_device_session(socket, msgobj.header.deviceID);
        var hash_key = socket.service_hashkey;
        if(idx>=0)
        {
            // authDevice의 경우 세션이 있더라도.. 서비스 key를 사용 
            if(msgobj.header.msgID!="callService" || msgobj.body.serviceName!="authDevice")
            {
                hash_key = socket.device_sessions[idx].secret;
                console.log("idx:", idx, "hash_key", hash_key);
            }
            else
                console.log("service_hashkey", hash_key);
        }
        else 
            console.log("service_hashkey", hash_key);

        var msghash = waviotmsg.make_waviot_hashcode(msgobj.body, "base64", hash_key);
        var msgorghash = "";
        if(msgobj.header.hashcode)  msgorghash = msgobj.header.hashcode;

        if(msgorghash!=msghash)
        {
            logger.info("Invalid WAVIOT CLIENT MSG!! DEV HASH[%s] SVC HASH [%s]",msgorghash, msghash );
            logger.info("Invalid WAVIOT CLIENT MSG!! [%s]",JSON.stringify(msgobj) );
            
            return;
        }
        var idx = check_device_session(socket, msgobj.header.deviceID);
        if(idx>=0)
        {
            if(msgobj.header.msgID=="reportAttr" || msgobj.header.msgID=="reportSubdevAttr" )
            {
                // reportAttr과 reportSubdevAttr의 경우에는 일방적으로 전달되는 것으로 장치 로그인 정보의 session_time이
                // 있으면 그정보를 확인한다. 
                if(socket.device_sessions[idx].use_session_expire == true 
                    && check_session_expired(socket.device_sessions[idx].session_time)>0)
                {
                    amqp_ch.unbindQueue(queue, amqp_exhg, socket.device_sessions[idx].session_key);
                    socket.device_sessions.splice(idx, 1);
                    return;
                }
            }
            else
            if(check_device_response_session(msgobj)==false)
            {
                // remove device session info and unbind queue
                if(idx>=0)
                {
                    amqp_ch.unbindQueue(queue, amqp_exhg, socket.device_sessions[idx].session_key);
                    socket.device_sessions.splice(idx, 1);
                    return;
                }
            }

        }

        switch(msgID)
        {
            case "callService" :
            amqp_on_call_service(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "login" :
            amqp_on_login(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "getAttr" :
            amqp_on_get_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "setAttr" :
            amqp_on_set_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "getSubdevAttr" :
            amqp_on_get_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "setSubdevAttr" :
            amqp_on_set_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "addAttr" :
            amqp_on_add_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "delAttr" :
            amqp_on_del_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "reportAttr" :
            amqp_on_report_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            case "reportSubdevAttr" :
            amqp_on_report_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
            break;
            default:
            logger.info("userDefined Message ["+msgobj.header.msgID+"]");
            msgobj.body.deviceID = msgobj.header.deviceID;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
        }

    },

    // RECV MSG from SOCKET.IO. it is from a client
    socket_io_event: function(event_name, sessions, socket, amqp_ch, queue, amqp_exhg, data) {
        logger.info('socket_io_event event[%s] : [%s]', event_name, data==null?"":data);


        try {
            var msgobj = null; 
            if(data!=null && data!="")
            {
                msgobj  = JSON.parse(data);
                // session information check
                var idx = check_device_session(socket, msgobj.header.deviceUID);
                if(idx<0 && event_name!="callService" && event_name!="login")
                {
                    /* callService의 경우 내부에서 처리 */
                    logger.info("before login with [%s].", msgobj.header.deviceUID);
                    var errormsg = {code:"401", uuid:msgobj.header.deviceUID, msgID: event_name, message:"unauthorized"};
                    sessions.in(socket.id).emit('error',{'msg':JSON.stringify(errormsg)});
                    return;
                }

                if(waviotmsg.check_msg_validation(msgobj, socket.session_secret)==false)
                {
                    logger.info("Invalid WAVIOT.SOCK MSG!!");
                    var errormsg = {code:"1000", uuid:msgobj.header.deviceUID, msgID: event_name, message:"Invalid Message"};
                    sessions.in(socket.id).emit('error',{'msg':JSON.stringify(errormsg)});
                    return;
                }

            }

            if(msgobj!=null)
            {
                switch(event_name)
                {
                    case "disconnect":
                        unbind_device_allsession (socket, amqp_ch, amqp_exhg, queue);
                        break;
                    case "callService":
                        socket_on_call_service(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj );
                        break;
                    case "login":
                        socket_on_login(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj );
                        break;
                    case "getAttr":
                        socket_on_get_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj );
                        break;
                    case "setAttr":
                        socket_on_set_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj );
                        break;
                    case "getSubdevAttr":
                        socket_on_get_subdev_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj );
                        break;
                    case "setSubdevAttr":
                        socket_on_set_subdev_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj );
                        break;
                    case "addAttr":
                        socket_on_add_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj );
                        break;
                    case "delAttr":
                        socket_on_del_attr(sessions, idx, socket, amqp_ch, queue, amqp_exhg, msgobj );
                        break;
    
                }
            }
            else
            {
                switch(event_name)
                {
                    case "disconnect":
                        unbind_device_allsession (socket, amqp_ch, amqp_exhg, queue);
                        break;
                }                    
            }
        } catch (error) {
        logger.info(">>> socket_io_event event[" +event_name +"] ERROR!! / " + error);         
        }
        
    },

    null_function: function (){}
};


module.exports = socket_client;
