// sockets.js
var sessionio = require('socket.io');
var waviotmsg = require("../modules/waviotmsg");
var amqp = require('amqplib');
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var apiClients = require('../models/apiClients');
var serviceCode = require('../models/serviceCode');
var crypto = require('crypto');
var socketDevice = require("./io.session.device");
var socketClient = require("./io.session.client");
var logger = global.logger;
var cloudClientStorage = require('../models/cloudClientStorage');

// today - expired
// under 0  : remain time 
function check_session_expired(session_expired)
{
    var today = moment();    
    var expired = moment(session_expired);
    return  today.diff(expired, 'minutes');
}


function check_live_session_expired(socket, service_code, key, session_key, session_time)
{
    var remain_time = check_session_expired(session_time)
    if(remain_time>0)
    {
      // check session time again..
      apiClients.findOne({service_code: service_code, key:key, session_key:session_key })
      .exec(function(err, item) 
      {
          if(err || item==null)
          {
            logger.info("Session ["+session_key+"] is expired. Invalid Session!!");
            socket.disconnect();
            return;
          }
          // check new session is updated?
          remain_time = check_session_expired(item.session_time);
          if(remain_time<=0)
          {
            logger.info("Session ["+session_key+"] is updated. [1]");
            socket.session_time = item.session_time;
          }
          else
          {
            logger.info("Session ["+session_key+"] is expired. Invalid Session[3]!!");
            socket.disconnect();
            return;
          }
      });
    }
    else if(remain_time>-30)
    {
      // if reamain_time is below 30 mins, update session from DB
      apiClients.findOne({service_code: service_code, key:key, session_key:session_key })
      .exec(function(err, item) 
      {
          if(err || item==null)
          {
            logger.info("Session ["+session_key+"] is expired. Invalid Session[2]!!");
            socket.disconnect();
            return;
          }
          // check new session is updated?
          remain_time = check_session_expired(item.session_time);
          if(remain_time<=0)
          {
            logger.info("Session ["+session_key+"] is updated. [2]");
            socket.session_time = item.session_time;
          }
      });
    }
}

function isUseSeviceHashKey(msgobj)
{
  if(msgobj.header.msgID=="resHeartbeat") return true;
  if(msgobj.header.msgID=="callService" && msgobj.body.serviceName=="authDevice") return true;
  
  return false;
}

function cloudSetValueHandler(sessions, socket, msgobj)
{
    var keyName = msgobj.body.keyName;
    var keyValue = msgobj.body.keyValue;
    var resBody = {resCode:0, keyName:keyName};

    cloudClientStorage.findOne()
        .where('service_code').equals(socket.service_code)
        .where('uuid').equals(socket.uuid)
        .where('client_key').equals(socket.clientkey)
        .where('key_name').equals(keyName)
        .exec(function(err, item) {
        if(err)
        {
            resBody.resCode = -1;
            logger.info("cloudSetValueHandler : DB FIND ERROR!!", service_code, uuid, key, value);
            sessions.in(socket.id).emit("cloudSetValue",{'msg':JSON.stringify(resBody)});
        }
        else
        {
            if(item==null)
            {
                var newCloudValue= new cloudClientStorage({
                    service_code: socket.service_code, 
                    uuid: socket.uuid,
                    client_key: socket.clientkey,
                    key_name:keyName,
                    key_value: keyValue
                });


                cloudClientStorage.findOneAndUpdate(
                    {service_code: socket.service_code, uuid: socket.uuid, client_key:socket.clientkey, key_name: keyName}, 
                    newCloudValue, 
                    {upsert: true, new: true, runValidators: true}, // options
                    function (err, doc) { // callback
                        if(err)
                        {
                          resBody.resCode = -1;
                          logger.info("cloudSetValueHandler : DB APPEND ERROR!!");
                        }
                        sessions.in(socket.id).emit("cloudSetValue",{'msg':JSON.stringify(resBody)});

                    }
                );                     
                /*
                newCloudValue.save(function(err, item){
                    if(err)
                    {
                        resBody.resCode = -1;
                        logger.info("cloudSetValueHandler : DB APPEND ERROR!!");
                    }
                    sessions.in(socket.id).emit("cloudSetValue",{'msg':JSON.stringify(resBody)});
                });
*/
            }
            else
            {
                item.last_updated_date = Date.now();
                item.key_value = keyValue;
                item.save_count++;
                item.save(function(err, updateitem) {
                    if(err) {
                        resBody.resCode = -1;
                        logger.info("cloudSetValueHandler : DB UPDATE ERROR!!");
                    }
                    sessions.in(socket.id).emit("cloudSetValue",{'msg':JSON.stringify(resBody)});
                });

            }
        }
    });
}

function cloudGetValueHandler(sessions, socket, msgobj)
{
    var keyName = msgobj.body.keyName;
    var keyValue = "";
    var resBody = {resCode:0, keyName:keyName, keyValue:""};

    cloudClientStorage.findOne()
        .where('service_code').equals(socket.service_code)
        .where('uuid').equals(socket.uuid)
        .where('client_key').equals(socket.clientkey)
        .where('key_name').equals(keyName)
        .exec(function(err, item) {
        if(err || item==null)
        {
            if(err)
            {
              resBody.resCode = -1; // system error or no data
              logger.info("cloudClientStorage : DB FIND ERROR!!", service_code, uuid, key, value);
            }
            else
              resBody.resCode = 404;
            sessions.in(socket.id).emit("cloudGetValue",{'msg':JSON.stringify(resBody)});
        }
        else
        {
            item.last_inquired_date = Date.now();
            item.restore_count++;
            resBody.keyValue = item.key_value;
            item.save(function(err, updateitem) {
                if(err) {
                    logger.info("cloudGetValueHandler : DB UPDATE ERROR but ignore!!");
                } 
                sessions.in(socket.id).emit("cloudGetValue",{'msg':JSON.stringify(resBody)});
            });
        }
    });
}



/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
function handle_connection(ns, sessions, amqp_conn, amqp_exhg)
{
  return function(socket) {
      logger.info("NAMESPACE:" + ns);
      logger.info("connect : " + socket.id + ", clientkey=" + socket.handshake.query['clientkey']+", sessionkey :" + socket.handshake.query['sessionkey']);

      // save session information. 
      socket.service_code = socket.handshake.query['service_code'];
      socket.clientkey = socket.handshake.query['clientkey'];
      socket.sessionkey = socket.handshake.query['sessionkey'];
      socket.device_sessions = [];

      //logger.info("connect : " + socket.id + ", service_code=" + socket.service_code);

      if(socket.service_code==null || socket.service_code=="" ||
         socket.clientkey==null || socket.clientkey=="" ||
         socket.sessionkey==null || socket.sessionkey=="" )
      {
        logger.info("Invalid session information!! [0]");
        socket.disconnect();
        return;
      }

      serviceCode.findOne({service_code: socket.service_code})
      .exec(function(err, item) {
          if(err || item==null)
          {
            logger.info("Invalid session information!! [1]");
            socket.disconnect();
            return;
          }

          socket.service_hashkey = item.service_hashkey;
          logger.info("Service Hash:" + socket.service_hashkey +".");

          // check sesion information. 
          apiClients.findOne({service_code: socket.service_code, key:socket.clientkey, session_key:socket.sessionkey })
          .exec(function(err, item) 
          {
              if(err || item==null)
              {
                logger.info("Invalid session information!! [1]");
                socket.disconnect();
                return;
              }
              if(check_session_expired(item.session_time)>0)
              {
                logger.info("Invalid session information!! [2]");
                socket.disconnect();
                return;
              }
              socket.session_time = item.session_time;
              socket.session_secret = item.secret;
              socket.uuid = item.uuid;

              // check device session or not
              if(item.is_device)
                socket.is_device = item.is_device;
              else
                socket.is_device = false;

              if(socket.is_device)
              {
                socket.routingkey = socket.uuid;             
                socket.systemRoutingKey = "rpc_" + socket.service_code;   
              }
              else
                socket.routingkey = socket.service_code+"_"+socket.clientkey+"/#"+socket.sessionkey;
              // 키정보에 문제가 없으면 AMQP 채널/큐 생성..
              // 아래 라우팅키로 rpc 응답을 받는다. 

              amqp_conn.createChannel().then(function(amqp_ch) {
                try {
                  // 서비스 데몬에서는 프로세스를 종료해서 재시작함으로 연결이 복구 될 수 있도록 했는데 
                  // socket.io 버번에서는 애매한 상황함. 
                  // 네트워크에러에 따른 재시작 부분에 대한 고민이 필요함. 
                  amqp_ch.on("error", function(err) {
                    logger.info(">>>>> AMQP channel error : " + socket.id + ", clientkey=" + socket.clientkey+", sessionkey :" + socket.sessionkey+", err:"+err.message);
                    if(socket.connected) socket.disconnect();
                  });
                  amqp_ch.on("close", function() {
                    logger.info(">>>>> AMQP channel be closed : " + socket.id + ", clientkey=" + socket.clientkey+", sessionkey :" + socket.sessionkey);
                    if(socket.connected) socket.disconnect();
                  });
                  var queue = 'queue_'+socket.id+"/#"+socket.sessionkey;
                  if(socket.is_device)
                    queue = 'queue_'+socket.uuid;
                  var ok = amqp_ch.assertQueue(queue, {autoDelete:true, durable: false});
                  var ok = amqp_ch.bindQueue(queue, amqp_exhg, socket.routingkey);
                  var ok = ok.then(function() {
                    amqp_ch.consume(queue, amqp_consumer, {noAck: true});
                  });
                  ok.then(function() {
                    if(socket.is_device)
                      logger.info('>>>>> New SOCK.IO SESSION: Client Key[%s], Session Key[%s].\nReady to Device service!!', socket.clientkey, socket.sessionkey);      
                    else
                      logger.info('>>>>> New SOCK.IO SESSION: Client Key[%s], Session Key[%s].\nReady to service!!', socket.clientkey, socket.sessionkey);      
                  });

                  /*
                    // socket.id is null now. so closed this function.
                  setInterval(function() {
                    sessions.in(socket.id).emit("socket.ping", socket.id);
                    logger.debug(">>>>> ping : [%d]", socket.id);
                  }, 10*60*1000);
                  */
                  /////////////////////////////////////////////////////////////////////////////////////////
                  /////////////////////////////////////////////////////////////////////////////////////////
                  function amqp_consumer(msg) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    try {
                      var msgobj = JSON.parse(msg.content.toString());

                      if(socket.is_device==false)
                      {
                        // sock.io 클라이언트에 대한 해더 hashkey검사는 안쪽에서 처리 
                        // call sevice:authDevice는 서비스 해쉬키를 사용하고 나머지는 디바이스 secret값을 사용한다. 
                        // a clinet got a message from AMQP : 
                        socketClient.ampq_consumer(msgobj.header.msgID, sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                      }
                      else
                      {
                        // a device got a message from AMQP 
                        // 2016.12.30 해쉬코드 생성시에 서비스 데몬과 authDevice 서버스 콜은 서비스 hash 키를 사용하고 
                        // 그외는 모드 장치의 secret값을 사용한다. 
                        var devicehash = "";
                        if(isUseSeviceHashKey(msgobj))
                          devicehash = waviotmsg.make_waviot_hashcode(msgobj.body, "base64", socket.service_hashkey);
                        else
                          devicehash = waviotmsg.make_waviot_hashcode(msgobj.body, "base64", socket.session_secret);

                        var deviceorghash = "";
                        if(msgobj.header.hashcode)  deviceorghash = msgobj.header.hashcode;

                        if(deviceorghash!=devicehash)
                        {
                          logger.info("Invalid WAVIOT MSG!! DEV HASH[%s] SVC HASH [%s]",deviceorghash, devicehash );
                          return;
                        }

                        // 디바이스 수신 메시지는 클라이언트로 부터 전달된 회신 정보가 있을 수 있음
                        // 이 정보를 xclient항목으로 정의하고 그 값을 채워넣는다. 
                        var xclient = {replyTo:"", correlationId:"", sessionKey:msgobj.header.sessionKey};
                        if(msg.properties)
                        {
                          if(msg.properties.replyTo) xclient.replyTo = msg.properties.replyTo;
                          if(msg.properties.correlationId) xclient.correlationId = msg.properties.correlationId;
                        }

                        socketDevice.ampq_consumer(msgobj.header.msgID, sessions, socket, amqp_ch, queue, amqp_exhg, msgobj, xclient);
                      }

                      /*
                      // check message invalidation
                      var devicehash = waviotmsg.make_waviot_hashcode(msgobj.body, "base64", socket.service_hashkey);
                      var deviceorghash = "";

                      if(msgobj.header.hashcode)  deviceorghash = msgobj.header.hashcode;

                      if(deviceorghash!=devicehash)
                      {
                        logger.info("Invalid WAVIOT MSG!! DEV HASH[%s] SVC HASH [%s]",deviceorghash, devicehash );
                        return;
                      }
                      
                      if(socket.is_device==false)
                        socketClient.ampq_consumer(msgobj.header.msgID, sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                      else
                      {
                        var xclient = {replyTo:"", correlationId:"", sessionKey:msgobj.header.sessionKey};
                        if(msg.properties)
                        {
                          if(msg.properties.replyTo) xclient.replyTo = msg.properties.replyTo;
                          if(msg.properties.correlationId) xclient.correlationId = msg.properties.correlationId;
                        }

                        socketDevice.ampq_consumer(msgobj.header.msgID, sessions, socket, amqp_ch, queue, amqp_exhg, msgobj, xclient);
                      }
                      */                  
                    } catch (error) {
                      logger.info(">>>>> device_consumer: " + socket.id + ", error " + error);
                    }

                    /*
                    logger.info("device_consumer: msg ["+msg.content.toString()+"]");
                    sessions.in(socket.id).emit('message_send',{'msg':msg.content.toString(),'from':'AMQP'});
                    */
                  }
                  /////////////////////////////////////////////////////////////////////////////////////////
                  /////////////////////////////////////////////////////////////////////////////////////////
                  /////////////////////////////////////////////////////////////////////////////////////////

                  socket.on('disconnect', function () {
                    try {
                      logger.info(">>>>> disconnect : " + socket.id + ", clientkey=" + socket.clientkey+", sessionkey :" + socket.sessionkey);

                      if(socket.is_device==false)
                        socketClient.socket_io_event("disconnect", sessions, socket, amqp_ch, queue, amqp_exhg, "");
                        
                      amqp_ch.unbindQueue(queue, amqp_exhg, socket.routingkey);
                      amqp_ch.deleteQueue(queue);
                      amqp_ch.close();
                      
                    } catch (error) {
                      logger.info(">>>>> session:disconnect: " + error);            
                    }
                  });

                  // wlink sock.io messages
                  socket.on('reqHeartbeat', function(data)
                  {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////
                    
                    if(socket.is_device==true)
                      socketDevice.socket_io_event("reqHeartbeat", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });
                  
                  socket.on('pushEvent', function(data)
                  {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////
                    
                    if(socket.is_device==true)
                      socketDevice.socket_io_event("pushEvent", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('callService', function(data)
                  {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////
                    
                    if(socket.is_device==false)
                      socketClient.socket_io_event("callService", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                    else
                      socketDevice.socket_io_event("callService", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('login', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("login", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('getAttr', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("getAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                    else
                      socketDevice.socket_io_event("getAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('setAttr', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("setAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                    else
                      socketDevice.socket_io_event("setAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('getSubdevAttr', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("getSubdevAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                    else
                      socketDevice.socket_io_event("getSubdevAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('setSubdevAttr', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("setSubdevAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                    else
                      socketDevice.socket_io_event("setSubdevAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('addAttr', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("addAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                    else
                      socketDevice.socket_io_event("addAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('delAttr', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("delAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                    else
                      socketDevice.socket_io_event("delAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('bindSessionKey', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==true)
                      socketDevice.socket_io_event("bindSessionKey", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('unbindSessionKey', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==true)
                      socketDevice.socket_io_event("unbindSessionKey", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('reportAttr', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==true)
                      socketDevice.socket_io_event("reportAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('reportSubdevAttr', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==true)
                      socketDevice.socket_io_event("reportSubdevAttr", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('bindDevice', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("bindDevice", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('unbindDevice', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////

                    if(socket.is_device==false)
                      socketClient.socket_io_event("unbindDevice", sessions, socket, amqp_ch, queue, amqp_exhg, data);
                  });

                  socket.on('cloudSetValue', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////
                    var msgobj = JSON.parse(data);
                    logger.info("cloudSetValue:", data);
                    if(waviotmsg.check_msg_validation(msgobj, socket.session_secret)==false)
                    {
                        logger.info("Invalid WAVIOT.SOCK MSG!!");
                        var errormsg = {code:"1000", uuid:msgobj.header.deviceUID, msgID: 'cloudSetValue', message:"Invalid Message"};
                        sessions.in(socket.id).emit('error',{'msg':JSON.stringify(errormsg)});
                        return;
                    }

                    cloudSetValueHandler(sessions, socket,msgobj);
                  });

                  socket.on('cloudGetValue', function(data) {
                    /////////////////////////////////////////////////////////////////
                    // check session expired.
                    check_live_session_expired(socket, socket.service_code, socket.clientkey, socket.sessionkey, socket.session_time);
                    /////////////////////////////////////////////////////////////////
                    var msgobj = JSON.parse(data);
                    logger.info("cloudGetValue:", data);
                    if(waviotmsg.check_msg_validation(msgobj, socket.session_secret)==false)
                    {
                        logger.info("Invalid WAVIOT.SOCK MSG!!");
                        var errormsg = {code:"1000", uuid:msgobj.header.deviceUID, msgID: 'cloudGetValue', message:"Invalid Message"};
                        sessions.in(socket.id).emit('error',{'msg':JSON.stringify(errormsg)});
                        return;
                    }
                    cloudGetValueHandler(sessions, socket,msgobj);
                  });
                
                } catch (error) {
                  logger.error(">>>>> Cacth AMQP error : " + error);
                }
              });  // AMQP

          });

      });
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
module.exports.listen = function(app, amqp_list) {
    io = sessionio.listen(app);

    logger.info(">>>>> Starting SOCK.IO SESSION.....");
    
    for(idx in amqp_list)
    {
      sessions = io.of(amqp_list[idx].ns);
      sessions.on('connection', handle_connection(amqp_list[idx].ns, sessions, amqp_list[idx].conn, amqp_list[idx].exhg));
    }

  return io;
}
