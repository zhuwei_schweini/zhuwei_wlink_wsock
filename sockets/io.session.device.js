var sessionio = require('socket.io');
var waviotmsg = require("../modules/waviotmsg");
var amqp = require('amqplib');
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var apiClients = require('../models/apiClients');
var serviceCode = require('../models/serviceCode');
var crypto = require('crypto');
var logger = global.logger;

///////////////////////////////////////////////////////////////////////////////////////////////////
// 아래 메시지는 시스템 데몬과 통신하는 메시지로 hash key로 service hashhey를 사용
function socket_req_heartbeat(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("reqHeartbeat", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.service_hashkey, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, socket.systemRoutingKey, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("[SOCK] reqHeartbeat: push to [%s] / [%s] [%d] [%s]", socket.systemRoutingKey, msgobj.body.targetUID, msgobj.body.challenge, JSON.stringify(res));
    logger.debug("reqHeartbeat: MSG [%s]", JSON.stringify(devicemsg));
}

function socket_push_event(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("pushEvent", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.service_hashkey, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, socket.systemRoutingKey, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: "none", replyTo: socket.routingkey });
    logger.info("[SOCK] pushEvent: push to [%s] / [%s] [%s] [%s]", socket.systemRoutingKey, socket.uuid, msgobj.body.eventType, JSON.stringify(res));
    logger.debug("pushEvent: MSG [%s]", JSON.stringify(devicemsg));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
function socket_call_service(sessions, socket, amqp_ch, queue, amqp_exhg, hashkey,  msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("callService", socket.service_code, socket.uuid, msgobj.header.clientKey, "", hashkey, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, msgobj.xclient.replyTo, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: msgobj.xclient.correlationId });
    logger.info("[SOCK] callService: push to [%s] / [%s] [%s]", msgobj.xclient.replyTo, socket.uuid, JSON.stringify(res));
    logger.debug("callService: hashkey [%s]", hashkey);
    logger.debug("callService: MSG [%s]", JSON.stringify(devicemsg));
}

function socket_get_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("getAttr", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, msgobj.xclient.replyTo, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: msgobj.xclient.correlationId });
    logger.info("[SOCK] getAttr: push to [%s] / [%s] [%s]", msgobj.xclient.replyTo, socket.uuid, JSON.stringify(res));
    logger.debug("getAttr: MSG [%s]", JSON.stringify(devicemsg));
}

function socket_set_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("setAttr", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, msgobj.xclient.replyTo, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: msgobj.xclient.correlationId });
    logger.info("[SOCK] setAttr: push to [%s] / [%s] [%s]", msgobj.xclient.replyTo, socket.uuid, JSON.stringify(res));
    logger.debug("setAttr: MSG [%s]", JSON.stringify(devicemsg));
}

function socket_get_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("getSubdevAttr", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, msgobj.xclient.replyTo, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: msgobj.xclient.correlationId });
    logger.info("[SOCK] getSubdevAttr: push to [%s] / [%s] [%s]", msgobj.xclient.replyTo, socket.uuid, JSON.stringify(res));
    logger.debug("getSubdevAttr: MSG [%s]", JSON.stringify(devicemsg));
}

function socket_set_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("setSubdevAttr", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, msgobj.xclient.replyTo, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: msgobj.xclient.correlationId });
    logger.info("[SOCK] setSubdevAttr: push to [%s] / [%s] [%s]", msgobj.xclient.replyTo, socket.uuid, JSON.stringify(res));
    logger.debug("setSubdevAttr: MSG [%s]", JSON.stringify(devicemsg));
}

function socket_add_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("addAttr", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, msgobj.xclient.replyTo, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: msgobj.xclient.correlationId });
    logger.info("[SOCK] addAttr: push to [%s] / [%s] [%s]", msgobj.xclient.replyTo, socket.uuid, JSON.stringify(res));
    logger.debug("addAttr: MSG [%s]", JSON.stringify(devicemsg));
}

function socket_del_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("delAttr", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, msgobj.xclient.replyTo, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: msgobj.xclient.correlationId });
    logger.info("[SOCK] delAttr: push to [%s] / [%s] [%s]", msgobj.xclient.replyTo, socket.uuid, JSON.stringify(res));
    logger.debug("delAttr: MSG [%s]", JSON.stringify(devicemsg));
}

function socket_error(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    var devicemsg =  waviotmsg.make_waviot_msg("error", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
    var res = amqp_ch.publish(amqp_exhg, msgobj.xclient.replyTo, new Buffer(JSON.stringify(devicemsg)), 
            {contentType: "application/json", deliveryMode:2, correlationId: msgobj.xclient.correlationId });
    logger.info("[SOCK] error: push to [%s] / [%s] [%s]", msgobj.xclient.replyTo, socket.uuid, JSON.stringify(res));
    logger.debug("ERROR: MSG [%s]", JSON.stringify(devicemsg));
}


function socket_bind(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    if(msgobj.body.bindkey)
    {
        socket.bindkey = msgobj.body.bindkey;
        logger.info("[SOCK] socket_bind: new key [%s]", socket.bindkey);
    }
    else
        logger.info("[SOCK] socket_bind: ERROR");
}

function socket_unbind(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    socket.bindkey = "";
    logger.info("[SOCK] socket_unbind");
}

function socket_report_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    if(socket.bindkey)
    {
        // hashkey는 디바이스 세션키로 함.
        var devicemsg =  waviotmsg.make_waviot_msg("reportAttr", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
        var res = amqp_ch.publish(amqp_exhg, socket.bindkey, new Buffer(JSON.stringify(devicemsg)), 
                {contentType: "application/json", deliveryMode:2});
        logger.info("[SOCK] reportAttr: push to [%s]", JSON.stringify(res));
    }
    else
        logger.info("[SOCK] reportAttr: NO BINDING KEY!!");
}

function socket_report_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj)
{
    if(socket.bindkey)
    {
        // hashkey는 디바이스 세션키로 함.
        var devicemsg =  waviotmsg.make_waviot_msg("reportSubdevAttr", socket.service_code, socket.uuid, msgobj.header.clientKey, "", socket.session_secret, msgobj.body); 
        var res = amqp_ch.publish(amqp_exhg, socket.bindkey, new Buffer(JSON.stringify(devicemsg)), 
                {contentType: "application/json", deliveryMode:2});
        logger.info("[SOCK] reportSubdevAttr: push to [%s]", JSON.stringify(res));
    }
    else
        logger.info("[SOCK] reportSubdevAttr: NO BINDING KEY!!");
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
var socket_device = {
    // RECV MSG from AMQP. it is from clients or System Daemon
    // 클라이언트로 부터 수신된 메시지 amqp메시지 임 
    ampq_consumer: function(msgID, sessions, socket, amqp_ch, queue, amqp_exhg, msgobj, xclient) {

        logger.debug("DEVICE:ampq_consumer :" + JSON.stringify(msgobj));
        /* device session */
        /*
            클라이언트로 부터 전달되는 callService, getAttr, setAttr, addAttr, delAttr
            요청에 대해서 디바이스로 전송. 클라이언트의 rpc replyTo, correlationId 값을 보존해야 함. 
        */
        switch(msgID)
        {
            // from system daemon
            case "resHeartbeat" : 
            /* 디바이스 socket.io에서는 매30분 마다 전송해야하고..  이 응답에는 세션 정보업데이트가 있다. 
                디바이스에게 신규 세션값을 전달해야 함. 
            */
            logger.info("[AMQP] resHeartbeat:", msgobj.body.targetUID, msgobj.body.response);
            msgobj.body.xclient = xclient;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;

            // from clients
            case "callService" :  
            logger.info("[AMQP] callService:", msgobj.body.serviceName);
            msgobj.body.xclient = xclient;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
            case "getAttr" :
            logger.info("[AMQP] getAttr:", msgobj.body.attrName);
            msgobj.body.xclient = xclient;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
            case "setAttr" :
            logger.info("[AMQP] setAttr:", msgobj.body.attrName);
            msgobj.body.xclient = xclient;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
            case "getSubdevAttr" :
            logger.info("[AMQP] getSubdevAttr:", msgobj.body.subDeviceID, msgobj.body.attrName);
            msgobj.body.xclient = xclient;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
            case "setSubdevAttr" :
            logger.info("[AMQP] setSubdevAttr:", msgobj.body.subDeviceID, msgobj.body.attrName);
            msgobj.body.xclient = xclient;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
            case "addAttr" :
            logger.info("[AMQP] addAttr:");
            msgobj.body.xclient = xclient;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
            case "delAttr" :
            logger.info("[AMQP] delAttr:");
            msgobj.body.xclient = xclient;
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
            default:
            logger.info("[AMQP] userDefined Device Message ["+msgobj.header.msgID+"]");
            sessions.in(socket.id).emit(msgobj.header.msgID,{'msg':JSON.stringify(msgobj.body)});
            break;
        }

    },

    // RECV from SOCKET.IO. it is from device.
    // 장치에서 전달된 메시지를 클라인트측의 amqp queue로 전송함.
    socket_io_event: function(event_name, sessions, socket, amqp_ch, queue, amqp_exhg, data) {
        logger.debug('DEVICE:socket_io_event event[%s] : [%s]', event_name, data==null?"":data);

        try {
            var msgobj=null;
            if(data!=null && data!="")
            {
                msgobj = JSON.parse(data);

                if(waviotmsg.check_msg_validation(msgobj, socket.session_secret)==false)
                {
                    logger.info("DEVICE: Invalid WAVIOT.SOCK MSG!!");
                    return;
                }
            }

            if(msgobj!=null)
            switch(event_name)
            {
                // to system daemon
                case "reqHeartbeat":
                    socket_req_heartbeat(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                case "pushEvent" :
                    socket_push_event(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                    
                // 장치에서 능동적으로 발생하는 메시지(reportAttr)를 전달하기 위한 라우팅키 등록/해제 
                case "bindSessionKey": 
                    socket_bind(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                
                    break;
                case "unbindSessionKey": 
                    socket_unbind(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                
                // 아래 이벤트드른 클라이언트의 요청에 대한 응답임
                // AMQP 메시지를 생성해서 클라이언트가 요청시에 전달한 replyTo, correlationId로 전달하면 됨
                case "callService": 
                    // 2016.12.26 authDevice 응답은 서비스 해쉬키롤 처리, 나머지는 로그인 이후에므로 장치의 secret으로 처리 
                    if(msgobj.body.serviceName=="authDevice")
                    {
                        console.log(">>>>>>>> msgobj.serviceName==authDevice,",  socket.service_hashkey);
                        socket_call_service(sessions, socket, amqp_ch, queue, amqp_exhg, socket.service_hashkey, msgobj);

                    }
                    else

                    {
                        console.log(">>>>>>>> msgobj.serviceName!=authDevice,",  socket.session_secret);
                        socket_call_service(sessions, socket, amqp_ch, queue, amqp_exhg, socket.session_secret, msgobj);

                    }
                    break;
                case "getAttr": 
                    socket_get_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                case "setAttr": 
                    socket_set_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                case "getSubdevAttr": 
                    socket_get_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                case "setSubdevAttr": 
                    socket_set_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                case "addAttr": 
                    socket_add_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                case "delAttr": 
                    socket_del_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                case "error": 
                    socket_error(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;

                // 아래 이벤트는 장치에서 발생한 정보임
                // "bind"를 통해서 등록한 라우팅키로 전달함
                case "reportAttr": 
                    socket_report_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                case "reportSubdevAttr": 
                    socket_report_subdev_attr(sessions, socket, amqp_ch, queue, amqp_exhg, msgobj);
                    break;
                default:
                logger.info("userDefined Socket.io event ["+event_name+"]");
                break;
                

            }
            


        }catch (error) {
        logger.info(">>> DEVICE: socket_io_event event[" +event_name +"] JSON Parsing ERROR!! /" + error);         
        }



    },
    null_function: function (){}
};


module.exports = socket_device;
