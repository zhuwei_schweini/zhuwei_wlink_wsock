// server.js
// alex@win-star.com
// Date : 2016.11.12

var express = require('express');
var mongoose = require('mongoose');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var Redis = require('ioredis');
var amqp = require('amqplib');
var cluster = require('cluster');
var os = require('os');
var sticky = require('sticky-session');
var http  = require('http');
var path = require('path');
//var config = require(path.join(__dirname,'config'));
var bodyParser = require('body-parser');
var methodOverridde = require('method-override');
var waviotUtil = require("./modules/waviotUtil");
var AWS = require("aws-sdk");

mongoose.Promise = global.Promise;
///////////////////////////////////////////////////////////////////////////////
// load configuration
var service_type = process.env.WLINK_SVC;
//global.config = config.real;

//global.config.path = path.join(__dirname);
var Logger = require('./modules/logger');
var logger = new Logger('rest-api-admin');
logger.level = process.env.LOG_LEVEL; //'debug';
global.logger = logger;

if(service_type!=null && service_type!="")
{
    console.log("http_server >>> WLINK SYSTEM :", service_type);
    //global.config = config[service_type];
}
else 
    console.log(">>> use REAL configuration!!!");

//waviotUtil.init_global_service();  
//global.config.service_type = service_type;
///////////////////////////////////////////////////////////////////////////////
//global.config.path = path.join(__dirname);

//var redisClient = new Redis.Cluster(global.config.redis);
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

process.on('uncaughtException', function (err) {
    console.log('uncaughtException 발생 : ' + err);
    process.exit();
});

if (cluster.isMaster) {
    os.cpus().forEach(function (cpu) {
        cluster.fork();
    });

    cluster.on('exit', function(worker, code, signal) {
        if (code == 200) {
            cluster.fork();
        }
    });
}
else
{
    
    var admin_site = require('./sites/appAdmin.js').listen();
//    var service_site = require('./sites/appService.js').listen();
}

