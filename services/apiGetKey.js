var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');

var apiClients = require('../models/apiClients');
var serviceCode = require('../models/serviceCode');


function s4(seed_uuid) {
    var rand = random_seed.create(seed_uuid);
    return ((1 +  rand.random()+Math.random()) * 0x10000 | 0).toString(16).substring(1);
}

function gen_api_key(service_code, seed_uuid)
{
    return service_code+"_"+s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid);
}

function gen_api_secret(seed_uuid) {
    return s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) +
                s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid);
}

function gen_session_key(seed_uuid) {
    return s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) +
                s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid);
}

// get client key
router.post('/:uuid', function(req, res){
    try {
        var uuid = req.params.uuid;
        var service_code = req.body.service_code;
        var service_tag = req.body.service_tag;
        serviceCode.findOne({service_code: service_code, service_tag:service_tag }).exec(function(err, servicecode){
            if(err || servicecode==null)
            {
                res.status(400).send("Bad Request.")
            }
            else
            {
                var expired_date = servicecode.expired_date;
                apiClients.findOne({service_code: service_code, uuid:uuid })
                    .exec(function(err, item) {
                    if(err)
                        res.status(500).send("System Error.")
                    else
                    {
                        if(item==null)
                        {
                            // make new key
                            var apiclients= new apiClients({
                                service_code: service_code, 
                                uuid: uuid,
                                key: gen_api_key(service_code, uuid),
                                secret: gen_api_secret(uuid),
                                expired_date: expired_date,
                                is_device: false,
                                session_key: ""
                            });


                            apiClients.findOneAndUpdate(
                                {service_code: service_code, uuid: uuid}, 
                                apiclients, 
                                {upsert: true, new: true, runValidators: true}, // options
                                function (err, doc) { // callback
                                    if(err)
                                        res.status(500).send("System Error.");
                                    else
                                    {
                                        var _expired_date = moment(doc.expired_date);
                                        return res.send({service_code: doc.service_code, 
                                            uuid:doc.uuid, 
                                            key: doc.key, 
                                            secret:doc.secret, 
                                            expired_date:_expired_date.format("YYYYMMDDZZ")});
                                    }

                                }
                            );                     

/*
                            apiclients.save(function(err, item){
                                if(err)
                                    res.status(500).send("System Error.");
                                else
                                {
                                    var _expired_date = moment(item.expired_date);
                                    return res.send({service_code: item.service_code, 
                                        uuid:item.uuid, 
                                        key: item.key, 
                                        secret:item.secret, 
                                        expired_date:_expired_date.format("YYYYMMDDZZ")});
                                }
                            });
                            */
                        }
                        else
                        {
                            // return the old key
                            var _expired_date = moment(item.expired_date);
                            return res.send({service_code: item.service_code, 
                                    uuid:item.uuid, 
                                    key: item.key, 
                                    secret:item.secret,
                                    expired_date:_expired_date.format("YYYYMMDDZZ")});
                        }
                    }
                });
            }
        });
        
    } catch (error) {
        res.status(400).send("Bad Request."+error)
    }    
});


module.exports = router;
