// POST : user management interface
//     hmac_md5(time+api_key+service_code+command+account_type+account_id, secret).
//     Hex String Format
// 

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');
var crypto = require('crypto');

var apiClients = require('../models/apiClients');
var users = require('../models/users');
var userDevices = require('../models/userDevices');
var busboy = require('connect-busboy');
var ctrlUserVerification = require('../controllers/userVerification');
const asyncHandler = require('express-async-handler')

var timeCheck = true;
var timeLimit = 15; // 15 minutes
var random_key_timeLimit = 5; // 5 minuts
var login_token_timeLimit = 30; // 30 days
var login_fail_limit = 5; // 5회 재시도 가능
var login_fail_timeLimit = 30; // 30 minuts

var logger = global.logger;
var log_module = "USERV2";

// true : expired  즉 로그인 가능하다.
// false : not expired.  즉 로그인 불가
function check_login_fail_expired(login_fail_expire) {
    var today = moment();
    var expired = moment(login_fail_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if (diff < 0)
        return true;
    return false;
}


// true : expired
// false : not expired. it is valid.
function check_random_key_expired(random_key_expire) {
    var today = moment();
    var expired = moment(random_key_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if (diff < 0)
        return true;

    // time has problem
    if (diff > random_key_timeLimit)
        return true;

    return false;

}

// true : expired
// false : not expired. it is valid.
function check_login_token_expired(login_token_expired) {
    var today = moment();
    var expired = moment(login_token_expired);
    var diff = expired.diff(today, 'days');

    // time is passed
    if (diff < 0)
        return true;

    // time has problem
    if (diff > login_token_timeLimit)
        return true;

    return false;
}

// today - expired
// over 0  : expired 
function check_msg_expired(msgtime, limit_time) {
    var _msgtime = moment(msgtime, "YYYYMMDDHHmmssZZ");
    var expired = moment();
    expired.add(-limit_time, "minute");

    var diff = expired.diff(_msgtime, 'minutes');

    //console.log(expired.format('YYYYMMDDHHmmssZZ')+" DIFF: " + diff);

    if (diff < (-limit_time))
        return -diff;

    else
        return diff;
}

function s4(seed_uuid) {
    var rand = random_seed.create(seed_uuid);
    return ((1 + rand.random() + Math.random()) * 0x10000 | 0).toString(16).substring(1);
}

function gen_session_key(seed_uuid) {
    return s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) +
        s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid);
}


// auth 검사
function get_users_auth(msgtime, apikey, service_code, command, account_type, account_id, secret) {
    var plain = msgtime + apikey + service_code + command + account_type + account_id;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");
}

// new and update user info
async function update_user_info(res, msgtime, service_code, client_uuid, client_apikey,
    command, account_type, account_id, user_key,
    name, first_name, last_name, gender, accept_terms_of_service,
    timezone, email, mobile, password, birthday, access_token, access_token_expired,
    amazon_id, amazon_name, amazon_email, amazon_access_token,
    login_token) {
    if (command != "reg_user" && command != "update_user") {
        res.status(400).send("Bad Request.")
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Invalid Command!",
            log_module, command, service_code, client_uuid, client_apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                });

            if (user == null) {
                if (command == "update_user") {
                    res.status(400).send("Bad Request.")
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Invalid Command!, Aleady Exists",
                        log_module, command, service_code, client_uuid, client_apikey, account_type, account_id);
                    return;
                }
                else {
                    var pic_random_id = "userpic_" + gen_session_key(client_uuid + account_id);
                    // register new 
                    var newuser = new users(
                        {
                            service_code: service_code,
                            account_type: account_type,
                            account_id: account_id,
                            // user_key, 
                            last_client_uuid: client_uuid,
                            last_client_apikey: client_apikey,
                            name: name,
                            first_name: first_name,
                            last_name: last_name,
                            gender: gender,
                            accept_terms_of_service: accept_terms_of_service,
                            timezone: timezone,
                            email: email,
                            mobile: mobile,
                            birthday: moment(birthday, "YYYYMMDD"),
                            // 자체 인증 코드에 대한 처리. 지금은 항상 true
                            verification_date: moment(),
                            verification_flag: true,

                            // 2017.09.26 AMAZON User info
                            amazon_id: amazon_id,
                            amazon_name: amazon_name,
                            amazon_email: amazon_email,
                            amazon_access_token: amazon_access_token,
                            /////////////////////////////////////////////////
                            // 20181121 added user picture
                            pic_id: pic_random_id,
                            pic_key: "",
                            /////////////////////////////////////////////////      
                            password: password,
                            access_token: access_token,
                            access_token_expired: access_token_expired
                        }
                    );
                    let updateuser = await newuser.save();
                    res.send(
                        {
                            service_code: service_code,
                            key: client_apikey,
                            user_key: updateuser._id.toHexString()
                        }
                    );
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Create Success:%s",
                        log_module, command, service_code, client_uuid, client_apikey, account_type, account_id, updateuser._id.toHexString());
                }
            } else {
                var request_validation = true;

                if (command == "update_user") {
                    var api_key_error = false;
                    // login_token 검사
                    if (login_token == "" || user.login_token != login_token || check_login_token_expired(user.login_token_expired) == true)
                        request_validation = false;
                    else
                        // user key 검사
                        if (user_key == "" || user_key != user._id.toHexString())
                            request_validation = false;

                    if (user.last_login_uuid != client_uuid || user.last_login_apikey != client_apikey) {
                        api_key_error = true;
                        request_validation = false;
                    }

                    if (request_validation == false) {
                        if (api_key_error) {
                            res.status(402).send("Bad Request.")
                            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
                                log_module, command, service_code, client_uuid, client_apikey, account_type, account_id);
                        }
                        else {
                            res.status(400).send("Bad Request.")
                            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                                log_module, command, service_code, client_uuid, client_apikey, account_type, account_id);
                        }
                        return;
                    }

                    // password, access_token, access_token_expired는 별도 함수로 갱신한다. 
                    // update info
                    user.last_updated_time = moment();
                    user.last_client_uuid = client_uuid;
                    user.last_client_apikey = client_apikey;
                    user.name = name;
                    user.first_name = first_name;
                    user.last_name = last_name;
                    user.gender = gender;
                    user.timezone = timezone;
                    user.email = email;
                    user.mobile = mobile;
                    user.email = email;
                    user.mobile = mobile;
                    user.birthday = moment(birthday, "YYYYMMDD");
                    // 2017.09.26 AMAZON User info
                    user.amazon_id = amazon_id;
                    user.amazon_name = amazon_name;
                    user.amazon_email = amazon_email;
                    user.amazon_access_token = amazon_access_token;

                    // 20181121 added user picture
                    if (user.pic_id == null || user.pic_id == "") {
                        var pic_random_id = "userpic_" + gen_session_key(client_uuid + account_id);
                        user.pic_id = pic_random_id;
                        user.pic_key = "";
                    }

                    let updateuser = await user.save();
                    res.status(command == "update_user" ? 200 : 201).send(
                        {
                            service_code: service_code,
                            key: client_apikey,
                            user_key: updateuser._id.toHexString()
                        }
                    );
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Update Success:%s",
                        log_module, command, service_code, client_uuid, client_apikey, account_type, account_id, updateuser._id.toHexString());

                }
                else if (command == "reg_user") {
                    // 이미 가입된 정보로 재가입이 들어오는 경우.. 
                    // 만일 client uuid와 apikey가 동일하면, 201에러 전달
                    if (user.last_login_uuid == client_uuid && user.last_login_apikey == client_apikey) {
                        // res.status(201).send(
                        //     {
                        //         service_code: service_code,
                        //         key: client_apikey,
                        //         user_key: user._id.toHexString()
                        //     }
                        // );
                        res.status(201).send(
                            {
                                service_code: service_code, 
                                key: client_apikey, 
                                user_key:user._id.toHexString()
                            }
                        );
                        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Aleady Exists:201",
                            log_module, command, service_code, client_uuid, client_apikey, account_type, account_id);
                        return;
                    }
                    else {
                        // login token 유효기간이 지났으면 
                        // 기존 정보를 삭제하고, 신규 아이디 생성
                        if (user.login_token_expired != null && user.login_token_expired != "" && check_login_token_expired(user.login_token_expired) == false) {
                            res.status(403).send("Locked.")
                            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Locked:403",
                                log_module, command, service_code, client_uuid, client_apikey, account_type, account_id);
                            return;
                        }
                        else {
                            var pic_id = user.pic_id;
                            var pic_key = user.pic_key;

                            //사용자 정보 삭제 후, 신규 생성 처리 
                            await user.remove();
                            if (pic_id != null && pic_id != "" && pic_key != "") {
                                // picture정보가 있으면 gridfs에서 삭제하기
                                gfs.remove({ filename: pic_id },
                                    function (err) {
                                    }
                                );

                            }
                            // register new 
                            var pic_random_id = "userpic_" + gen_session_key(client_uuid + account_id);
                            var newuser = new users(
                                {
                                    service_code: service_code,
                                    account_type: account_type,
                                    account_id: account_id,
                                    // user_key, 
                                    last_client_uuid: client_uuid,
                                    last_client_apikey: client_apikey,
                                    last_login_uuid: client_uuid,
                                    last_login_apikey: client_apikey,
                                    name: name,
                                    first_name: first_name,
                                    last_name: last_name,
                                    gender: gender,
                                    accept_terms_of_service: accept_terms_of_service,
                                    timezone: timezone,
                                    email: email,
                                    mobile: mobile,
                                    birthday: moment(birthday, "YYYYMMDD"),
                                    // 자체 인증 코드에 대한 처리. 지금은 항상 true
                                    verification_date: moment(),
                                    verification_flag: true,

                                    // 2017.09.26 AMAZON User info
                                    amazon_id: amazon_id,
                                    amazon_name: amazon_name,
                                    amazon_email: amazon_email,
                                    amazon_access_token: amazon_access_token,
                                    /////////////////////////////////////////////////
                                    // 20181121 added user picture
                                    pic_id: pic_random_id,
                                    pic_key: "",
                                    /////////////////////////////////////////////////
                                    password: password,
                                    access_token: access_token,
                                    access_token_expired: access_token_expired
                                }
                            )
                            let updateuser = await users.save();
                            res.send(
                                {
                                    service_code: service_code,
                                    key: client_apikey,
                                    user_key: updateuser._id.toHexString()
                                }
                            );
                            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Recreate Success:%s",
                                log_module, command, service_code, client_uuid, client_apikey, account_type, account_id, updateuser._id.toHexString());
                        }

                    }

                }
                else {
                    res.status(400).send("Bad Request.")
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!!!",
                        log_module, command, service_code, client_uuid, client_apikey, account_type, account_id);
                    return;
                }
            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:[%s]",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}


async function del_user_info(res, msgtime, service_code, client_uuid, client_apikey,
    account_type, account_id, user_key, login_token) {
    var resmsg = { retCode: "0000", msg: "success" };
    var conn = mongoose.connection;
    var gfs = Grid(conn.db);
    var command = "del_user_info";

    try {
        let item = users.findOne(
            {
                "$and": [
                    { "service_code": service_code },
                    { "account_type": account_type },
                    { "account_id": account_id }
                ]
            }
        );

        if (item == null) {
            res.status(404).send("No Data.");
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:No Data",
                log_module, command, service_code, uuid, apikey, account_type, account_id);
        }
        else {
            // login_token 검사
            if (user_key != item._id.toHexString() || login_token == "" || item.login_token != login_token || check_login_token_expired(item.login_token_expire) == true) {
                res.status(401).send("Unauthorized. [3]");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Invalid Session",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
            }
            else {
                var pic_id = item.pic_id;
                var pic_key = item.pic_key;

                item.remove(function (err) {
                    if (err)
                        res.status(500).send("System Error.");
                    else {
                        res.send(JSON.stringify(resmsg, null, "\t"));
                        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!",
                            log_module, command, service_code, uuid, apikey, account_type, account_id);
                    }

                    if (pic_id != null && pic_id != "" && pic_key != "") {
                        // picture정보가 있으면 gridfs에서 삭제하기
                        gfs.remove({ filename: pic_id },
                            function (err) {
                            }
                        );

                    }
                });
            }
        }
    } catch (error) {
        res.status(400).send(error);
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:[%s]",
            log_module, command, service_code, uuid, apikey, account_type, account_id, error);
    }
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "find_user",
	"time": "20170621173500+0800",
	"auth": "17a72c8ca008e0da35780e9db465fb70",	
	"user_key": "594a1946975c172574880e7a"
}

*/
async function do_find_user(req, res, service_code, uuid, apikey, account_type, account_id) {
    var command = "do_find_user";

    try {
        var user = await users.findOne(
            {
                "$and": [
                    { "service_code": service_code },
                    { "account_type": account_type },
                    { "account_id": account_id }
                ]
            }
        );

        if (user == null) {
            res.status(404).send("Not Found.");
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:No Data",
                log_module, command, service_code, uuid, apikey, account_type, account_id);
            return;
        }
        else {
            // update random_key / random_key_expire
            user.random_key = gen_session_key(uuid);
            random_key_time = moment();
            random_key_time.add(random_key_timeLimit, "minute");
            user.random_key_expired = random_key_time;
            key_validity = false;

            if (user.last_login_apikey != "" && apikey == user.last_login_apikey)
                key_validity = true;
            else
                if (user.allow_multiple_login != null && user.allow_multiple_login == true)
                    key_validity = true;

            if (key_validity == true) {
                user.last_client_uuid = uuid;
                user.last_client_apikey = apikey;
            }

            let updateuser = await user.save();

            res.send(
                {
                    service_code: service_code,
                    key: apikey,
                    key_validity: key_validity,
                    user_key: updateuser._id.toHexString(),
                    random_key: updateuser.random_key,
                    random_key_expired: random_key_time.format('YYYYMMDDHHmmssZZ')
                }
            );

            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, key_validity:%s, Success!!!",
                log_module, command, service_code, uuid, apikey, account_type, account_id, key_validity ? "true" : "false");
        }
    } catch (error) {
        res.status(500).send("System Error." + error);
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "reg_user",
	"time": "20170621145100+0800",
	"auth": "cccfa07490616e817b6ec67eab134f4d",	
	
	
	"first_name" : "alex",
	"last_name" : "chang",
	"accept_terms_of_service" : "yes",
	"password" : "12341234",
	"access_token" : "",
	"access_token_expired" : "",
	"gender" : "male",
	"timezone" : "8",
	"email" : "",
	"mobile" : "",
	"birthday" : "20170720"
}

{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"wechat",
	"account_id":"wechatid",
    "command": "reg_user",
	"time": "20170621181300+0800",
	"auth": "1f57fe0580a94c6077562c36b8c65442",	
	
	"first_name" : "alex",
	"last_name" : "chang",
	"accept_terms_of_service" : "yes",
	"password" : "",
	"access_token" : "access_token",
	"access_token_expired" : "",
	"gender" : "male",
	"timezone" : "8",
	"email" : "",
	"mobile" : "",
	"birthday" : "20170720"
}
*/
async function do_reg_user(req, res, service_code, uuid, apikey, account_type, account_id) {
    var msgtime = req.body.time;
    var command = "reg_user";
    var user_key = "";
    var first_name = req.body.first_name == null ? "" : req.body.first_name;
    var last_name = req.body.last_name == null ? "" : req.body.last_name;
    var name = req.body.name == null ? first_name + " " + last_name : req.body.name;
    var gender = req.body.gender == null ? "" : req.body.gender;
    var accept_terms_of_service = true;  // just YES
    var timezone = req.body.timezone == null ? "" : req.body.timezone;
    var email = req.body.email == null ? "" : req.body.email;
    var birthday = req.body.birthday == null ? "" : req.body.birthday;
    if (email == "" && account_type == "email") email = account_id;
    birthday = birthday == "" ? "19000101" : birthday;

    var mobile = req.body.mobile == null ? "" : req.body.mobile;
    if (mobile == "" && account_type == "mobile") mobile = account_id;

    var amazon_id = req.body.amazon_id == null ? "" : req.body.amazon_id;
    var amazon_name = req.body.amazon_name == null ? "" : req.body.amazon_name;
    var amazon_email = req.body.amazon_email == null ? "" : req.body.amazon_email;
    var amazon_access_token = req.body.amazon_access_token == null ? "" : req.body.amazon_access_token;

    var access_token = req.body.access_token == null ? "" : req.body.access_token;
    var access_token_expired = req.body.access_token_expired == null ? "" : req.body.access_token_expired;
    var password = req.body.password == null ? "" : req.body.password;
    var login_token = "";

    var request_validation = true;

    // it is required valiees
    if (first_name == "" || last_name == "") {
        request_validation = false;
        console.log("first_name", first_name, "last_name", last_name);
    }

    if ((account_type == "email" || account_type == "mobile") && password == "") {
        request_validation = false;
        console.log("password error")
    }

    if (account_type != "email" && account_type != "mobile" && access_token == "") {
        request_validation = false;
        console.log("access_token error")
    }

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        await update_user_info(res, msgtime, service_code, uuid, apikey, command,
            account_type, account_id, user_key,
            name, first_name, last_name, gender, accept_terms_of_service,
            timezone, email, mobile, password, birthday,
            access_token, access_token_expired,
            amazon_id, amazon_name, amazon_email, amazon_access_token,
            login_token);
    }
}

///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "update_user",
	"time": "20170621174200+0800",
	"auth": "fd9bc93c0c30b728cf3f98ff2e88385b",	

	"user_key": "594a1946975c172574880e7a",
	"login_token": "83b2a98a11d29cf13166b17340ad46f2",	
	"first_name" : "alex",
	"last_name" : "chang",
	"gender" : "male",
	"timezone" : "8",
	"email" : "",
	"mobile" : "",
	"birthday" : "20170720"
}
*/
async function do_update_user(req, res, service_code, uuid, apikey, account_type, account_id) {
    var msgtime = req.body.time;
    var command = "update_user";
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    var login_token = req.body.login_token == null ? "" : req.body.login_token;;

    var first_name = req.body.first_name == null ? "" : req.body.first_name;
    var last_name = req.body.last_name == null ? "" : req.body.last_name;
    var name = req.body.name == null ? first_name + " " + last_name : req.body.name;
    var gender = req.body.gender == null ? "" : req.body.gender;
    var accept_terms_of_service = true;  // just YES
    var timezone = req.body.timezone == null ? "" : req.body.timezone;
    var email = req.body.email == null ? "" : req.body.email;
    if (email == "" && account_type == "email") email = account_id;
    var birthday = req.body.birthday == null ? "" : req.body.birthday;

    var mobile = req.body.mobile == null ? "" : req.body.mobile;
    if (mobile == "" && account_type == "mobile") mobile = account_id;

    var amazon_id = req.body.amazon_id == null ? "" : req.body.amazon_id;
    var amazon_name = req.body.amazon_name == null ? "" : req.body.amazon_name;
    var amazon_email = req.body.amazon_email == null ? "" : req.body.amazon_email;
    var amazon_access_token = req.body.amazon_access_token == null ? "" : req.body.amazon_access_token;

    var access_token = "";
    var access_token_expired = "";
    var password = "";

    var request_validation = true;

    // it is required valiees
    if (user_key == "" || login_token == "")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        await update_user_info(res, msgtime, service_code, uuid, apikey, command,
            account_type, account_id, user_key,
            name, first_name, last_name, gender, accept_terms_of_service,
            timezone, email, mobile, password, birthday,
            access_token, access_token_expired,
            amazon_id, amazon_name, amazon_email, amazon_access_token,
            login_token);
    }

}


///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "link_with_amazon",
	"time": "20170621174200+0800",
	"auth": "fd9bc93c0c30b728cf3f98ff2e88385b",	

	"user_key": "594a1946975c172574880e7a",
	"login_token": "83b2a98a11d29cf13166b17340ad46f2",	
	"amazon_id" : "alex",
	"amazon_name" : "chang",
    "amazon_email" : "emale",
    "amazon_access_token" : "amazon_access_token"
}
*/
async function do_link_with_amazon(req, res, service_code, uuid, apikey, account_type, account_id) {
    var msgtime = req.body.time;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    var login_token = req.body.login_token == null ? "" : req.body.login_token;;
    var command = "do_link_with_amazon";

    var amazon_id = req.body.amazon_id == null ? "" : req.body.amazon_id;
    var amazon_name = req.body.amazon_name == null ? "" : req.body.amazon_name;
    var amazon_email = req.body.amazon_email == null ? "" : req.body.amazon_email;
    var amazon_access_token = req.body.amazon_access_token == null ? "" : req.body.amazon_access_token;

    var request_validation = true;

    // it is required valiees
    if (user_key == "" || login_token == "" || amazon_name == "" || amazon_email == "")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                }
            );
            if (user == null) {
                res.status(401).send("Bad Request.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
            else {
                request_validation = true;
                request_validation_pos = 0;
                if (user_key != user._id.toHexString() || login_token != user.login_token) {
                    request_validation = false;
                    request_validation_pos = 1;
                }
                else {
                    if (check_login_token_expired(user.login_token_expired)) {
                        request_validation = false;
                        request_validation_pos = 2;
                    }
                }
                if (request_validation == false) {
                    res.status(401).send("Bad Request." + request_validation_pos);
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    return;
                }
                else {
                    // 2017.09.26 AMAZON User info
                    user.amazon_id = amazon_id;
                    user.amazon_name = amazon_name;
                    user.amazon_email = amazon_email;
                    user.amazon_access_token = amazon_access_token;
                    user.last_updated_time = moment();

                    let updateuser = await user.save();
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    res.send(
                        {
                            service_code: service_code,
                            key: apikey,
                            user_key: updateuser._id.toHexString()
                        }
                    );
                }

            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:>>>>>>>%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"wechat",
	"account_id":"wechatid",
    "command": "unreg_user",
	"time": "20170621174200+0800",
	"auth": "fd9bc93c0c30b728cf3f98ff2e88385b",	

	"user_key": "594a1946975c172574880e7a",
	"login_token": "83b2a98a11d29cf13166b17340ad46f2"
}
*/
async function do_unreg_user(req, res, service_code, uuid, apikey, account_type, account_id) {
    var command = "do_unreg_user";
    var msgtime = req.body.time;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    var login_token = req.body.login_token == null ? "" : req.body.login_token;;

    var request_validation = true;

    // it is required valiees
    if (user_key == "" || login_token == "")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        await del_user_info(res, msgtime, service_code, uuid, apikey,
            account_type, account_id, user_key, login_token);
    }

}

///////////////////////////////////////////////////////////////////////////////////
// password
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "login",
	"time": "20170621173900+0800",
	"auth": "f5fb93135685188d3ae0b3f2e3736638",	
	"password" : "12341234"
	"access_token" : ""
}

*/
async function do_login(req, res, service_code, uuid, apikey, account_type, account_id) {
    var random_key = req.body.random_key == null ? "" : req.body.random_key;
    var password = req.body.password == null ? "" : req.body.password;
    var access_token = req.body.access_token == null ? "" : req.body.access_token;
    var request_validation = true;
    var errmsg = ""
    var command = "do_login";

    if ((account_type == "email" || account_type == "mobile") && password == "")
        request_validation = false;
    else
        if ((account_type != "email" && account_type != "mobile") && access_token == "")
            request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            let user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                }
            );
            if (user == null) {
                res.status(401).send("Invalid User Information.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
            else {
                request_validation = true;
                request_validation_no = 0;
                if ((account_type == "email" || account_type == "mobile") && password != user.password) {
                    request_validation = false;
                    request_validation_no = 1;
                }
                else
                    if ((account_type != "email" && account_type != "mobile") && access_token != user.access_token) {
                        request_validation = false;
                        request_validation_no = 2;
                    }

                if (request_validation == false) {
                    // 2017.08.11
                    // 로그인 실패 카운트 관리
                    user.login_fail_count++;
                    // login_fail_time은 로그인 가능한 시간을 가지고 있다.
                    login_possible_time = moment();
                    login_possible_time.add(login_fail_timeLimit, "minute");
                    user.login_fail_expired = login_possible_time;

                    await user.save();
                    if (user.login_fail_count < login_fail_limit) {
                        errmsg = "Invalid User Information." + request_validation_no;
                        res.status(401).send(errmsg);
                        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]",
                            log_module, command, service_code, uuid, apikey, account_type, account_id, errmsg);

                    }
                    else {
                        errmsg = "Locked";
                        res.status(403).send(errmsg);
                        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]",
                            log_module, command, service_code, uuid, apikey, account_type, account_id, errmsg);
                    }
                    return;
                }
                else {
                    // 2017.08.11
                    // 로그인 실패 카운트크가 5회 이상이면  30분이 지나야 재로그인이 가능하다. 
                    if (user.login_fail_count >= login_fail_limit && check_login_fail_expired(user.login_fail_expired) == false) {
                        errmsg = "Locked";
                        res.status(403).send(errmsg);
                        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]",
                            log_module, command, service_code, uuid, apikey, account_type, account_id, errmsg);
                        return;
                    }
                    // 2017.08.10 
                    // 이전에 로그인에 성공한 정보와 지금 접속하는 장비가 다를 경우에 대한 처리 
                    // 이 경우 로그인을 허가하지 않는다.
                    if ((user.last_login_uuid != null && user.last_login_uuid != uuid) ||
                        (user.last_login_apikey != null && user.last_login_apikey != apikey)) {
                        // 2018.12.07 tester id
                        if (user.allow_multiple_login != null && user.allow_multiple_login == true) {
                            // tester id의 경우, 단말 정보를 그냥 갱신한다. 
                            user.last_login_uuid = uuid;
                            user.last_login_apikey = apikey;
                            logger.info("INFO:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ALLOW MULTIPLE LOGIN!!! Ingnore to check device",
                                log_module, command, service_code, uuid, apikey, account_type, account_id);
                        }
                        else
                            // uuid / apikey 갱신을 위해서는  find_user로 random_key를 갱신 후에 전달해야한다.
                            // randomkey 검사
                            if (random_key == "" || random_key != user.random_key || check_random_key_expired(user.random_key_expired)) {
                                errmsg = "Invalid Client Information.";
                                res.status(402).send(errmsg);
                                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, error:[%s]",
                                    log_module, command, service_code, uuid, apikey, account_type, account_id, errmsg);
                                return;
                            }
                    }
                    // update login_token / login_token_expired
                    user.login_token = gen_session_key(uuid);
                    login_token_time = moment();
                    login_token_time.add(login_token_timeLimit, "day");
                    user.login_token_expired = login_token_time;

                    user.last_client_uuid = uuid;
                    user.last_client_apikey = apikey;

                    user.last_login_time = moment();
                    user.login_count++;
                    user.login_fail_count = 0;
                    user.device_login_fail_count = 0;

                    // 2017.08.10 
                    // 실제 성공한 로그인 아이디 저장
                    user.last_login_uuid = uuid;
                    user.last_login_apikey = apikey;

                    // mobile no를 변경해야하는 경우를 위한 random key 정보 전달
                    user.random_key = gen_session_key(uuid);
                    random_key_time = moment();
                    random_key_time.add(random_key_timeLimit, "minute");
                    user.random_key_expired = random_key_time;

                    let updateuser = await user.save();
                    res.send(
                        {
                            service_code: service_code,
                            key: apikey,
                            user_key: updateuser._id.toHexString(),
                            login_token: updateuser.login_token,
                            login_token_expired: login_token_time.format('YYYYMMDDHHmmssZZ'),
                            random_key: updateuser.random_key,
                            random_key_expired: random_key_time.format('YYYYMMDDHHmmssZZ'),
                            mobile: updateuser.mobile,
                            email: updateuser.email,
                            timezone: updateuser.timezone,
                            gender: updateuser.gender,
                            last_name: updateuser.last_name,
                            first_name: updateuser.first_name,
                            birthday: updateuser.birthday == null ? "" : moment(updateuser.birthday).format('YYYYMMDD')

                        }
                    );
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                }

            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:[%s]",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}

//  일반 로그인과 다른점은 apikey가 디바이스의 것인지 확인하는 것과
//  login token을 갱신하지 않는다. 
async function do_login_from_device(req, res, service_code, uuid, apikey, account_type, account_id) {
    var password = req.body.password == null ? "" : req.body.password;
    var access_token = req.body.access_token == null ? "" : req.body.access_token;
    var request_validation = true;
    var command = "do_login_from_device";

    if ((account_type == "email" || account_type == "mobile") && password == "")
        request_validation = false;
    else
        if ((account_type != "email" && account_type != "mobile") && access_token == "")
            request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                }
            );

            if (user == null) {
                res.status(401).send("Invalid User Information.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
            else {
                request_validation = true;
                request_validation_no = 0;
                if ((account_type == "email" || account_type == "mobile") && password != user.password) {
                    request_validation = false;
                    request_validation_no = 1;
                }
                else
                    if ((account_type != "email" && account_type != "mobile") && access_token != user.access_token) {
                        request_validation = false;
                        request_validation_no = 2;
                    }
                    else if (user.device_login_fail_count >= login_fail_limit) {
                        request_validation = false;
                        request_validation_no = 3;
                    }

                if (request_validation == false) {
                    //res.status(401).send("Invalid User Information."+ request_validation_no);
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!! %d",
                        log_module, command, service_code, uuid, apikey, account_type, account_id, request_validation_no);
                    user.device_login_fail_count++;

                    await user.save();
                    if (request_validation_no == 3)
                        res.status(403).send("Locked");
                    else
                        res.status(401).send("Invalid User Information." + request_validation_no);
                    return;
                }
                else {
                    // login token등은 갱신하지 않고. 접근한 device정보만 저장한다.
                    user.last_device_uuid = uuid;
                    user.last_device_apikey = apikey;
                    user.last_device_login_time = moment();
                    user.device_login_count++;
                    user.device_login_fail_count = 0;

                    let updateuser = await user.save();
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    res.send(
                        {
                            service_code: service_code,
                            key: apikey,
                            user_key: updateuser._id.toHexString(),
                            login_token: updateuser.login_token,
                            login_token_expired: moment(updateuser.login_token_expired).format('YYYYMMDDHHmmssZZ'),
                            mobile: updateuser.mobile,
                            email: updateuser.email,
                            timezone: updateuser.timezone,
                            gender: updateuser.gender,
                            last_name: updateuser.last_name,
                            first_name: updateuser.first_name,
                            birthday: updateuser.birthday == null ? "" : moment(updateuser.birthday).format('YYYYMMDD')

                        }
                    );
                }

            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:>>>>>>%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////
// random_key
// login_token
// user_key

/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "check_login",
	"time": "20170621173200+0800",
	"auth": "b8e61c14e4c6970648f59da281da570b",	
    "login_token": "2ee9f62584634faac87b5c1ab1d126ba",
    "random_key": "cb88849aa50575a0829a78e7cb790d5e",
    "user_key": "594a1946975c172574880e7a"
}
*/
async function do_check_login(req, res, service_code, uuid, apikey, account_type, account_id) {
    var random_key = req.body.random_key == null ? "" : req.body.random_key;
    var login_token = req.body.login_token == null ? "" : req.body.login_token;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    var request_validation = true;
    var command = "do_check_login";

    if (random_key == "" || login_token == "" || user_key == "")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                }
            );
            if (user == null) {
                res.status(401).send("Bad Request.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
            else {
                request_validation = true;
                request_validation_pos = 0;
                if (user_key != user._id.toHexString() || random_key != user.random_key || login_token != user.login_token) {
                    request_validation = false;
                    request_validation_pos = 1;
                }
                else {
                    if (check_random_key_expired(user.random_key_expired)) {
                        request_validation = false;
                        request_validation_pos = 2;
                    }
                    else if (check_login_token_expired(user.login_token_expired)) {
                        request_validation = false;
                        request_validation_pos = 3;
                    }
                }
                if (request_validation == false) {
                    res.status(401).send("Bad Request." + request_validation_pos);
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR%d:Bad Request!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id, request_validation_pos);
                    return;
                }
                else {
                    // update login_token / login_token_expired
                    user.login_token = gen_session_key(uuid);
                    login_token_time = moment();
                    login_token_time.add(login_token_timeLimit, "day");
                    user.login_token_expired = login_token_time;

                    user.last_client_uuid = uuid;
                    user.last_client_apikey = apikey;

                    user.last_login_time = moment();
                    user.check_login_count++;

                    let updateuser = await user.save();
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    res.send(
                        {
                            service_code: service_code,
                            key: apikey,
                            user_key: updateuser._id.toHexString(),
                            login_token: updateuser.login_token,
                            login_token_expired: login_token_time.format('YYYYMMDDHHmmssZZ')
                        }
                    );
                }

            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:>>>>%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}

/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "get_user",
	"time": "20170621173200+0800",
	"auth": "b8e61c14e4c6970648f59da281da570b",	
    "login_token": "2ee9f62584634faac87b5c1ab1d126ba",
    "user_key": "594a1946975c172574880e7a"
}
*/
async function do_get_user(req, res, service_code, uuid, apikey, account_type, account_id) {
    var login_token = req.body.login_token == null ? "" : req.body.login_token;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    var request_validation = true;
    var command = "do_get_user";

    if (login_token == "" || user_key == "")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                }
            );
            if (user == null) {
                res.status(401).send("Bad Request.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
            else {
                request_validation = true;
                request_validation_pos = 0;
                if (user_key != user._id.toHexString() || login_token != user.login_token) {
                    request_validation = false;
                    request_validation_pos = 1;
                }
                else {
                    if (check_login_token_expired(user.login_token_expired)) {
                        request_validation = false;
                        request_validation_pos = 3;
                    }
                }
                if (request_validation == false) {
                    res.status(401).send("Bad Request." + request_validation_pos);
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    return;
                }
                else {
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    res.send(
                        {
                            service_code: service_code,
                            key: apikey,
                            user_key: user._id.toHexString(),
                            login_token: user.login_token,
                            mobile: user.mobile,
                            email: user.email,
                            timezone: user.timezone,
                            gender: user.gender,
                            last_name: user.last_name,
                            first_name: user.first_name,
                            birthday: user.birthday == null ? "" : moment(user.birthday).format('YYYYMMDD')
                        }
                    );

                }

            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:>>>>>%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}



//////////////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "update_password",
	"time": "20170621173200+0800",
	"auth": "b8e61c14e4c6970648f59da281da570b",	
    "login_token": "2ee9f62584634faac87b5c1ab1d126ba",
    "user_key": "594a1946975c172574880e7a",
    "password": "11111111"
}
*/
async function do_update_password(req, res, service_code, uuid, apikey, account_type, account_id) {
    var login_token = req.body.login_token == null ? "" : req.body.login_token;
    var password = req.body.password == null ? "" : req.body.password;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    var request_validation = true;
    var command = "do_update_password";

    if (login_token == "" || user_key == "" || password == "")
        request_validation = false;

    if (account_type != "email" && account_type != "mobile")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                }
            );
            if (user == null) {
                res.status(401).send("Bad Request.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
            else {
                request_validation = true;
                request_validation_pos = 0;
                if (user_key != user._id.toHexString() || login_token != user.login_token) {
                    request_validation = false;
                    request_validation_pos = 1;
                }
                else {
                    if (check_login_token_expired(user.login_token_expired)) {
                        request_validation = false;
                        request_validation_pos = 2;
                    }
                }
                if (request_validation == false) {
                    res.status(401).send("Bad Request." + request_validation_pos);
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Bad Request!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    return;
                }
                else {
                    user.password = password;
                    user.last_updated_time = moment();

                    let updateuser = await user.save();
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                        log_module, "do_update_password", service_code, uuid, apikey, account_type, account_id);
                    res.send(
                        {
                            service_code: service_code,
                            key: apikey,
                            user_key: updateuser._id.toHexString()
                        }
                    );
                }

            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:>>>>>%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "update_account_id",
	"time": "20170621173200+0800",
	"auth": "b8e61c14e4c6970648f59da281da570b",	
    "login_token": "2ee9f62584634faac87b5c1ab1d126ba",
    "user_key": "594a1946975c172574880e7a",
    "new_account_id": "13612341234"
}
*/
async function do_update_account_id(req, res, service_code, uuid, apikey, account_type, account_id) {
    var login_token = req.body.login_token == null ? "" : req.body.login_token;
    var new_account_id = req.body.new_account_id == null ? "" : req.body.new_account_id;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    var request_validation = true;
    var command = "do_update_account_id";

    if (login_token == "" || user_key == "" || new_account_id == "")
        request_validation = false;

    if (account_type != "email" && account_type != "mobile")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne({
                "$and": [
                    { "service_code": service_code },
                    { "account_type": account_type },
                    { "account_id": account_id }
                ]
            });
            if (user == null) {
                res.status(401).send("Bad Request.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            } else {
                //校验用户的登录信息
                request_validation = true;
                request_validation_pos = 0;
                if (user_key != user._id.toHexString() || login_token != user.login_token) {
                    request_validation = false;
                    request_validation_pos = 1;
                }
                else {
                    if (check_login_token_expired(user.login_token_expired)) {
                        request_validation = false;
                        request_validation_pos = 2;
                    }
                }

                if (request_validation == false) //校验信息有问题
                {
                    res.status(401).send("Bad Request." + request_validation_pos);
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Bad Request!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    return;
                } else {
                    //校验没有问题后、判断新账号是否已被注册过
                    var newUser = await users.findOne(
                        {
                            "$and": [
                                { "service_code": service_code },
                                { "account_type": account_type },
                                { "account_id": new_account_id }
                            ]
                        }
                    );

                    //新账号并未被注册
                    if (newUser == null) {
                        user.account_id = new_account_id;
                        user.last_updated_time = moment();
                        user.login_token = "expired";
                        user.login_token_expired = moment();

                        var updateuser = await user.save();

                        if (updateuser != null) {
                            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                                log_module, command, service_code, uuid, apikey, account_type, account_id);
                            res.send(
                                {
                                    service_code: service_code,
                                    key: apikey,
                                    user_key: updateuser._id.toHexString(),
                                    account_type: updateuser.account_type,
                                    account_id: updateuser.account_id
                                }
                            );
                        } else {
                            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Failed!!!",
                                log_module, command, service_code, uuid, apikey, account_type, account_id);
                        }
                    } else {
                        //该账号已经被别人注册过，不允许更换
                        logger.info("This new_account_id:%s has been registed!! in client:%s ", new_account_id, uuid);
                        res.status(201).send("Aleady Exists..");
                        return;
                    }
                }
            }
        } catch (error) {
            res.status(500).send("System Error." + err);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////
// random_key
// user_key

/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "update_password2",
	"time": "20170621173200+0800",
	"auth": "b8e61c14e4c6970648f59da281da570b",	
    "random_key": "cb88849aa50575a0829a78e7cb790d5e",
    "user_key": "594a1946975c172574880e7a",
    "password": "11111111"
}
*/
async function do_update_password2(req, res, service_code, uuid, apikey, account_type, account_id) {
    var random_key = req.body.random_key == null ? "" : req.body.random_key;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    var password = req.body.password == null ? "" : req.body.password;
    var request_validation = true;
    var command = "do_update_password2";

    if (random_key == "" || password == "" || user_key == "")
        request_validation = false;

    if (account_type != "email" && account_type != "mobile")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                }
            );
            if (user == null) {
                res.status(401).send("Bad Request.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
            else {
                request_validation = true;
                request_validation_pos = 0;
                if (user_key != user._id.toHexString() || random_key != user.random_key) {
                    request_validation = false;
                    request_validation_pos = 1;
                }
                else {
                    if (check_random_key_expired(user.random_key_expired)) {
                        request_validation = false;
                        request_validation_pos = 2;
                    }
                }
                if (request_validation == false) {
                    res.status(401).send("Bad Request." + request_validation_pos);
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Bad Request!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    return;
                }
                else {
                    user.password = password;
                    user.last_updated_time = moment();
                    // 2017.08.11 
                    // 로그인 실패 횟수 초기화 
                    user.login_fail_count = 0;
                    user.device_login_fail_count = 0;
                    user.login_token = "expired";
                    user.login_token_expired = moment();

                    let updateuser = await user.save();
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    res.send(
                        {
                            service_code: service_code,
                            key: apikey,
                            user_key: updateuser._id.toHexString()
                        }
                    );
                }

            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:>>>>>>%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////
// random_key
// user_key

/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"wechat",
	"account_id":"wechatid",
    "command": "update_access_token",
	"time": "20170621173200+0800",
	"auth": "b8e61c14e4c6970648f59da281da570b",	
    "random_key": "cb88849aa50575a0829a78e7cb790d5e",
    "user_key": "594a46c4b14e3f2c4a6fdce7,
    "old_access_token": "11111111",
    "access_token": "11111111",
    "access_token_expired": "11111111"
}
*/

// 2017.07.18: 다른 장비에서의 중복 로그인 또는 앱 재설치등으로 인한 갱신처리를 위해서 old_access_token 검사를 제외함
//             즉, random_key만으로 클라이언트 유효성을 검사함. 
async function do_update_access_token(req, res, service_code, uuid, apikey, account_type, account_id) {
    var random_key = req.body.random_key == null ? "" : req.body.random_key;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;;
    //old_access_token = req.body.old_access_token==null ? "" : req.body.old_access_token;
    var access_token = req.body.access_token == null ? "" : req.body.access_token;
    var access_token_expired = req.body.access_token_expired == null ? "" : req.body.access_token_expired;
    var request_validation = true;
    var command = "do_update_access_token";

    if (random_key == "" || access_token == "" || user_key == "")
        request_validation = false;

    if (account_type == "email" || account_type == "mobile")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "account_type": account_type },
                        { "account_id": account_id }
                    ]
                }
            );
            if (user == null) {
                res.status(401).send("Bad Request.");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
            else {
                request_validation = true;
                request_validation_pos = 0;
                if (user_key != user._id.toHexString() || user.random_key == null || random_key != user.random_key) {
                    request_validation = false;
                    request_validation_pos = 1;
                }
                else {
                    if (check_random_key_expired(user.random_key_expired)) {
                        request_validation = false;
                        request_validation_pos = 2;
                    }
                }
                if (request_validation == false) {
                    res.status(401).send("Bad Request." + request_validation_pos);
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    return;
                }
                else {
                    user.access_token = access_token;
                    user.access_token_expired = access_token_expired;
                    user.last_updated_time = moment();

                    let updateuser = await user.save();
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    res.send(
                        {
                            service_code: service_code,
                            key: apikey,
                            user_key: updateuser._id.toHexString()
                        }
                    );
                }

            }
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:>>>>>>>%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}



///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "account_type":"mobile",
	"account_id":"18681457712",
    "command": "get_device",
	"time": "20170621174200+0800",
	"auth": "fd9bc93c0c30b728cf3f98ff2e88385b",	

	"user_key": "594a1946975c172574880e7a",
	"login_token": "83b2a98a11d29cf13166b17340ad46f2",	
}
*/
async function get_device(req, res, service_code, uuid, apikey, account_type, account_id) {
    var msgtime = req.body.time;
    var user_key = req.body.user_key == null ? "" : req.body.user_key;
    var login_token = req.body.login_token == null ? "" : req.body.login_token;
    var command = "get_device";

    var msgtime = req.body.time;
    var access_token = "";
    var access_token_expired = "";

    var request_validation = true;

    // it is required valiees
    if (user_key == "" || login_token == "")
        request_validation = false;

    if (request_validation == false) {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!",
            log_module, command, service_code, uuid, apikey, account_type, account_id);
        return;
    }
    else {
        // user_key 유효성 검사
        try {
            var user = await users.findOne(
                {
                    "$and": [
                        { "service_code": service_code },
                        { "_id": mongoose.Types.ObjectId(user_key) }
                    ]
                }
            );

            if (user == null || login_token != user.login_token || check_login_token_expired(user.login_token_expired))
                request_validation = false;

            if (request_validation == false) {
                res.status(400).send("Bad Request.[2]");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:Bad Request!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }

            query = { "service_code": service_code, "user_key": user_key };
            fields = { "service_code": 1, "user_key": 1, "device_uuid": 1, "device_name": 1, "device_key": 1 };
            sorts = {
                sort: { "last_updated_time": -1 }
            };

            var devices = await userDevices.find(query, fields, sorts);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, Success!!!",
                log_module, command, service_code, uuid, apikey, account_type, account_id);
            res.send(JSON.stringify(devices, null, "\t"));

            return true;
        } catch (error) {
            res.status(500).send("System Error." + error);
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, ERROR:>>>>>>>%s",
                log_module, command, service_code, uuid, apikey, account_type, account_id, error);
        }
    }
}



// user management interface
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "account_type": "",
    "account_id": "",
    "command": "",
    .....
	"auth": "4b1a308f29b66779d43075adeb9e93dd"	
}
*/
router.post('/:uuid', asyncHandler(async (req, res) => {
    try {
        var uuid = req.params.uuid;
        var service_code = req.body.service_code;
        var apikey = req.body.api_key;
        var account_type = req.body.account_type;
        var account_id = req.body.account_id;
        var command = req.body.command;
        var msgtime = req.body.time;

        var auth = req.body.auth;

        logger.info("REQ:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s",
            log_module, command, msgtime, service_code, uuid, apikey, account_type, account_id);

        if (uuid == null || uuid == "" ||
            service_code == null || service_code == "" ||
            apikey == null || apikey == "" ||
            account_type == null || account_type == "" ||
            account_id == null || account_id == "" ||
            command == null || command == "") {
            res.status(400).send("Bad Request.[0]");
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:Bad Request!",
                log_module, command, service_code, uuid, apikey, account_type, account_id);
            return;
        }

        // message 시간 검사
        if (timeCheck == true) {
            if (req.body.time != null) {
                msgtime = req.body.time;
                if (check_msg_expired(msgtime, timeLimit) > 0) {
                    res.status(401).send("Unauthorized. [1]")
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:Bad Request!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    return;
                }

            }
            else {
                res.status(400).send("Bad Request.[0]");
                logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:Bad Request!!!",
                    log_module, command, service_code, uuid, apikey, account_type, account_id);
                return;
            }
        }

        // client key 유효성 검사 
        apiClients.findOne({ service_code: service_code, uuid: uuid, key: apikey })
            .exec(function (err, item) {
                if (err) {
                    res.status(500).send("System Error." + err)
                    return;
                }
                if (item == null) {
                    res.status(404).send("Not Found [1].")
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:Bad Request!!!!",
                        log_module, command, service_code, uuid, apikey, account_type, account_id);
                    return;
                }

                var secret = item.secret;

                // message auth 검사 
                var pauth = get_users_auth(msgtime, apikey, service_code, command, account_type, account_id, secret);
                if (auth != pauth) {
                    res.status(401).send("Unauthorized. [2]");
                    logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:Bad Request!!!! [%s]",
                        log_module, command, service_code, uuid, apikey, account_type, account_id, pauth);
                    return;
                }

                switch (command) {
                    case "reg_user": // ok
                        do_reg_user(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "unreg_user":  // ok
                        do_unreg_user(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "login": // ok
                        do_login(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "login_from_device": // ok
                        if (item.is_device == true)
                            do_login_from_device(req, res, service_code, uuid, apikey, account_type, account_id);
                        else
                            res.status(400).send("Bad Request.")
                        break;
                    case "find_user": // ok
                        do_find_user(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "check_login": // ok
                        do_check_login(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "get_user": // ok
                        do_get_user(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "update_user":  // ok
                        do_update_user(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "update_password":  // ok
                        do_update_password(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "update_password2": //ok
                        do_update_password2(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "update_access_token": // ok
                        do_update_access_token(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "update_account_id": //ok
                        do_update_account_id(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "get_devices": // ok
                        get_device(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "link_with_amazon": //
                        do_link_with_amazon(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "fileupload":
                        do_pic_file_upload(req, res, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "send_verification_code":
                        ctrlUserVerification.post_send_email_verification_code(req, res, item, service_code, uuid, apikey, account_type, account_id);
                        break;
                    case "check_verification_code":
                        ctrlUserVerification.post_check_email_verification_code(req, res, item, service_code, uuid, apikey, account_type, account_id);
                        break;
                    default:
                        res.status(400).send("Bad Request.")
                        break;
                }
            });

    } catch (error) {
        res.status(400).send("Bad Request.")
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, >>>>> ERROR:[%s]",
            log_module, command, service_code, uuid, apikey, account_type, account_id, pauth, error);
    }
}));




///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////


var file_session_timeLimit = 5;
// true : expired
// false : not expired. it is valid.
function check_file_session_key_expired(file_session_key_expire) {
    var today = moment();
    var expired = moment(file_session_key_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if (diff < 0)
        return true;

    // time has problem
    if (diff > file_session_timeLimit)
        return true;

    return false;
}

async function do_pic_file_upload(req, res, service_code, uuid, apikey, account_type, account_id) {
    try {
        var user = await users.findOne(
            {
                "$and": [
                    { "account_id": account_id },
                    { "account_type": account_type },
                    { "service_code": service_code }
                ]
            });

        if (user == null) {
            res.status(400).send("Bad Request.2")
            logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, No User Data",
                log_module, "do_pic_file_upload", service_code, uuid, apikey, account_type, account_id);
            return;
        }
        user.file_session_key = gen_session_key(uuid);
        file_session_key_time = moment();
        file_session_key_time.add(file_session_timeLimit, "minute");
        user.file_session_key_expired = file_session_key_time;

        // 20181121 added user picture
        if (user.pic_id == null || user.pic_id == "") {
            var pic_random_id = "userpic_" + gen_session_key(uuid + account_id);
            user.pic_id = pic_random_id;
            user.pic_key = "";
        }

        let updateUser = await user.save();
        res.send(
            {
                service_code: service_code,
                api_key: apikey,
                pic_id: updateUser.pic_id,
                file_session_key: updateUser.file_session_key,
                file_session_key_expired: file_session_key_time.format('YYYYMMDDHHmmssZZ')
            }
        );
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, pic_id:%s, file_session_key:%s",
            log_module, "do_pic_file_upload", service_code, uuid, apikey, account_type, account_id, updateUser.pic_id, updateUser.file_session_key);
    } catch (error) {
        res.status(500).send("System Error." + error)
        logger.info("RES:: module:%s, command:%s, service_code:%s, uuid:%s, apikey:%s, account_type:%s, account_id:%s, System Error>>>>%s",
            log_module, "do_pic_file_upload", service_code, uuid, apikey, account_type, account_id, error);
        return;
    }
}


var fs = require('fs');
var Grid = require("gridfs-stream");
Grid.mongo = mongoose.mongo;

router.post('/pic/:picuuid',
    busboy({
        limits: {
            fileSize: 10 * 1024 * 1024
        }
    }),
    function (req, res) {
        try {
            var picuuid = req.params.picuuid;
            var msg = picuuid.toString().split("#");
            var pic_id = msg[0] == null ? "" : msg[0];
            var file_session_key = msg[1] == null ? "" : msg[1];
            var conn = mongoose.connection;
            var gfs = Grid(conn.db);
            var command = "upload_user_pic";
            var resmsg = { retCode: "0000", msg: "success" };

            logger.info("REQ:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Upload User Picture File!",
                log_module, command, pic_id, file_session_key);
            if (pic_id == "" || file_session_key == "") {
                res.status(400).send("Bad Request.");
                return;
            }

            req.pipe(req.busboy);
            req.busboy.on('field', function (fieldname, val) {
                //console.log(fieldname, val);
            });

            req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
                console.log("Uploading: ", filename, encoding, mimetype);
                file_ext = filename.toString().toLowerCase().substr(filename.toString().length - 4);

                // png 파일명만 처리 한다. 
                if (file_ext != ".png") {
                    console.log("not PNG!!")
                    res.status(400).send("Bad Request.")
                    logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, File is not a PNG File!",
                        log_module, command, pic_id, file_session_key);
                    return;
                }

                // db조회 부분을 busboy와 통합할 수 없음. 
                // 결국 파일을 업로드 완료 후 처리해야 함 
                let writeStream = gfs.createWriteStream({
                    filename: pic_id,
                    mode: 'w',
                    content_type: mimetype
                });

                if (writeStream == null) {
                    res.status(400).send("Bad Request.")
                    logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Bad Request",
                        log_module, command, pic_id, file_session_key, error);
                    return;
                }

                file.pipe(writeStream);

                writeStream.on('close', (file) => {
                    //console.log("written to DB:")

                    users.findOne({ pic_id: pic_id, file_session_key: file_session_key })
                        .exec(function (err, item) {
                            upload_success = true;
                            if (err != null || item == null) {
                                upload_success = false;
                                console.log("Invald file session key!!");
                            }

                            if (upload_success == true && check_file_session_key_expired(item.file_session_key_expired) == true) {
                                upload_success = false;
                                console.log("Invald file session key time!!");
                            }

                            if (upload_success == false) {
                                // 업로드된 파일 삭제
                                gfs.remove({ _id: file._id.toHexString() },
                                    function (err) {
                                        console.log("DELETE UPLOAD  " + file._id.toHexString());
                                    }
                                );
                                res.status(400).send("Bad Request.")
                                return;
                            }

                            // 이전 화일 삭제
                            var old_pic_key = "";
                            if (item.pic_key != null)
                                old_pic_key = item.pic_key;
                            else
                                old_pic_key = "";

                            item.pic_key = file._id.toHexString();
                            item.save(function (err, upateitem) {
                                if (err != null || upateitem == null) {
                                    gfs.remove({ _id: file._id.toHexString() },
                                        function (err) {
                                            console.log("DELETE " + file._id.toHexString());
                                        }
                                    );

                                    res.status(400).send("Bad Request.")
                                    return;
                                }

                                // 정상 업로드 후 이전에 등록 이미지 삭제
                                if (old_pic_key != "") {
                                    gfs.remove({ _id: old_pic_key },
                                        function (err) {
                                            console.log("DELETE OLD FILE " + old_pic_key);
                                        }
                                    );
                                }
                                logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Success!!!",
                                    log_module, command, pic_id, file_session_key);

                                return res.status(200).send({
                                    message: JSON.stringify(resmsg),
                                    file: file
                                });
                            });
                        });
                });
            });

            req.busboy.on('finish', function () {
                logger.info("INFO:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Succeed to upload file!",
                    log_module, command, pic_id, file_session_key);
                //console.log("finish");
            });
            //writeStream.end();
        } catch (error) {
            res.status(400).send("Bad Request.")
            logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Bad Request:%s",
                log_module, "upload_user_pic", pic_id, file_session_key, error);
        }
    });


router.get('/pic/:picuuid',
    function (req, res) {
        var conn = mongoose.connection;
        var gfs = Grid(conn.db);
        logger.info("REQ:: module:%s, command:%s, pic_id:%s", log_module, "get_user_pic", req.params.picuuid);

        gfs.files.find({
            filename: req.params.picuuid
        }).toArray((err, files) => {
            if (files.length === 0) {
                return res.status(400).send("Bad Request.");
            }
            let data = [];
            let readstream = gfs.createReadStream({
                filename: files[0].filename
            });

            readstream.on('data', (chunk) => {
                data.push(chunk);
            });

            readstream.on('end', () => {
                data = Buffer.concat(data);
                let img = 'data:image/png;base64,' + Buffer(data).toString('base64');
                res.end(img);
                console.log("sent", files[0].filename);
            });

            readstream.on('error', (err) => {
                console.log('An error occurred!', err);
                return res.status(400).send("Bad Request.");
                //throw err;
            });
        });

    });

module.exports = router;
