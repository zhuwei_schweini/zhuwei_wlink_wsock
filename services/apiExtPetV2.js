// POST : Extention PET management interface
//     hmac_md5(time+api_key+service_code+ext_name+command, secret).
//     Hex String Format
// 

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');
var crypto = require('crypto');

var waviotUtil = require("../modules/waviotUtil");
var apiClients = require('../models/apiClients');
var pets = require('../models/pets');
var users = require('../models/users');
var userExtPets = require('../models/userExtPets');
var devices =require('../models/devices');
var petFoodHistory = require('../models/petFoodHistory');
var petWaterHistory = require('../models/petWaterHistory');
var petStore = require('../models/petStore');
var petHealth = require('../models/petHealth');
var calorie = require('../models/calorie');
var busboy = require('connect-busboy');
const asyncHandler = require('express-async-handler')

var timeCheck = true;
var timeLimit = 15; // 15 minutes
var extName = "ExtPetsV1"
var logger = global.logger;
var log_module = "ExtPET";

var fs = require('fs');
var Grid = require("gridfs-stream");
Grid.mongo = mongoose.mongo;

// 사용자가 장치와의 팻 바인딩 정보를 수정한 경우, 이전에 연결되어 있던 정보를 제거한다.
async function clear_user_device_pet_info(service_code, user_key, device_uuid, device_type, new_userExtPet_id)
{
    var conditions = {
        service_code:service_code,
        user_key:user_key, 
        device_type:device_type, 
        device_uuid:device_uuid, 
        _id:{$ne:mongoose.Types.ObjectId(new_userExtPet_id)}        
    };
    var fieldsToSet = {
        device_uuid:"", device_apikey:"", device_pet_key:""
    };
    var options = {"multi":true};

    try {
        var result = await userExtPets.update( conditions, fieldsToSet, options);
        console.log("clear_user_device_pet_info : count - " + result.nModified);
        logger.info("userExtPets:: module:%s, command:%s, device_type:%s, device_uuid:%s, user_key:%s, userExtPet_id:%s, count:%d", 
                log_module, "clear_user_device_pet_info", 
                device_type, device_uuid, user_key, new_userExtPet_id, result.nModified);

        return true;
    } catch (error) {
        console.log("cannot clear_user_device_pet_info :"+error);
        return false;
    }
}


// device에서 생성한 pet정보가 삭제된 경우 해당 pet정보를 바인딩하고 있는 정보를 지운다.
async function clear_user_device_pet_info_all(service_code,  device_uuid, device_type, device_pet_key)
{
    var conditions = {
        service_code:service_code,
        device_pet_key: device_pet_key
    };
    var fieldsToSet = {
        device_uuid:"", device_apikey:"", device_pet_key:""
    };
    var options = {"multi":true};

    try {
        var result = await userExtPets.update( conditions, fieldsToSet, options);
        console.log("clear_user_device_pet_info_all : count - " + result.nModified);
        logger.info("userExtPets:: module:%s, command:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, count:%d", 
                log_module, "clear_user_device_pet_info_all", 
                device_type, device_uuid, device_pet_key, result.nModified);

        return true;
    } catch (error) {
        console.log("cannot clear_user_device_pet_info_all :"+error);
        return false;
    }
}

async function update_user_pets(service_code, client_uuid, client_apikey, user_key, user_login_token, pet_key, pet_name,
    device_type,
    device_uuid,
    device_apikey,
    device_pet_key
    )
{
    if(service_code=="" || client_uuid=="" || client_apikey=="" || user_key=="" || user_login_token=="" || pet_key=="" || pet_name=="")
        return false;

    if(device_type=="") device_type="0";
    try {
        var retval = true;
        var user = await users.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"_id":mongoose.Types.ObjectId(user_key)}
                ]        
            }
        );

        if(user==null)
            return false;

        if(user_login_token!=user.login_token)
            return false;

        if(waviotUtil.check_login_token_expired(user.login_token_expired))
        {
            console.log("login_token_expired");
            return false;
        }

        var userPet = await userExtPets.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"user_key": user_key},
                    {"pet_key": pet_key},
                    {"device_type": device_type}
                ]        
            }
        );

        if(device_type!="0" && userPet==null)
        {
            // 할당되지 않은 pet정보를 찾는다. 
            userPet = await userExtPets.findOne(
                {
                    "$and": [
                        {"service_code": service_code}, 
                        {"user_key": user_key},
                        {"pet_key": pet_key},
                        {"device_type": "0"}
                    ]        
                }
            );
        }

        if(userPet==null)
        {
            // 신규 정보
            // register new 
            var newUserPet = new userExtPets(
                {
                    service_code: service_code, 
                    user_key : user_key,
                    pet_key: pet_key,
                    device_type: device_type,
                    
                    pet_name: pet_name,
                    ref_count: 1,
                    client_uuid: client_uuid,  
                    client_apikey: client_apikey,
                    last_client_uuid: client_uuid,  
                    last_client_apikey: client_apikey,

                    device_uuid: device_uuid,
                    device_apikey: device_apikey,
                    device_pet_key : device_pet_key
                }
            )
            var update_user_pet = await userExtPets.findOneAndUpdate(
                {service_code: service_code, user_key: user_key, pet_key:pet_key, device_type: device_type}, 
                newUserPet, 
                {upsert: true, new: true, runValidators: true});

            // 해당 사용자의 다른 pet정보에 장치 정보가 할당되어 있으면, 정보를 지운다
            if(device_uuid!="" && update_user_pet!=null)
            {
                retval = await clear_user_device_pet_info(service_code, user_key, device_uuid, device_type, update_user_pet._id.toHexString());
            }

            if(update_user_pet!=null)
                logger.info("userExtPets:: module:%s, command:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, user_login_token:%s, pet_key:%s, pet_name:%s", 
                    log_module, "CREATE", 
                    device_type, device_uuid, device_pet_key, user_key, user_login_token, 
                    pet_key, pet_name);
            else
                logger.info("userExtPets:: module:%s, command:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, user_login_token:%s, pet_key:%s, pet_name:%s", 
                    log_module, "CREATE FAIL", 
                    device_type, device_uuid, device_pet_key, user_key, user_login_token, 
                    pet_key, pet_name);

        }
        else
        {
            // 정보 변경
            userPet.pet_name = pet_name;
            userPet.last_updated_time = moment();
            userPet.last_client_uuid = client_uuid;
            userPet.last_client_apikey = client_apikey;
            userPet.device_uuid = device_uuid;
            userPet.device_apikey = device_apikey;
            userPet.device_pet_key = device_pet_key;
            
            if(userPet.device_type=="0") 
                userPet.device_type = device_type;
                
            //userPet.ref_count++;
            var updateitem = await userPet.save();
            // 해당 사용자의 다른 pet정보에 장치 정보가 할당되어 있으면, 정보를 지운다
            if(device_uuid!="" && updateitem!=null)
            {
                retval = await clear_user_device_pet_info(service_code, user_key, device_uuid, device_type, updateitem._id.toHexString());
            }                        
            if(updateitem!=null)
                logger.info("userExtPets:: module:%s, command:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, user_login_token:%s, pet_key:%s, pet_name:%s", 
                    log_module, "UPDATE", 
                    device_type, device_uuid, device_pet_key, user_key, user_login_token, 
                    pet_key, pet_name);
            else
                logger.info("userExtPets:: module:%s, command:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, user_login_token:%s, pet_key:%s, pet_name:%s", 
                    log_module, "UPDATE FAIL", 
                    device_type, device_uuid, device_pet_key, user_key, user_login_token, 
                    pet_key, pet_name);
}
        return retval;
    } catch (error) {
        console.log("cannot update_user_pets :"+error);
        return false;
    }
}

async function del_user_pets(service_code, user_key, user_login_token, pet_key)
{
    if(service_code=="" || user_key=="" || user_login_token=="" || pet_key=="")
        return false;

    try {
        var user = await users.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"_id":mongoose.Types.ObjectId(user_key)}
                ]        
            }
        );
        if(user==null)
            return false;
        if(user_login_token!=user.login_token)
            return false;
        if(waviotUtil.check_login_token_expired(user.login_token_expired))
            return false;

        var userPet = await userExtPets.findOne(
                {
                    "$and": [
                        {"service_code": service_code}, 
                        {"user_key": user_key},
                        {"pet_key": pet_key}
                    ]        
                }
            );

        if(userPet==null)
            return false;

        await userPet.remove();
        return true;
    } catch (error) {
        console.log("cannot del_user_pets :"+error);
        return false;      
    }
}


// new pet info
async function create_pet_info(res, msgtime, service_code, client_uuid, client_apikey, 
        user_key, user_login_token,  // 옵션 값 
        pet_name, pet_type, pet_gender, birthday, 
        breed_group, breed, neutralization,
        weight, height, 
        body_height, 
        length, back_length, 
        width,neck_length,
        girth_length,
        pregnancy,
        food,
        device_manage_key, 
        device_type,
        device_uuid,
        device_apikey,
        device_pet_key
    )
{
    try {
        // pet정보 특정상 중복 검사는 없음. 
        // register new 
        var pic_random_id = "petpic_"+waviotUtil.gen_session_key(client_uuid+pet_name);
        var user_device_type = device_type;

        // 사용자가 직접 등록하는 팻 정보
        // 장치만 이 값을 전달한다. 
        if(device_manage_key==null || device_manage_key=="")  user_device_type ="0"; 

        var newpet = new pets(
            {
                service_code: service_code, 
                name: pet_name,
                type: pet_type, // cat, dog    
                gender: pet_gender,  // male, female
                birthday : birthday,
                breed_group: breed_group,
                breed: breed,
                neutralization : neutralization,
                weight: weight,
                height: height,
                body_height: body_height, 
                length: length,
                back_length: back_length,
                width: width, 
                neck_length: neck_length,
                girth_length: girth_length,
                pregnancy: pregnancy,
                food: food,
                pic_id: pic_random_id,
                pic_key : "",
                client_uuid: client_uuid,
                client_apikey: client_apikey,
                device_manage_key: device_manage_key,
                device_type: user_device_type
            }
        );

        var newitem = await newpet.save();

        res.send(
            {
                service_code: service_code, 
                pet_key:newitem._id.toHexString(),
                pic_id: newitem.pic_id,
                api_key: client_apikey,
                device_uuid : device_uuid,
                device_type: device_type,
                device_apikey : device_apikey,
                device_pet_key: device_pet_key        
            }
        );

        logger.info("RES:: module:%s, command:%s, msgtime:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, user_login_token:%s, device_manage_key:%s, NEW pet_key:%s", 
        log_module, "REGPET", msgtime, 
        device_type, device_uuid, device_pet_key, user_key, user_login_token, device_manage_key,
        newitem._id.toHexString());

        // user정보가 존재할 경우. 그 정보를 기록/갱신한다. 
        if(user_key!=null && user_key!="" && user_login_token!=null && user_login_token!="")
        {
            await update_user_pets(service_code, client_uuid, client_apikey, 
                    user_key, user_login_token, newitem._id.toHexString(), 
                    newitem.name,
                    device_type,
                    device_uuid,
                    device_apikey,
                    device_pet_key);
        }

        return true;
    } catch (error) {
        res.status(500).send("System Error."+error);
        logger.info("ERROR:: module:%s, command:%s, device_pet_key:%s, %s", 
                log_module, "create_pet_info", device_pet_key, error); 
        return false;
    }
}


async function update_device_ref_pet_info(
    service_code, device_pet_key, except_user_pet_key, 
    pet_name, pet_type, pet_gender, birthday, 
    breed_group, breed, neutralization,
    weight, height, 
    body_height, 
    length, back_length, 
    width,neck_length,
    girth_length, pregnancy, food
)
{
    // 바인딩 정보가 없는 경우에는 무시한다. 
    if(device_pet_key==null || device_pet_key=="")
    {
        return 0;
    }

    // device_pet_key를 가지고 있는 사용자 pet정보를 검색
    query = {"service_code": service_code, "device_pet_key":device_pet_key};
    fields =  {"service_code":1, "user_key":1, "pet_key":1, "pet_name":1, "_id":0, 
    "device_uuid" : 1,
    "device_apikey" : 1,
    "device_pet_key" : 1};
    sorts = {
        sort : {"last_updated_time":-1 }
    };

    try {
        var userpets = await userExtPets.find(query,fields,sorts);
        var count = 0;
        for (let index = 0; index < userpets.length; index++) {
            refpet = userpets[index];
            if(except_user_pet_key!=refpet.pet_key) 
            {
                var petinfo = await pets.findOne(
                    {
                        "$and": [
                            {"_id": mongoose.Types.ObjectId(refpet.pet_key)}, 
                            {"service_code": service_code} 
                        ]        
                    });
                
                if(petinfo!=null)
                {
                    petinfo.name = pet_name;
                    petinfo.type = pet_type;
                    petinfo.gender = pet_gender;
                    petinfo.birthday = birthday;
                    petinfo.breed_group = breed_group;
                    petinfo.breed = breed;
                    petinfo.neutralization = neutralization;
                    petinfo.weight = weight;
                    petinfo.height = height;
                    petinfo.body_height = body_height;
                    petinfo.length = length;
                    petinfo.back_length = back_length;
                    petinfo.width = width;
                    petinfo.neck_length = neck_length;
                    petinfo.girth_length = girth_length;
                    petinfo.pregnancy = pregnancy;
                    petinfo.food = food;
                    petinfo.last_updated_time = moment();
        
                    await petinfo.save();
                }
                else
                    console.log("SKIPPED update pet info ", refpet.pet_key)
                count = count + 1;
            }
        };

        logger.info("userExtPets:: module:%s, command:%s, device_pet_key:%s, count:%d", 
                log_module, "update_device_ref_pet_info", 
                device_pet_key, count);

        return count;
    } catch (error) {
        logger.info("ERROR:: module:%s, command:%s, device_pet_key:%s, %s", 
                log_module, "update_device_ref_pet_info", device_pet_key, error); 
        return -1;
    }
}


async function update_pet_info(res, msgtime, service_code, client_uuid, client_apikey, 
    pet_key, user_key, user_login_token,
    pet_name, pet_type, pet_gender, birthday, 
    breed_group, breed, neutralization,
    weight, height, 
    body_height, 
    length, back_length, 
    width,neck_length,
    girth_length,
    pregnancy,
    food,
    device_manage_key, 
    device_type,
    device_uuid,
    device_apikey,
    device_pet_key
)
{

    try {
        var pet = await  pets.findOne(
            {
                "$and": [
                    {"_id": mongoose.Types.ObjectId(pet_key)}, 
                    {"service_code": service_code} 
                ]        
            })
        if(pet==null)
        {
            res.status(400).send("Bad Request.")
            return;
        }

        pet.name = pet_name;
        pet.type = pet_type;
        pet.gender = pet_gender;
        pet.birthday = birthday;
        pet.breed_group = breed_group;
        pet.breed = breed;
        pet.neutralization = neutralization;
        pet.weight = weight;
        pet.height = height;
        pet.body_height = body_height;
        pet.length = length;
        pet.back_length = back_length;
        pet.width = width;
        pet.neck_length = neck_length;
        pet.girth_length = girth_length; 
        pet.pregnancy = pregnancy;
        pet.food = food;           
        pet.last_client_uuid = client_uuid;
        pet.last_client_apikey = client_apikey;
        pet.device_manage_key =device_manage_key;
        pet.last_updated_time = moment();

        var updatepet = await pet.save();
    
        res.status(200).send(
            {
                service_code: service_code, 
                pet_key:updatepet._id.toHexString(),
                pic_id: updatepet.pic_id,
                apikey: client_apikey, 
                device_type : device_type, 
                device_uuid : device_uuid,
                device_apikey : device_apikey,
                device_pet_key : device_pet_key
            }
        );

        logger.info("RES:: module:%s, command:%s, msgtime:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, login_token:%s, device_manage_key:%s, pet_key:%s", 
        log_module, "UPDATEPET", msgtime, 
        device_type, device_uuid, device_pet_key, user_key, user_login_token, device_manage_key, pet_key);         

        if(user_key!=null && user_key!="" && user_login_token!=null && user_login_token!="")
        {
            await update_user_pets(service_code, client_uuid, client_apikey, 
                    user_key, user_login_token, updatepet._id.toHexString(), 
                    updatepet.name,
                    device_type,
                    device_uuid,
                    device_apikey,
                    device_pet_key);
        }

        // device에서 pet정보를 갱신했을 경우, 연결되어 있는 사용자의 pet정보도 갱신한다. 
        if(device_manage_key!="")
        {
            // device_pet_key가 없는 경우는 장치이거나 사용자가 바인딩하지 않은 경보
            await update_device_ref_pet_info(
                service_code, pet._id.toHexString(), "",
                pet_name, pet_type, pet_gender, birthday, 
                breed_group, breed, neutralization,
                weight, height, 
                body_height, 
                length, back_length, 
                width,neck_length,
                girth_length, pregnancy, food
            )

        }
        else
        {
            await update_device_ref_pet_info(
                service_code, device_pet_key, pet._id.toHexString(), 
                pet_name, pet_type, pet_gender, birthday, 
                breed_group, breed, neutralization,
                weight, height, 
                body_height, 
                length, back_length, 
                width,neck_length,
                girth_length, pregnancy, food
            )
        }
    } catch (error) {
        logger.info("ERROR:: module:%s, command:%s, pet_name:%s, error:%s", 
                log_module, "update_pet_info", pet_name, error);
        res.status(500).send("System Error."+error)
    }
}

async function update_device_pet_info(
    device_petinfo, 
    pet_name, pet_type, pet_gender, birthday, 
    breed_group, breed, neutralization,
    weight, height, 
    body_height, 
    length, back_length, 
    width,neck_length,
    girth_length,
    pregnancy,
    food
)
{
    try {
        device_petinfo.name = pet_name;
        device_petinfo.type = pet_type;
        device_petinfo.gender = pet_gender;
        device_petinfo.birthday = birthday;
        device_petinfo.breed_group = breed_group;
        device_petinfo.breed = breed;
        device_petinfo.neutralization = neutralization;
        device_petinfo.weight = weight;
        device_petinfo.height = height;
        device_petinfo.body_height = body_height;
        device_petinfo.length = length;
        device_petinfo.back_length = back_length;
        device_petinfo.width = width;
        device_petinfo.neck_length = neck_length;
        device_petinfo.girth_length = girth_length;  
        device_petinfo.pregnancy = pregnancy;
        device_petinfo.food = food;          
        device_petinfo.last_updated_time = moment();
    
        await device_petinfo.save();
    } catch (error) {
        logger.info("ERROR:: module:%s, command:%s, pet_name:%s, error:%s", 
                log_module, "update_device_pet_info", pet_name, error);
    }
}

async function del_pet_info(res, msgtime, service_code, client_uuid, client_apikey, 
        pet_key, user_key, user_login_token, device_manage_key)
{
    try {
        var conn = mongoose.connection;
        var gfs = Grid(conn.db);
        var resmsg = {retCode: "0000", msg: "success"};
        var item = pets.findOne(
            {
                "$and": [
                    {"_id": mongoose.Types.ObjectId(pet_key)}, 
                    {"service_code": service_code} 
                ]        
            }
        );

        if(item==null)
        {
            res.status(404).send("No Data.");
            logger.info("RES:: module:%s, command:%s, msgtime:%s, user_key:%s, login_token:%s, device_manage_key:%s, pet_key:%s, NO DATA", 
                    log_module, "DELETEPET", msgtime, 
                    user_key, user_login_token, device_manage_key,
                    pet_key); 
            return;
        }

        var pic_id = item.pic_id;
        var pic_key = item.pic_key;
        var device_type = pets.device_type;
            
        await item.remove();
        res.send(JSON.stringify(resmsg,null,"\t"));

        logger.info("RES:: module:%s, command:%s, msgtime:%s, user_key:%s, login_token:%s, device_manage_key:%s, pet_key:%s", 
                log_module, "DELETEPET", msgtime, 
                user_key, user_login_token, device_manage_key,
                pet_key); 

        if(user_key!=null && user_key!="" && user_login_token!=null && user_login_token!="")
            await del_user_pets(service_code, user_key, user_login_token, pet_key);

        // device일 경우 관련 user binding 정보 clear
        if(device_manage_key!=null && device_manage_key!="")
           await clear_user_device_pet_info_all(service_code, client_uuid, device_type, pet_key);

        if(pic_id!=null && pic_id!="" && pic_key!="")
        {
            // picture정보가 있으면 gridfs에서 삭제하기
            gfs.remove({ filename: pic_id}, 
                function (err) {
                console.log("DELETE PET PICTURE :"+pic_id);
                }   
            );
        }
    } catch (error) {
        logger.info("ERROR:: module:%s, command:%s, msgtime:%s, user_key:%s, login_token:%s, device_manage_key:%s, pet_key:%s, %s", 
                log_module, "del_pet_info", msgtime, 
                user_key, user_login_token, device_manage_key, pet_key, error); 
        res.status(500).send("Bad Requests");
    }
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/*

{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "create",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //compulsory
    "pet_name" : "dogname",
    "pet_type" : "dog".       // lower case
    "pet_gender" : "male"    // lower case

    // optional
    "birthday" : "20170601", 
    "breed_group": "",
    "breed" : "",
    "neutralization" : false,
    "weight": 18.9,  //kg
    "height": 30.1,  //cm
    "body_height": 0, //cm
    "length": 0, //cm
    "back_length": 0,//cm
    "width": 0, //cm
    "neck_length": 0,//cm
    "girth_length": 0,    //cm
    
    // the followings are optional values if has login user information. 
    "user_key": "123121231231323",
    "login_token" : "12123123123",
    "device_uuid" : "device_uuid1",
    "device_pet_key" : "device_uuid1"
}
*/
async function do_reg_pet(req, res, service_code, client_uuid, client_apikey)
{
    var msgtime = req.body.time;
    var pet_name = req.body.name==null ? "" : req.body.name;
    var pet_type = req.body.type==null ? "" : req.body.type.toString().toLowerCase();
    var pet_gender = req.body.gender==null ?  "": req.body.gender.toString().toLowerCase();
    var birthday = req.body.birthday==null ? "" : req.body.birthday;

    var breed_group = req.body.breed_group==null ? "" : req.body.breed_group;
    var breed = req.body.breed==null ? "" : req.body.breed;
    var neutralization = req.body.neutralization==null ? false : req.body.neutralization;

    var weight = req.body.weight==null ? 0.0 : req.body.weight;
    var height = req.body.height==null ? 0.0 : req.body.height;
    var body_height = req.body.body_height==null ? 0.0 : req.body.body_height;
    var length = req.body.length==null ? 0.0 : req.body.length;
    var back_length = req.body.back_length==null ? 0.0 : req.body.back_length;
    var width = req.body.width==null ? 0.0 : req.body.width;
    var neck_length = req.body.neck_length==null ? 0.0 : req.body.neck_length;
    var girth_length = req.body.girth_length==null ? 0.0 : req.body.girth_length;
    var device_uuid = req.body.device_uuid==null ? "" : req.body.device_uuid;
    var device_pet_key = req.body.device_pet_key==null ? "" : req.body.device_pet_key;
    var user_key  = req.body.user_key==null ? "" : req.body.user_key;
    var login_token  = req.body.login_token==null ? "" : req.body.login_token;

    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;

    var pregnancy = req.body.pregnancy==null ? "none" : req.body.pregnancy;
    var food = req.body.food==null ? "" : req.body.food;

    var request_validation = true;
    var validation_step = 0;

    logger.info("REQ:: module:%s, command:%s, msgtime:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, login_token:%s, device_manage_key:%s, pet_name:%s, pet_type:%s, pet_gender:%s, birthday:%s, breed_group:%s, neutralization:%s", 
                log_module, "REGPET", msgtime, 
                device_type, device_uuid, device_pet_key, user_key, login_token, device_manage_key,
                pet_name, pet_type, pet_gender, birthday, breed_group, neutralization ? "Yes" : "No"); 
    
    // it is required valiees
    if(pet_name=="")
    {
        validation_step = 1;
        request_validation = false;
    }

    if(pet_type!="dog" && pet_type!="cat")
    {
        validation_step = 2;
        request_validation = false;
    }

    if(pet_gender!="male" && pet_gender!="female")
    {
        validation_step = 3;
        request_validation = false;
    }

    if(device_uuid!="" && device_type=="")
    {
        validation_step = 4;
        request_validation = false;
    }

    if(device_manage_key!="" && device_type=="")
    {
        validation_step = 5;
        request_validation = false;
    }

    if(request_validation==false)
    {
        logger.info("RES:: module:%s, command:%s, msgtime:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, login_token:%s, device_manage_key:%s, Bad Request!", 
            log_module, "REGPET", msgtime, 
            device_type, device_uuid, device_pet_key, user_key, login_token, device_manage_key);
        res.status(400).send("Bad Request :"+validation_step.toString());
        return;
    }

    var subcode = 0;

    try {
        // device_uuid가 존재하지 않으면 독립적인 pet정보를 생성한다. 
        if(device_uuid=="")
        {
            // 사용자가 임의로 등록하는 정보이거나 장치에서 등록하는 정보
            await create_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                user_key, login_token, 
                pet_name, pet_type, pet_gender, birthday,
                breed_group, breed, neutralization, 
                weight, height, body_height, length, back_length, 
                width, neck_length, girth_length, pregnancy, food,
                device_manage_key, device_type,
                "", "", "");
        }
        else
        {
            // device_uuid가 존재하면, device가 가지고 있는 pet정보를 조회한다.
            var findop = {};
            if(device_pet_key=="")
                findop = {"service_code": service_code, "client_uuid":device_uuid, "device_type":device_type};
            else
                findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
            subcode = 1;
            var device_pet = await pets.findOne(findop);

            if(device_pet==null)
            {
                // device pet 정보 생성
                // device 정보 검색 
                findop = {"service_code": service_code, "uuid":device_uuid};
                subcode = 2;
                var device_info = await devices.findOne(findop);

                if(device_info==null)
                {
                    res.status(404).send("Invalid Device info");
                    logger.info("RES:: module:%s, command:%s, msgtime:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, login_token:%s, device_manage_key:%s, Invalid Device info!", 
                        log_module, "REGPET", msgtime, 
                        device_type, device_uuid, device_pet_key, user_key, login_token, device_manage_key);
                    return;                    
                }
                device_apikey = device_info.client_key;
                // device pet 정보 생성
                // register new device pet 
                var pic_random_id = "petpic_"+waviotUtil.gen_session_key(device_uuid+pet_name);
                var newdevicepet = new pets(
                    {
                        service_code: service_code, 
                        name: pet_name,
                        type: pet_type, // cat, dog    
                        gender: pet_gender,  // male, female
                        birthday : birthday,
                        breed_group: breed_group,
                        breed: breed,
                        neutralization : neutralization,
                        weight: weight,
                        height: height,
                        body_height: body_height, 
                        length: length,
                        back_length: back_length,
                        width: width, 
                        neck_length: neck_length,
                        girth_length: girth_length,
                        pregnancy: pregnancy,
                        food: food,
                        pic_id: pic_random_id,
                        pic_key : "",
                        device_manage_key : device_uuid,  // 자동 생성
                        device_type : device_type,
                        client_uuid: device_uuid,
                        client_apikey: device_apikey
                    }
                )

                subcode = 3;
                let newpetitem = await newdevicepet.save();
                device_pet_key = newpetitem._id.toHexString();
                await  create_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                                user_key, login_token, 
                                pet_name, pet_type, pet_gender, birthday,
                                breed_group, breed, neutralization, 
                                weight, height, body_height, length, back_length, 
                                width, neck_length, girth_length, pregnancy, food,
                                "", device_type,
                                device_uuid, device_apikey, device_pet_key);
            }
            else
            {
                device_apikey = device_pet.client_apikey;
                device_pet_key  = device_pet._id.toHexString();
                subcode = 4;
                await create_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                    user_key, login_token, 
                    pet_name, pet_type, pet_gender, birthday,
                    breed_group, breed, neutralization, 
                    weight, height, body_height, length, back_length, 
                    width, neck_length, girth_length, pregnancy, food,
                    "", device_type,
                    device_uuid, device_apikey, device_pet_key);
                subcode = 5;
                // device  pet 정보도 갱신
                await update_device_pet_info(device_pet,
                    pet_name, pet_type, pet_gender, birthday,
                    breed_group, breed, neutralization, 
                    weight, height, body_height, length, back_length, 
                    width, neck_length, girth_length, pregnancy, food
                );
            }
        }
    } catch (error) {
        logger.info("ERROR:: module:%s, command:%s, msgtime:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, login_token:%s, device_manage_key:%s, %s, %s", 
            log_module, "do_reg_pet", msgtime, 
            device_type, device_uuid, device_pet_key, user_key, login_token, device_manage_key, subcode.toString(), error);
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}

///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "update",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //compulsory
    "pet_key" : "5987fe0580a94c6077562c36b8c65442",
    "pet_name" : "dogname",
    "pet_type" : "dog".       // lower case
    "pet_gender" : "male"    // lower case

    // optional
    "birthday" : "20170601", 
    "breed_group": "",
    "breed" : "",
    "neutralization" : false,
    "weight": 18.9,  //kg
    "height": 30.1,  //cm
    "body_height": 0, //cm
    "length": 0, //cm
    "back_length": 0, //cm
    "width": 0, //cm
    "neck_length": 0, //cm
    "girth_length": 0,    //cm
    
    // the followings are optional values if has login user information. 
    "user_key": "123121231231323",
    "login_token" : "12123123123",
    "device_uuid" : "device_uuid1",
    "device_pet_key" : "device_pet_key"
*/
async function do_update_pet(req, res, service_code, client_uuid, client_apikey)
{
    var msgtime = req.body.time;
    var pet_key = req.body.pet_key==null ? "" : req.body.pet_key;
    var pet_name = req.body.name==null ? "" : req.body.name;
    var pet_type = req.body.type==null ? "" : req.body.type.toString().toLowerCase();
    var pet_gender = req.body.gender==null ?  "": req.body.gender.toString().toLowerCase();
    var birthday = req.body.birthday==null ? "" : req.body.birthday;

    var breed_group = req.body.breed_group==null ? "" : req.body.breed_group;
    var breed = req.body.breed==null ? "" : req.body.breed;
    var neutralization = req.body.neutralization==null ? false : req.body.neutralization;

    var weight = req.body.weight==null ? 0.0 : req.body.weight;
    var height = req.body.height==null ? 0.0 : req.body.height;
    var body_height = req.body.body_height==null ? 0.0 : req.body.body_height;
    var length = req.body.length==null ? 0.0 : req.body.length;
    var back_length = req.body.back_length==null ? 0.0 : req.body.back_length;
    var width = req.body.width==null ? 0.0 : req.body.width;
    var neck_length = req.body.neck_length==null ? 0.0 : req.body.neck_length;
    var girth_length = req.body.girth_length==null ? 0.0 : req.body.girth_length;
    var device_pet_key = req.body.device_pet_key==null ? "" : req.body.device_pet_key;
    var device_uuid = req.body.device_uuid==null ? "" : req.body.device_uuid;

    var user_key  = req.body.user_key==null ? "" : req.body.user_key;
    var login_token  = req.body.login_token==null ? "" : req.body.login_token;

    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;

    var pregnancy = req.body.pregnancy==null ? "none" : req.body.pregnancy;
    var food = req.body.food==null ? "" : req.body.food;

    logger.info("REQ:: module:%s, command:%s, msgtime:%s, device_type:%s, device_uuid:%s, device_pet_key:%s, user_key:%s, login_token:%s, device_manage_key:%s, pet_key:%s, pet_name:%s, pet_type:%s, pet_gender:%s, birthday:%s, breed_group:%s, neutralization:%s", 
                log_module, "UPDATEPET", msgtime, 
                device_type, device_uuid, device_pet_key, user_key, login_token, device_manage_key,
                pet_key, pet_name, pet_type, pet_gender, birthday, breed_group, neutralization ? "Yes" : "No"); 

    var request_validation = true;
    
    var validation_step = 0;
    // it is required valiees
    if(pet_key=="" || pet_name=="")
    {
        validation_step = 1;
        request_validation = false;
    }

    if(pet_type!="dog" && pet_type!="cat")
    {
        validation_step = 2;
        request_validation = false;
    }

    if(pet_gender!="male" && pet_gender!="female")
    {
        validation_step = 3;
        request_validation = false;
    }

    if(device_uuid!="" && device_type=="")
    {
        validation_step = 4;
        request_validation = false;
    }

    if(device_manage_key!="" && device_type=="")
    {
        validation_step = 5;
        request_validation = false;
    }

    if(request_validation==false)
    {
        res.status(400).send("Bad Request :"+validation_step.toString());
        console.log("Bad Request : UPDATEPET "+validation_step.toString());
        return;
    }
 
    var subcode = 0;
    try {
        
        // device_uuid가 존재하지 않으면, 클라이언트의 정보만 갱신한다.. 
        if(device_uuid=="")
        {
            await update_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                pet_key, user_key, login_token, 
                pet_name, pet_type, pet_gender, birthday,
                breed_group, breed, neutralization, 
                weight, height, body_height, length, back_length, 
                width, neck_length, girth_length, pregnancy, food,
                device_manage_key, device_type,
                "", "", "");
        }
        else
        {
            // device_uuid가 존재하면, device가 가지고 있는 pet정보를 조회한다.
            var findop = {};

            if(device_pet_key=="")
                findop = {"service_code": service_code, "client_uuid":device_uuid, "device_type":device_type};
            else
                findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};

            subcode = 1;
            device_pet = await pets.findOne(findop);

            if(device_pet==null)
            {
                // device pet 정보 생성
                // device 정보 검색 
                findop = {"service_code": service_code, "uuid":device_uuid};
                subcode = 2;
                var device_info = await devices.findOne(findop);
                if(device_info==null)
                {
                    res.status(404).send("Invalid Device info");
                    return;                    
                }
                device_apikey = device_info.client_key;

                // device pet 정보 생성
                // register new device pet 
                var pic_random_id = "petpic_"+waviotUtil.gen_session_key(device_uuid+pet_name);
                var newdevicepet = new pets(
                        {
                            service_code: service_code, 
                            name: pet_name,
                            type: pet_type, // cat, dog    
                            gender: pet_gender,  // male, female
                            birthday : birthday,
                            breed_group: breed_group,
                            breed: breed,
                            neutralization : neutralization,
                            weight: weight,
                            height: height,
                            body_height: body_height, 
                            length: length,
                            back_length: back_length,
                            width: width, 
                            neck_length: neck_length,
                            girth_length: girth_length,
                            pregnancy: pregnancy,
                            food: food,
                            pic_id: pic_random_id,
                            pic_key : "",
                            device_manage_key : device_uuid,  // 자동 생성
                            device_type : device_type,
                            client_uuid: device_uuid,
                            client_apikey: device_apikey
                        }
                    )

                subcode = 3;
                var item = await newdevicepet.save();
                device_pet_key = item._id.toHexString();
                await update_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                    pet_key, user_key, login_token, 
                    pet_name, pet_type, pet_gender, birthday,
                    breed_group, breed, neutralization, 
                    weight, height, body_height, length, back_length, 
                    width, neck_length, girth_length, pregnancy, food,
                    device_manage_key, device_type,
                    device_uuid, device_apikey, device_pet_key);
            }
            else
            {
                device_apikey = device_pet.client_apikey;
                device_pet_key  = device_pet._id.toHexString();

                await update_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                    pet_key, user_key, login_token, 
                    pet_name, pet_type, pet_gender, birthday,
                    breed_group, breed, neutralization, 
                    weight, height, body_height, length, back_length, 
                    width, neck_length, girth_length, pregnancy, food,
                    device_manage_key, device_type,
                    device_uuid, device_apikey, device_pet_key);
                    
                // device  pet 정보도 갱신
                await update_device_pet_info(device_pet,
                    pet_name, pet_type, pet_gender, birthday,
                    breed_group, breed, neutralization, 
                    weight, height, body_height, length, back_length, 
                    width, neck_length, girth_length, pregnancy, food
                );
            }
        }
    } catch (error) {
        console.log("Error do_update_pet : ", error)        
        res.status(500).send("System Error. subcode:", subcode);        
    }
}

///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "read",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //compulsory
    "pet_key" : "5987fe0580a94c6077562c36b8c65442",
}
*/

async function do_read_pet(req, res, service_code, client_uuid, client_apikey)
{
    var msgtime = req.body.time;
    var pet_key = req.body.pet_key==null ? "" : req.body.pet_key;
    var request_validation = true;

    logger.info("REQ:: module:%s, command:%s, msgtime:%s, client_uuid:%s, pet_key:%s", 
                log_module, "READPET", msgtime, client_uuid, pet_key);

    // it is required valiees
    if(pet_key=="")
        request_validation = false;

    if(request_validation==false)
    {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, msgtime:%s, client_uuid:%s, pet_key:%s, BED REQUEST!", 
                    log_module, "READPET", msgtime, client_uuid, pet_key);
        return;
    }

    var subcode = 0;
    try {
        // user_key 유효성 검사
        var pet = await pets.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"_id":mongoose.Types.ObjectId(pet_key)}
                ]        
            }
            ,
            {
                "service_code": 1,
                "name": 1,
                "type": 1,
                "gender": 1,
                "birthday": 1,
                "breed_group": 1,
                "breed": 1,
                "neutralization": 1,
                "weight": 1,
                "height": 1,
                "body_height": 1, 
                "length": 1,
                "back_length": 1,
                "width": 1,
                "neck_length": 1,
                "girth_length": 1,
                "pregnancy": 1,
                "food": 1,
                "pic_id": 1,
                "time_zone" : 1,
                "last_updated_time": 1,
                "device_manage_key" : 1,
                "device_type" : 1
            }
        );
        
        if(pet==null)
            request_validation = false;

        if(request_validation==false)
        {
            res.status(400).send("Bad Request.[2]");
            logger.info("RES:: module:%s, command:%s, msgtime:%s, client_uuid:%s, pet_key:%s, BED REQUEST!!", 
                log_module, "READPET", msgtime, client_uuid, pet_key);
            return;
        }
        res.send(JSON.stringify(pet,null,"\t"));
        logger.info("RES:: module:%s, command:%s, msgtime:%s, client_uuid:%s, pet_key:%s, pet name:%s, Success", 
                    log_module, "READPET", msgtime, client_uuid, pet_key, pet.name);
    } catch (error) {
        console.log("Error do_read_pet : ", error)        
        res.status(500).send("System Error. subcode:", subcode);
    }
}




///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "getpets",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //option
	"user_key": "594a1946975c172574880e7a",
    "login_token" : "12123123123"
}
*/

async function do_get_pets(req, res, service_code, client_uuid, client_apikey)
{
    var msgtime = req.body.time;
    var user_key = req.body.user_key==null ? "" : req.body.user_key;
    var login_token = req.body.login_token==null ? "" : req.body.login_token;
    logger.info("REQ:: module:%s, command:%s, msgtime:%s, client_uuid:%s, user_key:%s, login_token:%s", 
                log_module, "GETPETS", msgtime, client_uuid, user_key, login_token);

    var request_validation = true;

    var subcode = 0;

    try {
        if(user_key!="" && login_token!="")
        {
            // user기준으로 조회
            // user_key 유효성 검사
            var user = await users.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"_id":mongoose.Types.ObjectId(user_key)}
                ]
            });

            if(user==null || login_token!=user.login_token || waviotUtil.check_login_token_expired(user.login_token_expired))
                request_validation = false;

            if(request_validation==false)
            {
                res.status(400).send("Bad Request.[2]");
                logger.info("RES:: module:%s, command:%s, msgtime:%s, client_uuid:%s, user_key:%s, login_token:%s, BAD REQUEST!", 
                    log_module, "GETPETS", msgtime, client_uuid, user_key, login_token);
                return;
            }

            query = {"service_code": service_code, "user_key":user_key};
            fields =  {"service_code":1, "user_key":1, "pet_key":1, "pet_name":1, "_id":0, 
            "device_uuid" : 1,
            "device_apikey" : 1,
            "device_pet_key" : 1,
            "device_type" : 1};
            sorts = {
                sort : {"last_updated_time":-1 }
            };

            subcode = 2;
            var extpets = await userExtPets.find(query,fields,sorts);
            res.send(JSON.stringify(extpets,null,"\t"));
            logger.info("RES:: module:%s, command:%s, msgtime:%s, client_uuid:%s, user_key:%s, login_token:%s, Count[%d], Success!", 
                log_module, "GETPETS", msgtime, client_uuid, user_key, login_token, extpets.length);
        }
        else
        {
            // 장치 기준
            query = {"service_code": service_code, "client_apikey":client_apikey, "client_uuid":client_uuid};
            fields =  {"service_code":1, "client_apikey":1, "client_uuid":1, "name":1, "device_type":1
            };
            sorts = {
                sort : {"last_updated_time":-1 }
            };

            subcode = 3;
            var petlist = await pets.find(query,fields,sorts);
            jsonstring = JSON.stringify(petlist,null,"\t");
            jsonstring = jsonstring.replace(/\"_id\":/g, "\"pet_key\":");
            res.send(jsonstring);
            logger.info("RES:: module:%s, command:%s, msgtime:%s, client_uuid:%s, user_key:%s, login_token:%s, Count[%d], Success!!", 
                log_module, "GETPETS", msgtime, client_uuid, user_key, login_token, petlist.length);
        }
    } catch (error) {
        console.log("Error do_get_pets : ", error)
        res.status(500).send("System Error. subcode:", subcode);        
    }
}





///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "delete",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //compulsory
    "pet_key" : "5987fe0580a94c6077562c36b8c65442",

    // the followings are optional values if has login user information. 
    "user_key": "123121231231323",
    "login_token" : "12123123123"    
}
*/
async function do_delete_pet(req, res, service_code, uuid, apikey, account_type, account_id)
{
    var msgtime = req.body.time;
    var pet_key = req.body.pet_key==null ? "" : req.body.pet_key;;
    var user_key = req.body.user_key==null ? "" : req.body.user_key;;
    var login_token = req.body.login_token==null ? "" : req.body.login_token;;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    
    logger.info("REQ:: module:%s, command:%s, msgtime:%s, user_key:%s, login_token:%s, device_manage_key:%s, pet_key:%s", 
                log_module, "DELETEPET", msgtime, 
                user_key, login_token, device_manage_key,
                pet_key); 

    await del_pet_info(res, msgtime, service_code, uuid, apikey, 
        pet_key, user_key, login_token, device_manage_key); 
}

///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "fileupload",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    //compulsory
    "pet_key" : "5987fe0580a94c6077562c36b8c65442", 
}
*/

var file_session_timeLimit = 5;
// true : expired
// false : not expired. it is valid.
function check_file_session_key_expired(file_session_key_expire)
{
    var today = moment();
    var expired = moment(file_session_key_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
        
    // time has problem
    if(diff>file_session_timeLimit)
        return true;

    return false;
    
}

async function do_pic_file_upload(req, res, service_code, uuid, apikey)
{
    var pet_key = req.body.pet_key==null ? "" : req.body.pet_key;

    if(pet_key=="")
    {
        res.status(400).send("Bad Request.1")
        return;
    }

    pets.findOne(
        {
            "$and": [
                {"_id": mongoose.Types.ObjectId(pet_key)}, 
                {"client_uuid": uuid}, 
                {"client_apikey": apikey}, 
                {"service_code": service_code} 
            ]        
        })
        .exec(function(err, pet) 
        {
            if(err)
            {
                res.status(500).send("System Error."+err)
                return;
            }
    
            if(pet==null)
            {
                res.status(400).send("Bad Request.2")
                return;
            }
            pet.file_session_key = waviotUtil.gen_session_key(uuid);
            file_session_key_time  = moment();
            file_session_key_time.add(file_session_timeLimit, "minute");
            pet.file_session_key_expired = file_session_key_time;
        
            pet.save(function(err, updatepet) {
                if(err) {
                    res.status(500).send("System Error. "+err);
                } else {
                    res.send(
                        {
                            service_code: service_code, 
                            api_key: apikey, 
                            pic_id :updatepet.pic_id,
                            file_session_key: updatepet.file_session_key, 
                            file_session_key_expired: file_session_key_time.format('YYYYMMDDHHmmssZZ')
                        }
                    );
                }
            });


        });


}




// pet  management interface
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "",
    .....
	"auth": "4b1a308f29b66779d43075adeb9e93dd"	
}
*/
router.post('/:uuid', asyncHandler(async (req, res) => {
    try {
        var uuid = req.params.uuid;
        var service_code = req.body.service_code;
        var apikey = req.body.api_key;
        var ext_name = req.body.ext;
        var command = req.body.command;
        var auth = req.body.auth;

        if(uuid==null || uuid=="" ||
           service_code==null || service_code=="" || 
           apikey==null || apikey=="" || 
           ext_name==null || ext_name=="" || 
           command==null || command=="")
        {
            res.status(400).send("Bad Request.[0]");
            return;
        }

        // message 시간 검사
        if(timeCheck==true)
        {
            if(req.body.time!=null)
            {
                msgtime = req.body.time;
                if(waviotUtil.check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        if(ext_name!=extName)
        {
            res.status(400).send("Bad Request.[1]");
            return;
        }

        // client key 유효성 검사 
        apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey })
                .exec(function(err, item) 
        {
            if(err)
            {
                res.status(500).send("System Error."+err)
                return;
            }
            if(item==null)
            {
                res.status(404).send("Not Found [1].")
                return;
            }

            if(ext_name!=extName)
            {
                res.status(400).send("Bad Request.");
                return;
            }

            var secret = item.secret;

            // message auth 검사 
            var pauth = waviotUtil.get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret);
            if(auth!=pauth)
            {
                res.status(401).send("Unauthorized. [2]");
                return;
            }

            switch (command) {
                case "create": 
                    do_reg_pet(req, res, service_code, uuid, apikey);
                    break;
                case "read": // 특정 pet정보 조회
                    do_read_pet(req, res, service_code, uuid, apikey);
                    break;
                case "getpets": // 장치나 사용자에 등록된 pet목록 조회
                    do_get_pets(req, res, service_code, uuid, apikey);
                    break;
                case "update": 
                    do_update_pet(req, res, service_code, uuid, apikey);
                    break;
                case "delete": 
                    do_delete_pet(req, res, service_code, uuid, apikey);
                    break;
                case "fileupload": 
                    do_pic_file_upload(req, res, service_code, uuid, apikey);
                    break;
                default:
                    res.status(400).send("Bad Request.")
                    break;
            }
        });

    } catch (error) {
        res.status(400).send("Bad Request."+error)
    }    


}))




///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
router.post('/pic/:picuuid', 
busboy({
    limits: {
        fileSize: 10 * 1024 * 1024
    }
}),
asyncHandler(async (req, res) => {
    try {
        var picuuid = req.params.picuuid;
        var msg = picuuid.toString().split("#");
        var pic_id = msg[0]==null ? "" : msg[0];
        var file_session_key = msg[1]==null ? "" : msg[1];
        var conn = mongoose.connection;
        var gfs = Grid(conn.db);
        var command = "upload_pet_pic";
        var resmsg = {retCode: "0000", msg: "success"};

        logger.info("REQ:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Upload User Picture File!", 
                log_module, command, pic_id, file_session_key);     
        if(pic_id=="" || file_session_key=="")
        {
            res.status(400).send("Bad Request.");
            return;
        }

        req.pipe(req.busboy);
        req.busboy.on('field', function(fieldname, val) 
        {
            //console.log(fieldname, val);
        });

        req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) 
        {
            console.log("Uploading: ", filename,encoding, mimetype);
            file_ext = filename.toString().toLowerCase().substr(filename.toString().length - 4);

            // png 파일명만 처리 한다. 
            if(file_ext!=".png")
            {
                console.log("not PNG!!")
                res.status(400).send("Bad Request.")
                logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, File is not a PNG File!", 
                    log_module, command, pic_id, file_session_key); 
                return;                
            }
            
            // db조회 부분을 busboy와 통합할 수 없음. 
            // 결국 파일을 업로드 완료 후 처리해야 함 
            let writeStream = gfs.createWriteStream({
                filename:  pic_id,
                mode: 'w',
                content_type: mimetype
            });

            if(writeStream==null)
            {
                res.status(400).send("Bad Request.")
                logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Bad Request", 
                    log_module, command, pic_id, file_session_key, error); 
                return;
            }

            file.pipe(writeStream);
            
            writeStream.on('close', (file) => {
                //console.log("written to DB:")

                pets.findOne({pic_id: pic_id, file_session_key:file_session_key})
                .exec(function(err, item) 
                {
                    upload_success = true;
                    if(err!=null || item==null)
                    {
                        upload_success = false;
                        console.log("Invald file session key!!");                    
                    }
        
                    if(upload_success==true &&  check_file_session_key_expired(item.file_session_key_expired)==true)
                    {
                        upload_success = false;
                        console.log("Invald file session key time!!");                    
                    }
                    
                    if(upload_success==false)
                    {
                        // 업로드된 파일 삭제
                        gfs.remove({ _id: file._id.toHexString()}, 
                            function (err) {
                            console.log("DELETE UPLOAD  "+file._id.toHexString());
                            }   
                        );
                        res.status(400).send("Bad Request.")
                        return;                        
                    }

                    // 이전 화일 삭제
                    var old_pic_key = "";
                    if(item.pic_key!=null)
                        old_pic_key=item.pic_key;
                    else
                        old_pic_key = "";
        
                    item.pic_key = file._id.toHexString();
                    item.save(function(err, upateitem)
                    {
                        if(err!=null || upateitem==null)
                        {
                            gfs.remove({ _id: file._id.toHexString()}, 
                                function (err) {
                                    console.log("DELETE "+file._id.toHexString());
                                }
                            );

                            res.status(400).send("Bad Request.")
                            return;
                        }

                        // 정상 업로드 후 이전에 등록 이미지 삭제
                        if(old_pic_key!="")
                        {
                            gfs.remove({ _id: old_pic_key}, 
                                function (err) {
                                    console.log("DELETE OLD FILE "+old_pic_key);
                                }
                            );
                        }
                        logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Success!!!", 
                            log_module, command, pic_id, file_session_key); 
            
                        return res.status(200).send({
                            message: JSON.stringify(resmsg),
                            file: file
                        });
                    });
                });              
            });
        });

        req.busboy.on('finish', function()
        {
            logger.info("INFO:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Succeed to upload file!", 
                log_module, command, pic_id, file_session_key); 
            //console.log("finish");
        });       
        //writeStream.end();
    } catch (error) {
        res.status(400).send("Bad Request.")
        logger.info("RES:: module:%s, command:%s, pic_id:%s, file_session_key:%s, Bad Request:%s", 
            log_module, "upload_user_pic", pic_id, file_session_key, error); 
    }
}));

router.get('/pic/:picuuid', 
asyncHandler(async (req, res) => {
    var conn = mongoose.connection;
    var gfs = Grid(conn.db);

    logger.info("REQ:: module:%s, command:%s, pic_id:%s", log_module, "get_pet_pic", req.params.picuuid); 

    gfs.files.find({
        filename: req.params.picuuid
    }).toArray((err, files) => {
        if (files.length === 0) {
            return res.status(400).send("Bad Request.");
        }
        let data = [];
        let readstream = gfs.createReadStream({
            filename: files[0].filename
        });

        readstream.on('data', (chunk) => {
            data.push(chunk);
        });

        readstream.on('end', () => {
            data = Buffer.concat(data);
            let img = 'data:image/png;base64,' + Buffer(data).toString('base64');
            res.end(img);
            console.log("sent", files[0].filename);
        });

        readstream.on('error', (err) => {
            console.log('An error occurred!', err);
            return res.status(400).send("Bad Request.");
            //throw err;
        });
    });

}))



//////////////////////////////////////////////////////////////////////////////////////////////////////////
// pet food data history
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "",
    "deivice_uuid" : "...",
    "deivice_petkey" : "...",
    .....
	"auth": "4b1a308f29b66779d43075adeb9e93dd"	
}
*/
router.post('/food/:uuid', 
asyncHandler(async (req, res) => {
    try {
        var uuid = req.params.uuid;
        var service_code = req.body.service_code;
        var apikey = req.body.api_key;
        var ext_name = req.body.ext;
        var command = req.body.command;
        var auth = req.body.auth;

        // 관련 장치와 해당 장치에 연결된 pet정보
        var device_uuid = req.body.device_uuid;
        var device_pet_key = req.body.device_pet_key;

        if(uuid==null || uuid=="" || 
           device_pet_key==null || device_pet_key=="" ||
           device_uuid==null || device_uuid==""  ||
           service_code==null || service_code=="" || 
           apikey==null || apikey=="" || 
           ext_name==null || ext_name=="" || 
           command==null || command=="")
        {
            res.status(400).send("Bad Request.[0]");
            return;
        }

        // message 시간 검사
        if(timeCheck==true)
        {
            if(req.body.time!=null)
            {
                msgtime = req.body.time;
                if(waviotUtil.check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        if(ext_name!=extName)
        {
            res.status(400).send("Bad Request.[1]");
            return;
        }

        // client key 유효성 검사 
        apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey })
                .exec(function(err, item) 
        {
            if(err)
            {
                res.status(500).send("System Error."+err)
                return;
            }
            if(item==null)
            {
                res.status(404).send("Not Found [1].")
                return;
            }

            if(ext_name!=extName)
            {
                res.status(400).send("Bad Request.");
                return;
            }

            var secret = item.secret;

            // message auth 검사 
            var pauth = waviotUtil.get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret);
            if(auth!=pauth)
            {
                res.status(401).send("Unauthorized. [2]");
                return;
            }

            switch (command) {
                case "getLastIntakeInfo": 
                    do_get_last_food_intake_info(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;
                case "pushIntakeData": 
                    do_push_food_intake_data(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;
                case "getIntakeData": 
                    do_get_food_intake_data(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;

                    
                case "getLastFeedInfo": 
                    do_get_last_food_feed_info(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;
                case "pushFeedData": 
                    do_push_food_feed_data(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;
                case "getFeedData": 
                    do_get_food_feed_data(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;



                default:
                    res.status(400).send("Bad Request.")
                    break;
            }
        });

    } catch (error) {
        res.status(400).send("Bad Request."+error)
    }    

    
}))


// 최종 갱신 정보와 오늘의 정보가 있을 경우 그 정보를 회신한다. 
// device_uuid / device_pet_key를 기준으롤 정보를 조회한다. 
// 해당  장치와 해당 장치를 바인딩한 client_uuid만 정보를 읽을 수 있다. 
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "getLastIntakeInfo",
    "deivice_uuid" : "...",
    "petkey" : "...",
    .....
    "auth": "4b1a308f29b66779d43075adeb9e93dd",
    "device_manage_key" : "..."
}
*/

async function do_get_last_food_intake_info(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;

    var subcode = 0;
    var findop = {};

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            subcode = 2;
            // 일반 클라이언트일 경우네는 해당 정치를 바인딩하고 있어야 한다. 
            findop = {"service_code":service_code, "client_uuid":client_uuid, "client_apikey":client_apikey, "device_uuid":device_uuid, "device_pet_key": device_pet_key};
            var userPet = await userExtPets.findOne(findop);
            if(userPet==null)
            {
                res.status(401).send("Bad Request[3].");
                return;
            }
        }

        subcode = 3;
        //응답 메시지 구성
        
        let resmesg = {
            // pet basic info
            name : device_pet.name,
            type : device_pet.type,
            gender: device_pet.gender,
            birthday : device_pet.birthday, 
            breed_group: device_pet.breed_group,
            breed: device_pet.breed,
            neutralization : device_pet.neutralization,
            weight: device_pet.weight,
            height: device_pet.height,
            time_zone : device_pet.time_zone,

            // 
            last_food_intake_date : "", 
            last_food_intake_weight : 0,
            last_food_intake_index : 0
        }

        if(device_pet.last_food_intake_date!=null)
        {
            let last_food_intake_date = moment(device_pet.last_food_intake_date);
            resmesg.last_food_intake_date = last_food_intake_date.format("YYYYMMDDZZ").toString();
        }
        
        // 마지막  데이터 조회
        findop = {"service_code":service_code, "device_uuid":device_uuid, "pet_key": device_pet_key, "type":"intake"};
        fields =  {"value":1, "daily_sum":1, "date":1, "last_field_index":1 };
        sorts = {
            sort : {"ts":-1 }
        };
    
        let last_intake_data = await petFoodHistory.findOne(findop, fields,sorts);
        if(last_intake_data==null)
        {
            // 위에 pet db에 있는 값으로 전달
            //resmesg.last_food_intake_date = "";
            //resmesg.last_food_intake_weight = 0;
        }
        else
        {
            resmesg.last_food_intake_date = last_intake_data.date;
            resmesg.last_food_intake_weight = last_intake_data.daily_sum;
            resmesg.last_food_intake_index = last_intake_data.last_field_index;
        }

        res.send(JSON.stringify(resmesg,null,"\t"));
    } catch (error) {
        console.log("Error do_get_last_intake_info : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}


// 음식 섭취 기록을 저장
// 
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "pushIntakeData",
    "deivice_uuid" : "...",
    "petkey" : "...",
    .....
    "auth": "4b1a308f29b66779d43075adeb9e93dd",
    "device_manage_key" : "...",

"data" : [
        { "20181114+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235
                           ] 
       },
       { "20181115+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235
                           ] 
       },
       {
        "20181116+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
                           ]
       }
    ]  
}
*/
async function do_push_food_intake_data(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;
    var time_zone  = req.body.time_zone==null ? "" : req.body.time_zone;
    var intake_data = req.body.data;

    var subcode = 0;
    var findop = {};

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            res.status(401).send("Bad Request[3].");
            return;
        }

        subcode = 3;
        let datacount = 0;
        // Data 읽기
        for( var key in intake_data ) {
            let daily_data = intake_data[key];
            for( var key_date in daily_data ) {
                //console.log( key_date + '=>' + daily_data[key_date] );
                await update_food_intake_history(device_pet, service_code, client_uuid, client_apikey, device_uuid, device_pet_key, device_manage_key, key_date, time_zone, daily_data[key_date])
                datacount+=1;
            }
        }

        let retmsg = {result:"OK", count:datacount};
        res.send(JSON.stringify(retmsg,null,"\t"));
    } catch (error) {
        console.log("Error do_push_intake_data : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}


async function update_food_intake_history(device_pet, service_code, client_uuid, client_apikey, device_uuid, device_pet_key, device_manage_key, history_date, time_zone,  history_data)
{
    var last_field_index = 0; 
    var daily_sum = 0;

    for(let i=0; i<history_data.length; i++)
    {
        if(history_data[i]!=-1)
        {
            last_field_index=i+1;
            daily_sum+=history_data[i];
        }
        else
            history_data[i] = 0;
    }

    var findop = {"service_code" : service_code, "device_uuid" : device_uuid, "device_manage_key" : device_manage_key, "type" : "intake", "date" : history_date}
    var foodIntakeData = await petFoodHistory.findOne(findop);
    var ts_tz = moment(history_date, "YYYYMMDDZZ");
    if(time_zone!="")
    {
        var tz_diff = moment.tz(moment.utc(), time_zone).utcOffset();
        ts_tz.add(-tz_diff, "minute");
    }

    if(foodIntakeData==null)
    {
        var newFoodIntake = new petFoodHistory({
            service_code : service_code,
            device_uuid : device_uuid,
            device_manage_key : device_manage_key ,
            pet_key : device_pet_key,
            type : "intake",
            date : history_date,
            ts : moment(history_date, "YYYYMMDDZZ"), 
            value : history_data,
            last_field_index : last_field_index,
            daily_sum : daily_sum,
            time_zone : time_zone,
            ts_tz : ts_tz,
            udpate_count : 1
        });
        foodIntakeData = await newFoodIntake.save();
    }
    else
    {
        foodIntakeData.value = history_data;
        foodIntakeData.last_field_index = last_field_index;
        foodIntakeData.daily_sum = daily_sum;
        foodIntakeData.last_update_date = Date.now();
        foodIntakeData.udpate_count += 1;
        foodIntakeData.time_zone = time_zone;
        foodIntakeData.ts_tz = ts_tz;

        await foodIntakeData.save();
    }

    // time zone 갱신
    device_pet.time_zone = time_zone;

    // 만일 갱신되는 자료가 동일한 날짜이거나 새로운 날짜이면.. device_pet의 정보를 갱신한다. 
    if (device_pet.last_food_intake_date==null)
    {
        device_pet.last_food_intake_data = daily_sum;
        device_pet.last_food_intake_date = foodIntakeData.ts;    
    }
    else
    {
        let last_update_date = moment(device_pet.last_food_intake_date);
        let new_update_date = moment(foodIntakeData.ts);
        let diffday = new_update_date.diff(last_update_date, "days");

        //console.log("diffday:",diffday, "last_update_date:", last_update_date.format("YYYYMMDD"), "new_update_date:", new_update_date.format("YYYYMMDD"))
        if(diffday>=0)
        {
            device_pet.last_food_intake_data = daily_sum;
            device_pet.last_food_intake_date = foodIntakeData.ts;    
        }
    }

    await device_pet.save();
    
    //console.log(foodIntakeData);
    //console.log("==========================================================================================");
}


async function do_get_food_intake_data(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var query_type = req.body.query_type==null ? "daily" : req.body.query_type;
    var start_date = req.body.start_date==null ? "" : req.body.start_date;
    var end_date = req.body.end_date==null ? "" : req.body.end_date;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;

    var subcode = 0;
    var findop = {};
    var todaydate = moment();

    if(start_date=="") 
        start_date = todaydate.format("YYYYMMDDZZ").toString();
    if(end_date=="") 
        end_date = todaydate.format("YYYYMMDDZZ").toString();

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            subcode = 2;
            // 일반 클라이언트일 경우네는 해당 정치를 바인딩하고 있어야 한다. 
            findop = {"service_code":service_code, "client_uuid":client_uuid, "client_apikey":client_apikey, "device_uuid":device_uuid, "device_pet_key": device_pet_key};
            var userPet = await userExtPets.findOne(findop);
            if(userPet==null)
            {
                res.status(401).send("Bad Request[3].");
                return;
            }

            device_manage_key = device_pet.device_manage_key;
        }

        var start_ts =  moment(start_date, "YYYYMMDDZZ");
        var end_ts =  moment(end_date, "YYYYMMDDZZ");
        if(device_pet.time_zone!=null && device_pet.time_zone!="")
        {
            var tz_diff = moment.tz(moment.utc(), device_pet.time_zone).utcOffset();
            start_ts.add(-tz_diff, "minute");
            end_ts.add(-tz_diff, "minute");
        }         
        var start_dt =  start_ts.toDate();
        var end_dt =  end_ts.toDate();

        subcode = 3; 
        var foodIntakeData = [];
        if(query_type=="daily")
        {
            findop = {
                $and:[
                    {"service_code" : service_code}, 
                    {"device_uuid" : device_uuid},
                    {"device_manage_key" : device_manage_key},
                    {"type" : "intake"},
                    {"ts_tz" : {$gte: start_dt}},
                    {"ts_tz" : {$lte: end_dt}}
                ]
            };
            let fields =  {"date":1, "last_field_index":1, "value":1, "daily_sum":1};
            let sorts = {
                sort : {"ts":-1 }
            };
            //console.log(findop);
            foodIntakeData = await petFoodHistory.find(findop, fields, sorts);
        }
        else if(query_type=="weekly")
        {
            findop = [
                {
                    $match: {
                        "service_code" : service_code, 
                        "device_uuid" : device_uuid, 
                        "device_manage_key" : device_manage_key, 
                        "type" : "intake", 
                        "ts_tz" : {$gte: start_dt, $lte: end_dt}
                    }
                },
                {
                    $group: {
                        _id: {$week: '$ts_tz'},
                        value: {$sum: '$daily_sum'}
                    }
                }
            ];

            //console.log(JSON.stringify(findop,null,"\t"));
            foodIntakeData = await petFoodHistory.aggregate(findop);
        }
        else if(query_type=="monthly")
        {
            findop = [
                {
                    $match: {
                        "service_code" : service_code, 
                        "device_uuid" : device_uuid, 
                        "device_manage_key" : device_manage_key, 
                        "type" : "intake", 
                        "ts_tz" : {$gte: start_dt, $lte: end_dt}
                    }
                },
                {
                    $group: {
                        _id: {$month: '$ts_tz'},
                        value: {$sum: '$daily_sum'}
                    }
                }
            ];

            //console.log(JSON.stringify(findop,null,"\t"));
            foodIntakeData = await petFoodHistory.aggregate(findop);

        }
        else
        {
            res.status(401).send("Bad Request[4].");
            return;            
        }

        if(foodIntakeData==null || foodIntakeData.length==0)
        {
            res.status(404).send("No Data.");
            return;            
        }

        //let retmsg = {result:"OK", count:datacount};
        res.send(JSON.stringify(foodIntakeData,null,"\t"));
    } catch (error) {
        console.log("Error do_get_intake_data : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FOOD FEED HISTORY
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "getLastFeedInfo",
    "deivice_uuid" : "...",
    "petkey" : "...",
    .....
    "auth": "4b1a308f29b66779d43075adeb9e93dd",
    "device_manage_key" : "..."
}
*/

async function do_get_last_food_feed_info(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;

    var subcode = 0;
    var findop = {};

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            subcode = 2;
            // 일반 클라이언트일 경우네는 해당 정치를 바인딩하고 있어야 한다. 
            findop = {"service_code":service_code, "client_uuid":client_uuid, "client_apikey":client_apikey, "device_uuid":device_uuid, "device_pet_key": device_pet_key};
            var userPet = await userExtPets.findOne(findop);
            if(userPet==null)
            {
                res.status(401).send("Bad Request[3].");
                return;
            }
        }

        subcode = 3;
        //응답 메시지 구성
        
        let resmesg = {
            // pet basic info
            name : device_pet.name,
            type : device_pet.type,
            gender: device_pet.gender,
            birthday : device_pet.birthday, 
            breed_group: device_pet.breed_group,
            breed: device_pet.breed,
            neutralization : device_pet.neutralization,
            weight: device_pet.weight,
            height: device_pet.height,
            time_zone : device_pet.time_zone,

            // 
            last_food_feed_date : "", 
            last_food_feed_weight : 0,
            last_food_feed_index : 0
        }

        if(device_pet.last_food_feed_date!=null)
        {
            let last_food_feed_date = moment(device_pet.last_food_feed_date);
            resmesg.last_food_feed_date = last_food_feed_date.format("YYYYMMDDZZ").toString();
        }
        
        // 마지막  데이터 조회
        findop = {"service_code":service_code, "device_uuid":device_uuid, "pet_key": device_pet_key, "type":"feed"};
        fields =  {"value":1, "daily_sum":1, "date":1, "last_field_index":1 };
        sorts = {
            sort : {"ts":-1 }
        };
    
        let last_feed_data = await petFoodHistory.findOne(findop, fields,sorts);
        if(last_feed_data==null)
        {
            // 위에 pet db에 있는 값으로 전달
            //resmesg.last_food_feed_date = "";
            //resmesg.last_food_feed_weight = 0;
        }
        else
        {
            resmesg.last_food_feed_date = last_feed_data.date;
            resmesg.last_food_feed_weight = last_feed_data.daily_sum;
            resmesg.last_food_feed_index = last_feed_data.last_field_index;
        }

        res.send(JSON.stringify(resmesg,null,"\t"));
    } catch (error) {
        console.log("Error do_get_last_food_feed_info : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}


// 음식 피딩 기록을 저장
// 
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "pushFeedData",
    "deivice_uuid" : "...",
    "petkey" : "...",
    .....
    "auth": "4b1a308f29b66779d43075adeb9e93dd",
    "device_manage_key" : "...",

"data" : [
        { "20181114+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235
                           ] 
       },
       { "20181115+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235
                           ] 
       },
       {
        "20181116+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
                           ]
       }
    ]  
}
*/
async function do_push_food_feed_data(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;
    var time_zone  = req.body.time_zone==null ? "" : req.body.time_zone;
    var feed_data = req.body.data;

    var subcode = 0;
    var findop = {};

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            res.status(401).send("Bad Request[3].");
            return;
        }

        subcode = 3;
        let datacount = 0;
        // Data 읽기
        for( var key in feed_data ) {
            let daily_data = feed_data[key];
            for( var key_date in daily_data ) {
                //console.log( key_date + '=>' + daily_data[key_date] );
                await update_food_feed_history(device_pet, service_code, client_uuid, client_apikey, device_uuid, device_pet_key, device_manage_key, key_date, time_zone, daily_data[key_date])
                datacount+=1;
            }
        }

        let retmsg = {result:"OK", count:datacount};
        res.send(JSON.stringify(retmsg,null,"\t"));
    } catch (error) {
        console.log("Error do_push_feed_data : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}


async function update_food_feed_history(device_pet, service_code, client_uuid, client_apikey, device_uuid, device_pet_key, device_manage_key, history_date, time_zone,  history_data)
{
    var last_field_index = 0; 
    var daily_sum = 0;

    for(let i=0; i<history_data.length; i++)
    {
        if(history_data[i]!=-1)
        {
            last_field_index=i+1;
            daily_sum+=history_data[i];
        }
        else
            history_data[i] = 0;
    }

    var findop = {"service_code" : service_code, "device_uuid" : device_uuid, "device_manage_key" : device_manage_key, "type" : "feed", "date" : history_date}
    var foodFeedData = await petFoodHistory.findOne(findop);
    var ts_tz = moment(history_date, "YYYYMMDDZZ");
    if(time_zone!="")
    {
        var tz_diff = moment.tz(moment.utc(), time_zone).utcOffset();
        ts_tz.add(-tz_diff, "minute");
    }

    if(foodFeedData==null)
    {
        var newFoodFeed = new petFoodHistory({
            service_code : service_code,
            device_uuid : device_uuid,
            device_manage_key : device_manage_key ,
            pet_key : device_pet_key,
            type : "feed",
            date : history_date,
            ts : moment(history_date, "YYYYMMDDZZ"), 
            value : history_data,
            last_field_index : last_field_index,
            daily_sum : daily_sum,
            time_zone : time_zone,
            ts_tz : ts_tz,
            udpate_count : 1
        });
        foodFeedData = await newFoodFeed.save();
    }
    else
    {
        foodFeedData.value = history_data;
        foodFeedData.last_field_index = last_field_index;
        foodFeedData.daily_sum = daily_sum;
        foodFeedData.last_update_date = Date.now();
        foodFeedData.udpate_count += 1;
        foodFeedData.time_zone = time_zone;
        foodFeedData.ts_tz = ts_tz;

        await foodFeedData.save();
    }

    // time zone 갱신
    device_pet.time_zone = time_zone;

    // 만일 갱신되는 자료가 동일한 날짜이거나 새로운 날짜이면.. device_pet의 정보를 갱신한다. 
    if (device_pet.last_food_feed_date==null)
    {
        device_pet.last_food_feed_data = daily_sum;
        device_pet.last_food_feed_date = foodFeedData.ts;    
    }
    else
    {
        let last_update_date = moment(device_pet.last_food_feed_date);
        let new_update_date = moment(foodFeedData.ts);
        let diffday = new_update_date.diff(last_update_date, "days");

        //console.log("diffday:",diffday, "last_update_date:", last_update_date.format("YYYYMMDD"), "new_update_date:", new_update_date.format("YYYYMMDD"))
        if(diffday>=0)
        {
            device_pet.last_food_feed_data = daily_sum;
            device_pet.last_food_feed_date = foodFeedData.ts;    
        }
    }

    await device_pet.save();
}


async function do_get_food_feed_data(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var query_type = req.body.query_type==null ? "daily" : req.body.query_type;
    var start_date = req.body.start_date==null ? "" : req.body.start_date;
    var end_date = req.body.end_date==null ? "" : req.body.end_date;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;

    var subcode = 0;
    var findop = {};
    var todaydate = moment();

    if(start_date=="") 
        start_date = todaydate.format("YYYYMMDDZZ").toString();
    if(end_date=="") 
        end_date = todaydate.format("YYYYMMDDZZ").toString();

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            subcode = 2;
            // 일반 클라이언트일 경우네는 해당 정치를 바인딩하고 있어야 한다. 
            findop = {"service_code":service_code, "client_uuid":client_uuid, "client_apikey":client_apikey, "device_uuid":device_uuid, "device_pet_key": device_pet_key};
            var userPet = await userExtPets.findOne(findop);
            if(userPet==null)
            {
                res.status(401).send("Bad Request[3].");
                return;
            }

            device_manage_key = device_pet.device_manage_key;
        }

        var start_ts =  moment(start_date, "YYYYMMDDZZ");
        var end_ts =  moment(end_date, "YYYYMMDDZZ");
        if(device_pet.time_zone!=null && device_pet.time_zone!="")
        {
            var tz_diff = moment.tz(moment.utc(), device_pet.time_zone).utcOffset();
            start_ts.add(-tz_diff, "minute");
            end_ts.add(-tz_diff, "minute");
        }         
        var start_dt =  start_ts.toDate();
        var end_dt =  end_ts.toDate();

        subcode = 3; 
        var foodFeedData = [];
        if(query_type=="daily")
        {
            findop = {
                $and:[
                    {"service_code" : service_code}, 
                    {"device_uuid" : device_uuid},
                    {"device_manage_key" : device_manage_key},
                    {"type" : "feed"},
                    {"ts_tz" : {$gte: start_dt}},
                    {"ts_tz" : {$lte: end_dt}}
                ]
            };
            let fields =  {"date":1, "last_field_index":1, "value":1, "daily_sum":1};
            let sorts = {
                sort : {"ts":-1 }
            };
            //console.log(findop);
            foodFeedData = await petFoodHistory.find(findop, fields, sorts);
        }
        else if(query_type=="weekly")
        {
            findop = [
                {
                    $match: {
                        "service_code" : service_code, 
                        "device_uuid" : device_uuid, 
                        "device_manage_key" : device_manage_key, 
                        "type" : "feed", 
                        "ts_tz" : {$gte: start_dt, $lte: end_dt}
                    }
                },
                {
                    $group: {
                        _id: {$week: '$ts_tz'},
                        value: {$sum: '$daily_sum'}
                    }
                }
            ];

            //console.log(JSON.stringify(findop,null,"\t"));
            foodFeedData = await petFoodHistory.aggregate(findop);
        }
        else if(query_type=="monthly")
        {
            findop = [
                {
                    $match: {
                        "service_code" : service_code, 
                        "device_uuid" : device_uuid, 
                        "device_manage_key" : device_manage_key, 
                        "type" : "feed", 
                        "ts_tz" : {$gte: start_dt, $lte: end_dt}
                    }
                },
                {
                    $group: {
                        _id: {$month: '$ts_tz'},
                        value: {$sum: '$daily_sum'}
                    }
                }
            ];

            //console.log(JSON.stringify(findop,null,"\t"));
            foodFeedData = await petFoodHistory.aggregate(findop);

        }
        else
        {
            res.status(401).send("Bad Request[4].");
            return;            
        }

        if(foodFeedData==null || foodFeedData.length==0)
        {
            res.status(404).send("No Data.");
            return;            
        }

        //let retmsg = {result:"OK", count:datacount};
        res.send(JSON.stringify(foodFeedData,null,"\t"));
    } catch (error) {
        console.log("Error do_get_feed_data : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}












////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WATER HISTORY
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
router.post('/water/:uuid', 
asyncHandler(async (req, res) => {
    try {
        var uuid = req.params.uuid;
        var service_code = req.body.service_code;
        var apikey = req.body.api_key;
        var ext_name = req.body.ext;
        var command = req.body.command;
        var auth = req.body.auth;

        // 관련 장치와 해당 장치에 연결된 pet정보
        var device_uuid = req.body.device_uuid;
        var device_pet_key = req.body.device_pet_key;

        if(uuid==null || uuid=="" || 
           device_pet_key==null || device_pet_key=="" ||
           device_uuid==null || device_uuid==""  ||
           service_code==null || service_code=="" || 
           apikey==null || apikey=="" || 
           ext_name==null || ext_name=="" || 
           command==null || command=="")
        {
            res.status(400).send("Bad Request.[0]");
            return;
        }

        // message 시간 검사
        if(timeCheck==true)
        {
            if(req.body.time!=null)
            {
                msgtime = req.body.time;
                if(waviotUtil.check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        if(ext_name!=extName)
        {
            res.status(400).send("Bad Request.[1]");
            return;
        }

        // client key 유효성 검사 
        apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey })
                .exec(function(err, item) 
        {
            if(err)
            {
                res.status(500).send("System Error."+err)
                return;
            }
            if(item==null)
            {
                res.status(404).send("Not Found [1].")
                return;
            }

            if(ext_name!=extName)
            {
                res.status(400).send("Bad Request.");
                return;
            }

            var secret = item.secret;

            // message auth 검사 
            var pauth = waviotUtil.get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret);
            if(auth!=pauth)
            {
                res.status(401).send("Unauthorized. [2]");
                return;
            }

            switch (command) {
                case "getLastIntakeInfo": 
                    do_get_last_water_intake_info(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;
                case "pushIntakeData": 
                    do_push_water_intake_data(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;
                case "getIntakeData": 
                    do_get_water_intake_data(req, res, service_code, uuid, apikey, device_uuid, device_pet_key);
                    break;
                 
                default:
                    res.status(400).send("Bad Request.")
                    break;
            }
        });

    } catch (error) {
        res.status(400).send("Bad Request."+error)
    }    

    
}))


// 최종 갱신 정보와 오늘의 정보가 있을 경우 그 정보를 회신한다. 
// device_uuid / device_pet_key를 기준으롤 정보를 조회한다. 
// 해당  장치와 해당 장치를 바인딩한 client_uuid만 정보를 읽을 수 있다. 
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "getLastIntakeInfo",
    "deivice_uuid" : "...",
    "petkey" : "...",
    .....
    "auth": "4b1a308f29b66779d43075adeb9e93dd",
    "device_manage_key" : "..."
}
*/

async function do_get_last_water_intake_info(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;

    var subcode = 0;
    var findop = {};

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            subcode = 2;
            // 일반 클라이언트일 경우네는 해당 정치를 바인딩하고 있어야 한다. 
            findop = {"service_code":service_code, "client_uuid":client_uuid, "client_apikey":client_apikey, "device_uuid":device_uuid, "device_pet_key": device_pet_key};
            var userPet = await userExtPets.findOne(findop);
            if(userPet==null)
            {
                res.status(401).send("Bad Request[3].");
                return;
            }
        }

        subcode = 3;
        //응답 메시지 구성
        
        let resmesg = {
            // pet basic info
            name : device_pet.name,
            type : device_pet.type,
            gender: device_pet.gender,
            birthday : device_pet.birthday, 
            breed_group: device_pet.breed_group,
            breed: device_pet.breed,
            neutralization : device_pet.neutralization,
            weight: device_pet.weight,
            height: device_pet.height,
            time_zone : device_pet.time_zone,

            // 
            last_water_intake_date : "", 
            last_water_intake_weight : 0,
            last_water_intake_index : 0
        }

        if(device_pet.last_water_intake_date!=null)
        {
            let last_water_intake_date = moment(device_pet.last_water_intake_date);
            resmesg.last_water_intake_date = last_water_intake_date.format("YYYYMMDDZZ").toString();
        }
        
        // 마지막  데이터 조회
        findop = {"service_code":service_code, "device_uuid":device_uuid, "pet_key": device_pet_key, "type":"intake"};
        fields =  {"value":1, "daily_sum":1, "date":1, "last_field_index":1 };
        sorts = {
            sort : {"ts":-1 }
        };
    
        let last_intake_data = await petWaterHistory.findOne(findop, fields,sorts);
        if(last_intake_data==null)
        {
            // 위에 pet db에 있는 값으로 전달
            //resmesg.last_water_intake_date = "";
            //resmesg.last_water_intake_weight = 0;
        }
        else
        {
            resmesg.last_water_intake_date = last_intake_data.date;
            resmesg.last_water_intake_weight = last_intake_data.daily_sum;
            resmesg.last_water_intake_index = last_intake_data.last_field_index;
        }

        res.send(JSON.stringify(resmesg,null,"\t"));
    } catch (error) {
        console.log("Error do_get_last_water_intake_info : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}


// 물 섭취 기록을 저장
// 
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "pushIntakeData",
    "deivice_uuid" : "...",
    "petkey" : "...",
    .....
    "auth": "4b1a308f29b66779d43075adeb9e93dd",
    "device_manage_key" : "...",

"data" : [
        { "20181114+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235
                           ] 
       },
       { "20181115+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235
                           ] 
       },
       {
        "20181116+0800" : [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 
                           80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 
                           -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
                           ]
       }
    ]  
}
*/
async function do_push_water_intake_data(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;
    var time_zone  = req.body.time_zone==null ? "" : req.body.time_zone;
    var intake_data = req.body.data;

    var subcode = 0;
    var findop = {};

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            res.status(401).send("Bad Request[3].");
            return;
        }

        subcode = 3;
        let datacount = 0;
        // Data 읽기
        for( var key in intake_data ) {
            let daily_data = intake_data[key];
            for( var key_date in daily_data ) {
                //console.log( key_date + '=>' + daily_data[key_date] );
                await update_water_intake_history(device_pet, service_code, client_uuid, client_apikey, device_uuid, device_pet_key, device_manage_key, key_date, time_zone, daily_data[key_date])
                datacount+=1;
            }
        }

        let retmsg = {result:"OK", count:datacount};
        res.send(JSON.stringify(retmsg,null,"\t"));
    } catch (error) {
        console.log("Error do_push_water_intake_data : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}


async function update_water_intake_history(device_pet, service_code, client_uuid, client_apikey, device_uuid, device_pet_key, device_manage_key, history_date, time_zone,  history_data)
{
    var last_field_index = 0; 
    var daily_sum = 0;

    for(let i=0; i<history_data.length; i++)
    {
        if(history_data[i]!=-1)
        {
            last_field_index=i+1;
            daily_sum+=history_data[i];
        }
        else
            history_data[i] = 0;
    }

    var findop = {"service_code" : service_code, "device_uuid" : device_uuid, "device_manage_key" : device_manage_key, "type" : "intake", "date" : history_date}
    var waterIntakeData = await petWaterHistory.findOne(findop);
    var ts_tz = moment(history_date, "YYYYMMDDZZ");
    if(time_zone!="")
    {
        var tz_diff = moment.tz(moment.utc(), time_zone).utcOffset();
        ts_tz.add(-tz_diff, "minute");
    }

    if(waterIntakeData==null)
    {
        var newWaterIntake = new petWaterHistory({
            service_code : service_code,
            device_uuid : device_uuid,
            device_manage_key : device_manage_key ,
            pet_key : device_pet_key,
            type : "intake",
            date : history_date,
            ts : moment(history_date, "YYYYMMDDZZ"), 
            value : history_data,
            last_field_index : last_field_index,
            daily_sum : daily_sum,
            time_zone : time_zone,
            ts_tz : ts_tz,
            udpate_count : 1
        });
        waterIntakeData = await newWaterIntake.save();
    }
    else
    {
        waterIntakeData.value = history_data;
        waterIntakeData.last_field_index = last_field_index;
        waterIntakeData.daily_sum = daily_sum;
        waterIntakeData.last_update_date = Date.now();
        waterIntakeData.udpate_count += 1;
        waterIntakeData.time_zone = time_zone;
        waterIntakeData.ts_tz = ts_tz;

        await waterIntakeData.save();
    }

    // time zone 갱신
    device_pet.time_zone = time_zone;

    // 만일 갱신되는 자료가 동일한 날짜이거나 새로운 날짜이면.. device_pet의 정보를 갱신한다. 
    if (device_pet.last_water_intake_date==null)
    {
        device_pet.last_water_intake_data = daily_sum;
        device_pet.last_water_intake_date = waterIntakeData.ts;    
    }
    else
    {
        let last_update_date = moment(device_pet.last_water_intake_date);
        let new_update_date = moment(waterIntakeData.ts);
        let diffday = new_update_date.diff(last_update_date, "days");

        //console.log("diffday:",diffday, "last_update_date:", last_update_date.format("YYYYMMDD"), "new_update_date:", new_update_date.format("YYYYMMDD"))
        if(diffday>=0)
        {
            device_pet.last_water_intake_data = daily_sum;
            device_pet.last_water_intake_date = waterIntakeData.ts;    
        }
    }

    await device_pet.save();   
}


async function do_get_water_intake_data(req, res, service_code, client_uuid, client_apikey, device_uuid, device_pet_key)
{
    var msgtime = req.body.time;
    var query_type = req.body.query_type==null ? "daily" : req.body.query_type;
    var start_date = req.body.start_date==null ? "" : req.body.start_date;
    var end_date = req.body.end_date==null ? "" : req.body.end_date;
    var device_manage_key = req.body.device_manage_key==null ? "" : req.body.device_manage_key;
    var device_type = req.body.device_type==null ? "" : req.body.device_type;

    var subcode = 0;
    var findop = {};
    var todaydate = moment();

    if(start_date=="") 
        start_date = todaydate.format("YYYYMMDDZZ").toString();
    if(end_date=="") 
        end_date = todaydate.format("YYYYMMDDZZ").toString();

    try {
        findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
        //pet정보를 조회
        subcode = 1;
        let device_pet = await pets.findOne(findop);
        if(device_pet==null)
        {
            res.status(404).send("Invalid Device info");
            return;
        }

        if(client_uuid==device_uuid)
        {
            // 장치가 조회를 할 경우에는 device_manage_key가 존재해야하고 동일해야 한다. 
            if(device_manage_key=="" || device_manage_key!=device_pet.device_manage_key)
            {
                res.status(401).send("Bad Request[2].");
                return;
            }
            subcode = 11;
        }
        else
        {
            subcode = 2;
            // 일반 클라이언트일 경우네는 해당 정치를 바인딩하고 있어야 한다. 
            findop = {"service_code":service_code, "client_uuid":client_uuid, "client_apikey":client_apikey, "device_uuid":device_uuid, "device_pet_key": device_pet_key};
            var userPet = await userExtPets.findOne(findop);
            if(userPet==null)
            {
                res.status(401).send("Bad Request[3].");
                return;
            }
            device_manage_key = device_pet.device_manage_key;
        }

        var start_ts =  moment(start_date, "YYYYMMDDZZ");
        var end_ts =  moment(end_date, "YYYYMMDDZZ");
        if(device_pet.time_zone!=null && device_pet.time_zone!="")
        {
            var tz_diff = moment.tz(moment.utc(), device_pet.time_zone).utcOffset();
            start_ts.add(-tz_diff, "minute");
            end_ts.add(-tz_diff, "minute");
        }         
        var start_dt =  start_ts.toDate();
        var end_dt =  end_ts.toDate();

        subcode = 3; 
        var waterIntakeData = [];
        if(query_type=="daily")
        {
            findop = {
                $and:[
                    {"service_code" : service_code}, 
                    {"device_uuid" : device_uuid},
                    {"device_manage_key" : device_manage_key},
                    {"type" : "intake"},
                    {"ts_tz" : {$gte: start_dt}},
                    {"ts_tz" : {$lte: end_dt}}
                ]
            };
            let fields =  {"date":1, "last_field_index":1, "value":1, "daily_sum":1};
            let sorts = {
                sort : {"ts":-1 }
            };
            //console.log(findop);
            waterIntakeData = await petWaterHistory.find(findop, fields, sorts);
        }
        else if(query_type=="weekly")
        {
            findop = [
                {
                    $match: {
                        "service_code" : service_code, 
                        "device_uuid" : device_uuid, 
                        "device_manage_key" : device_manage_key, 
                        "type" : "intake", 
                        "ts_tz" : {$gte: start_dt, $lte: end_dt}
                    }
                },
                {
                    $group: {
                        _id: {$week: '$ts_tz'},
                        value: {$sum: '$daily_sum'}
                    }
                }
            ];

            //console.log(JSON.stringify(findop,null,"\t"));
            waterIntakeData = await petWaterHistory.aggregate(findop);
        }
        else if(query_type=="monthly")
        {
            findop = [
                {
                    $match: {
                        "service_code" : service_code, 
                        "device_uuid" : device_uuid, 
                        "device_manage_key" : device_manage_key, 
                        "type" : "intake", 
                        "ts_tz" : {$gte: start_dt, $lte: end_dt}
                    }
                },
                {
                    $group: {
                        _id: {$month: '$ts_tz'},
                        value: {$sum: '$daily_sum'}
                    }
                }
            ];

            //console.log(JSON.stringify(findop,null,"\t"));
            waterIntakeData = await petWaterHistory.aggregate(findop);
        }
        else
        {
            res.status(401).send("Bad Request[4].");
            return;            
        }

        if(waterIntakeData==null || waterIntakeData.length==0)
        {
            res.status(404).send("No Data.");
            return;            
        }

        //let retmsg = {result:"OK", count:datacount};
        res.send(JSON.stringify(waterIntakeData,null,"\t"));
    } catch (error) {
        console.log("Error do_get_last_intake_info : ", error)        
        res.status(500).send("System Error. subcode:"+subcode.toString());

    }
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pet Store 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const ctrlPetStore = require('../controllers/petStore')
router.put('/store/:uuid', asyncHandler(ctrlPetStore.putNewStore));
router.post('/store/:uuid', asyncHandler(ctrlPetStore.postPetStore));
router.post('/store/pic/:picuuid', 
busboy({
    limits: {
        fileSize: 10 * 1024 * 1024
    }
}),
asyncHandler(ctrlPetStore.postUploadPetStoreImage));
router.get('/store/pic/:picuuid', asyncHandler(ctrlPetStore.getPetStoreImage));


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pet Media 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const ctrlPetMedia = require('../controllers/petMedia')
router.put('/media/:uuid', asyncHandler(ctrlPetMedia.putNewMedia));
router.post('/media/:uuid', asyncHandler(ctrlPetMedia.postPetMedia));
router.post('/media/pic/:picuuid', 
busboy({
    limits: {
        fileSize: 10 * 1024 * 1024
    }
}),
asyncHandler(ctrlPetMedia.postUploadPetStoreImage));
router.get('/media/pic/:picuuid', asyncHandler(ctrlPetMedia.getPetStoreImage));


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Healthy
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// pet  management interface
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "",
    .....
	"auth": "4b1a308f29b66779d43075adeb9e93dd"	
}
*/
router.post('/health/:uuid', asyncHandler(async (req, res) => {
    try {
        var uuid = req.params.uuid;
        var service_code = req.body.service_code;
        var apikey = req.body.api_key;
        var ext_name = req.body.ext;
        var command = req.body.command;
        var auth = req.body.auth;

        if(uuid==null || uuid=="" ||
           service_code==null || service_code=="" || 
           apikey==null || apikey=="" || 
           ext_name==null || ext_name=="" || 
           command==null || command=="")
        {
            res.status(400).send("Bad Request.[0]");
            return;
        }

        // message 시간 검사
        if(timeCheck==true)
        {
            if(req.body.time!=null)
            {
                msgtime = req.body.time;
                if(waviotUtil.check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        if(ext_name!=extName)
        {
            res.status(400).send("Bad Request.[1]");
            return;
        }

        // client key 유효성 검사 
        apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey })
                .exec(function(err, item) 
        {
            if(err)
            {
                res.status(500).send("System Error."+err)
                return;
            }
            if(item==null)
            {
                res.status(404).send("Not Found [1].")
                return;
            }

            if(ext_name!=extName)
            {
                res.status(400).send("Bad Request.");
                return;
            }

            var secret = item.secret;

            // message auth 검사 
            var pauth = waviotUtil.get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret);
            console.log("pauth === " + pauth);
            if(auth!=pauth)
            {
                res.status(401).send("Unauthorized. [2]");
                return;
            }

            switch (command) {
                case "create": 
                    //do_reg_pet(req, res, service_code, uuid, apikey);
                    break;
                case "read": 
                    console.log("get health feeding advise");
                    do_get_health(req, res, service_code, uuid, apikey);
                    break;
                default:
                    res.status(400).send("Bad Request.")
                    break;
            }
        });

    } catch (error) {
        res.status(400).send("Bad Request."+error);
    }    
}));

async function do_get_health(req, res, service_code, uuid, api_key)
{
    var sex = req.body.sex;
    var age = req.body.age;
    var weight = req.body.weight;
    var petType = req.body.petType;
    var category = req.body.category;
    var foodType = req.body.foodType;
    var physiologyStatus = req.body.physiologyStatus;
    var feedPlan = req.body.feedPlan;
    var msgtime = req.body.time;
    var client_uuid = uuid;

    var request_validation = true;
    var retmsg;
    var planData;
    var datacount;

    logger.info("REQ:: module:%s, command:%s, msgtime:%s, client_uuid:%s, category:%s", 
                log_module, "READHEALTH", msgtime, client_uuid, category);

    if (category == "") {
        request_validation = false;
    }

    if(request_validation==false)
    {
        res.status(400).send("Bad Request.[1]");
        logger.info("RES:: module:%s, command:%s, msgtime:%s, client_uuid:%s, category:%s, BED REQUEST!", 
                    log_module, "READHEALTH", msgtime, client_uuid, category);
        return;
    }

    try {

        let healthData = await petHealth.findOne({"name": petType});
        let calorieData = await calorie.findOne({"food": foodType});
        console.log("calorie.food.foodType == " + calorieData.value);
        console.log("calorie.name == " + calorieData.name);
        
        console.log("healthData.feedPlan.keep == " + healthData.feedPlan.keep);
        if (feedPlan == "keep") {
            planData = healthData.feedPlan.keep;
            console.log("planData == " + planData);
        } else if (feedPlan == "desc") {
            planData = healthData.feedPlan.desc;
        } else if (feedPlan == "incr") {
            planData = healthData.feedPlan.desc;
        }

        datacount = Math.sqrt(Math.sqrt(weight*weight*weight)) * planData*100;
        retmsg = {result:"OK", calories:datacount, recommendation:(datacount/calorieData.value)};
        res.send(JSON.stringify(retmsg,null,"\t"));
    } catch (error) {
        res.status(400).send("Bad Request."+error);
    }
}

module.exports = router;
