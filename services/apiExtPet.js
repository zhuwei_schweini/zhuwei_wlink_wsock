// POST : Extention PET management interface
//     hmac_md5(time+api_key+service_code+ext_name+command, secret).
//     Hex String Format
// 

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');
var crypto = require('crypto');

var waviotUtil = require("../modules/waviotUtil");
var apiClients = require('../models/apiClients');
var pets = require('../models/pets');
var users = require('../models/users');
var userExtPets = require('../models/userExtPets');
var devices =require('../models/devices');
var busboy = require('connect-busboy');
//const asyncHandler = require('express-async-handler')

var timeCheck = true;
var timeLimit = 15; // 15 minutes
var extName = "ExtPetsV1"

// today - expired
// over 0  : expired 
function check_msg_expired(msgtime, limit_time)
{
    var _msgtime = moment(msgtime, "YYYYMMDDHHmmssZZ");    
    var expired = moment();
    expired.add(-limit_time, "minute");

    var diff =  expired.diff(_msgtime, 'minutes');

    //console.log(expired.format('YYYYMMDDHHmmssZZ')+" DIFF: " + diff);

    if(diff<(-limit_time))
        return -diff;

    else 
        return diff;
}


// auth 검사
function get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret)
{
    var plain = msgtime+apikey+service_code+ext_name+command;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");
}

function clear_user_device_det_info(service_code, user_key, device_uuid, new_userExtPet_id)
{
    var conditions = {
        service_code:service_code,
        user_key:user_key, 
        device_uuid:device_uuid, 
        _id:{$ne:mongoose.Types.ObjectId(new_userExtPet_id)}        
    };
    var fieldsToSet = {
        device_uuid:"", device_apikey:"", device_pet_key:""
    };
    var options = {"multi":true};

    userExtPets.update(
        conditions,
        fieldsToSet,
        options,
        function(err,result) {
            if(err)
                console.log("cannot clear_user_device_det_info :"+err);
            else
                console.log("clear_user_device_det_info : count - " + result.nModified);
        }
     );
}

function update_user_pets(service_code, client_uuid, client_apikey, user_key, user_login_token, pet_key, pet_name,
    device_uuid,
    device_apikey,
    device_pet_key
    )
{
    if(service_code=="" || client_uuid=="" || client_apikey=="" || user_key=="" || user_login_token=="" || pet_key=="" || pet_name=="")
        return false;

    // user_key 유효성 검사
    users.findOne(
        {
            "$and": [
                {"service_code": service_code}, 
                {"_id":mongoose.Types.ObjectId(user_key)}
            ]        
        }
    )
    .exec(function(err, user) 
    {
        if(err)
            return false;
        if(user==null)
            return false;
        if(user_login_token!=user.login_token)
            return false;
        if(waviotUtil.check_login_token_expired(user.login_token_expired))
        {
            console.log("login_token_expired");
            return false;
        }
        userExtPets.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"user_key": user_key},
                    {"pet_key": pet_key}
                ]        
            }
        )
        .exec(function(err, userPet) 
        {
            if(err)
                return false;
            if(userPet==null)
            {
                // 신규 정보
               // register new 
                var newUserPet = new userExtPets(
                    {
                        service_code: service_code, 
                        user_key : user_key,
                        pet_key: pet_key,
                        pet_name: pet_name,
                        ref_count: 1,
                        client_uuid: client_uuid,  
                        client_apikey: client_apikey,
                        last_client_uuid: client_uuid,  
                        last_client_apikey: client_apikey,
                        device_uuid: device_uuid,
                        device_apikey: device_apikey,
                        device_pet_key : device_pet_key
                    }
                )
                userExtPets.findOneAndUpdate(
                    {service_code: service_code, user_key: user_key, pet_key:pet_key}, 
                    newUserPet, 
                    {upsert: true, new: true, runValidators: true}, // options
                    function (err, update_user_pet) { // callback
                        if(err)
                            return false;

                    // 해당 사용자의 다른 pet정보에 장치 정보가 할당되어 있으면, 정보를 지운다
                    if(device_uuid!="" && update_user_pet!=null)
                        {
                            clear_user_device_det_info(service_code, user_key, device_uuid, update_user_pet._id.toHexString());
                        }
                                    
                        return true;
                    }
                ); 

            }
            else
            {
                // 정보 변경
                userPet.pet_name = pet_name;
                userPet.last_updated_time = moment();
                userPet.last_client_uuid = client_uuid;
                userPet.last_client_apikey = client_apikey;
                userPet.device_uuid = device_uuid;
                userPet.device_apikey = device_apikey;
                userPet.device_pet_key = device_pet_key;
                //userPet.ref_count++;
                userPet.save(function(err, updateitem) {
                    if(err)
                        return false;

                    // 해당 사용자의 다른 pet정보에 장치 정보가 할당되어 있으면, 정보를 지운다
                    if(device_uuid!="" && updateitem!=null)
                    {
                        clear_user_device_det_info(service_code, user_key, device_uuid, updateitem._id.toHexString());
                    }                        
                    return true;                    
                });
                
            }
         
        });   
    });   
    return true;
}


function del_user_pets(service_code, user_key, user_login_token, pet_key)
{

    if(service_code=="" || user_key=="" || login_token=="" || pet_key=="")
        return false;

    // user_key 유효성 검사
    users.findOne(
        {
            "$and": [
                {"service_code": service_code}, 
                {"_id":mongoose.Types.ObjectId(user_key)}
            ]        
        }
    )
    .exec(function(err, user) 
    {
        if(err)
            return false;
        if(user==null)
            return false;
        if(user_login_token!=user.login_token)
            return false;
        if(waviotUtil.check_login_token_expired(user.login_token_expired))
        {
            return false;
        }

        userExtPets.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"user_key": user_key},
                    {"pet_key": pet_key}
                ]        
            }
        )
        .exec(function(err, userPet) 
        {
            if(err!=null || userPet==null)
                return false;
            userPet.remove(function (err) {
                if(err!=null) return false;
            });
/*
            // 만일  참조횟수 기능을 넣게되면. 
            if(userPet.ref_count==1)
            {
                userPet.remove(function (err) {
                    if(err!=null) return false;
                });
            }
            else
            {
                userPet.ref_count--;
                userPet.save(function(err, updateitem) {
                    if(err)
                        return false;
                    return true;                    
                });                
            }
*/ 

        });
    });   
    return true;
}


// new pet info
function create_pet_info(res, msgtime, service_code, client_uuid, client_apikey, 
        user_key, user_login_token,  // 옵션 값 
        pet_name, pet_type, pet_gender, birthday, 
        breed_group, breed, neutralization,
        weight, height, 
        body_height, 
        length, back_length, 
        width,neck_length,
        girth_length,
        device_uuid,
        device_apikey,
        device_pet_key
    )
{
    // pet정보 특정상 중복 검사는 없음. 
    // register new 
    var pic_random_id = "petpic_"+waviotUtil.gen_session_key(client_uuid+pet_name);
    var newpet = new pets(
        {
            service_code: service_code, 
            name: pet_name,
            type: pet_type, // cat, dog    
            gender: pet_gender,  // male, female
            birthday : birthday,
            breed_group: breed_group,
            breed: breed,
            neutralization : neutralization,
            weight: weight,
            height: height,
            body_height: body_height, 
            length: length,
            back_length: back_length,
            width: width, 
            neck_length: neck_length,
            girth_length: girth_length,
            pic_id: pic_random_id,
            pic_key : "",
            client_uuid: client_uuid,
            client_apikey: client_apikey
        }
    )

    newpet.save(function(err, item){
        if(err)
        {
            res.status(500).send("System Error.");
        }
        else
        {
            res.send(
                {
                    service_code: service_code, 
                    pet_key:item._id.toHexString(),
                    pic_id: item.pic_id,
                    api_key: client_apikey,
                    device_uuid : device_uuid,
                    device_apikey : device_apikey,
                    device_pet_key: device_pet_key        
                }
            );       

            // user정보가 존재할 경우. 그 정보를 기록/갱신한다. 
            if(user_key!=null && user_key!="" && user_login_token!=null && user_login_token!="")
            {
                update_user_pets(service_code, client_uuid, client_apikey, 
                        user_key, user_login_token, item._id.toHexString(), 
                        item.name,
                        device_uuid,
                        device_apikey,
                        device_pet_key);
            }
        }
    });

}


function update_device_ref_pet_info(
    service_code, device_pet_key, except_user_pet_key, 
    pet_name, pet_type, pet_gender, birthday, 
    breed_group, breed, neutralization,
    weight, height, 
    body_height, 
    length, back_length, 
    width,neck_length,
    girth_length
)
{
    // device_pet_key를 가지고 있는 사용자 pet정보를 검색
    query = {"service_code": service_code, "device_pet_key":device_pet_key};
    fields =  {"service_code":1, "user_key":1, "pet_key":1, "pet_name":1, "_id":0, 
    "device_uuid" : 1,
    "device_apikey" : 1,
    "device_pet_key" : 1};
    sorts = {
        sort : {"last_updated_time":-1 }
    };

    //console.log(query, except_user_pet_key);
    userExtPets.find(query,fields,sorts,
    function(error,userpets){
        if(userpets.length>0)
        {
            userpets.forEach(refpet => {
                if(except_user_pet_key!=refpet.pet_key) 
                {
                    pets.findOne(
                        {
                            "$and": [
                                {"_id": mongoose.Types.ObjectId(refpet.pet_key)}, 
                                {"service_code": service_code} 
                            ]        
                        })
                        .exec(function(err, pet) 
                        {
                            //console.log("update pet info ", refpet.pet_key)
                            if(pet!=null)
                            {
                                pet.name = pet_name;
                                pet.type = pet_type;
                                pet.gender = pet_gender;
                                pet.birthday = birthday;
                                pet.breed_group = breed_group;
                                pet.breed = breed;
                                pet.neutralization = neutralization;
                                pet.weight = weight;
                                pet.height = height;
                                pet.body_height = body_height;
                                pet.length = length;
                                pet.back_length = back_length;
                                pet.width = width;
                                pet.neck_length = neck_length;
                                pet.girth_length = girth_length;            
                                pet.last_updated_time = moment();
                    
                                pet.save();
                            }
                        });
                }
                else
                    console.log("SKIPPED update pet info ", refpet.pet_key)
            });
        }
    });    

}


function update_pet_info(res, msgtime, service_code, client_uuid, client_apikey, 
    pet_key, user_key, user_login_token,
    pet_name, pet_type, pet_gender, birthday, 
    breed_group, breed, neutralization,
    weight, height, 
    body_height, 
    length, back_length, 
    width,neck_length,
    girth_length,
    device_uuid,
    device_apikey,
    device_pet_key
)
{
    pets.findOne(
    {
        "$and": [
            {"_id": mongoose.Types.ObjectId(pet_key)}, 
            {"service_code": service_code} 
        ]        
    })
    .exec(function(err, pet) 
    {
        if(err)
        {
            res.status(500).send("System Error."+err)
            return;
        }

        if(pet==null)
        {
            res.status(400).send("Bad Request.")
            return;
        }
        else
        {
            pet.name = pet_name;
            pet.type = pet_type;
            pet.gender = pet_gender;
            pet.birthday = birthday;
            pet.breed_group = breed_group;
            pet.breed = breed;
            pet.neutralization = neutralization;
            pet.weight = weight;
            pet.height = height;
            pet.body_height = body_height;
            pet.length = length;
            pet.back_length = back_length;
            pet.width = width;
            pet.neck_length = neck_length;
            pet.girth_length = girth_length;            
            pet.last_client_uuid = client_uuid;
            pet.last_client_apikey = client_apikey;
            pet.last_updated_time = moment();

            pet.save(function(err, updatepet) {
                if(err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send(
                        {
                            service_code: service_code, 
                            pet_key:updatepet._id.toHexString(),
                            pic_id: updatepet.pic_id,
                            apikey: client_apikey, 
                            device_uuid : device_uuid,
                            device_apikey : device_apikey,
                            device_pet_key : device_pet_key
                        }
                    );
                    if(user_key!=null && user_key!="" && user_login_token!=null && user_login_token!="")
                    {
                        update_user_pets(service_code, client_uuid, client_apikey, 
                                user_key, user_login_token, updatepet._id.toHexString(), 
                                updatepet.name,
                                device_uuid,
                                device_apikey,
                                device_pet_key);
                    }


                    // device에서 pet정보를 갱신했을 경우, 연결되어 있는 사용자의 pet정보도 갱신한다. 
                    if(device_pet_key=="")
                    {
                        // device_pet_key가 없는 경우는 장치이거나 사용자가 바인딩하지 않은 경보
                        update_device_ref_pet_info(
                            service_code, pet._id.toHexString(), "",
                            pet_name, pet_type, pet_gender, birthday, 
                            breed_group, breed, neutralization,
                            weight, height, 
                            body_height, 
                            length, back_length, 
                            width,neck_length,
                            girth_length
                        )
   
                    }
                    else
                    {
                        update_device_ref_pet_info(
                            service_code, device_pet_key, pet._id.toHexString(), 
                            pet_name, pet_type, pet_gender, birthday, 
                            breed_group, breed, neutralization,
                            weight, height, 
                            body_height, 
                            length, back_length, 
                            width,neck_length,
                            girth_length
                        )
   
                    }

                }
            });

        }
    });
}

function update_device_pet_info(
    device_petinfo, 
    pet_name, pet_type, pet_gender, birthday, 
    breed_group, breed, neutralization,
    weight, height, 
    body_height, 
    length, back_length, 
    width,neck_length,
    girth_length
)
{
    device_petinfo.name = pet_name;
    device_petinfo.type = pet_type;
    device_petinfo.gender = pet_gender;
    device_petinfo.birthday = birthday;
    device_petinfo.breed_group = breed_group;
    device_petinfo.breed = breed;
    device_petinfo.neutralization = neutralization;
    device_petinfo.weight = weight;
    device_petinfo.height = height;
    device_petinfo.body_height = body_height;
    device_petinfo.length = length;
    device_petinfo.back_length = back_length;
    device_petinfo.width = width;
    device_petinfo.neck_length = neck_length;
    device_petinfo.girth_length = girth_length;            
    device_petinfo.last_updated_time = moment();

    device_petinfo.save(function(err, updatepet) 
    {
         // nothing to do..
    });
}

function del_pet_info(res, msgtime, service_code, client_uuid, client_apikey, 
        pet_key, user_key, user_login_token)
{
    pets.findOne(
            {
                "$and": [
                    {"_id": mongoose.Types.ObjectId(pet_key)}, 
                    {"service_code": service_code} 
                ]        
            }
        )
        .exec(function(err, item) {
        if(err)
            res.status(400).send(err);
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
            {
                pic_id = item.pic_id;
                pic_key = item.pic_key;
                
                item.remove(function (err) {
                    if(err)
                        res.status(500).send("System Error.");
                    else
                        res.send("Success");

                    if(user_key!=null && user_key!="" && user_login_token!=null && user_login_token!="")
                        del_user_pets(service_code, user_key, user_login_token, pet_key);

                    if(pic_id!="" && pic_key!="")
                    {
                        // picture정보가 있으면 gridfs에서 삭제하기
                        gfs.remove({ filename: pic_id}, 
                            function (err) {
                            }
                        );

                    }
                });
            }
        }
    });
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/*

{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "create",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //compulsory
    "pet_name" : "dogname",
    "pet_type" : "dog".       // lower case
    "pet_gender" : "male"    // lower case

    // optional
    "birthday" : "20170601", 
    "breed_group": "",
    "breed" : "",
    "neutralization" : false,
    "weight": 18.9,  //kg
    "height": 30.1,  //cm
    "body_height": 0, //cm
    "length": 0, //cm
    "back_length": 0,//cm
    "width": 0, //cm
    "neck_length": 0,//cm
    "girth_length": 0,    //cm
    
    // the followings are optional values if has login user information. 
    "user_key": "123121231231323",
    "login_token" : "12123123123",
    "device_uuid" : "device_uuid1",
    "device_pet_key" : "device_uuid1"
}
*/
function do_reg_pet(req, res, service_code, client_uuid, client_apikey)
{
    msgtime = req.body.time;
    pet_name = req.body.name==null ? "" : req.body.name;
    pet_type = req.body.type==null ? "" : req.body.type.toString().toLowerCase();
    pet_gender = req.body.gender==null ?  "": req.body.gender.toString().toLowerCase();
    birthday = req.body.birthday==null ? "" : req.body.birthday;

    breed_group = req.body.breed_group==null ? "" : req.body.breed_group;
    breed = req.body.breed==null ? "" : req.body.breed;
    neutralization = req.body.neutralization==null ? false : req.body.neutralization;

    weight = req.body.weight==null ? 0.0 : req.body.weight;
    height = req.body.height==null ? 0.0 : req.body.height;
    body_height = req.body.body_height==null ? 0.0 : req.body.body_height;
    length = req.body.length==null ? 0.0 : req.body.length;
    back_length = req.body.back_length==null ? 0.0 : req.body.back_length;
    width = req.body.width==null ? 0.0 : req.body.width;
    neck_length = req.body.neck_length==null ? 0.0 : req.body.neck_length;
    girth_length = req.body.girth_length==null ? 0.0 : req.body.girth_length;
    device_uuid = req.body.device_uuid==null ? "" : req.body.device_uuid;
    device_pet_key = req.body.device_pet_key==null ? "" : req.body.device_pet_key;
    user_key  = req.body.user_key==null ? "" : req.body.user_key;
    login_token  = req.body.login_token==null ? "" : req.body.login_token;

    device_key = req.body.device_key==null ? "" : req.body.device_key;
    device_type = req.body.device_type==null ? "0" : req.body.device_type;

    var request_validation = true;
    
    // it is required valiees
    if(pet_name=="")
    {
        request_validation = false;
    }

    if(pet_type!="dog" && pet_type!="cat")
    {
        request_validation = false;
    }

    if(pet_gender!="male" && pet_gender!="female")
    {
        request_validation = false;
    }

    if(request_validation==false)
    {
        res.status(400).send("Bad Request.[1]");
        return;
    }
    else
    {
        // device_uuid가 존재하지 않으면 독립적인 pet정보를 생성한다. 
        if(device_uuid=="")
        {
            create_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                user_key, login_token, 
                pet_name, pet_type, pet_gender, birthday,
                breed_group, breed, neutralization, 
                weight, height, body_height, length, back_length, 
                width, neck_length, girth_length, 
                device_key, "0",
                "", "", "");
        }
        else
        {
            // device_uuid가 존재하면, device가 가지고 있는 pet정보를 조회한다.
            var findop = {};

            if(device_pet_key=="")
                findop = {"service_code": service_code, "client_uuid":device_uuid};
            else
                findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};
            pets.findOne(findop)
            .exec(function(err, device_pet) 
            {
                if(err!=null)
                {
                    res.status(500).send("System Error. sub code [0]");
                    return;                    
                } 
                
                if(device_pet==null)
                {
                    // device pet 정보 생성
                    // device 정보 검색 
                    findop = {"service_code": service_code, "uuid":device_uuid};
                    devices.findOne(findop)
                    .exec(function(device_err, device_info) 
                    {
                        if(device_err!=null || device_info==null)
                        {
                            res.status(404).send("Invalid Device info");
                            return;                    
                        }
                        device_apikey = device_info.client_key;

                        // device pet 정보 생성
                        {
                            // register new device pet 
                            var pic_random_id = "petpic_"+waviotUtil.gen_session_key(device_uuid+pet_name);
                            var newdevicepet = new pets(
                                {
                                    service_code: service_code, 
                                    name: pet_name,
                                    type: pet_type, // cat, dog    
                                    gender: pet_gender,  // male, female
                                    birthday : birthday,
                                    breed_group: breed_group,
                                    breed: breed,
                                    neutralization : neutralization,
                                    weight: weight,
                                    height: height,
                                    body_height: body_height, 
                                    length: length,
                                    back_length: back_length,
                                    width: width, 
                                    neck_length: neck_length,
                                    girth_length: girth_length,
                                    pic_id: pic_random_id,
                                    pic_key : "",
                                    device_key : device_key,
                                    device_type : device_type,
                                    client_uuid: device_uuid,
                                    client_apikey: device_apikey
                                }
                            )

                            newdevicepet.save(function(err, item){
                                if(err)
                                {
                                    res.status(500).send("System Error.");
                                }
                                else
                                {   
                                    device_pet_key = item._id.toHexString();
                                    create_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                                        user_key, login_token, 
                                        pet_name, pet_type, pet_gender, birthday,
                                        breed_group, breed, neutralization, 
                                        weight, height, body_height, length, back_length, 
                                        width, neck_length, girth_length, 
                                        device_key, "0",
                                        device_uuid, device_apikey, device_pet_key);
                                }
                            });
                        }
                    });
                }
                else
                {
                    device_apikey = device_pet.client_apikey;
                    device_pet_key  = device_pet._id.toHexString();
                    create_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                        user_key, login_token, 
                        pet_name, pet_type, pet_gender, birthday,
                        breed_group, breed, neutralization, 
                        weight, height, body_height, length, back_length, 
                        width, neck_length, girth_length, 
                        device_key, "0",
                        device_uuid, device_apikey, device_pet_key);

                    // device  pet 정보도 갱신
                    update_device_pet_info(device_pet,
                        pet_name, pet_type, pet_gender, birthday,
                        breed_group, breed, neutralization, 
                        weight, height, body_height, length, back_length, 
                        width, neck_length, girth_length
                    );
                }
            });   

        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "update",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //compulsory
    "pet_key" : "5987fe0580a94c6077562c36b8c65442",
    "pet_name" : "dogname",
    "pet_type" : "dog".       // lower case
    "pet_gender" : "male"    // lower case

    // optional
    "birthday" : "20170601", 
    "breed_group": "",
    "breed" : "",
    "neutralization" : false,
    "weight": 18.9,  //kg
    "height": 30.1,  //cm
    "body_height": 0, //cm
    "length": 0, //cm
    "back_length": 0, //cm
    "width": 0, //cm
    "neck_length": 0, //cm
    "girth_length": 0,    //cm
    
    // the followings are optional values if has login user information. 
    "user_key": "123121231231323",
    "login_token" : "12123123123",
    "device_uuid" : "device_uuid1",
    "device_pet_key" : "device_pet_key"
*/
function do_update_pet(req, res, service_code, client_uuid, client_apikey)
{
    msgtime = req.body.time;
    pet_key = req.body.pet_key==null ? "" : req.body.pet_key;
    pet_name = req.body.name==null ? "" : req.body.name;
    pet_type = req.body.type==null ? "" : req.body.type.toString().toLowerCase();
    pet_gender = req.body.gender==null ?  "": req.body.gender.toString().toLowerCase();
    birthday = req.body.birthday==null ? "" : req.body.birthday;

    breed_group = req.body.breed_group==null ? "" : req.body.breed_group;
    breed = req.body.breed==null ? "" : req.body.breed;
    neutralization = req.body.neutralization==null ? false : req.body.neutralization;

    weight = req.body.weight==null ? 0.0 : req.body.weight;
    height = req.body.height==null ? 0.0 : req.body.height;
    body_height = req.body.body_height==null ? 0.0 : req.body.body_height;
    length = req.body.length==null ? 0.0 : req.body.length;
    back_length = req.body.back_length==null ? 0.0 : req.body.back_length;
    width = req.body.width==null ? 0.0 : req.body.width;
    neck_length = req.body.neck_length==null ? 0.0 : req.body.neck_length;
    girth_length = req.body.girth_length==null ? 0.0 : req.body.girth_length;
    device_pet_key = req.body.device_pet_key==null ? "" : req.body.device_pet_key;
    device_uuid = req.body.device_uuid==null ? "" : req.body.device_uuid;

    user_key  = req.body.user_key==null ? "" : req.body.user_key;
    login_token  = req.body.login_token==null ? "" : req.body.login_token;

    var request_validation = true;
    
    // it is required valiees
    if(pet_key=="" || pet_name=="")
    {
        request_validation = false;
    }

    if(pet_type!="dog" && pet_type!="cat")
    {
        request_validation = false;
    }

    if(pet_gender!="male" && pet_gender!="female")
    {
        request_validation = false;
    }

    if(request_validation==false)
    {
        res.status(400).send("Bad Request.[1]");
        return;
    }
    else
    {
        // device_uuid가 존재하지 않으면, 클라이언트의 정보만 갱신한다.. 
        if(device_uuid=="")
        {
            update_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                pet_key, user_key, login_token, 
                pet_name, pet_type, pet_gender, birthday,
                breed_group, breed, neutralization, 
                weight, height, body_height, length, back_length, 
                width, neck_length, girth_length, 
                "", "", "");
  
        }
        else
        {
            // device_uuid가 존재하면, device가 가지고 있는 pet정보를 조회한다.
            var findop = {};

            if(device_pet_key=="")
                findop = {"service_code": service_code, "client_uuid":device_uuid};
            else
                findop = {"service_code": service_code, "client_uuid":device_uuid, "_id":mongoose.Types.ObjectId(device_pet_key)};

            pets.findOne(findop)
            .exec(function(err, device_pet) 
            {
                if(err!=null)
                {
                    res.status(500).send("System Error. sub code [0]");
                    return;                    
                } 
                
                if(device_pet==null)
                {
                    // device pet 정보 생성
                    // device 정보 검색 
                    findop = {"service_code": service_code, "uuid":device_uuid};
                    devices.findOne(findop)
                    .exec(function(device_err, device_info) 
                    {
                        if(device_err!=null || device_info==null)
                        {
                            res.status(404).send("Invalid Device info");
                            return;                    
                        }
                        device_apikey = device_info.client_key;

                        // device pet 정보 생성
                        {
                            // register new device pet 
                            var pic_random_id = "petpic_"+waviotUtil.gen_session_key(device_uuid+pet_name);
                            var newdevicepet = new pets(
                                {
                                    service_code: service_code, 
                                    name: pet_name,
                                    type: pet_type, // cat, dog    
                                    gender: pet_gender,  // male, female
                                    birthday : birthday,
                                    breed_group: breed_group,
                                    breed: breed,
                                    neutralization : neutralization,
                                    weight: weight,
                                    height: height,
                                    body_height: body_height, 
                                    length: length,
                                    back_length: back_length,
                                    width: width, 
                                    neck_length: neck_length,
                                    girth_length: girth_length,
                                    pic_id: pic_random_id,
                                    pic_key : "",
                                    client_uuid: device_uuid,
                                    client_apikey: device_apikey
                                }
                            )

                            newdevicepet.save(function(err, item){
                                if(err)
                                {
                                    res.status(500).send("System Error.");
                                }
                                else
                                {   
                                    device_pet_key = item._id.toHexString();
                                    update_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                                        pet_key, user_key, login_token, 
                                        pet_name, pet_type, pet_gender, birthday,
                                        breed_group, breed, neutralization, 
                                        weight, height, body_height, length, back_length, 
                                        width, neck_length, girth_length, 
                                        device_uuid, device_apikey, device_pet_key);
                                }
                            });
                        }
                    });
                }
                else
                {
                    device_apikey = device_pet.client_apikey;
                    device_pet_key  = device_pet._id.toHexString();

                    update_pet_info(res, msgtime, service_code, client_uuid, client_apikey,
                        pet_key, user_key, login_token, 
                        pet_name, pet_type, pet_gender, birthday,
                        breed_group, breed, neutralization, 
                        weight, height, body_height, length, back_length, 
                        width, neck_length, girth_length, 
                        device_uuid, device_apikey, device_pet_key);
                        
                    // device  pet 정보도 갱신
                    update_device_pet_info(device_pet,
                        pet_name, pet_type, pet_gender, birthday,
                        breed_group, breed, neutralization, 
                        weight, height, body_height, length, back_length, 
                        width, neck_length, girth_length
                    );
                }
            });   

        }
    }    
}

///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "read",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //compulsory
    "pet_key" : "5987fe0580a94c6077562c36b8c65442",
}
*/
function do_read_pet(req, res, service_code, client_uuid, client_apikey)
{
    pet_key = req.body.pet_key==null ? "" : req.body.pet_key;
    var request_validation = true;
    
    // it is required valiees
    if(pet_key=="")
        request_validation = false;

    if(request_validation==false)
    {
        res.status(400).send("Bad Request.[1]");
        return;
    }
    else
    {

        // user_key 유효성 검사
        pets.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"_id":mongoose.Types.ObjectId(pet_key)}
                ]        
            }
            ,
            {
                "service_code": 1,
                "name": 1,
                "type": 1,
                "gender": 1,
                "birthday": 1,
                "breed_group": 1,
                "breed": 1,
                "neutralization": 1,
                "weight": 1,
                "height": 1,
                "body_height": 1, 
                "length": 1,
                "back_length": 1,
                "width": 1,
                "neck_length": 1,
                "girth_length": 1,
                "pic_id": 1,
                "time_zone" : 1,
                "last_updated_time": 1
            }
        )
        .exec(function(err, pet) 
        {
            if(err!=null || pet==null)
                request_validation = false;

            if(request_validation==false)
            {
                res.status(400).send("Bad Request.[2]");
                return;
            }

            res.send(JSON.stringify(pet,null,"\t"));

        });   
        return true;
    }
}




///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "getpets",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //option
	"user_key": "594a1946975c172574880e7a",
    "login_token" : "12123123123"
}
*/

function do_get_pets(req, res, service_code, client_uuid, client_apikey)
{
    user_key = req.body.user_key==null ? "" : req.body.user_key;
    login_token = req.body.login_token==null ? "" : req.body.login_token;

    var request_validation = true;

    if(user_key!="" && login_token!="")
    {
        // user기준으로 조회
        // user_key 유효성 검사
        users.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"_id":mongoose.Types.ObjectId(user_key)}
                ]
            }
        )
        .exec(function(err, user) 
        {
            if(err!=null || user==null || login_token!=user.login_token || waviotUtil.check_login_token_expired(user.login_token_expired))
                request_validation = false;

            if(request_validation==false)
            {
                res.status(400).send("Bad Request.[2]");
                return;
            }

            query = {"service_code": service_code, "user_key":user_key};
            fields =  {"service_code":1, "user_key":1, "pet_key":1, "pet_name":1, "_id":0, 
            "device_uuid" : 1,
            "device_apikey" : 1,
            "device_pet_key" : 1};
            sorts = {
                sort : {"last_updated_time":-1 }
            };

            userExtPets.find(query,fields,sorts,
            function(error,pets){
                if(error)
                    res.status(500).send("Bad Request.")
                else
                {
                    res.send(JSON.stringify(pets,null,"\t"));
                }
            });

        });   
        return true;
    }
    else
    {
        // 장치 기준
        query = {"service_code": service_code, "client_apikey":client_apikey, "client_uuid":client_uuid};
        fields =  {"service_code":1, "client_apikey":1, "client_uuid":1, "name":1
        };
        sorts = {
            sort : {"last_updated_time":-1 }
        };

        pets.find(query,fields,sorts,
        function(error,petlist){
            if(error)
                res.status(500).send("Bad Request.");
            else
            {
                jsonstring = JSON.stringify(petlist,null,"\t");
                jsonstring = jsonstring.replace(/\"_id\":/g, "\"pet_key\":");
                res.send(jsonstring);
            }
        });

        return true;
    }    
}





///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "delete",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    
    //compulsory
    "pet_key" : "5987fe0580a94c6077562c36b8c65442",

    // the followings are optional values if has login user information. 
    "user_key": "123121231231323",
    "login_token" : "12123123123"    
}
*/
function do_delete_pet(req, res, service_code, uuid, apikey, account_type, account_id)
{
    msgtime = req.body.time;
    pet_key = req.body.pet_key==null ? "" : req.body.pet_key;;
    user_key = req.body.user_key==null ? "" : req.body.user_key;;
    login_token = req.body.login_token==null ? "" : req.body.login_token;;
    
    del_pet_info(res, msgtime, service_code, uuid, apikey, 
        pet_key, user_key, login_token); 
}

///////////////////////////////////////////////////////////////////////////////////////
/*
{
	"service_code" : "WLINK_A0001",
	"api_key":"a9d0d7293c2169f890c4",
    "command": "fileupload",
	"time": "20170621181300+0800",
    "auth": "1f57fe0580a94c6077562c36b8c65442",	
    //compulsory
    "pet_key" : "5987fe0580a94c6077562c36b8c65442", 
}
*/

var file_session_timeLimit = 5;
// true : expired
// false : not expired. it is valid.
function check_file_session_key_expired(file_session_key_expire)
{
    var today = moment();
    var expired = moment(file_session_key_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
        
    // time has problem
    if(diff>file_session_timeLimit)
        return true;

    return false;
    
}
function do_pic_file_upload(req, res, service_code, uuid, apikey, account_type, account_id)
{
    pet_key = req.body.pet_key==null ? "" : req.body.pet_key;

    if(pet_key=="")
    {
        res.status(400).send("Bad Request.1")
        return;
    }

    pets.findOne(
        {
            "$and": [
                {"_id": mongoose.Types.ObjectId(pet_key)}, 
                {"client_uuid": uuid}, 
                {"client_apikey": apikey}, 
                {"service_code": service_code} 
            ]        
        })
        .exec(function(err, pet) 
        {
            if(err)
            {
                res.status(500).send("System Error."+err)
                return;
            }
    
            if(pet==null)
            {
                res.status(400).send("Bad Request.2")
                return;
            }
            pet.file_session_key = waviotUtil.gen_session_key(uuid);
            file_session_key_time  = moment();
            file_session_key_time.add(file_session_timeLimit, "minute");
            pet.file_session_key_expired = file_session_key_time;
        
            pet.save(function(err, updatepet) {
                if(err) {
                    res.status(500).send("System Error. "+err);
                } else {
                    res.send(
                        {
                            service_code: service_code, 
                            api_key: apikey, 
                            pic_id :updatepet.pic_id,
                            file_session_key: updatepet.file_session_key, 
                            file_session_key_expired: file_session_key_time.format('YYYYMMDDHHmmssZZ')
                        }
                    );
                }
            });


        });


}




// pet  management interface
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "ext": "ExtPetsV1",    
    "command": "",
    .....
	"auth": "4b1a308f29b66779d43075adeb9e93dd"	
}
*/
router.post('/:uuid', function(req, res){
    try {
        var uuid = req.params.uuid;
        var service_code = req.body.service_code;
        var apikey = req.body.api_key;
        var ext_name = req.body.ext;
        var command = req.body.command;
        var auth = req.body.auth;

        if(uuid==null || uuid=="" ||
           service_code==null || service_code=="" || 
           apikey==null || apikey=="" || 
           ext_name==null || ext_name=="" || 
           command==null || command=="")
        {
            res.status(400).send("Bad Request.[0]");
            return;
        }

        // message 시간 검사
        if(timeCheck==true)
        {
            if(req.body.time!=null)
            {
                msgtime = req.body.time;
                if(check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        if(ext_name!=extName)
        {
            res.status(400).send("Bad Request.[1]");
            return;
        }

        // client key 유효성 검사 
        apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey })
                .exec(function(err, item) 
        {
            if(err)
            {
                res.status(500).send("System Error."+err)
                return;
            }
            if(item==null)
            {
                res.status(404).send("Not Found [1].")
                return;
            }

            if(ext_name!=extName)
            {
                res.status(400).send("Bad Request.");
                return;
            }

            var secret = item.secret;

            // message auth 검사 
            var pauth = get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret);
            if(auth!=pauth)
            {
                res.status(401).send("Unauthorized. [2]");
                return;
            }

            switch (command) {
                case "create": 
                    do_reg_pet(req, res, service_code, uuid, apikey);
                    break;
                case "read": // 특정 pet정보 조회
                    do_read_pet(req, res, service_code, uuid, apikey);
                    break;
                case "getpets": // 장치나 사용자에 등록된 pet목록 조회
                    do_get_pets(req, res, service_code, uuid, apikey);
                    break;
                case "update": 
                    do_update_pet(req, res, service_code, uuid, apikey);
                    break;
                case "delete": 
                    do_delete_pet(req, res, service_code, uuid, apikey);
                    break;
                case "fileupload": 
                    do_pic_file_upload(req, res, service_code, uuid, apikey);
                    break;
                default:
                    res.status(400).send("Bad Request.")
                    break;
            }
        });

    } catch (error) {
        res.status(400).send("Bad Request."+error)
    }    
});

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

var fs = require('fs');
let Grid = require("gridfs-stream");
Grid.mongo = mongoose.mongo;
var db = mongoose.connection;
gfs = Grid(db, mongoose.mongo);

router.post('/pic/:picuuid', 
busboy({
    limits: {
        fileSize: 10 * 1024 * 1024
    }
}),
function(req, res){
try {
    var picuuid = req.params.picuuid;
    var msg = picuuid.toString().split("#");
    var pic_id = msg[0]==null ? "" : msg[0];
    var file_session_key = msg[1]==null ? "" : msg[1];

    console.log(picuuid);
    req.pipe(req.busboy);
    
    if(pic_id=="" || file_session_key=="")
    {
        res.status(400).send("Bad Request.");
        return;
    }

    req.busboy.on('field', function(fieldname, val) 
    {
        console.log(fieldname, val);
    });

    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) 
    {
        console.log("Uploading: ", filename,encoding, mimetype);
        file_ext = filename.toString().toLowerCase().substr(filename.toString().length - 4);

        // png 파일명만 처리 한다. 
        if(file_ext!=".png")
        {
            console.log("not PNG!!")
            res.status(400).send("Bad Request.")
            return;                
        }
        
        // db조회 부분을 busboy와 통합할 수 없음. 
        // 결국 파일을 업로드 완료 후 처리해야 함 
        let writeStream = gfs.createWriteStream({
            filename:  pic_id,
            mode: 'w',
            content_type: mimetype
        });
        file.pipe(writeStream);
        
        writeStream.on('close', (file) => {
            console.log("written to DB:")

            pets.findOne({pic_id: pic_id, file_session_key:file_session_key})
            .exec(function(err, item) 
            {
                upload_success = true;
                if(err!=null || item==null)
                {
                    upload_success = false;
                    console.log("Invald file session key!!");                    
                }
    
                if(upload_success==true &&  check_file_session_key_expired(item.file_session_key_expired)==true)
                {
                    upload_success = false;
                    console.log("Invald file session key time!!");                    
                }
                
                if(upload_success==false)
                {
                    // 업로드된 파일 삭제
                    gfs.remove({ _id: file._id.toHexString()}, 
                        function (err) {
                        console.log("DELETE UPLOAD  "+file._id.toHexString());
                        }   
                    );
                    res.status(400).send("Bad Request.")
                    return;                        
                }
                

                // 이전 화일 삭제
                old_pic_key=item.pic_key;
    
                item.pic_key = file._id.toHexString();
                item.save(function(err, upateitem)
                {
                    if(err!=null || item==null)
                    {
                        gfs.remove({ _id: file._id.toHexString()}, 
                            function (err) {
                                console.log("DELETE "+file._id.toHexString());
                            }
                        );

                        res.status(400).send("Bad Request.")
                        return;
                    }

                    // 정상 업로드 후 이전에 등록 이미지 삭제
                    if(old_pic_key!="")
                    {
                        gfs.remove({ _id: old_pic_key}, 
                            function (err) {
                                console.log("DELETE OLD FILE "+old_pic_key);
                            }
                        );
                    }
                    return res.status(200).send({
                        message: 'Success',
                        file: file
                    });
                });
            });              
        });
    });

    req.busboy.on('finish', function()
    {
        console.log("finish");
    });       
    //writeStream.end();
} catch (error) {
    res.status(400).send("Bad Request."+error)
}    
});


router.get('/pic/:picuuid', 
function(req, res){
    gfs.files.find({
        filename: req.params.picuuid
    }).toArray((err, files) => {
        if (files.length === 0) {
            return res.status(400).send("Bad Request.");
        }
        let data = [];
        let readstream = gfs.createReadStream({
            filename: files[0].filename
        });

        readstream.on('data', (chunk) => {
            data.push(chunk);
        });

        readstream.on('end', () => {
            data = Buffer.concat(data);
            let img = 'data:image/png;base64,' + Buffer(data).toString('base64');
            res.end(img);
            console.log("sent", files[0].filename);
        });

        readstream.on('error', (err) => {
            console.log('An error occurred!', err);
            return res.status(400).send("Bad Request.");
            //throw err;
        });
    });

});
module.exports = router;
