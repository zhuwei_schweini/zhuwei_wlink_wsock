var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');
var crypto = require('crypto');

var apiClients = require('../models/apiClients');
var serviceCode = require('../models/serviceCode');

var timeCheck = true;
var timeLimit = 15; // 15 minutes

function get_session_auth(msgtime, service_code, service_tag, uuid, apikey, secret)
{
    var plain = msgtime+service_code+service_tag+uuid+apikey;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");
}

function s4(seed_uuid) {
    var rand = random_seed.create(seed_uuid);
    return ((1 +  rand.random()+Math.random()) * 0x10000 | 0).toString(16).substring(1);
}

function gen_session_key(seed_uuid) {
    return s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid) +
                s4(seed_uuid) + s4(seed_uuid) + s4(seed_uuid);
}

// today - expired
// over 0  : expired 
function check_service_expired(expired_date)
{
    var today = moment();
    var expired = moment(expired_date);
    return  today.diff(expired, 'days');
}

// today - expired
// under 0  : remain time 
function check_session_expired(session_expired)
{
    var today = moment();    
    var expired = moment(session_expired);
    return  today.diff(expired, 'minutes');
}

// today - expired
// over 0  : expired 
function check_msg_expired(msgtime, limit_time)
{
    var _msgtime = moment(msgtime, "YYYYMMDDHHmmssZZ");    
    var expired = moment();
    expired.add(-limit_time, "minute");

    var diff =  expired.diff(_msgtime, 'minutes');

    //console.log(expired.format('YYYYMMDDHHmmssZZ')+" DIFF: " + diff);

    if(diff<(-limit_time))
        return -diff;

    else 
        return diff;
}

// get session key
router.post('/:apikey', function(req, res){
    try {
        var apikey = req.params.apikey;
        var uuid = req.body.uuid;
        var msgtime = "";
        var service_code = req.body.service_code;
        var service_tag = req.body.service_tag;
        var auth = req.body.auth;

        if(timeCheck==true)
        {
            if(req.body.time!=null)
            {
                msgtime = req.body.time;
                if(check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        serviceCode.findOne({service_code: service_code, service_tag:service_tag })
        .exec(function(err, servicecode){
            if(err || servicecode==null)
            {
                res.status(400).send("Bad Request.[1]");
                return;
            }

            var expired_date = servicecode.expired_date;
            var session_expired_time = servicecode.session_expired_time;

            // service code validateion
            var diff = check_service_expired(expired_date);

            if(diff>0)  // invalid service code
            {
                res.status(401).send("Unauthorized. [1]")
                return;
            }

            apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey })
                .exec(function(err, item) 
            {
                if(err)
                {
                    res.status(500).send("System Error."+err)
                    return;
                }
                if(item==null)
                {
                    res.status(404).send("Not Found.")
                    return;
                }

                if(check_service_expired(item.expired_date)>0)
                {
                    res.status(401).send("Unauthorized. [2]")
                    return;
                }

                // checkk auth
                var sauth = get_session_auth(msgtime, service_code, service_tag, uuid, apikey, item.secret);
                
                if(auth!=sauth)
                {
                    res.status(401).send("Unauthorized. [3]")
                    return;
                }

                // check session time 
                // if already passed, generate new session key
                if(item.session_time==null || check_session_expired(item.session_time)>0)
                {
                    // make new session key 
                    item.session_key = gen_session_key(uuid);
                    session_time  = moment();
                    session_time.add(session_expired_time, "minute");
                    item.session_time = session_time;
                    item.last_inquired_date = Date.now();
                    item.ref_counter++;

                    item.save(function(err, updateitem) {
                        if(err) {
                            res.status(500).send("System Error. [1] "+err);
                        } else {
                            var session_time = moment(updateitem.session_time);
                            res.send({service_code: updateitem.service_code, 
                                    uuid:updateitem.uuid, 
                                    key: updateitem.key, 
                                    session_key:updateitem.session_key, 
                                    session_time:session_time.format('YYYYMMDDHHmmssZZ')});
                        }
                    });
                    
                    return;
                }
                
                diff = check_session_expired(item.session_time);
                // if remain before 30mins, extend time.
                if(diff<0 && diff>=-30)
                {
                    // make new session key 
                    session_time  = moment();
                    session_time.add(session_expired_time, "minute");
                    item.session_time = session_time;
                    item.last_inquired_date = Date.now();
                    item.ref_counter++;

                    item.save(function(err, updateitem) {
                        if(err) {
                            res.status(500).send("System Error.");
                        } else {
                            var session_time = moment(updateitem.session_time);
                            res.send({service_code: updateitem.service_code, 
                                    uuid:updateitem.uuid, 
                                    key: updateitem.key, 
                                    session_key:updateitem.session_key, 
                                    session_time:session_time.format('YYYYMMDDHHmmssZZ')});
                        }
                    });
                    
                    return;
                }

                // else  just reault same key
                var session_time = moment(item.session_time);
                res.send({service_code: item.service_code, 
                        uuid:item.uuid, 
                        key: item.key, 
                        session_key:item.session_key, 
                        session_time:session_time.format('YYYYMMDDHHmmssZZ')});
                return;
            });
        });
        
    } catch (error) {
        res.status(400).send("Bad Request.")
    }    
});

module.exports = router;
