// POST : register or update
//     hmac_md5(time+api_key+service_code+device_uuid+device_key+push_enabled+push_vendor
//              +push_interval+push_threshold, secret). Hex String Format
// DEL  : unregister
// GET  : get list
// 
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');
var crypto = require('crypto');

var waviotUtil = require("../modules/waviotUtil");
var apiClients = require('../models/apiClients');
var devices = require('../models/devices');
var pushlist = require('../models/pusheventList');
var pushEventBadge = require('../models/pusheventBadge');
var users = require('../models/users');
var userDevices = require('../models/userDevices');

var timeCheck = true;
var timeLimit = 15; // 15 minutes

var logger = global.logger;
var log_module = "PUSH";

function check_service_expired(expired_date)
{
    var today = moment();
    var expired = moment(expired_date);
    return  today.diff(expired, 'days');
}

// today - expired
// over 0  : expired 
function check_msg_expired(msgtime, limit_time)
{
    var _msgtime = moment(msgtime, "YYYYMMDDHHmmssZZ");    
    var expired = moment();
    expired.add(-limit_time, "minute");

    var diff =  expired.diff(_msgtime, 'minutes');

    //console.log(expired.format('YYYYMMDDHHmmssZZ')+" DIFF: " + diff);

    if(diff<(-limit_time))
        return -diff;

    else 
        return diff;
}

// auth 검사
function get_pushevent_reg_auth(msgtime, apikey, service_code, device_uuid, device_name, device_key, push_enabled, secret)
{
    var plain = msgtime+apikey+ service_code+device_uuid+device_name+device_key+push_enabled;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");
}

function get_pushevent_del_auth(msgtime, apikey, service_code, device_uuid, push_vendor, push_token, secret)
{
    var plain = msgtime+apikey+ service_code+device_uuid+push_vendor+push_token;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");    
}


// device push정보 갱신
function update_device_push(res, msgtime, client_uuid, apikey, service_code, device_uuid, device_name, device_key, push_enabled, 
                    push_lang, push_test, push_vendor, push_token, push_interval, push_threshold)
{
    var resmsg = {retCode: "0000", msg: "success"};
    var command = "update_device_push";

    // device.push_clients에 동일 정보 검사 
    devices.findOne(
        {
            "$and": [
                    {"service_code": service_code}, 
                    {"uuid":device_uuid}, 
                    //{"push_clients.client_uuid": client_uuid},  
                    {"push_clients.platform": push_vendor}, 
                    {"push_clients.token": push_token}
                  ]        
        }
    )
    .exec(function(err, device) 
    {

        if(err)
        {
            res.status(500).send("System Error.")
            logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, System Error:%s", 
                log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token, err); 

            return;
        }

        if(device==null)
        {

            // device는 있는지만 push_clients에 데이터가 없는 경우 임
            devices.findOneAndUpdate(
                {
                    "service_code": service_code, 
                    "uuid":device_uuid,
                },
                { 
                    "$addToSet": { 
                        "push_clients" : {
                            "client_uuid": client_uuid,
                            "name": device_name,
                            "lang": push_lang,
                            "token": push_token,
                            "platform": push_vendor,
                            "test": push_test,
                            "device_key": device_key,
                            "push_enabled": push_enabled,
                            "push_interval": push_interval,
                            "push_threshold": push_threshold,
                            "last_calling_time" : moment().format("YYYYMMDDHHmmss"),  //서버 시간을 저장
                            "last_updated_time": moment(msgtime, "YYYYMMDDHHmmssZZ")
                        }
                    } 
                },
                function(err,newpush) {
                    if(err)
                    {
                        res.status(500).send("System Error.")
                        logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, System Error[device new push client]:%s", 
                            log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token, err); 
                        return;
                    }

                    if(newpush)
                    {
                        logger.info("RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Success.", 
                            log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
                        res.send(JSON.stringify(resmsg,null,"\t"));
                    }
                    else
                    {
                        res.status(400).send("Not found.");
                        logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, No device push client", 
                            log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
                    }
                    return;
                }
            );
        }
        else
        {

            // 이미 한번 등록이 된 push token 임. 
            devices.findOneAndUpdate(
                {"$and": [
                    {"service_code": service_code}, 
                    {"uuid":device_uuid}, 
                    //{"push_clients.client_uuid": client_uuid}, // 서브문서의 업데이트가 or처럼 동작하는것 같음
                    // 현재 확실한 unique조건은 {push_vendor, push_token} 이므로 이렇게 조회함. 
                    {"push_clients.platform": push_vendor}, 
                    {"push_clients.token": push_token}
                  ]
                },
                { 
                    "$set": {
                        "push_clients.$.name": device_name,
                        "push_clients.$.lang": push_lang,
                        "push_clients.$.device_key": device_key,
                        "push_clients.$.push_enabled": push_enabled,
                        "push_clients.$.push_interval": push_interval,
                        "push_clients.$.push_threshold": push_threshold,
                        "push_clients.$.last_calling_time" : moment().format("YYYYMMDDHHmmss"), //서버 시간을 저장
                        "push_clients.$.last_updated_time": moment(msgtime, "YYYYMMDDHHmmssZZ")
                    },
                },
                function(err,pushclient) {
                    if(err)
                    {
                        res.status(500).send("System Error.")
                        logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, System Error[device update push client]:%s", 
                            log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token, err); 
                        return;
                    }

                    if(pushclient)
                    {
                        logger.info("RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Success.", 
                            log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
                        res.send(JSON.stringify(resmsg,null,"\t"));
                    }
                    else
                    {
                        logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, No device push client", 
                            log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
                        res.status(400).send("Not found.");
                    }
                    return;
                }
            );
        }
    });
}


function del_device_push(res, msgtime, client_uuid, apikey, service_code, device_uuid, push_vendor, push_token)
{
    var resmsg = {retCode: "0000", msg: "success"};
    var command = "del_device_push";

    devices.findOneAndUpdate(
        {"service_code": service_code, "uuid":device_uuid},
        { 
            "$pull": { 
                "push_clients" : {
                    "client_uuid": client_uuid,
                    "token": push_token,
                    "platform": push_vendor
                }
            } 
        }, 
        function(err,item) {
            if(err)
            {
                res.status(500).send("System Error.")
                logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, System error:%s", 
                    log_module, command, msgtime, service_code, client_uuid, apikey, device_uuid, push_vendor, push_token, err); 
                return;
            }
            if(item)
            {
                logger.info("RES:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Success", 
                    log_module, command, msgtime, service_code, client_uuid, apikey, device_uuid, push_vendor, push_token, err); 
                res.send(JSON.stringify(resmsg,null,"\t"));
            }
            else
            {
                logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Not found push clients", 
                    log_module, command, msgtime, service_code, client_uuid, apikey, device_uuid, push_vendor, push_token, err); 
                res.status(400).send("Not found.");
            }
            return;
        }
    );

}

/////////////////////////////////////////////////////////////////////////////////////////////////
// 2017.08.05 사용자정보 + 디바이스 정보 관리 부분 추가 

// 사용자정보에 디바이스 정보를 갱신한다. 
// 이 함수는 별도 추가함수로 에러처리를 추가 수행하지 않는다. 
// 값이 정상적이면 db처리를 수행한다. 
// 이 정보의 조회는 apiUser에서 처리한다. 
// 사용자의 구분은 service_code, user_key로 한다. 
// 비동기이기 때문에 return 값은 의미가 없다. 
function update_user_devices(service_code, client_uuid, apikey, user_key, login_token, device_uuid, device_name, device_key)
{
    if(service_code=="" || client_uuid=="" || apikey=="" || user_key=="" || login_token=="" 
            || device_uuid=="" || device_name=="" || device_key=="")
        return false;

    // user_key 유효성 검사
    users.findOne(
        {
            "$and": [
                {"service_code": service_code}, 
                {"_id":mongoose.Types.ObjectId(user_key)}
            ]        
        }
    )
    .exec(function(err, user) 
    {
        if(err)
            return false;
        if(user==null)
            return false;
        if(login_token!=user.login_token)
            return false;
        if(waviotUtil.check_login_token_expired(user.login_token_expired))
        {
            console.log("login_token_expired");
            return false;
        }

        userDevices.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"user_key": user_key},
                    {"device_uuid": device_uuid}
                ]        
            }
        )
        .exec(function(err, userDevice) 
        {
            if(err)
                return false;
            if(userDevice==null)
            {
                // 신규 정보
               // register new 
                var newuserDevice = new userDevices(
                    {
                        service_code: service_code, 
                        user_key : user_key,
                        device_uuid: device_uuid,
                        device_name: device_name,
                        device_key: device_key,
    
                        last_client_uuid: client_uuid,  
                        last_client_apikey: apikey,
                        last_login_token: login_token
                    }
                )
                userDevices.findOneAndUpdate(
                    {service_code: service_code, user_key: user_key, device_uuid:device_uuid}, 
                    newuserDevice, 
                    {upsert: true, new: true, runValidators: true}, // options
                    function (err, updateuser) { // callback
                        if(err)
                            return false;
                        return true;
                    }
                );     
            }
            else
            {
                // 정보 변경
                userDevice.device_name = device_name;
                userDevice.last_updated_time = moment();
                userDevice.last_client_uuid = client_uuid;
                userDevice.last_client_apikey = apikey;
                userDevice.last_login_token = login_token;

                userDevice.save(function(err, updateuserDevice) {
                    if(err)
                        return false;
                    return true;                    
                });
                
            }
         
        });   
    });   
    return true;
}

function del_user_devices(service_code, user_key, login_token, device_uuid)
{

    if(service_code=="" || user_key=="" || login_token=="" || device_uuid=="")
        return false;

    // user_key 유효성 검사
    users.findOne(
        {
            "$and": [
                {"service_code": service_code}, 
                {"_id":mongoose.Types.ObjectId(user_key)}
            ]        
        }
    )
    .exec(function(err, user) 
    {
        if(err)
            return false;
        if(user==null)
            return false;
        if(login_token!=user.login_token)
            return false;
        if(waviotUtil.check_login_token_expired(user.login_token_expired))
        {
            return false;
        }
        userDevices.findOne(
            {
                "$and": [
                    {"service_code": service_code}, 
                    {"user_key": user_key},
                    {"device_uuid": device_uuid}
                ]        
            }
        )
        .exec(function(err, userDevice) 
        {
            if(err!=null || userDevice==null)
                return false;

            userDevice.remove(function (err) {
                if(err!=null) return false;
            });
        });
    });   
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////
// 새로운 push event 등록
// uuid: 클라이언트 uuid 
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
    "device_name":"alextest2",
	"device_uuid":"uuid1",
    "device_key": "1bcce547a89ee2f8c00507b7794c17aa",
	"time": "20170602151100+0800",
    "push_lang": "1",
    "push_test": "0",
	"push_vendor" : "baidu",
	"push_token" : "token1",
	"push_enabled" : "0",
	"push_interval" : "30",
	"push_threshold" : "10",
	"auth": "2992494b808737c21627a946274dc144"	
	
}
    */
router.put('/:uuid', function(req, res){
    var client_uuid = req.params.uuid;
    var apikey = req.body.api_key;
    var service_code = req.body.service_code;
    var device_name = req.body.device_name;
    var device_uuid = req.body.device_uuid;
    var device_key = req.body.device_key;
    var push_test = req.body.push_test;
    var push_lang = req.body.push_lang;
    var push_vendor = req.body.push_vendor;
    var push_token = req.body.push_token;
    var push_enabled = req.body.push_enabled;
    var push_interval = req.body.push_interval;
    var push_threshold = req.body.push_threshold; 
    var auth = req.body.auth;
    var user_key = req.body.user_key; 
    var login_token = req.body.login_token; 
    var msgtime = req.body.time==null ? "" : req.body.time;
    var command = "post push";

    try {   
        logger.info("REQ:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, user_key:%s, login_token:%s", 
            log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token, user_key, login_token ); 
        // message 시간 검사
        if(timeCheck==true)
        {
            if(msgtime!="")
            {
                if(check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        if(push_vendor!="ios" && push_vendor!="google" && push_vendor!="baidu"  && push_vendor!="getui"  && push_vendor!="qq")
        {
            res.status(400).send("Bad Request.[1]");
            logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Invalid Push Vendor", 
                log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
            return;
        }

        // 2017.07.19 push_token이 없는 경우 발생 
        if(push_token==null || push_token=="")
        {
            res.status(400).send("Bad Request.[2]");
            logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Invalid Push Token", 
                log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
            return;
        }

        // client key 유효성 검사 
        apiClients.findOne({service_code: service_code, uuid:client_uuid, key:apikey })
                .exec(function(err, item) 
        {
            if(err)
            {
                res.status(500).send("System Error.")
                logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, System Error[apiClients]:%s", 
                    log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token, err); 
                return;
            }
            if(item==null)
            {
                res.status(404).send("Not Found [1].")
                logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Invalid Client Key", 
                   log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
                return;
            }

            var secret = item.secret;
            // message auth 검사 
            var pauth = get_pushevent_reg_auth(msgtime, apikey, service_code, device_uuid, device_name, device_key, push_enabled, secret)
            
            if(auth!=pauth)
            {
                res.status(401).send("Unauthorized. [3]");
                logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Invalid Auth Key", 
                   log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
                return;
            }
            // device uuid 유효성 검사 
            devices.findOne({service_code: service_code, uuid:device_uuid })
                .exec(function(err, item) 
            {
                if(err)
                {
                    res.status(500).send("System Error.")
                    logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s System Error[device]:%s", 
                        log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token, err); 
                    return;
                }
                if(item==null)
                {
                    res.status(404).send("Not Found [2].")
                    logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Invalid Device", 
                        log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token); 
                     return;
                }

                // 2017.08.05 사용자 정보가 존재할 경우, 자동으로 등록 처리한다. 
                if(login_token!=null && user_key!=null)
                {
                    // 내부적으로 비동기처리되는것으로 별도의 애러에 대한 처리는 하지 않는다. 
                    update_user_devices(service_code, client_uuid, apikey, user_key, login_token,
                                    device_uuid, device_name, device_key);
                }
                // devies.push_clients 등록 
                update_device_push(res, msgtime, client_uuid, apikey, service_code, device_uuid, device_name, device_key, push_enabled, 
                                    push_lang, push_test, push_vendor, push_token, push_interval, push_threshold);
            });
        });

    } catch (error) {
        res.status(400).send("Bad Request.")
        logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_name:%s, device_uuid:%s, push_vendor:%s, push_token, Catch Error:%s", 
            log_module, command, msgtime, service_code, client_uuid, apikey, device_name, device_uuid, push_vendor, push_token, error); 
    }    
});

// 장치별 push 클라이언트 삭제
// uuid: 클라이언트 uuid 
/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
	"device_uuid":"uuid1",
	"time": "20170602153200+0800",
	"push_vendor" : "baidu",
	"push_token" : "token1",
	"auth": "28b00177ad591b1ff4982f177234404e"	
}
*/
router.delete('/:uuid', function(req, res){
    var uuid = req.params.uuid;
    var apikey = req.body.api_key;
    var service_code = req.body.service_code;
    var device_uuid = req.body.device_uuid;
    var push_vendor = req.body.push_vendor;
    var push_token = req.body.push_token;
    var auth = req.body.auth;
    var user_key = req.body.user_key; 
    var login_token = req.body.login_token; 
    var msgtime = req.body.time==null ? "" : req.body.time;
    var command = "del push";
    
    try {
        logger.info("REQ:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, user_key:%s, login_token:%s", 
            log_module, command, msgtime, service_code, uuid, apikey, device_uuid, push_vendor, push_token, user_key, login_token ); 
  
        // message 시간 검사
        if(timeCheck==true)
        {
            if(msgtime!="")
            {
                if(check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        // client key 유효성 검사 
        apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey })
                .exec(function(err, item) 
        {
            if(err)
            {
                res.status(500).send("System Error.")
                logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, user_key:%s, login_token:%s. System Error[apiClients]:%s", 
                    log_module, command, msgtime, service_code, uuid, apikey, device_uuid, push_vendor, push_token, user_key, login_token, err); 
                return;
            }
            if(item==null)
            {
                res.status(404).send("Not Found [1].")
                logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Invalid Client Key", 
                   log_module, command, msgtime, service_code, uuid, apikey, device_uuid, push_vendor, push_token); 
                return;
            }

            var secret = item.secret;
            // message auth 검사 
            var pauth = get_pushevent_del_auth(msgtime, apikey, service_code, device_uuid, push_vendor, push_token, secret)
            
            if(auth!=pauth)
            {
                res.status(401).send("Unauthorized. [3]")
                return;
            }

             // device uuid 유효성 검사 
            devices.findOne({service_code: service_code, uuid:device_uuid })
                .exec(function(err, item) 
            {
                if(err)
                {
                    res.status(500).send("System Error.")
                    logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, user_key:%s, login_token:%s. System Error[devices]:%s", 
                        log_module, command, msgtime, service_code, uuid, apikey, device_uuid, push_vendor, push_token, user_key, login_token, err); 
                    return;
                }
                if(item==null)
                {
                    logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, Invalid Device", 
                    log_module, command, msgtime, service_code, uuid, apikey, device_uuid, push_vendor, push_token); 
                    res.status(404).send("Not Found [2].")
                    return;
                }

                
                // 2017.08.05 사용자 정보가 존재할 경우, 자동으로 삭제처리한다. 
                if(login_token!=null && user_key!=null)
                {
                    // 내부적으로 비동기처리되는것으로 별도의 애러에 대한 처리는 하지 않는다. 
                    del_user_devices(service_code, user_key, login_token, device_uuid);
                }
                // devies.push_clients 삭제 
                del_device_push(res, msgtime, uuid, apikey, service_code, device_uuid, push_vendor, push_token);
            });
        });

    } catch (error) {
        res.status(400).send("Bad Request.")
        logger.info("RES ERROR:: module:%s, command:%s, msgtime:%s, service_code:%s, uuid:%s, apikey:%s, device_uuid:%s, push_vendor:%s, push_token:%s, user_key:%s, login_token:%s. Catch Error:%s", 
            log_module, command, msgtime, service_code, uuid, apikey, device_uuid, push_vendor, push_token, user_key, login_token, error); 
    }    
});


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
function get_pushevent_list_auth(msgtime, apikey, service_code, device_uuid, push_vendor, push_token, secret)
{
    var plain = msgtime+apikey+ service_code+device_uuid+push_vendor+push_token;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");    
}

function clear_badge_count(service_code, platform, token)
{
    pushEventBadge.findOne({service_code: service_code, platform: platform, token : token})
    .exec(function(err, badgeitem) {
        if(err) return;
        if(badgeitem==null) return;

        badgeitem.last_update_time = moment().format('YYYYMMDDHHmmss');
        badgeitem.last_badge_count = 0;
        badgeitem.save(function(err,  badgeupdateitem) {
            //if(err) {
            //    logger.info("clear_badge_count update error:", err)
            //}
            //logger.debug("clear_badge_count update: OK", idx,  service_code,  platform, token);
        });
    });
}

function send_push_event_list(res, msgtime, uuid, apikey, service_code, device_uuid, push_vendor, push_token, sdate, edate, event_id, page, perpage)
{
    query = {"service_code": service_code, "client_uuid":uuid, "platform": push_vendor, "push_token":push_token  }; 

    if(device_uuid!="all")
        query.device_uuid = device_uuid;
        
    if(sdate!="" && edate=="")
    {
        var start_date = moment(sdate, "YYYYMMDDHHmmssZZ");
        query.event_time = { $gte: start_date.toDate()};

    }
    else if(sdate!="" && edate!="")
    {
        var start_date = moment(sdate, "YYYYMMDDHHmmssZZ");
        var end_date = moment(edate, "YYYYMMDDHHmmssZZ");
        end_date = moment(end_date).add(1, 'days');
        query.event_time = { $gte: start_date.toDate(), $lt:end_date.toDate()};
    }

    if(event_id!="")
    {
        query._id = {$gt: mongoose.Types.ObjectId(event_id)};
    }

    fields =  {"device_uuid":1, "lang":1, "push_token":1, "event_time":1, "event_type":1, "event_value":1, "event_msg":1};
    sorts = {
        sort : {"event_time":-1 }
    };

    if(page>0 && perpage>0)
    {
        sorts.skip = (page-1)*perpage;
        sorts.limit = perpage;
    }

    //console.log(query);

    pushlist.find(query,fields,sorts,
    function(error,events){
        if(error)
            res.status(500).send("Bad Request.")
        else
        {
            res.send(JSON.stringify(events,null,"\t"));
        }
    });

}


// push event 리스트 조회

/*
{
	"service_code" : "WLINK_A0000",
	"api_key":"WLINK_A0000cc4ba0105aa4e254c2b4ad08",
	"device_uuid":"uuid1",
	"time": "20170602183500+0800",
	"push_vendor" : "baidu",
	"push_token" : "token1",
	"sdate" : "20170601+0800",
	"edate" : "20170630+0800",
    "event_id": "59313d7a00f5b816bc896d4f", 
    "page" : 3,
    "perpage" : 5,
	"auth": "4b1a308f29b66779d43075adeb9e93dd"	
}
*/
router.post('/:uuid', function(req, res){
    try {
        var uuid = req.params.uuid;
        var service_code = req.body.service_code;
        var apikey = req.body.api_key;
        var push_vendor = req.body.push_vendor;
        var push_token = req.body.push_token;

        var event_id = req.body.event_id;
        var device_uuid = req.body.device_uuid;
        var sdate = req.body.sdate;
        var edate = req.body.edate;
        var page = req.body.page;
        var perpage = req.body.perpage;
        
        var auth = req.body.auth;

        // message 시간 검사
        if(timeCheck==true)
        {
            if(req.body.time!=null)
            {
                msgtime = req.body.time;
                if(check_msg_expired(msgtime, timeLimit)>0)
                {
                    res.status(401).send("Unauthorized. [1]")
                    return;
                }
                
            }
            else
            {
                res.status(400).send("Bad Request.[0]");
                return;
            }
        }

        // client key 유효성 검사 
        apiClients.findOne({service_code: service_code, uuid:uuid, key:apikey })
                .exec(function(err, item) 
        {
            if(err)
            {
                res.status(500).send("System Error."+err)
                return;
            }
            if(item==null)
            {
                res.status(404).send("Not Found [1].")
                return;
            }

            var secret = item.secret;

            // message auth 검사 
            var pauth = get_pushevent_list_auth(msgtime, apikey, service_code, device_uuid, push_vendor, push_token,  secret)
            
            if(auth!=pauth)
            {
                res.status(401).send("Unauthorized. [3]")
                return;
            }

            if(device_uuid==null)
                device_uuid = "all";

            if(sdate==null)
                sdate = "";
            if(edate==null)
                edate = "";

            if(event_id==null)
                event_id = "";

            if(page==null)
                page = 0;
            if(perpage==null)
                perpage = 0;

            // badge count clear
            clear_badge_count(service_code, push_vendor, push_token);
            
            // devies.push_clients 삭제 
            send_push_event_list(res, msgtime, uuid, apikey, service_code, device_uuid, push_vendor, push_token, sdate, edate, event_id, page, perpage);
        });

    } catch (error) {
        res.status(400).send("Bad Request."+error)
    }    
});

module.exports = router;
