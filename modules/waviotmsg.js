var moment = require('moment-timezone');
var crypto = require('crypto');
var uuid = require('node-uuid');
var waviot_version = "1.0";
var waviot_hashkey = "ea15b9b61f8947c8d2004e0c6033d8bc";

function check_msg_expired(msgtime, limit_time)
{
    var _msgtime = moment(msgtime, "YYYYMMDDHHmmssZZ");    
    var expired = moment();
    expired.add(-limit_time, "minute");

    var diff =  expired.diff(_msgtime, 'minutes');

    //console.log(expired.format('YYYYMMDDHHmmssZZ')+" DIFF: " + diff);

    if(diff<(-limit_time))
        return -diff;

    else 
        return diff;
}


function make_body_string(body)
{
    var bodystr = "";
    for (var i in body) {
        if (body[i] !== null && typeof(body[i])=="object") {
            bodystr += make_body_string(body[i]);
        }
        else
            bodystr += body[i].toString();
    }

    return bodystr;
}

function make_waviot_hashcode(body, encoding, hashkey) {
    var bodystr = make_body_string(body);
    //console.log("bodystr[", bodystr,"]")
    var hmac = crypto.createHmac("md5", hashkey);
    hmac.update(bodystr);
    return hmac.digest(encoding);
} 

function make_error_body(errno) {
  var error_msg = {errno: errno};
  return error_msg;
}

function waviot_sock_msg_header(msgid, uid, key, rid, sessionkey, secret)
{
    var header = {msgID: msgid, deviceUID: uid, clientKey: key, randomID: rid, sessionKey: sessionkey };
    header.msgTime = moment().format('YYYYMMDDHHmmssZZ');

    var hmac = crypto.createHmac("md5", secret);
    hmac.update(header.msgTime+msgid+key+rid+sessionkey);

    header.auth = hmac.digest("hex");
    return header;
}

function _make_waviot_msg(msgID, groupID, deviceID, clientKey, ipAddress, hashkey, body)
{
    var waviot_msg = {waviot: waviot_version};
    var waviot_header = {msgID: msgID, msgTime: moment().format('YYYYMMDDHHmmssZZ') , groupID: groupID, deviceID: deviceID, clientKey: clientKey}; 
    if(ipAddress!=null && ipAddress!="")
        waviot_header.ipAddress = ipAddress;

    waviot_msg.header = waviot_header;
    waviot_msg.body  = body;
    waviot_msg.header.hashcode = make_waviot_hashcode(waviot_msg.body, "base64", hashkey);
    
    return waviot_msg;
}

var waviotmsg = {
    make_waviot_msg: function(msgID, groupID, deviceID, clientKey, ipAddress, hashkey, body) {
        return _make_waviot_msg(msgID, groupID, deviceID, clientKey, ipAddress, hashkey, body);
    },

    make_error_msg: function(errno, groupID, deviceID, clientKey, ipAddress, hashkey ) {
        var errBody = make_error_body(errno);
        return _make_waviot_msg("error", groupID, deviceID, clientKey,ipAddress, hashkey, errBody);
    },

    make_waviot_sock_msg: function (msgid, uid, key, rid, sessionkey, secret, body) 
    {
        var msg_header = waviot_sock_msg_header(msgid, uid, key, rid, sessionkey, secret);
        var msgobj = {header:msg_header, body:body};

        return msgobj;
    },
    make_waviot_hashcode: function (body, encoding, hashkey) 
    {
        return make_waviot_hashcode(body, encoding, hashkey);
    },

    check_msg_validation: function(msgobj, hashkey)
    {
        if(msgobj==null || msgobj.header==null || msgobj.body==null || hashkey==null || hashkey=="")
            return false;

        var header = msgobj.header;

        // check hash
        hashstr = header.msgTime+header.msgID+header.clientKey+header.randomID+header.sessionKey;
        var hmac = crypto.createHmac("md5", hashkey);
        hmac.update(hashstr);

        var auth = hmac.digest("hex");

        if(auth!=header.auth)
            return false;

        // check message time
        if(check_msg_expired(header.msgTime, 10)>0)
            return false;

        // clientkey, sessiokey???
        
        return true;
    },

    FindIgnoreKeyValue: function (obj, val) {
        for (key in obj) {
            if(key.toLowerCase() == val.toLowerCase())
                return obj[key];
        }
        return null;
    }

   
};


module.exports = waviotmsg;
