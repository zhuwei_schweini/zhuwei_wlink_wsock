var winston = require('winston');
var DailyRotateFile = require('winston-daily-rotate-file');
//winston.emitErrs = true;

var fs = require('fs');
var moment = require('moment');
var path = require('path');

var logDir = path.join(__dirname) + '/logs';
if(!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

var logger = function(name) {
    newLogger = new winston.Logger({
        transports: [
            new (winston.transports.Console)({
                colorize: true,
                timestamp: localTimeStamp
            }),
            new DailyRotateFile({
                filename: logDir + '/' + name + '_',
                datePattern: 'yyyyMMdd.log',
                prepend: false,
                localTime: true,
                timestamp: localTimeStamp,
                json: false,
                compress: true
            })
        ],
        exceptionHandlers: [
            new DailyRotateFile({
                filename: logDir + '/' + name + '_e_',
                datePattern: 'yyyyMMdd.log',
                prepend: false,
                localTime: true,
                timestamp: localTimeStamp,
                json: false,
                compress: true
            })
        ],
        exitOnError: false
    });

    return newLogger;
};

function localTimeStamp() {
    return moment().format('YYYY/MM/DD HH:mm:ss');
}

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding) {
        logger.info(messgae);
    }
}
