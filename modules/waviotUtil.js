var moment = require('moment-timezone');
var crypto = require('crypto');
var uuid = require('node-uuid');
var random_seed = require('random-seed');
var AWS = require("aws-sdk");
const random_key_timeLimit = 5; // 5 minuts
const login_token_timeLimit = 30; // 30 days

var mongoose = require('mongoose');
var adminUsers = require('../models/adminUsers');


function check_random_key_expired(random_key_expire)
{
    var today = moment();
    var expired = moment(random_key_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
        
    // time has problem
    if(diff>random_key_timeLimit)
        return true;

    return false;
    
}

// true : expired
// false : not expired. it is valid.
function check_login_token_expired(login_token_expired)
{
    var today = moment();
    var expired = moment(login_token_expired);
    var diff = expired.diff(today, 'days');

    // time is passed
    if(diff<0)
        return true;

    // time has problem
    if(diff>login_token_timeLimit)
        return true;

    return false;
}

// today - expired
// over 0  : expired 
function check_msg_expired(msgtime, limit_time)
{
    var _msgtime = moment(msgtime, "YYYYMMDDHHmmssZZ");    
    var expired = moment();
    expired.add(-limit_time, "minute");

    var diff =  expired.diff(_msgtime, 'minutes');

    //console.log(expired.format('YYYYMMDDHHmmssZZ')+" DIFF: " + diff);

    if(diff<(-limit_time))
        return -diff;

    else 
        return diff;
}

function _s4(seed_uuid) {
    var rand = random_seed.create(seed_uuid);
    return ((1 +  rand.random()+Math.random()) * 0x10000 | 0).toString(16).substring(1);
}

function gen_session_key(seed_uuid) {
    return _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) +
                _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid);
}


function get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret)
{
    var plain = msgtime+apikey+service_code+ext_name+command;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");
}

const file_session_timeLimit = 5;
// true : expired
// false : not expired. it is valid.
function check_file_session_key_expired(file_session_key_expire)
{
    var today = moment();
    var expired = moment(file_session_key_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
        
    // time has problem
    if(diff>file_session_timeLimit)
        return true;

    return false;
 
}

function gen_file_session_key_time()
{
    var file_session_key_time  = moment();
    file_session_key_time.add(file_session_timeLimit, "minute");
    
    return file_session_key_time;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
const verification_session_timeLimit = 10;
// true : expired
// false : not expired. it is valid.
function check_verification_session_key_expired(verification_session_expired)
{
    var today = moment();
    var expired = moment(verification_session_expired);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
        
    // time has problem
    if(diff>verification_session_timeLimit)
        return true;

    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////
const check_verification_code_limit_timeout = 2;
// true : not over call limt time. 
// false : over the time limt. it is valid.
function check_verification_code_timelimit(verification_code_time)
{
    var today = moment();
    var expired = moment(verification_code_time);
    var diff = today.diff(expired, 'minute');

    console.log("check_verification_code_timelimit:"+diff)
    // time is passed
    if(diff<check_verification_code_limit_timeout)
        return true;

    return false;
}

function gen_verification_session_key_time()
{
    var verification_session_key_time  = moment();
    verification_session_key_time.add(verification_session_timeLimit, "minute");
    
    return verification_session_key_time;
}

function gen_verification_code()
{
    var today = moment();
    var seed_uuid = today.format("YYYYMMDDHHmmssZZ").toString();;
    var rand = random_seed.create(seed_uuid);
    return ((1 +  rand.random()+Math.random()) * 100000000 | 0).toString(10).substring(1, 7);
}

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
function init_global_service(configResult)
{
  let i;
  global.service = new Map();

  for(i=0; i<configResult.io_service.length; i++)
  {
    let config = {
      service_code : configResult.io_service[i].service_code,
      baidu : configResult.io_service[i].baidu, 
      getui : configResult.io_service[i].getui, 
      apn_debug : configResult.io_service[i].apn_debug, 
      apn_release : configResult.io_service[i].apn_release, 
      apn_topic : configResult.io_service[i].apn_topic, 
      aws : configResult.io_service[i].aws,
      smtp: configResult.io_service[i].smtp
    };
    config.aws_config = new AWS.Config(configResult.io_service[i].aws);
    global.service.set(configResult.io_service[i].service_code, config);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
function get_global_service(service_code)
{
    return global.service.get(service_code);
}


////////////////////////////////////////////////////////////////////////////////////////////////
async function is_admin_account(apiclient, service_code, uuid)
{
  if(apiclient.is_admin!=true) 
    return false;  

  try {
    // find adminUsers
    let query = {"_id":mongoose.Types.ObjectId(uuid)};
    let adminInfo = await adminUsers.findOne(query);
    if(adminInfo!=null)
    {
        return true;
    }

  } catch (error) {
    return false;  
  }
  return false;
}

var waviotUtil = {
    gen_session_key,
    check_msg_expired,
    check_login_token_expired,
    check_random_key_expired,
    get_ext_auth,
    check_file_session_key_expired,
    gen_file_session_key_time,
    init_global_service,
    get_global_service,
    gen_verification_code,
    check_verification_code_timelimit,
    check_verification_session_key_expired,
    gen_verification_session_key_time,
    is_admin_account
};

module.exports = waviotUtil;
