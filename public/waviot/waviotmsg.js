/*
 * WAVIOT WEB SOCKET.IO Interface
 * alex@win-star.com 
 * data : 2016.11.19
 */

function waviot_get_time()
{
  var msgtime = new Date();
  msgtimestr =   msgtime.getFullYear() + 
                ("00" + (msgtime.getMonth() + 1)).slice(-2) +  
                ("00" + msgtime.getDate()).slice(-2) + 
                ("00" + msgtime.getHours()).slice(-2) +  
                ("00" + msgtime.getMinutes()).slice(-2) + 
                ("00" + msgtime.getSeconds()).slice(-2);

  var toffset = msgtime.getTimezoneOffset();
  if(toffset>=0)
      msgtimestr = msgtimestr + '-' + ("00" + toffset/60).slice(-2) + ("00" + toffset%60).slice(-2);
  else
      msgtimestr = msgtimestr + '+' + ("00" + (-toffset)/60).slice(-2) + ("00" + (-toffset)%60).slice(-2);
 
  return msgtimestr;
}

/*
 * uid : target device uid
 * key, rid, sessionkey : sender's information
 */
function waviot_sock_msg_header(msgid, uid, key, rid, sessionkey, secret)
{
  var header = {msgID: msgid, deviceUID: uid, clientKey: key, randomID: rid, sessionKey: sessionkey };
  header.msgTime = waviot_get_time();
  header.auth = md5(header.msgTime+msgid+key+rid+sessionkey, secret, false);

  return header;
}

function make_waviot_msg(msgid, uid, key, rid, sessionkey, secret, body) 
{
    var msg_header = waviot_sock_msg_header(msgid, uid, key, rid, sessionkey, secret);
    var msgobj = {header:msg_header, body:body};

    return msgobj;
}
