// server.js
// alex@win-star.com
// Date : 2016.11.12

var express = require('express');
var mongoose = require('mongoose');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var Redis = require('ioredis');
var amqp = require('amqplib');
var cluster = require('cluster');
var os = require('os');
var sticky = require('sticky-session');
var http  = require('http');
var path = require('path');
var config = require(path.join(__dirname,'config'));
var bodyParser = require('body-parser');
var methodOverridde = require('method-override');

// use localtest configuration
global.config = config.sandbox;
//global.config = config.localtest;
global.config.path = path.join(__dirname);

//var redisClient = new Redis.Cluster(global.config.redis);
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

process.on('uncaughtException', function (err) {
	console.log('uncaughtException 발생 : ' + err);
});

if (cluster.isMaster) {
    os.cpus().forEach(function (cpu) {
        cluster.fork();
    });

    cluster.on('exit', function(worker, code, signal) {
        if (code == 200) {
            cluster.fork();
        }
    });
}
else 
{
    
    var admin_site = require('./sites/appAdmin.js').listen();
    var service_site = require('./sites/appService.js').listen();
}

