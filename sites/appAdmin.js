// appAdmin.js
// alex@win-star.com
// Date : 2016.11.12

var express = require('express');
var mongoose = require('mongoose');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var Redis = require('ioredis');
var amqp = require('amqplib');
var cluster = require('cluster');
var os = require('os');
var http  = require('http');
var bodyParser = require('body-parser');
var methodOverridde = require('method-override');
var busboy = require('connect-busboy');
var morgan = require('morgan');
var rfs = require('rotating-file-stream');
var fs = require('fs');
var path = require('path');
var configSchema = require('../models/config.js');
var waviotUtil = require("../modules/waviotUtil");

//var config = global.config;

// env to config
var config = 
{
  node_env : process.env.NODE_ENV || "development",
  port :  process.env.PORT || 3001,
  log_path : process.env.LOG_PATH || "/logs",
  db : process.env.DB,
  redis: process.env.REDIS,
  jwt_sceret : process.env.JWT_SECRET,
  session_time : parseInt(process.env.ONLINE_SESSION_TIME) || 2,
  path : path.join(__dirname),
  IPLOC_KEY : process.env.IPLOC_KEY,
  OPENWEATHER_KEY : process.env.OPENWEATHER_KEY,
  WEATHERCOM_KEY : process.env.WEATHERCOM_KEY,
  isProduction : (cfg) => cfg.node_env!=="development",
}

module.exports.listen = async function()
{
    var redisClient = new Redis.Cluster(config.redis);
    var logDirectory = config.path +  '/logs';
    var accessLogStream = rfs('access-admin.log', {
        interval: '1d', // rotate daily
        path: logDirectory,
        compress: 'gzip' 
    })
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // DB Connection
    var conn = mongoose.connection;
    var admin_port;

    try {
        mongoose.Promise = global.Promise;
        await mongoose.connect(config.db, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false });

        conn.once("open", function () {
            logger.info("DB is connected");
        });

        if (config.node_env == "development")
            configName = "localtest";
        else if (config.node_env == "testing")
            configName = "sandbox";
        else if (config.node_env == "production")
            configName = "real";

        var result = await configSchema.findOne({"name":configName});
        if (result != null) {
            console.log("findOne admin_port : " + result.admin_port);
            admin_port = result.admin_port;
            waviotUtil.init_global_service(result);
        }
    } catch (error) {
        logger.info("DB Error: " + error);
    }

    //////////////////////////////////////////////////////////////////////////
    // FOR ADMIN SITE
    var app_admin = express();
    app_admin.use(morgan('common', {stream: accessLogStream}));    
    var http_admin = http.createServer(app_admin);

    app_admin.set('view engine', 'ejs');
    app_admin.use(express.static(config.path + '/public'));
    app_admin.set('views', (config.path + '/views'));
    app_admin.use(bodyParser.json());
    app_admin.use(bodyParser.urlencoded({     // to support URL-encoded bodies
        extended: true
      }))
    app_admin.use(methodOverridde("_method"));
    app_admin.use(session({
        secret: 'ilovewinstarswavlinkwlink',
        store: new RedisStore({client: redisClient, 
            ttl: 10*60 // 10 mins
        }),
        resave: false,
        saveUninitialized: true,
        cookie: {
            maxAge: 1000 * 60 * 60
        }
    }));

    var admin = require('../routes/admin.js');

    app_admin.get('/', function(req, res){
        res.redirect("/admin")
    });

    app_admin.get('/sessiontest', function(req, res){
        var sess = req.session;
        req.session.touch();
        res.render("sessiontest", {
            username : sess.user_name, 
            userid  : sess.user_id
        });
    });

    app_admin.get('/sessiontest_igd', function(req, res){
        var sess = req.session;
        req.session.touch();
        res.render("sessiontest_igd", {
            username : sess.user_name, 
            userid  : sess.user_id
        });
    });

    app_admin.get('/devsession', function(req, res){
        var sess = req.session;
        req.session.touch();
        res.render("devsession", {
            username : sess.user_name, 
            userid  : sess.user_id
        });
    });

    app_admin.get('/sessiontest_pet', function(req, res){
        var sess = req.session;
        req.session.touch();
        res.render("sessiontest_pet", {
            username : sess.user_name, 
            userid  : sess.user_id
        });
    });

    app_admin.use('/admin', admin);

    // serviceCode handler
    var apiServiceCode = require('../routes/apiServiceCode.js');
    app_admin.use('/api/serviceCode', apiServiceCode);

    // clients handler for management
    var apiClients = require('../routes/apiClients.js');
    app_admin.use('/api/clientKey', apiClients);

    // get active session handler 
    var apiSession = require('../routes/apiActiveSessions.js');
    app_admin.use('/api/asessions', apiSession);

    var apiDevices = require('../routes/apiDevices.js');
    app_admin.use('/api/device', apiDevices);

    var apiDevices = require('../routes/apiNetTraffic.js');
    app_admin.use('/api/nettraffic', apiDevices);

    var apiGetKey = require('../services/apiGetKey.js');
    app_admin.use('/api/getKey', apiGetKey);

    var apiGetDeviceKey = require('../services/apiGetDeviceKey.js');
    app_admin.use('/api/getDeviceKey', apiGetDeviceKey);

    // get session handler 
    var apiSession = require('../services/apiSession.js');
    app_admin.use('/api/session', apiSession);

    // get push event handler 
    var apiPush = require('../services/apiPush.js');
    app_admin.use('/api/pushevent', apiPush);

    // get user handler 
    var apiUser = require('../services/apiUserV2.js');
    app_admin.use('/api/user', apiUser);

    // Extension PET
    var apiExtPet = require('../services/apiExtPetV2.js');
    app_admin.use('/api/ext/pet', apiExtPet);
      
    // start admin server
    console.log("ADMIN Port is "+ admin_port);

    http_admin.listen(admin_port, function() {
        console.log('app admin Server listening at port %d', admin_port);
    });
}
