// appService.js
// alex@win-star.com
// Date : 2016.11.12

var express = require('express');
var mongoose = require('mongoose');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var Redis = require('ioredis');
var amqp = require('amqplib');
var cluster = require('cluster');
var os = require('os');
//var sticky = require('sticky-session');
var fs = require('fs');
var path = require('path');
var https  = require('https');
var bodyParser = require('body-parser');
var methodOverridde = require('method-override');

var morgan = require('morgan');
var rfs = require('rotating-file-stream');
var fs = require('fs');
var configSchema = require('../models/config.js');
var waviotUtil = require("../modules/waviotUtil");


//var config = global.config;
var configPath = path.join(__dirname);
var redisClient = new Redis.Cluster(process.env.REDIS);

module.exports.listen = async function()
{
    var logDirectory = process.env.log +  '/logs';
    var accessLogStream = rfs('access-service.log', {
        interval: '1d', // rotate daily
        path: logDirectory,
        compress: 'gzip' 
    })

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // DB Connection
    var conn = mongoose.connection;
    var service_port;

    try {
        await mongoose.connect(process.env.DB, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false });

        conn.once('open', function() {
            logger.info("DB is connected");
        });

        if (process.env.NODE_ENV == "development")
            configName = "localtest";
        else if (process.env.NODE_ENV == "testing")
            configName = "sandbox";
        else if (process.env.NODE_ENV == "production")
            configName = "real";

        var result = await configSchema.findOne({"name":configName});

        if (result != null) {
            console.log("findOne service_port: " + result.service_port);
            service_port = result.service_port;
            waviotUtil.init_global_service(result);
        }
    } catch (error) {
        console.log(new Date(), "app service DB Error: " + error);
    }

    var app_service = express();
    app_service.use(morgan('common', {stream: accessLogStream}));    

    var ssl_options = {
        key: fs.readFileSync('/etc/letsencrypt/live/wsocktest.wavlink.org-0001/privkey.pem'),
        cert: fs.readFileSync('/etc/letsencrypt/live/wsocktest.wavlink.org-0001/cert.pem'),
        requestCert: false,
        rejectUnauthorized: false    
    };
    var http_service = https.createServer(ssl_options, app_service);
    app_service.use(bodyParser.json());
    app_service.use(methodOverridde("_method"));
    app_service.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
      });

    app_service.use(express.static(configPath + '/public_api'));

    var apiGetKey = require('../services/apiGetKey.js');
    app_service.use('/api/getKey', apiGetKey);

    var apiGetDeviceKey = require('../services/apiGetDeviceKey.js');
    app_service.use('/api/getDeviceKey', apiGetDeviceKey);

    // get session handler 
    var apiSession = require('../services/apiSession.js');
    app_service.use('/api/session', apiSession);

    // get push event handler 
    var apiPush = require('../services/apiPush.js');
    app_service.use('/api/pushevent', apiPush);

    // get user handler 
    var apiUser = require('../services/apiUserV2.js');
    app_service.use('/api/user', apiUser);

    // Extension PET
    var apiExtPet = require('../services/apiExtPetV2.js');
    app_service.use('/api/ext/pet', apiExtPet);
          
    // start server
    console.log(new Date(), "API Service Port is "+ service_port);

    http_service.listen(service_port, function() {
        console.log(new Date(), 'app service Server listening at port %d', service_port);
    });
/*
    sticky.listen(http_service, config.service_port, function()
    {
        console.log('Server listening at port %d', config.service_port);
    });    
*/
}