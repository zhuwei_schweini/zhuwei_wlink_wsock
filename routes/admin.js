var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var mongoose = require('mongoose');
var password_hashkey = "w1asdf1sadfea15b9b61f8947c8d2004e0c6033d8bc";
var moment = require('moment-timezone');

var adminUsers = require('../models/adminUsers');

function check_login(req, res)
{
    if(req.session.wlink_session==null || req.session.wlink_session=="")
    {   
        res.redirect("/admin/login");
        return false;
    }

    return true;
}

/* GET users listing. */
router.get('/', function(req, res, next) 
{
    if(check_login(req, res)==false) return;
/*
    if(req.session.login_key==null || req.session.login_key=="")
    {   
        res.redirect("/admin/login");
        return;
    }
*/
    var sess = req.session;
    res.render("admin/index",
        {
            username : sess.user_name, 
            userid  : sess.user_id
        });
});

router.get('/register', function(req, res, next) {
    res.render("admin/register");
});

function make_login_session_key(id, name, password)
{
    var str = id+name+password+moment().format('YYYYMMDDHHmmssZZ');
    var hmac = crypto.createHmac("md5", password_hashkey);
    hmac.update(str);
    login_session_key = hmac.digest("hex");
    return login_session_key;
}

router.post('/register', function(req, res, next) {

    var name = req.body.user_name;
    var id = req.body.user_email;
    var password = req.body.user_password;

    var hmac = crypto.createHmac("md5", password_hashkey);
    hmac.update(id+password);
    password_auth = hmac.digest("hex");

    var useradmin = new adminUsers({
        id: id,
        name: name, 
        password: password_auth,
        confirmed_id: false
    });

    useradmin.save(function(err, item){
        if(err)
            res.status(400).send("Bad Request.");
        else
        {
            res.status(200).send("OK");
        }
    });


});

router.get('/login', function(req, res, next) {
    //if(check_login(req, res)==true) return;
    
    res.render("admin/login");
});

router.get('/logout', function(req, res, next) {
    
    if(req.session.wlink_session)
    {
        req.session.destroy(function(){
            res.redirect('/admin');
        });
    } else {
        res.redirect('/admin');
    }    
});

router.post('/login', function(req, res, next) {
    //if(check_login(req, res)==true) return;
    var id = req.body.user_email;
    var password = req.body.user_password;

    if(id.trim().length==0 || password.trim().length==0)
    {
        res.status(400).send("Bad Reqeuest.");
        return;
    }

    var hmac = crypto.createHmac("md5", password_hashkey);
    hmac.update(id+password);
    password_auth = hmac.digest("hex");
    adminUsers.findOne({id:id, password: password_auth })
        .exec(function(err, item) 
    {
        if(err)
        {
            res.status(400).send("Bad Reqeuest.");
            return;
        }

        if(item==null)
        {
            res.status(404).send("Not Found.");
            return;
        }
        if(item.confirmed_id==false)
        {
            res.status(401).send("unauthorized");
            return;
        }

        item.last_login_date = Date.now();
        if(item.login_count)
            item.login_count++;
        else 
            item.login_count = 1;
        item.save(function(err, updateitem) {
            if(err) {
                res.status(500).send("System Error. [1] "+err);
            } else {
                req.session.user_id = updateitem.id;
                req.session.user_name = updateitem.name;
                req.session.wlink_session=make_login_session_key(updateitem.id, updateitem.name, updateitem.password);
                
                res.status(200).send("OK");
            }
        });
    });
});


router.get('/serviceCode', function(req, res, next) {
    var sess = req.session;
    if(check_login(req, res)==false) return;
    req.session.touch();
    res.render("admin/serviceCode",
        {
            username : sess.user_name, 
            userid  : sess.user_id
        });
});
router.get('/apiClients', function(req, res, next) {
    var sess = req.session;
    
    if(check_login(req, res)==false) return;
    req.session.touch();
    res.render("admin/apiClients",
        {
            username : sess.user_name, 
            userid  : sess.user_id
        });
});

router.get('/sessions', function(req, res, next) {
    var sess = req.session;
    
    if(check_login(req, res)==false) return;
    req.session.touch();
    res.render("admin/activeSessions",
        {
            username : sess.user_name, 
            userid  : sess.user_id
        });
});


router.get('/devices', function(req, res, next) {
    var sess = req.session;
    
    if(check_login(req, res)==false) return;
    req.session.touch();
    res.render("admin/devices",
        {
            username : sess.user_name, 
            userid  : sess.user_id
        });
});


router.get('/nettraffic', function(req, res, next) {
    var sess = req.session;
    
    if(check_login(req, res)==false) return;
    req.session.touch();
    res.render("admin/nettraffic",
        {
            username : sess.user_name, 
            userid  : sess.user_id
        });
});

module.exports = router;
