var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var serviceCode = require('../models/serviceCode');

function check_login(req, res)
{
    if(req.session.wlink_session==null || req.session.wlink_session=="")
    {   
        res.status(401).send("unauthorized");
        return false;
    }

    return true;
}

// POST (new service code)
router.post('/', function(req, res){
    var service_name = req.body.service_name;
    var service_code = req.body.service_code.toUpperCase();
    var service_tag = req.body.service_tag;
    var service_type = req.body.service_type;
    var service_hashkey = req.body.service_hashkey;
    var expired_date = Date.parse(req.body.expired_date);
    var session_expired_time = req.body.session_expired_time;

    if(check_login(req,res)==false) return false;
    req.session.touch();

    console.log("expired_date:"+expired_date);
    console.log('POST servicecode: ' + JSON.stringify(req.body));

    var servicecode= new serviceCode({
        service_name: service_name,
        service_code: service_code,
        service_tag: service_tag, 
        service_type: service_type, 
        service_hashkey: service_hashkey, 
        expired_date: expired_date,
        session_expired_time: session_expired_time
    });

    servicecode.save(function(err, item){
        if(err)
            res.status(500).send(err);
        else
            res.send(item);
    });
});

// put (update service code)
router.put('/:id', function(req, res){
    var id = req.params.id;
    var service_name = req.body.service_name;
    var service_code = req.body.service_code;
    var service_type = req.body.service_type;
    var service_hashkey = req.body.service_hashkey;
    var service_tag = req.body.service_tag;
    var expired_date = Date.parse(req.body.expired_date);
    var session_expired_time = req.body.session_expired_time;
    if(check_login(req,res)==false) return false;
    req.session.touch();
    
    serviceCode.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
            res.status(400).send(err);
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
            {
                item.last_update_date = Date.now();
                item.service_name = service_name;
                item.service_code = service_code;
                item.service_tag = service_tag;
                item.service_type = service_type;
                item.service_hashkey = service_hashkey; 
                item.expired_date = expired_date;
                item.session_expired_time = session_expired_time;

                item.save(function(err, updateitem) {
                    if(err) {
                        res.status(500).send(err);
                    } else {
                        res.send(updateitem);
                    }
                });
            }
        }
    });
});

// delete (delte service code)
router.delete('/:id', function(req, res){
    var id = req.params.id;
    if(check_login(req,res)==false) return false;
    req.session.touch();
    
    serviceCode.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
            res.status(400).send(err);
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
            {
                item.remove(function (err) {
                    if(err)
                        res.status(400).send(err);
                    else
                        res.send("Delete OK");
                });
            }
        }
    });
});

// get item info 
router.get('/:id', function(req, res){
    var id = req.params.id;
    if(check_login(req,res)==false) return false;
    req.session.touch();

    serviceCode.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
        {
            res.status(400).send(err);
            console.log(err);
        }
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
                res.send(item);
        }
    });
});

// get list for Datatables
router.get('/', function(req, res){
    var params = JSON.parse(JSON.stringify(req.body));

    // for sessiontest, do not check login 
    //if(check_login(req,res)==false) return false;
    req.session.touch();

    var data = {
        draw: params.draw,
        recordsTotal: 0,
        recordsFiltered: 0,
        data: []
    }

    serviceCode.find()
        //.select("")
        .limit(parseInt(params.length))
        .exec(function(err, servicecodes) {
            if(err) {
                data.recordsTotal  = -1;
                data.recordsFiltered  = -1;
                res.json(data);  
                console.log(err);
                
            } else {
                data.recordsTotal  = servicecodes.length;
                data.recordsFiltered  = servicecodes.length;
                data.data = servicecodes;
                res.json(data);    
            }
        });  
});


module.exports = router;
