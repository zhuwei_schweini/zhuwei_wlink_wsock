
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var apiClients = require('../models/apiClients');

function check_login(req, res)
{
    if(req.session.wlink_session==null || req.session.wlink_session=="")
    {   
        res.status(401).send("unauthorized");
        return false;
    }

    return true;
}


// POST (new client key)
router.post('/', function(req, res){
    var service_code = req.body.service_code.toUpperCase();
    var uuid = req.body.uuid;
    var key = req.body.key;
    var secret = req.body.secret;
    var expired_date = Date.parse(req.body.expired_date);   
    var session_key = req.body.session_key;

    if(check_login(req,res)==false) return false;
    req.session.touch();

    var apiclients= new apiClients({
        service_code: service_code, 
        uuid: uuid,
        key:key,
        secret: secret,
        expired_date: expired_date,
        session_key: session_key
    });

    apiclients.save(function(err, item){
        if(err)
            res.status(500).send(err);
        else
            res.send(item);
    });
});

// put (update client key)
router.put('/:id', function(req, res){
    var id = req.params.id;
    var service_code = req.body.service_code.toUpperCase();
    var uuid = req.body.uuid;
    var key = req.body.key;
    var expired_date = Date.parse(req.body.expired_date);
    var secret = req.body.secret;
    var session_key = req.body.session_key;

    if(check_login(req,res)==false) return false;
    req.session.touch();
        
    apiClients.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
            res.status(400).send(err);
        else
        {
            if(item==null)
            {
                res.status(404).send("No Data.");
            }
            else
            {
                item.last_update_date = Date.now();
                item.service_code = service_code;
                item.uuid = uuid;
                item.key = key;
                item.secret = secret;
                item.session_key = session_key;
                item.expired_date = expired_date;

                item.save(function(err, updateitem) {
                    if(err) {
                        res.status(500).send(err);
                    } else {
                        res.send(updateitem);
                    }
                });

            }
        }
    });
});

// delete (delte client key)
router.delete('/:id', function(req, res){
    var id = req.params.id;
    if(check_login(req,res)==false) return false;
    req.session.touch();
    
    apiClients.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
            res.status(400).send(err);
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
            {
                item.remove(function (err) {
                    if(err)
                        res.status(404).send(err);
                    else
                        res.send("Delete OK");
                });
            }
        }
    });
});

// get item info 
router.get('/:id', function(req, res){
    var id = req.params.id;
    if(check_login(req,res)==false) return false;
    req.session.touch();

    apiClients.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
            res.status(400).send(err);
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
                res.send(item);
        }
    });
});

// get list for Datatables
router.get('/', function(req, res){
    var params = JSON.parse(JSON.stringify(req.body));
    if(check_login(req,res)==false) return false;
    req.session.touch();

    var data = {
        draw: params.draw,
        recordsTotal: 0,
        recordsFiltered: 0,
        data: []
    }

    apiClients.find()
        //.select("")
        .limit(parseInt(params.length))
        .exec(function(err, servicecodes) {
            if(err) {
                data.recordsTotal  = -1;
                data.recordsFiltered  = -1;
                res.json(data);    
            } else {
                data.recordsTotal  = servicecodes.length;
                data.recordsFiltered  = servicecodes.length;
                data.data = servicecodes;
                res.json(data);    
            }
        });  
});


module.exports = router;
