var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var nettraffic = require('../models/nettraffic');

// get list for Datatables
router.get('/', function(req, res){
    var params = JSON.parse(JSON.stringify(req.body));

    // for sessiontest, do not check login 
    //if(check_login(req,res)==false) return false;
    req.session.touch();

    var data = {
        draw: params.draw,
        recordsTotal: 0,
        recordsFiltered: 0,
        data: []
    }

    var sample = [
{
    "_id" : "583fd5ccf2b07cc79d2bd895",
    "hostname" : "NAS",
    "ip_address" : "192.168.10.11",
    "mac_address" : "00:11:22:12:32:11",
    "dn_speed" : "23.6 Kbps",
    "up_speed" : "960.6 Kbps",
    "sum_dn_bytes" : "110.2 MB",
    "sum_up_bytes" : "10.1 MB"
},
{
    "_id" : "583fd68cf2b07cc79d2bd896",
    "hostname" : "Alex-iphone",
    "ip_address" : "192.168.10.120",
    "mac_address" : "90:8D:6C:11:32:A3",
    "dn_speed" : "100.6 Kbps",
    "up_speed" : "10.1 Kbps",
    "sum_dn_bytes" : "12.4 MB",
    "sum_up_bytes" : "752 KB"
},
{
    "_id" : "583fd730f2b07cc79d2bd897",
    "hostname" : "android-d17691d",
    "ip_address" : "192.168.10.121",
    "mac_address" : "14:3E:BF:FE:E6:A2",
    "dn_speed" : "0.0 Kbps",
    "up_speed" : "812.2 Kbps",
    "sum_dn_bytes" : "6.91MB",
    "sum_up_bytes" : "2.85MB"
},
{
    "_id" : "583fd7aff2b07cc79d2bd898",
    "hostname" : "alex5f4a",
    "ip_address" : "192.168.10.128",
    "mac_address" : "A8:66:7F:11:6C:92",
    "dn_speed" : "229 bps",
    "up_speed" : "186 Kbps",
    "sum_dn_bytes" : "1.5GB",
    "sum_up_bytes" : "812MB"
},
{
    "_id" : "583fd805f2b07cc79d2bd899",
    "hostname" : "MEIZU-PRO-5",
    "ip_address" : "192.168.10.100",
    "mac_address" : "68:3E:34:2A:A9:C9",
    "dn_speed" : "0.0 Kbps",
    "up_speed" : "0.0 Kbps",
    "sum_dn_bytes" : "3.93 MB",
    "sum_up_bytes" : "112 KB"
}


    ]

    nettraffic.find()
        //.select("")
        .limit(parseInt(params.length))
        .exec(function(err, servicecodes) {
            if(err) {
                data.recordsTotal  = -1;
                data.recordsFiltered  = -1;
                res.json(data);  
                console.log(err);
                
            } else {
                
                if(servicecodes.length>0)
                {
                data.recordsTotal  = servicecodes.length;
                data.recordsFiltered  = servicecodes.length;
                data.data = servicecodes;
                }
                else
                {
                data.recordsTotal  = sample.length;
                data.recordsFiltered  = sample.length;
                data.data = sample;
                console.log(JSON.stringify(sample));
                    
                }   
                res.json(data);    
            }
        });  
});


module.exports = router;
