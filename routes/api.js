var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var apiClients = require('../models/apiClients');
var serviceCode = require('../models/serviceCode');

var app = express();

router.get('/', function(req, res, next) {
  res.status(404).send("Not Found");
});


// getSession
// set routes
router.put('/getClients/:uuid', function(req, res){
    res.send("getClients:" + req.params.uuid + "/" + JSON.stringify(req.body));
});

router.put('/getSession/:uuid', function(req, res){
    res.send("getSession:" + req.params.uuid + "/" + JSON.stringify(req.body));
});


module.exports = router;
