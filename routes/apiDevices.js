var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var devices = require('../models/devices');

function check_login(req, res)
{
    if(req.session.wlink_session==null || req.session.wlink_session=="")
    {   
        res.status(401).send("unauthorized");
        return false;
    }

    return true;
}

// POST (new service code)
router.post('/', function(req, res){
    var service_code = req.body.service_code;
    var device_uuid = req.body.device_uuid;
    var device_type = req.body.device_type;
    var device_name = req.body.device_name;
    var device_key = req.body.device_key;
    var device_secret = req.body.device_secret;
    var expired_date = Date.parse(req.body.expired_date);

    if(check_login(req,res)==false) return false;
    req.session.touch();

    console.log("expired_date:"+expired_date);
    console.log('POST device: ' + JSON.stringify(req.body));

    var _devices= new devices({
        service_code: service_code,
        device_uuid: device_uuid,
        device_type: device_type, 
        device_name: device_name, 
        device_key: device_key, 
        device_secret: device_secret,
        expired_date: expired_date
    });

    _devices.save(function(err, item){
        if(err)
            res.status(500).send(err);
        else
            res.send(item);
    });
});

// put (update service code)
router.put('/:id', function(req, res){
    var id = req.params.id;
    var service_code = req.body.service_code;
    var device_uuid = req.body.device_uuid;
    var device_type = req.body.device_type;
    var device_name = req.body.device_name;
    var device_key = req.body.device_key;
    var device_secret = req.body.device_secret;
    var expired_date = Date.parse(req.body.expired_date);
    if(check_login(req,res)==false) return false;
    req.session.touch();
    
    devices.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
            res.status(400).send(err);
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
            {
                item.last_update_date = Date.now();
                item.service_code = service_code;
                item.device_uuid = device_uuid;
                item.device_type = device_type;
                item.device_name = device_name; 
                item.device_key = device_key; 
                item.device_secret = device_secret; 
                item.expired_date = expired_date;

                item.save(function(err, updateitem) {
                    if(err) {
                        res.status(500).send(err);
                    } else {
                        res.send(updateitem);
                    }
                });
            }
        }
    });
});

// delete (delte service code)
router.delete('/:id', function(req, res){
    var id = req.params.id;
    if(check_login(req,res)==false) return false;
    req.session.touch();
    
    devices.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
            res.status(400).send(err);
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
            {
                item.remove(function (err) {
                    if(err)
                        res.status(400).send(err);
                    else
                        res.send("Delete OK");
                });
            }
        }
    });
});

// get item info 
router.get('/:id', function(req, res){
    var id = req.params.id;
    if(check_login(req,res)==false) return false;
    req.session.touch();

    devices.findOne()
        .where('_id').equals(id)
        .exec(function(err, item) {
        if(err)
        {
            res.status(400).send(err);
            console.log(err);
        }
        else
        {
            if(item==null)
                res.status(404).send("No Data.");
            else
                res.send(item);
        }
    });
});

// get list for Datatables
router.get('/', function(req, res){
    var params = JSON.parse(JSON.stringify(req.body));

    // for sessiontest, do not check login 
    //if(check_login(req,res)==false) return false;
    req.session.touch();

    var data = {
        draw: params.draw,
        recordsTotal: 0,
        recordsFiltered: 0,
        data: []
    }

    devices.find()
        //.select("")
        .limit(parseInt(params.length))
        .exec(function(err, servicecodes) {
            if(err) {
                data.recordsTotal  = -1;
                data.recordsFiltered  = -1;
                res.json(data);  
                console.log(err);
                
            } else {
                data.recordsTotal  = servicecodes.length;
                data.recordsFiltered  = servicecodes.length;
                data.data = servicecodes;
                res.json(data);    
            }
        });  
});


module.exports = router;
