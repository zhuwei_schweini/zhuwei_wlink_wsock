var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var random_seed = require('random-seed');
var crypto = require('crypto');

var apiClients = require('../models/apiClients');

function check_login(req, res)
{
    if(req.session.wlink_session==null || req.session.wlink_session=="")
    {   
        res.status(401).send("unauthorized");
        return false;
    }

    return true;
}

// get list for Datatables
router.get('/', function(req, res){
    var params = JSON.parse(JSON.stringify(req.body));
    if(check_login(req,res)==false) return false;
    req.session.touch();

    var data = {
        draw: params.draw,
        recordsTotal: 0,
        recordsFiltered: 0,
        data: []
    }
 

    apiClients.find({"session_time": {"$gte": new Date()}})
        //.select("")
        .sort({session_time:-1})
        .limit(parseInt(params.length))
        .exec(function(err, servicecodes) {
            if(err) {
                data.recordsTotal  = -1;
                data.recordsFiltered  = -1;
                res.json(data);    
            } else {
                data.recordsTotal  = servicecodes.length;
                data.recordsFiltered  = servicecodes.length;
                data.data = servicecodes;
                res.json(data);    
            }
        });  
});

module.exports = router;
