// models/serviceCode.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var serviceCodeSchema = new Schema({
    service_name: String,
    service_code: String, 
    service_type: String, 
    service_tag: String, 
    service_hashkey: String, 
    expired_date: Date,
    session_expired_time: Number, 
    created_date: { type: Date, default: Date.now  },
    last_updated_date: { type: Date, default: Date.now  }
}, { collection: 'servicecodes' });
 
module.exports = mongoose.model('serviceCode', serviceCodeSchema);