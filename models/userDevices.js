// models/userDevices.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
// 서버스 사용자정보 기준으로 장치 정보관리 테이블
// 별도의 api를 만들지 말고, push관련 api와 통합해서 처리 
// user_key, login_token으로 구분. 
var userDevicesSchema = new Schema({
    service_code: String, 
    user_key: String,
    device_uuid: String,
    device_name: String,
    device_key: String,

    // 마지막으로 접근한 사용자 정보 
    last_client_uuid: String, //Schema.Types.ObjectId, 
    last_client_apikey: String, //Schema.Types.ObjectId, 
    last_login_token: String,        // email moble 등 인증 토큰
    
    created_time: { type: Date, default: Date.now  },
    last_updated_time: { type: Date, default: Date.now  },
}, { collection: 'userDevices' });
 
userDevicesSchema.index({ service_code: 1, user_key: 1, device_uuid:1 }); 
module.exports = mongoose.model('userDevices', userDevicesSchema);
