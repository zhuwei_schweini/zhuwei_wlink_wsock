// models/calorie.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var calorieSchema = new Schema({
    name: String,
    food: String,
    value: Number
}, { collection: 'calorie' });

module.exports = mongoose.model('calorie', calorieSchema);