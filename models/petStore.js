// models/petStore.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var petStoreSchema = new Schema({
    service_code: String, 
    name : String,
    source : String, // baidu, google, yahoo
    source_uuid : String, // unique id from source
    source_keyword1 : String,
    source_keyword2 : String,
    source_keyword3 : String,
    region : {
      country : String,  // CHINA, USA, CANADA, UK, Korea...
      province : String,  // GoungDong
      city : String  // ShenZhen
    },
    store_type : [String], // petshop, pethotel, pethospital, petgrooming, petbreeding
    geo_location : {
      type: { type: String },  // Point
      coordinates: []
    }, 
    description : String, 

    image_url : String,  // 대표 이미지
    ref_image_url : [String],  // 관련 이미지 

    // picture id of gridfs
    pic_id: String,  // 내부적으로 관리되는 파일명
    pic_key: String, // gridfs 키값
    // 파일 업로드 세션 키 
    file_session_key: String, 
    file_session_key_expired: Date,   // 유효시간 (최대 5분)

    address : String, 
    zipcode : String, 
    contact_no : String, 
    business_hours: String, 
    homepage : String,
    service_option : [String], // parking, reservation
    client_uuid : String,  // created client info
    client_apikey : String,  // created client info
    last_client_uuid : String, 
    last_client_apikey : String,
    created_date: { type: Date, default: Date.now  },
    last_updated_date: { type: Date, default: Date.now  }
}, { collection: 'petStore' });

petStoreSchema.index({ service_code: 1, name:1});
petStoreSchema.index({ service_code: 1, store_type: 1});
petStoreSchema.index({ service_code: 1, name:1, store_type: 1});
petStoreSchema.index({ geo_location: "2dsphere" });
module.exports = mongoose.model('petStore', petStoreSchema);