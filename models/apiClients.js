// models/apiclients.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var apiClientsSchema = new Schema({
    service_code: String, 
    uuid: String,
    key: String,
    secret: String,
    ref_counter: {type: Number, default:0},
    ip_address: {type:String, default:""},
    expired_date: Date,

    /* 2018.11.28 verification code */
    verification_method : {type:String, default:""}, // email or sms
    verification_type : {type:String, default:""},
    verification_code : {type:String, default:""},
    verification_updated : Date,
    verification_session_key : {type:String, default:""},
    verification_session_key_expired : Date,
    verification_code_used: {type:Boolean, default:false},
    verification_timed : Date,

    /* session informaiton */
    session_key: String,
    session_time: Date,
    is_device: Boolean,
    is_multiple_use: { type: Boolean, default: true} ,
    is_admin :  { type: Boolean, default: false},   // if true, uuid is userAdmin._id
    created_date: { type: Date, default: Date.now  },
    last_inquired_date: { type: Date, default: Date.now  }
}, { collection: 'apiclients' });
 
apiClientsSchema.index({ service_code: 1, uuid: 1 }); // schema level
module.exports = mongoose.model('apiClients', apiClientsSchema);