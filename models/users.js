// models/users.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
// 서버스 사용자 정보 관리
// 공통 정보, 외부계정 연동 고려. 
var userSchema = new Schema({
    service_code: String, 
    account_type: { type: String, default:"mobile"}, // mobile, wechat, qq, facebook, google, twiter and so on
    account_id: String,  // email이면 email, mobile이면 휴대폰 번호, 외부 연동이면 해당 서버의 access token
    //user_key: String,  // _id 값을 사용  

    name: String,
    first_name: String,
    last_name: String,
    gender: String,
    accept_terms_of_service: { type:Boolean, default:false},
    timezone: String,
    email: String,
    mobile: String,
    password: String,
    password_salt: String,
    password_hash_alg: String,
    birthday: Date,

    // 마지막으로 접근한 client id 
    last_client_uuid: String, //Schema.Types.ObjectId, 
    last_client_apikey: String, //Schema.Types.ObjectId, 

    // verification code: 이메일의 경우 사용 예정.
    verification_code: String,
    verification_code_expired: Date,  // 인증번호 유효기간 (기본 30분)
    verification_date: Date,  // 인증번호 유효기간 (기본 30분)
    verification_flag: { type:Boolean, default:false},  // 인증 여부 

    //random key
    random_key: String,        // 세션 확인을 위한 임시 인증번호
    random_key_expired: Date,   // 임시 인증번호 유효시간 (최대 5분)

    //아이디 생성시 이후 정상적인 로그인 접근을 확인하기 위한 값 
    //email이나 mobile일 경우 비밀번호 또는 인증코드를 로그인후 전달된다. 
    login_token: String,        // email moble 등 인증 토큰
    login_token_expired: Date,   // email moble 등 인증 토큰
    login_count: { type: Number, default: 0 }, 
    login_fail_count: { type: Number, default: 0 }, 
    login_fail_expired :Date ,  // 로그인 실패 초과시 다음 로그인 가능 시간 
    check_login_count: { type: Number, default: 0 }, 
    last_login_time: Date,

    // 2017.08.10
    last_login_uuid: String, // 마지막으로 로그인 성공한 client uuid 
    last_login_apikey: String, //마지막으로 로그인한 성공한 client apikey, 

    // access token 
    access_token: String,   
    access_token_expired: String,    

    // 2017.08.07 device를 통해서 로그인 접근이 가능하다. 
    // 마지막으로 접근한 device id 
    last_device_uuid: String, //Schema.Types.ObjectId, 
    last_device_apikey: String, //Schema.Types.ObjectId, 
    device_login_count: { type: Number, default: 0 }, 
    device_login_fail_count: { type: Number, default: 0 }, 
    last_device_login_time: Date,

    // AMAZON USER INFO for alexa
    amazon_id: String, 
    amazon_name: String,
    amazon_email: String, 
    amazon_access_token: String,

    // for testerid
    allow_multiple_login :  { type: Boolean, default: false },

    // picture id of gridfs
    pic_id: { type: String, default:""} ,  // 내부적으로 관리되는 파일명
    pic_key: String, // gridfs 키값
    // 파일 업로드 세션 키 
    file_session_key: String, 
    file_session_key_expired: Date,   // 유효시간 (최대 5분)

    created_time: { type: Date, default: Date.now  },
    last_updated_time: { type: Date, default: Date.now  },
}, { collection: 'users' });
 
userSchema.index({ service_code: 1, account_type: 1, account_id:1 }); 
userSchema.index({ service_code: 1, user_key: 1}); 
module.exports = mongoose.model('users', userSchema);

/*
  - 모든 유저관련 연동은 login_token 이 있어야 함
  - 로그인, 세션 업데이트가 login_token을 구하는 과정임
*/
// email / mobile
/*
  아래는 user_key를 모르는 상태에서 호출 가능
  - 등록 reg_user (apikey, type, id, password, user informatoin)  ==> user_key
  - 로그인  login (apikey, type, id, password) >> user_key, login_token, login_token_expire
  - 아이디 찾기 find_user(apikey, type, id) >> user_key(영구), random_no (5분 유효)

 아래는  login_token을 가지고 있는 상태에서 호출 가능 (로그인 정상 상태)
  - 사용자 정보 갱신 update_user(apikey, user_key, login_token, new user information)
  - 비번 갱신     update_password(apikey, user_key, login_token, new password)

  - 로그인 상태 확인 check_login (apikey, type, id, user_key, random_no, login_token) ==> login_token, login_token_expire

 아래는 꼭 폰번호 확인 후에 들어와야 함. 
  - 비번 분실 갱신 update_password2(apikey, type, id, user_key, random_no, new passowrd)
*/


// OAuth 사용
/*
  아래는 user_key를 모르는 상태에서 호출 가능
  - 등록 reg_user (apikey, type, id, access_token, user informatoin)  ==> user_key
  - 로그인  login (apikey, type, id, access_token)  ==> user_key, login_token, login_token_expire  
  - 아이디 찾기 find_user (apikey, type, id) >> user_key, random_no (5분 유효)

  아래는 폰번호에서 비번 변경과 동일... update_password2와 동일
  - 재등록** udpate_access_token (apikey, type, id, user_key, random_no, new_access_token)  ==> user_key

  아래는  login_token을 가지고 있는 상태에서 호출 가능 (로그인 정상 상태)
  - 사용자 정보 갱신 update_user(apikey, user_key, login_token, new user information)

  로그인 상태는 아니지만. 유효성 확인 및 정상일 경우 연장. 
  - 로그인 갱신 확인 check_login (apikey, type, id, user_key, random_no, login_token) ==> login_token, login_token_expire
*/
