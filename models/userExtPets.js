// models/userExtPets.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
// 서버스 사용자정보 기준으로 확정 정보인 pet 정보 관리를 위한 테이블
// 별도의 api를 만들지 말고, pet관련 api에서 로그인 후 획득 가능햔 user_key값을 기준으로 분류 처리 된다  
var userExtPetsSchema = new Schema({
    service_code: String, 
    user_key: String,
    pet_name: String,   // 외부 연동처리를 편하게 하기위해 이름을 별도 저장
    pet_key: String,    // 실제 pet정보가 관리되는 pet테이블의 key

    //20181110 디바이스 바인딩 정보 관리 
    //create나 update시에 요청 정보에 device_id가 존재하면 해당 장치에 등록된 pet정보 연결 시킨다. 
    //하나의 장치는 하나의 pet 정보만 가질 수 있다. 
    device_type : {type:String, default:"0"},  // 0 : user, 1: food feeder, 2 : tracker 
    device_uuid : {type:String, default:""},
    device_apikey : {type:String, default:""},
    device_pet_key : {type:String, default:""},

    // 등록 클라이언트 정보
    client_uuid: String, 
    client_apikey: String, 
    
    // 마지막으로 접근한 클라이언트 정보 
    last_client_uuid: String,
    last_client_apikey: String,

    ref_count: {type:Number, default: 0},
    
    created_time: { type: Date, default: Date.now  },
    last_updated_time: { type: Date, default: Date.now  },
}, { collection: 'userExtPets' });
 
userExtPetsSchema.index({ service_code: 1, user_key: 1 }); 
module.exports = mongoose.model('userExtPets', userExtPetsSchema);
