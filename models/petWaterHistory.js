// models/petwaterhistory.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var petWaterHistorySchema = new Schema({
    "service_code" : String,
    "device_uuid" : String,
    "device_manage_key" : { type:String, default:""} ,
    "pet_key" : { type:String, default:""} ,
    "type" : String,   // "intake" , "feed"
    "value" : [Number],   // 48pcs for every 30mins, 24hours*2
    "last_field_index" : Number, // 0 ~ 48 : 0 none / 48 : full
    "daily_sum" : { type:Number, default:0 },
    "udpate_count" : { type:Number, default:0 },
    "last_update_date" : { type: Date, default: Date.now  },
    'time_zone' : { type:String, default:""} ,
    "ts_tz" : Date,   // added timezone offset
    "ts" : Date,   // 20181115Z
    "date" : String   // 20181115Z
}, { collection: 'petWaterHistory' });

petWaterHistorySchema.index({ service_code: 1, device_uuid: 1 }); 
petWaterHistorySchema.index({ service_code: 1, device_uuid: 1, device_manage_key: 1 }); 
petWaterHistorySchema.index({ ts: 1 }); 
petWaterHistorySchema.index({ ts_tz: 1 }); 
petWaterHistorySchema.index({ date: 1 }); 
module.exports = mongoose.model('', petWaterHistorySchema);