// models/adminUsers.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var adminUsersSchema = new Schema({
    id: String, 
    password: String,
    name: String,
    department: String,
    confirmed_id: Boolean, 
    confirmed_date: Date,  
    created_date: { type: Date, default: Date.now  },
    last_login_date: Date,
    login_count: Number,

    // 
    client_uuid : String, 
    client_apikey : String
}, { collection: 'adminusers' });
 
module.exports = mongoose.model('adminUsers', adminUsersSchema);