// models/petHealth.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var petHealthSchema = new Schema({
    name: String,
    sex: {
        gentlemen: Number,
        szGentlemen: Number,
        lady: Number,
        szLady: Number
    },
    age: {
        one: Number,
        two: Number,
        three: Number,
        four: Number,
        five: Number,
        six: Number
    },
    weight: {
        one2two: Number
    },
    petType: {
        dog: Number,
        cat: Number
    },
    physiologyStatus: {
        pregnant: Number,
        lactation: Number,
        none: Number
    },
    feedPlan: {
        desc: Number,
        keep: Number,
        incr: Number
    }
}, { collection: 'healthFeed' });

module.exports = mongoose.model('healthFeed', petHealthSchema);