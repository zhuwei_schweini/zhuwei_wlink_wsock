// models/pets.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
// pet.io 서비스관련 DB
// pet info관리
var petSchema = new Schema({
    service_code: String, 
    name: String,
    type: String, // cat, dog    
    gender: String,  // male, female
    birthday : String, 
    breed_group: String,
    breed: String,
    neutralization : Boolean,
    weight: Number,
    height: Number,
    body_height: Number, 
    length: Number,
    back_length: Number,
    width: Number, 
    neck_length: Number,
    girth_length: Number,

    //2019.06.28  add pet pregenacy and food
    pregnancy: String,
    food: String,

    // 2018.5.4 장치 정보 관리를 위한 값. 클라이언트에서 전달되는 값 
    device_manage_key: String,
    // 2018.11.14 장치의 경우 장치 종류
    device_type : String, // 0 : user, 1 : food feeder, 2 : tracker

    // 소유 클라이언트(apiClient) 정보
    // 사용자 정보는 userPets에서 관리 
    client_uuid: String,
    client_apikey: String,
    last_client_uuid: String,
    last_client_apikey: String,

    // picture id of gridfs
    pic_id: { type: String, default:""} ,  // 내부적으로 관리되는 파일명
    pic_key: String, // gridfs 키값
    // 파일 업로드 세션 키 
    file_session_key: String, 
    file_session_key_expired: Date,   // 유효시간 (최대 5분)

    time_zone : String,
    // history data 정보 관련 필드
    last_food_intake_data : { type:Number, default:0},
    last_food_intake_date : Date,
    last_food_feed_data : { type:Number, default:0},
    last_food_feed_date : Date,

    last_water_intake_data : { type:Number, default:0},
    last_water_intake_date : Date,
    
    last_tracker_activity_data : { type:Number, default:0},
    last_tracker_activity_date : Date,
    last_tracker_location_data : { type:Number, default:0},
    last_tracker_location_date : Date,
    
    created_time: { type: Date, default: Date.now  },
    last_updated_time: { type: Date, default: Date.now  },
}, { collection: 'pets' });
 
petSchema.index({ service_code: 1, uuid: 1 }); 
petSchema.index({ service_code: 1, client_uuid: 1, client_key: 1 }); 
module.exports = mongoose.model('pets', petSchema);