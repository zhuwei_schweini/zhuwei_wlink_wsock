// models/devices.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var nettrafficSchema = new Schema({
    "hostname" : String,
    "ip_address" : String,
    "mac_address" : String,
    "dn_speed" : String,
    "up_speed" : String,
    "sum_dn_bytes" : String,
    "sum_up_bytes" : String
}, { collection: 'nettraffic' });
 
module.exports = mongoose.model('nettraffic', nettrafficSchema);