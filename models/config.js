var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var configSchema = new Schema({
  name: String, // english name
  id: String,   // unique id
  service_name: String,
  log_level: String,
  service_port: Number,
  admin_port: Number,
  mongodb_url: String,
  redis : [
      {
        port: Number,
        host: String
      }
  ], 
  io_port: Number,
  io_service: [
    {
        ns: String,
        service_code: String,
        exchange: String,
        amqp: String,
        baidu: {
                ak: String,
                sk: String
            },
        getui: {
                HOST: String,
                APPID: String,
                APPKEY: String,
                MASTERSECRET: String
            },
        apn_debug: {
                cert: String,
                key: String,
                production: Boolean
            },
        apn_release: {
                cert: String,
                key: String,
                production: Boolean
            }
    }
  ],
  api_port: Number,
  api_service: [
      {
          ns: String,
          service_code: Number,
          exchange: String,
          amqp: String,
          router: String
      }
  ]
}, { collection: 'config' });

module.exports = mongoose.model('config', configSchema);