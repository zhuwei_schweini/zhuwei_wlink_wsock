// models/cloudDeviceStorage.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var cloudDeviceStorageSchema = new Schema({
    service_code: String, 
    uuid: String,
    key_name: String,
    key_value: String,
    save_count: {type: Number, default:0},
    restore_count: {type: Number, default:0},    
    created_date: { type: Date, default: Date.now  },
    last_inquired_date: Date,
    last_updated_date: Date
}, { collection: 'cloudDeviceStorage' });
 
cloudDeviceStorageSchema.index({ service_code: 1, uuid: 1, key_name:1}); 

module.exports = mongoose.model('cloudDeviceStorage', cloudDeviceStorageSchema);
