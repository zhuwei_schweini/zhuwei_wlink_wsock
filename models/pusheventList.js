// models/pusheventList.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var pushevemtListSchema = new Schema({
    service_code: String, 
    client_uuid: String,
    platform: String,
    push_token : String,
    device_uuid: String,
    lang : String,
    event_time : { type: Date, default: Date.now  },
    event_msg : String,
    event_type : String,
    event_value : String,
    created_date: { type: Date, default: Date.now  },
}, { collection: 'pusheventList' });

pushevemtListSchema.index({ service_code: 1, platform: 1, push_token:1 }); // schema level
pushevemtListSchema.index({ event_time: -1 });

module.exports = mongoose.model('pusheventList', pushevemtListSchema);
