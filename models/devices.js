// models/devices.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
// service_code를 기준으로 cid를 사용할지가 결정된다. 
// socket.io를 사용해서 장치가 직접 client key를 생성하는 경우는
// device는 uuid가 장치마다 다르지만,
// 장치 생산시에 고정으로 client key를 사용하는 경우에는
// apiClients 상의  uuid가 cid이고, device uuid는 실제 장비의 uuid이다.
// 이것에 대한 구분은 이 테이블을 읽고 쓰는 프로세스에서 처리한다. 
// 이 테이블의 삽입은 보통 activeDevice 또는 reqHeaertbeat를 처리하는 프로세스에서 수행된다. 
// 
var devicesSchema = new Schema({
    service_code: String, 
    uuid: String,  // device_cid="" 이면 apiclients.uuid, 
    cid: String,   // apiclients.uuid
    client_key: String,  // apiclients.key
    expired_date: Date,

    name: String,
    type: String,  // service_code.type
    brand:String,
    category:String,
    sn:String,
    version:String, 

    created_time: { type: Date, default: Date.now  },
    last_updated_date: { type: Date, default: Date.now  },
    active_count: {type: Number, default: 0 },  // reqHeaertbeat 수신 횟수
    total_message_count: {type: Number, default: 0 },
    // 아래는 push notificaion을 위한 array
    push_clients: [{
        name: String,
        lang: String,
        client_uuid: String,
        token: String,
        device_key: String,
        platform: String,
        test:  {type: Number, default: 0 },
        // push_enabled 0: off, 1: off, -1: delete 
        push_enabled : {type: Number, default: 1 },
        push_interval : {type: Number, default: 0 },
        push_threshold : {type: Number, default: 0 },
        total_push_count : {type: Number, default: 0 },
        last_unsent_count : {type: Number, default: 0 },
        created_date : { type: Date, default: Date.now  },
        last_calling_time : String,
        last_updated_time : Date,
        last_push_time : String, // time string from event message 
        last_status: {type: Number, default: 0 },
        last_msg_time : Date,
        total_message_count: {type: Number, default: 0 }
    }]    
}, { collection: 'devices' });
 
devicesSchema.index({ service_code: 1, uuid: 1 }); 
module.exports = mongoose.model('devices', devicesSchema);