// models/pusheventBadge.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var pusheventBadgeSchema = new Schema({
    service_code: String, 
    platform: String, 
    token: String,     
    last_badge_count: Number,
    total_message_count: Number,
    create_date: { type: String, default: Date.now  },
    last_update_time: { type: String, default: Date.now  }
}, { collection: 'pusheventBadge' });

pusheventBadgeSchema.index({ service_code: 1, platform: 1, token:1 }); // schema level

module.exports = mongoose.model('pusheventBadge', pusheventBadgeSchema);
