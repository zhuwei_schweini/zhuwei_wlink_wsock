// models/petMedia.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var petMediaSchema = new Schema({
    service_code: String, 
    title  : {
      type: Map,
      of: String
    },
    source : String, // youtube,
    source_uuid : String, // source unique id for video
    source_channel : String,
    source_id : {
      type: Map,
      of: Schema.Types.Mixed
    },
    source_snippet : {
      type: Map,
      of: String
    },
    media_type : String, // tv, training, music
    pet_type : String, // dog, cat
    media_url : String, // media url
    source_keyword1 : String,
    source_keyword2 : String,
    source_keyword3 : String,
    description : String, 
    image_url : String,  // 대표 이미지
    // picture id of gridfs
    pic_id: String,  // 내부적으로 관리되는 파일명
    pic_key: String, // gridfs 키값
    // 파일 업로드 세션 키 
    file_session_key: String, 
    file_session_key_expired: Date,   // 유효시간 (최대 5분)
    status : { type:String, default:"0"},  // 0 : normal, 9 : deleted
    created_date: { type: Date, default: Date.now  },
    last_updated_date: { type: Date, default: Date.now  }
}, { collection: 'petMedia' });

module.exports = mongoose.model('petMedia', petMediaSchema);