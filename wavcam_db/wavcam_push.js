// models/apiclients.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment-timezone');
 
var wavcam_push_schema = new Schema({
    groupid: String, 
    uid: String, 
    last_msg_time: String,
    total_message_count: { type:Number, default:0 } , 
    clients: { type:[{
        name: String, 
        lang: Number, 
        token: String, 
        userkey: String,
        platform: String, 
        test : String,
        push_enabled: { type:Number, default: 1}, 
        push_interval: { type:Number, default: 0}, 
        push_threshold: { type:Number, default: 0}, 
        total_push_count: { type:Number, default: 0}, 
        last_unsent_count: { type:Number, default: 0},
        last_push_time: String,
        create_time: { type: String, default: moment().format('YYYYMMDDHHmmss')  },
        last_calling_time: { type: String, default: moment().format('YYYYMMDDHHmmss')  },
        last_msg_time: String,
        last_status: Number,
        total_message_count: Number,
        update_time: String
    }], default:[]},
    create_time: { type: String, default: moment().format('YYYYMMDDHHmmss')  },
    last_update_time: { type: String, default: moment().format('YYYYMMDDHHmmss')  }
}, { collection: 'wavcam_push' });
 
module.exports = mongoose.model('wavcam_push', wavcam_push_schema);
