// models/apiclients.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment-timezone');
 
var wavcam_activation_schema = new Schema({
    groupid: String, 
    uid: String, 
    last_msg_time: String,
    inactivate_flag: { type:Number, default:0 } , 
    create_time: { type: String, default: moment().format('YYYYMMDDHHmmss')  },
    last_update_time: { type: String, default: moment().format('YYYYMMDDHHmmss')  }
}, { collection: 'wavcam_activation' });
 
module.exports = mongoose.model('wavcam_activation', wavcam_activation_schema);
