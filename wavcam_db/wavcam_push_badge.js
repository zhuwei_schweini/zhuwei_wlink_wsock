// models/apiclients.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment-timezone');
 
var wavcam_push_badge_schema = new Schema({
    groupid: String, 
    platform: String, 
    token: String,     
    last_badge_count: Number,
    total_message_count: Number,
    create_date: { type: String, default: moment().format('YYYYMMDDHHmmss')  },
    last_update_time: { type: String, default: moment().format('YYYYMMDDHHmmss')  }
}, { collection: 'wavcam_push_badge' });
 
module.exports = mongoose.model('wavcam_push_badge', wavcam_push_badge_schema);
