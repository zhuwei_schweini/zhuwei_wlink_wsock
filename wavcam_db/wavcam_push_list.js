// models/apiclients.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment-timezone');
 
var wavcam_push_list_schema = new Schema({
    groupid: String,
    platform: String, 
    uid : {type: String, trim : true, index:true},
    token : {type: String, trim : true, index:true},
    eventAction : String,
    eventTime : {type: String, trim : true, index:true},
    lang : String,
    msg : String,
    create_date: { type: String, default: moment().format('YYYYMMDDHHmmss')  }
}, { collection: 'wavcam_push_list' });
 
module.exports = mongoose.model('wavcam_push_list', wavcam_push_list_schema);
