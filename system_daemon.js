var amqp = require('amqplib');
var moment = require('moment-timezone');
var mongoose = require('mongoose');
var crypto = require('crypto');
var uuid = require('node-uuid');
var path = require('path');
var util = require('util');
var waviotmsg = require("./modules/waviotmsg");
var serviceCode = require('./models/serviceCode');
var devices = require('./models/devices');
var pushEvent = require('./models/pusheventList');
var pushEventBadge = require('./models/pusheventBadge');
var apiClients = require('./models/apiClients');
var configSchema = require('./models/config.js');
//var config = require(path.join(__dirname, 'config'));
var event_config = require(path.join(__dirname, 'event_config'));
var cloudDeviceStorage = require('./models/cloudDeviceStorage.js');

mongoose.Promise = global.Promise;
// PUSH 서비스에 따라서 token의 유효성 결과를 돌려주는 경우가 있음. (현재 IOS)
// 그 정보를 기준으로 관리
var badDeveiceToken = [];

// apn npm install -S apn
var apn = require('apn');
// baidu pushing npm install -S bpush-nodejs
var bpush = require('bpush-nodejs');

// getui push system  
// getui는 수정한 모듈을 직접 모듈 디렉토리에 복사 후 아래 패키지 설치 필요 
// npm install request --save
// npm install protobufjs@3.8.2 --save
var GeTui = require('./getui/GT.push');
var GeTui_Target = require('./getui/getui/Target');
var GeTui_NotificationTemplate = require('./getui/getui/template/NotificationTemplate');
var GeTui_TransmissionTemplate = require('./getui/getui/template/TransmissionTemplate');
var GeTui_SingleMessage = require('./getui/getui/message/SingleMessage');

var SERVICE_CODE = "WLINK_A0001";
var system_UID = "SYSTEM_DAEMON_" + SERVICE_CODE;
var system_Queue = "system_queue_" + SERVICE_CODE;
var system_rpc_key = "rpc_" + SERVICE_CODE;
var curServiceCode;
var amqpUrl;
var amqp_exchange;

var baidu_config;
var getui_config;
var apns_real_config;
var apns_debug_config;
var apns_topic;

var log_level;
var name;
var io_service;

///////////////////////////////////////////////////////////////////////////////
// load configuration
var envrioment = process.env.NODE_ENV;
var service_type = process.env.WLINK_SVC;
var service_code = process.env.SERVICE_CODE;
//global.config = config.real;

console.log("service_type", service_type);
if (service_type != null && service_type != "") {
    console.log("daemon >>> WLINK SYSTEM :", service_type);
    //global.config = config[service_type];
}
else
    console.log(">>> use REAL configuration!!!");

///////////////////////////////////////////////////////////////////////////////

//global.config.path = path.join(__dirname);

if (service_code != null && service_code != "") {
    SERVICE_CODE = service_code;
    system_UID = "SYSTEM_DAEMON_" + SERVICE_CODE;
    system_Queue = "system_queue_" + SERVICE_CODE;
    system_rpc_key = "rpc_" + SERVICE_CODE;
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
process.on('uncaughtException', function (err) {
    logger.info('uncaughtException 발생 : ' + err);
    // 그냥 재시작
    process.exit();
});

//////////////////////////////////////////////////////////////////////////////////////////
async function connect_mongodb() {
    // DB Connection
    try {
        var db = mongoose.connection;

        // db.on("error", function (err) {
        //     logger.info("DB Error: " + err)
        // })
        await mongoose.connect(process.env.DB, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false });
        db.once("open", function () {
            logger.info("DB is connected");
            //system_daemon();
        });

        var configRes = await configSchema.findOne({ "name": service_type });

        if (configRes != null) {
            console.log("findOne log_level : " + configRes.log_level);
            console.log("findOne io_service.length : " + configRes.io_service.length);
            log_level = configRes.log_level;
            name = configRes.name;
            io_service = configRes.io_service;
    
            // logger.level = log_level; //'debug';
            // global.logger = logger;
            for (var i = 0; i < io_service.length; i++) {
                if (io_service[i].service_code == SERVICE_CODE) {
                    amqpUrl = io_service[i].amqp;
                    amqp_exchange = io_service[i].exchange;
                    logger.info("FOUND amqp information. exhg[%s]", amqp_exchange);
    
                    baidu_config = io_service[i].baidu;
                    getui_config = io_service[i].getui;
                    apns_debug_config = io_service[i].apn_debug;
                    apns_real_config = io_service[i].apn_release;
                    apns_topic = io_service[i].apn_topic;
                    logger.info("FOUND push information. baidu[%s]", JSON.stringify(baidu_config));
                    logger.info("FOUND push information. getui[%s]", JSON.stringify(getui_config));
                    logger.info("FOUND push information. apn_debug[%s]", JSON.stringify(apns_debug_config));
                    logger.info("FOUND push information. apn_release[%s]", JSON.stringify(apns_real_config));
                    logger.info("FOUND push information. apn_topic[%s]", JSON.stringify(apns_topic));
                }
            }
    
            if (amqpUrl == null || amqpUrl == "") {
                logger.info("cannot found a amqp information!!");
                process.exit(1);
            }
    
            if ((baidu_config == null && getui_config == null) || apns_debug_config == null || apns_real_config == null) {
                logger.info("cannot found push informations!!");
                process.exit(1);
            }

            apns_real = new apn.Provider(apns_real_config);
            apns_test = new apn.Provider(apns_debug_config);

            system_daemon();
        }
        
    } catch (error) {
        logger.info("DB Error: " + error)
    }
}

var Logger = require('./modules/logger');

var logger = new Logger('system_daemon_' + SERVICE_CODE);
logger.level = process.env.LOG_LEVEL; //'debug';
global.logger = logger;


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

async function make_resHeartbeat_body(targetUID, response) {
    var resHeartbeat_msg = { targetUID: targetUID, systemTime: moment().format('YYYYMMDDHHmmssZZ') };
    if (response != null && response != "")
        resHeartbeat_msg.response = response;
    return resHeartbeat_msg
}

async function make_cloudSaveValue_body(targetUID, resCode, key) {
    var cloudSaveValue_msg = { targetUID: targetUID, resCode: resCode, key: key, systemTime: moment().format('YYYYMMDDHHmmssZZ') };

    return cloudSaveValue_msg
}


async function make_cloudGetValue_body(targetUID, resCode, key, val) {
    var cloudGetValue_msg = { targetUID: targetUID, resCode: resCode, key: key, value: val, systemTime: moment().format('YYYYMMDDHHmmssZZ') };

    return cloudGetValue_msg
}

async function update_devices(service_code, uuid, cid, client_key, expired_date, name, type, brand, category, sn, version) {
    if (cid == "" || cid == null)
        devices.findOne()
            .where('service_code').equals(service_code)
            .where('uuid').equals(uuid)
            .exec(function (err, item) {
                if (err) {
                    logger.info("device activation : DB FIND ERROR!!", service_code, uuid);
                }
                else {
                    if (item == null)  // new item
                    {
                        var newDevice = new devices({
                            service_code: service_code,
                            uuid: uuid,
                            cid: "",
                            client_key: client_key,
                            type: type,
                            name: name,
                            brand: brand,
                            category: category,
                            sn: sn,
                            version: version,
                            expired_date: expired_date
                        });

                        devices.findOneAndUpdate(
                            { service_code: service_code, uuid: uuid },
                            newDevice,
                            { upsert: true, new: true, runValidators: true }, // options
                            function (err, doc) { // callback
                                if (err)
                                    logger.info("device activation : DB APPEND ERROR!!", service_code, uuid, curServiceCode.service_type);
                                else
                                    logger.info("device activation : New device", service_code, uuid, curServiceCode.service_type);
                            }
                        );

                        /*
                        
                                            newDevice.save(function(err, item){
                                                if(err)
                                                    logger.info("device activation : DB APPEND ERROR!!", service_code, uuid, curServiceCode.service_type);
                                                else
                                                    logger.info("device activation : New device", service_code, uuid, curServiceCode.service_type);
                                            });
                        */
                    }
                    else {
                        item.last_updated_date = Date.now();
                        item.active_count++;

                        if (type != null && type != "")
                            item.type = type;
                        if (name != null && name != "")
                            item.name = name;
                        if (brand != null && brand != "")
                            item.brand = brand;
                        if (category != null && category != "")
                            item.category = category;
                        if (sn != null && sn != "")
                            item.sn = sn;
                        if (version != null && version != "")
                            item.version = version;

                        item.save(function (err, updateitem) {
                            if (err) {
                                logger.info("device activation : DB UPDATE ERROR!!", service_code, uuid, curServiceCode.service_type);
                            }
                        });

                    }
                }
            });
    else
        devices.findOne()
            .where('service_code').equals(service_code)
            .where('uuid').equals(uuid)
            .where('cid').equals(cid)
            .exec(function (err, item) {
                if (err) {
                    logger.info("device activation : DB FIND ERROR!!", service_code, uuid, cid);
                }
                else {
                    if (item == null)  // new item
                    {
                        var newDevice = new devices({
                            service_code: service_code,
                            uuid: uuid,
                            cid: cid,
                            client_key: client_key,
                            type: type,
                            brand: brand,
                            category: category,
                            sn: sn,
                            version: version,
                            expired_date: expired_date
                        });

                        devices.findOneAndUpdate(
                            { service_code: service_code, uuid: uuid, cid: cid },
                            newDevice,
                            { upsert: true, new: true, runValidators: true }, // options
                            function (err, doc) { // callback
                                if (err)
                                    logger.info("device activation : DB APPEND ERROR!!", service_code, uuid, cid, curServiceCode.service_type);
                                else
                                    logger.info("device activation : New device", service_code, uuid, cid, curServiceCode.service_type);
                            }
                        );
                        /*
                                            newDevice.save(function(err, item){
                                                if(err)
                                                    logger.info("device activation : DB APPEND ERROR!!", service_code, uuid, cid, curServiceCode.service_type);
                                                else
                                                    logger.info("device activation : New device", service_code, uuid, cid, curServiceCode.service_type);
                                            });
                        */
                    }
                    else {
                        item.last_updated_date = Date.now();
                        item.active_count++;
                        if (type != null && type != "")
                            item.type = type;
                        if (name != null && name != "")
                            item.name = name;
                        if (brand != null && brand != "")
                            item.brand = brand;
                        if (category != null && category != "")
                            item.category = category;
                        if (sn != null && sn != "")
                            item.sn = sn;
                        if (version != null && version != "")
                            item.version = version;
                        item.save(function (err, updateitem) {
                            if (err) {
                                logger.info("device activation : DB UPDATE ERROR!!", service_code, uuid, cid, curServiceCode.service_type);
                            }
                        });

                    }
                }
            });


}

async function update_device_activation(msgobj) {
    var service_code = SERVICE_CODE;
    var uuid = msgobj.header.deviceID;
    var cid = "";
    var client_key = msgobj.header.clientKey;
    var name = "";
    var brand = "";
    var type = "";
    var category = "";
    var sn = "";
    var version = "";

    if (msgobj.header.cid != null)
        cid = msgobj.header.cid;

    if (msgobj.body.name != null)
        name = msgobj.body.name;
    else
        name = uuid;

    if (msgobj.body.brand != null)
        brand = msgobj.body.brand;
    if (msgobj.body.type != null)
        type = msgobj.body.type;
    else
        type = curServiceCode.service_type;

    if (msgobj.body.category != null)
        category = msgobj.body.category;
    if (msgobj.body.sn != null)
        sn = msgobj.body.sn;
    if (msgobj.body.version != null)
        version = msgobj.body.version;

    // check uuid validation
    if (cid == null || cid == "")
        apiClients.findOne()
            .where('service_code').equals(service_code)
            .where('uuid').equals(uuid)
            .where('key').equals(client_key)
            .exec(function (err, item) {
                if (err)
                    logger.info("device activation.uuid : DB FIND ERROR!!", service_code, uuid, client_key);
                else
                    if (item == null)
                        logger.info("device activation.uuid : Invalid UUID!! [1]", service_code, uuid, client_key);
                    else
                        update_devices(service_code, uuid, cid, client_key, item.expired_date, name, type, brand, category, sn, version);
            });
    else
        apiClients.findOne()
            .where('service_code').equals(service_code)
            .where('uuid').equals(cid)
            .where('key').equals(client_key)
            .exec(function (err, item) {
                if (err)
                    logger.info("device activation.uuid : DB FIND ERROR!!", service_code, cid, uuid, client_key);
                else
                    if (item == null)
                        logger.info("device activation.uuid : Invalid UUID!! [2]", service_code, cid, uuid, client_key);
                    else
                        update_devices(service_code, uuid, cid, client_key, item.expired_date, name, type, brand, category, sn, version);
            });
}

async function reqHeartbeatHandler(amqp_ch, amqp_msg, msgobj, hashkey) {
    var challenge = msgobj.body.challenge;
    var resBody = await make_resHeartbeat_body(msgobj.header.deviceID, challenge + 1);

    // device activation 관리 
    await update_device_activation(msgobj);

    var resHeartbeat = await waviotmsg.make_waviot_msg("resHeartbeat", SERVICE_CODE, system_UID, "", "", hashkey, resBody);

    logger.debug("resHeartbeat:" + JSON.stringify(resHeartbeat));
    logger.info("resHeartbeat:" + resHeartbeat.body.targetUID, resHeartbeat.body.response);
    await amqp_ch.publish(amqp_exchange, amqp_msg.properties.replyTo, new Buffer.from(JSON.stringify(resHeartbeat)),
        { contentType: "application/json", deliveryMode: 2, correlationId: amqp_msg.properties.correlationId });
}

async function sendCloudStorageResponseMessage(amqp_ch, amqp_msg, msgID, hashkey, deviceID, resCode, key, value) {
    var resBody;

    if (msgID == "cloudSaveValue")
        resBody = make_cloudSaveValue_body(deviceID, 0, key);
    else
        resBody = make_cloudGetValue_body(deviceID, 0, key, value);

    var cloudStorageValue = waviotmsg.make_waviot_msg(msgID, SERVICE_CODE, system_UID, "", "", hashkey, resBody);
    logger.info(msgID + ":" + JSON.stringify(cloudStorageValue));
    amqp_ch.publish(amqp_exchange, amqp_msg.properties.replyTo, new Buffer(JSON.stringify(cloudStorageValue)),
        { contentType: "application/json", deliveryMode: 2, correlationId: amqp_msg.properties.correlationId });

}

async function cloudSaveValueHandler(amqp_ch, amqp_msg, msgobj, hashkey) {
    var service_code = SERVICE_CODE;
    var uuid = msgobj.header.deviceID;
    var key = msgobj.body.key;
    var value = msgobj.body.value;

    cloudDeviceStorage.findOne()
        .where('service_code').equals(service_code)
        .where('uuid').equals(uuid)
        .where('key_name').equals(key)
        .exec(function (err, item) {
            if (err) {
                logger.info("cloudSaveValueHandler : DB FIND ERROR!!", service_code, uuid, key, value);
                sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudSaveValue", hashkey, msgobj.header.deviceID, -1, key, null);
            }
            else {
                if (item == null) {
                    var newCloudValue = new cloudDeviceStorage({
                        service_code: service_code,
                        uuid: uuid,
                        key_name: key,
                        key_value: value
                    });

                    cloudDeviceStorage.findOneAndUpdate(
                        { service_code: service_code, uuid: uuid, key_name: key },
                        newCloudValue,
                        { upsert: true, new: true, runValidators: true }, // options
                        function (err, doc) { // callback
                            if (err) {
                                logger.info("cloudSaveValueHandler : DB APPEND ERROR!!");
                                sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudSaveValue", hashkey, msgobj.header.deviceID, -1, key, null);
                            }
                            else {
                                sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudSaveValue", hashkey, msgobj.header.deviceID, 0, key, null);
                            }
                        }
                    );
                    /*
                                    newCloudValue.save(function(err, item){
                                        if(err)
                                        {
                                            logger.info("cloudSaveValueHandler : DB APPEND ERROR!!");
                                            sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudSaveValue", hashkey, msgobj.header.deviceID, -1, key, null);
                                        }
                                        else₩
                                        {
                                            sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudSaveValue", hashkey, msgobj.header.deviceID, 0, key, null);
                                        }
                                    });
                    */
                }
                else {
                    item.last_updated_date = Date.now();
                    item.key_value = value;
                    item.save_count++;
                    item.save(function (err, updateitem) {
                        if (err) {
                            logger.info("cloudSaveValueHandler : DB UPDATE ERROR!!");
                            sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudSaveValue", hashkey, msgobj.header.deviceID, -1, key, null);
                        } else {
                            sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudSaveValue", hashkey, msgobj.header.deviceID, 0, key, null);
                        }
                    });

                }
            }
        });
}

async function cloudGetValueHandler(amqp_ch, amqp_msg, msgobj, hashkey) {
    var service_code = SERVICE_CODE;
    var uuid = msgobj.header.deviceID;
    var key = msgobj.body.key;
    var value = "";

    cloudDeviceStorage.findOne()
        .where('service_code').equals(service_code)
        .where('uuid').equals(uuid)
        .where('key_name').equals(key)
        .exec(function (err, item) {
            if (err || item == null) {
                if (err) {
                    logger.info("cloudGetValueHandler : DB FIND ERROR!!", service_code, uuid, key, value);
                    sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudGetValue", hashkey, msgobj.header.deviceID, -1, key, value);
                }
                else
                    sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudGetValue", hashkey, msgobj.header.deviceID, 404, key, value);
            }
            else {
                item.last_inquired_date = Date.now();
                item.restore_count++;
                value = item.key_value;
                console.log("cloudGetValueHandler :Update Referce count!", service_code, uuid, key, value);
                item.save(function (err, updateitem) {
                    if (err) {
                        logger.info("cloudGetValueHandler : DB UPDATE ERROR but ignore!!");
                        sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudGetValue", hashkey, msgobj.header.deviceID, 0, key, value);
                    } else {
                        sendCloudStorageResponseMessage(amqp_ch, amqp_msg, "cloudGetValue", hashkey, msgobj.header.deviceID, 0, key, value);
                    }
                });
            }
        });
}

async function activeDeviceHandler(amqp_ch, amqp_msg, msgobj, hashkey) {
    update_device_activation(msgobj);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//apns_test = APNs(use_sandbox=True, cert_file='/home/wavlink/service/cer_debug.pem', key_file='/home/wavlink/service/cer_debug.pem' )
//apns_real = APNs(use_sandbox=False, cert_file='/home/wavlink/service/cer_release.pem', key_file='/home/wavlink/service/cer_release.pem' )
//var apns_real = new apn.Provider({  cert: "./cer_release.pem",  key: "./cer_release.pem",});
//var apns_test = new apn.Provider({  cert: "./cer_release.pem",  key: "./cer_release.pem",});
var apns_real ;
var apns_test ;

async function send_ios_push(token_hex, mesg, badge_count, test_flag) {
    try {
        var wavcam_client = [token_hex];
        let note = new apn.Notification({
            alert: mesg, badge: badge_count, sound: "default"
        });
        note.topic = apns_topic;
        logger.debug("Sending: [%s] to [%s]", note.compile(), token_hex);

        if (test_flag == 1)
            apns_test.send(note, wavcam_client).then(result => {
                logger.debug("IOS TEST sent:", result.sent.length);
                logger.debug("IOS TEST failed:", result.failed.length);
                // BadDeviceToken 에러에 대한 처리 필요. 
                if (result.failed.length) {
                    if (JSON.stringify(result.failed).indexOf("BadDeviceToken") != -1) {
                        if (badDeveiceToken.indexOf(token_hex) == -1) {
                            badDeveiceToken.push(token_hex);
                            logger.info("Found IOS BAD DEVICE TOKEN", token_hex);
                        }
                    }
                }
            });
        else
            apns_real.send(note, wavcam_client).then(result => {
                logger.debug("IOS PROD sent:", result.sent.length);
                logger.debug("IOS PROD failed:", result.failed.length);
                logger.debug(result.failed);
                // BadDeviceToken 에러에 대한 처리 필요. 
                if (result.failed.length) {
                    if (JSON.stringify(result.failed).indexOf("BadDeviceToken") != -1) {
                        if (badDeveiceToken.indexOf(token_hex) == -1) {
                            badDeveiceToken.push(token_hex);
                            logger.info("Found IOS BAD DEVICE TOKEN", token_hex);
                        }
                    }
                }
            });
    } catch (error) {
        logger.info("send_ios_push ERROR:", error)
    }
}

async function send_baidu_push(token_hex, title, mesg, badge_count, test_flag) {
    var tconfig = baidu_config; //{ak:"vG6c7iLiujK451rHc4BitsZO", sk:"Hags4aQF4z7Hz2wj1ugFtuAamQZQjaUc"};
    //mesg = mesg.replace('\n', '\n\n');

    //opts = {msg_type:0, expires:300} 
    var data = {
        msg: JSON.stringify({
            title: title,
            description: mesg,
            notification_builder_id: 10
        }),
        channel_id: token_hex,
        msg_type: bpush.constant.MSG_TYPE.NOTIFICATION,
        //deploy_status: bpush.constant.DEPLOY_STATUS.DEVELOPMENT,
        device_type: bpush.constant.DEVICE_TYPE.ANDROID,
        expires: 300

    };

    //new_push_msg = unicode(str(push_msg), "utf-8")
    //new_token_hex = unicode(str(token_hex), "utf-8")

    bpush.sendRequest(bpush.apis.pushMsgToSingleDevice, data, null, null, tconfig).then(function (data) {
        data = JSON.parse(data);
        logger.debug("send_baidu_push:", data);
    }).catch(function (e) {
        logger.info("send_baidu_push ERROR:", e);
    });

}

//////////////////////////////////////////////////////////////////////////////////////
async function send_getui_push(token_hex, title, mesg, badge_count, test_flag) {
    var tconfig = getui_config;
    // {"HOST": "'http://sdk.open.api.igexin.com/apiex.htm'", "APPID": "p8xdRj7MgTAylkdrte48D8", "APPKEY":"N0gxmBURcaAsc3sSzcgqE1", "MASTERSECRET":"Ab7j71gwtq7o1kCzLpSaU2"},
    var gt = new GeTui(tconfig["HOST"], tconfig["APPKEY"], tconfig["MASTERSECRET"]);

    var template = new GeTui_TransmissionTemplate({
        appId: tconfig["APPID"],
        appKey: tconfig["APPKEY"],
        transmissionType: 2,
        transmissionContent: mesg
    });
    /*    
        var template = new GeTui_NotificationTemplate({
            appId: tconfig["APPID"],
            appKey: tconfig["APPKEY"],
            title: title,
            text: mesg,
            //logo: 'http://www.igetui.com/logo.png',
            isRing: true,
            isVibrate: true,
            isClearable: true,
            transmissionType: 1,
            transmissionContent: mesg
        });    
    */
    var message = new GeTui_SingleMessage({
        isOffline: true,                        //是否离线
        offlineExpireTime: 3600 * 12 * 1000,    //离线时间
        data: template,                          //设置推送消息类型
        pushNetWorkType: 0                     //是否wifi ，0不限，1wifi
    });

    var target = new GeTui_Target({
        appId: tconfig["APPID"],
        clientId: token_hex
    });

    gt.pushMessageToSingle(message, target, function (err, res) {
        logger.debug("send_getui_push:", res);
        if (err != null && err.exception != null && err.exception instanceof RequestError) {
            var requestId = err.exception.requestId;
            logger.info("send_getui_push ERROR:", err.exception.requestId);
            gt.pushMessageToSingle(message, target, requestId, function (err, res) {
                logger.info("send_getui_push RETRY:", err);
                logger.info("send_getui_push RETRY:", res);
            });
        }
    });
}

function send_push_message(platform, token_hex, title, mesg, badge_count, test_flag) {
    if (platform == "ios") {
        logger.info("Send Push[ios] to [" + token_hex + "]!!")
        send_ios_push(token_hex, mesg, badge_count, test_flag);
    }
    else if (platform == "baidu") {
        logger.debug("Send Push[baidu] to [" + token_hex + "]!!")
        send_baidu_push(token_hex, title, mesg, badge_count, test_flag)
        //send_baidu_push("4108108582277042053", "test", "wavvam mesg", test_flag)
    }
    else if (platform == "getui") {
        logger.debug("Send Push[getui] to [" + token_hex + "]!!")
        send_getui_push(token_hex, title, mesg, badge_count, test_flag)
        //send_getui_push("2ee67c41966edadd397392b7932b081d", title, mesg, badge_count, test_flag)
    }
    else if (platform == "android") {

    }
    else if (platform == "qq") {

    } else {

    }
}


function insert_db_push_list(platform, lang, client_uuid, device_uuid, token, event_time, event_type, event_value, push_msg) {
    var new_push_event = new pushEvent({
        service_code: SERVICE_CODE,
        client_uuid: client_uuid,
        platform: platform,
        push_token: token,
        device_uuid: device_uuid,
        lang: lang,
        event_time: event_time.toDate(), // 이미  moment object
        event_msg: push_msg,
        event_type: event_type,
        event_value: event_value
    });

    new_push_event.save(function (err, newitem) {
        if (err) {
            logger.info("insert_db_push_list new:", err);
            return -1;
        }
        else {
            logger.debug("insert_db_push_list new: OK");
            return 1;
        }
    });
    return 0;
}

function make_push_message(event_type, lang, device_uuid, time, name, event_value) {
    var mapMessage = event_config[SERVICE_CODE][event_type];
    try {
        if (event_value != null && event_value != "")
            return util.format(mapMessage[lang], event_value, time, name);
        else
            return util.format(mapMessage[lang], time, name);
    } catch (error) {
        return "";
    }
}

// event_time : ISO DATE Type 문자열
// timestr : 장치 기준의 타임존으로 쉽게 읽을 수 있는 문자열 "2017/01/01 12:31:21"" 
function send_push_notification(client_uuid, device_uuid, platform, token, event_time, event_type, lang, name, timestr, event_value, push_test_mode, new_badge_count) {
    logger.info("send_push_notification: SERVICE_CODE [%s], CLIENT[%s], DEVICE [%s], platform [%s], token [%s], event_time [%s], event_type [%s], lang [%s], name [%s], timestr [%s], push_test_mode [%d], new_badge_count [%d]",
        SERVICE_CODE, client_uuid, device_uuid, platform, token, event_time, event_type, lang, name, timestr, push_test_mode, new_badge_count);

    try {
        push_msg = make_push_message(event_type, lang, device_uuid, timestr, name, event_value);
        if (push_msg != "") {
            // 이함수는 내부에서 비동기로 호출 처리됨.
            insert_db_push_list(platform, lang, client_uuid, device_uuid, token, event_time, event_type, event_value, push_msg);

            if (push_test_mode == 1) {
                logger.debug("pushEvent : send event(T) platform:" + platform + ", token:" + token + ", msg: " + push_msg + ", badge: " + new_badge_count);
                logger.info("pushEvent : send event(T) platform:%s, token:%d, UID:%s", platform, token, device_uuid);
                send_push_message(platform, token, name, push_msg, new_badge_count, 1);

            } else {
                logger.debug("pushEvent : send event(R) platform:" + platform + ", token:" + token + ", msg: " + push_msg + ", badge: " + new_badge_count);
                logger.info("pushEvent : send event(R) platform:%s, token:%d, UID:%s", platform, token, device_uuid);
                send_push_message(platform, token, name, push_msg, new_badge_count, 0);
            }
        }

    } catch (error) {
        logger.info("send_push_notification error", error);
    }
}


/*
#  return 0 : always do not send
#         1 : always send
#         -1 : do not send because of push time limit
#         2 : send before the time limit
#         3 : send over the threshold of message
#         -2 : no activation from client
#         -3 : user_key error
#         -4 : bad device token
*/
function check_push_information(device_uuid, token, msg_time, device_key, client) {
    var service_code = SERVICE_CODE;

    // check push_enabled
    if (client.push_enabled == null || client.push_enabled <= 0)
        return 0;

    if (badDeveiceToken.indexOf(token) != -1)
        return -4; // bad device token

    // clients session check 
    var session_days = 6
    var expired = moment();
    expired.add(-session_days, "day");
    session_time_str = expired.format('YYYYMMDDHHmmss');
    logger.debug(" TIME before 6days: ", session_time_str);
    if (client.last_calling_time != null && client.last_calling_time < session_time_str)
        return -2;  // 6일 동안 클라이언트 접속 기록이 없으면 메시지를 보내지 않는다. 

    client_device_key = client.device_key == null ? '' : client.device_key;
    if (device_key != '' && device_key != client_device_key) {
        logger.debug(">>>>> device_key:", device_key, "client_device_key:", client_device_key);
        return -3; // 암호가 변경된 경우 보내지 않는다. 
    }

    // 알림 발송 갯수 제한이 없을 경우에는 항상 전송한다.  
    push_interval = client.push_interval == null ? 0 : client.push_interval;
    push_threshold = client.push_threshold == null ? 0 : client.push_threshold;
    if (push_interval == 0 && push_threshold == 0)
        return 1;

    // check client last_push_time. is it passed over than push_interval? then return true
    if (client.last_push_time == null || client.last_push_time == "") return 1;

    last_push_time = client.last_push_time;
    last_msg_time = moment(last_push_time, 'YYYYMMDDHHmmssZZ');
    date_msg_time = moment(msg_time, 'YYYYMMDDHHmmssZZ');

    diff = date_msg_time.diff(last_msg_time, 'minutes');
    //logger.info("last_msg_time:", last_msg_time.format('YYYYMMDDHHmmss'));
    //logger.info("date_msg_time:", date_msg_time.format('YYYYMMDDHHmmss'));
    //logger.info("date_msg_time-last_msg_time:", diff);

    // 특정시간안에 너무 많은 이벤트가 발생할 경우,20을 지정했다면 매 20번째 이벤트 마다 실제 푸시는 보내는다. 
    if (diff < push_interval) {
        last_unsent_count = client.last_unsent_count == null ? 1 : client.last_unsent_count + 1;
        if (last_unsent_count == push_threshold)
            return 3;  // 메시지가 임계치에 도달해서 메시지를 발송함
        return -1; // 아직 발송하지 않음

    }
    return 2; // 시간이 지났으므로 메시지를 발솧함.
}


// 비동기가 발생하는 부분은 함수로 분리가 필요..  
function do_push_notification(idx, device_uuid, client_uuid, platform, token, event_time, event_type, lang, name, timestr, event_value, push_test_mode) {
    logger.debug("do_push_notification: ", idx, device_uuid, client_uuid, platform, token, event_time, event_type, lang, name, timestr, push_test_mode);

    if (token == null || token == "" || token == "(null)" || token == "NaN") {
        logger.info(">>>> token is null.", SERVICE_CODE, platform, device_uuid, client_uuid);
        return false;
    }

    pushEventBadge.findOne({ service_code: SERVICE_CODE, platform: platform, token: token })
        .exec(function (err, badgeitem) {
            if (err) {
                logger.info("update_db_push_badge_count:", err);
                contine;
            }
            if (badgeitem == null) {
                // app를 통해서 한번도 등록이 안된 장비임
                // make new one
                var new_push_badge = new pushEventBadge({
                    service_code: SERVICE_CODE,
                    platform: platform,
                    token: token,
                    total_message_count: 1,
                    last_badge_count: 1
                });

                pushEventBadge.findOneAndUpdate(
                    { service_code: SERVICE_CODE, platform: platform, token: token },
                    new_push_badge,
                    { upsert: true, new: true, runValidators: true }, // options
                    function (err, doc) { // callback
                        if (err) {
                            logger.info("update_db_push_badge_count new:", err);
                        }
                        else {
                            logger.debug("update_db_push_badge_count new: OK", idx, SERVICE_CODE, platform, token);
                            send_push_notification(client_uuid, device_uuid, platform, token,
                                event_time, event_type,
                                lang, name, timestr, event_value,
                                push_test_mode, 1);
                        }
                    }
                );

                /*
                
                            new_push_badge.save(function(err, newitem){
                                if(err)
                                {
                                    logger.info("update_db_push_badge_count new:", err);
                                }
                                else
                                {
                                    logger.debug("update_db_push_badge_count new: OK", idx,  SERVICE_CODE, platform, token);
                                    send_push_notification(client_uuid, device_uuid, platform, token,
                                                event_time, event_type, 
                                                lang, name, timestr, event_value,
                                                push_test_mode, 1);
                                }
                            });
                  */
            }
            else {
                // update
                badgeitem.last_update_time = moment().format('YYYYMMDDHHmmss');
                badgeitem.total_message_count++;
                badgeitem.last_badge_count++;
                badgeitem.save(function (err, badgeupdateitem) {
                    if (err) {
                        logger.info("update_db_push_message update:", err)
                    }
                    logger.debug("update_db_push_badge_count update: OK", idx, SERVICE_CODE, platform, token);
                    send_push_notification(client_uuid, device_uuid, platform, token,
                        event_time, event_type,
                        lang, name, timestr, event_value,
                        push_test_mode, badgeitem.last_badge_count);

                });


            }

        });

}

function pushEventHandler(amqp_ch, amqp_msg, msgobj, hashkey) {
    // 응답 메시지는 없음
    var device_uuid = msgobj.header.deviceID;
    var event_time = moment(msgobj.body.eventTime, "YYYYMMDDHHmmssZZ");
    var event_timestr = msgobj.body.eventTime.substr(0, 4) + "/" + msgobj.body.eventTime.substr(4, 2) + "/" + msgobj.body.eventTime.substr(6, 2) + " " + msgobj.body.eventTime.substr(8, 2) + ":" + msgobj.body.eventTime.substr(10, 2) + ":" + msgobj.body.eventTime.substr(12, 2);
    var event_type = msgobj.body.eventType;
    var device_key = msgobj.body.deviceKey;
    var event_value = msgobj.body.eventValue;

    if (event_value == null) event_value = "";

    logger.info("pushEventHandler:", SERVICE_CODE, device_uuid, msgobj.body.eventTime, event_type);


    // device를 검색해서 메시지를 전달할 수 있는 
    //이벤트 타입을 기준으로 메시지 작성
    devices.findOne({ service_code: SERVICE_CODE, uuid: device_uuid })
        .exec(function (err, item) {
            if (err) {
                logger.info("pushEventHandler ERROR:", err);
                return false;
            }
            if (item == null) {
                // 아무것도 하지 않음. 
                logger.info("pushEventHandler: unregistered device.", SERVICE_CODE, device_uuid);
                return true;
            }
            else {
                // update
                var len = item.push_clients.length;

                item.last_msg_time = moment(msgobj.header.msgTime, 'YYYYMMDDHHmmssZZ').toDate();
                item.last_update_time = moment().toDate();
                item.total_message_count++;
                if (len == 0)
                    logger.info("No push clients.", SERVICE_CODE, device_uuid);
                else
                    logger.info("The number of Push clients.", len, SERVICE_CODE, device_uuid);
                ///////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////
                // push메시지 발송 처리 
                for (var idx = 0; idx < len; idx++) {
                    logger.debug("CLIENT", idx, JSON.stringify(item.push_clients[idx]));
                    total_message_count = item.push_clients[idx].total_message_count == null ? 0 : item.push_clients[idx].total_message_count;
                    total_push_count = item.push_clients[idx].total_push_count == null ? 0 : item.push_clients[idx].total_push_count;
                    last_unsent_count = item.push_clients[idx].last_unsent_count == null ? 0 : item.push_clients[idx].last_unsent_count;
                    last_push_time = item.push_clients[idx].last_push_time == null ? '' : item.push_clients[idx].last_push_time;

                    //logger.debug ( ">>>> ", client.name, total_message_count, total_push_count, last_unsent_count, last_push_time);
                    push_state = check_push_information(device_uuid,
                        item.push_clients[idx].token, msgobj.body.eventTime, device_key, item.push_clients[idx]);

                    logger.debug(">>>> check_push_information = ", "idx:", idx, "len:", len, "state:", push_state)

                    if (push_state == 0) {
                        // 메시지 발송하지 않음 그러나 업데이는 함 
                        //do not send message but update 
                        //increase total message count only
                        total_message_count += 1
                    } else if (push_state == 1) {
                        // 항상 발송하는 경우 
                        // always send 
                        total_message_count += 1;
                        total_push_count += 1;
                        last_unsent_count = 0;
                        last_push_time = msgobj.body.eventTime;

                    } else if (push_state == -1) {
                        // 발송 시간 제한으로인해서 발송하지 않음
                        // -1 : do not send because of push time limit
                        total_message_count += 1
                        last_unsent_count += 1
                    } else if (push_state == -2) {
                        // 클라이언트(스마트폰)이 접속을 한동안 하지 않아서 발송은 하지 않음. 
                        // -2 : do not send because no feedback from clients
                        total_message_count += 1
                        last_unsent_count += 1
                    } else if (push_state == -3) {
                        // 비번이 변경된 경우, 발솧하지 않음. 
                        // -3 : password error
                        total_message_count += 1
                        last_unsent_count += 1
                    } else if (push_state == 2) {
                        // 시간 제한 전이라서 발송을 함. 
                        // 2 : send before the time limit
                        total_message_count += 1
                        total_push_count += 1
                        last_unsent_count = 0
                        last_push_time = msgobj.body.eventTime;
                    } else if (push_state == 3) {
                        // 메시지 발송 개수 제한을 발송되지 않다가 정해진 수에 도달한 경우 
                        // 3 : send over the threshold of message
                        total_message_count += 1
                        last_unsent_count += 1
                        last_unsent_count = 0
                        last_push_time = msgobj.body.eventTime;
                    } else if (push_state == -4) {
                        // token값이 문제가 있는 경우(black list)로 판단되는 경우 무시함. 
                        // push처리도 더이상 되지 않게 변경함. 
                        item.push_clients[idx].push_enabled = -1;
                        // 상태를 변경했으므로 이후 발송처리되지 않을 것으로 판단. badDeveiceToken에서 제거함
                        // 단 다른 카메라를 등록한 정보가 있으면 다시 발송될 수 있음. 
                        var tokenidx = badDeveiceToken.indexOf(item.push_clients[idx].token);
                        if (tokenidx > -1) {
                            badDeveiceToken.splice(tokenidx, 1);
                            logger.debug("removed token from badDeveiceToken:", tokenidx, item.push_clients[idx].token);
                        }
                        else
                            logger.debug("Failed to removed token from badDeveiceToken:", tokenidx, item.push_clients[idx].token);

                    } else {
                        // do nothing
                    }

                    // client clients information
                    item.push_clients[idx].total_message_count = total_message_count;
                    item.push_clients[idx].total_push_count = total_push_count;
                    item.push_clients[idx].last_unsent_count = last_unsent_count;
                    item.push_clients[idx].last_push_time = last_push_time;
                    item.push_clients[idx].last_status = push_state;
                    item.push_clients[idx].last_msg_time = moment().toDate(); //msgobj.body.eventTime;

                    // 1/2/3인 경우만 실제 메시지를 발송한다
                    if (push_state == 1 || push_state == 2 || push_state == 3) {
                        if (item.push_clients[idx].name == null || item.push_clients[idx].name == "")
                            name = device_uuid;
                        else
                            name = item.push_clients[idx].name;

                        do_push_notification(idx, device_uuid,
                            item.push_clients[idx].client_uuid,
                            item.push_clients[idx].platform,
                            item.push_clients[idx].token,
                            event_time, event_type,
                            item.push_clients[idx].lang, name, event_timestr,
                            event_value,
                            item.push_clients[idx].test);
                    }
                    else {
                        logger.info("pushEvent : Push is offed. token:", item.push_clients[idx].name, item.push_clients[idx].token);
                        if (item.push_clients[idx].token == "(null)" || item.push_clients[idx].token == "NaN") {
                            item.push_clients[idx].push_enabled = -1;
                        }
                    }
                } // the end of for loop


                ///////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////
                // 최종 저장은 여기서 처리 
                item.save(function (err, updateitem) {
                    if (err) {
                        logger.info("pushEventHandler update:", err)
                        return false;
                    }
                    else {
                        logger.debug("pushEventHandler update: ", SERVICE_CODE, device_uuid);
                        return true;
                    }
                });


            }

        });

}

function system_daemon() {
    logger.info('>>>>> CONFIGURATION: ' + process.env.NODE_ENV);
    // check service CODE
    serviceCode.findOne({ service_code: SERVICE_CODE })
        .exec(function (err, item) {
            if (err || item == null) {
                logger.info("ERROR. cannot find the service code:" + SERVICE_CODE);
                process.exit(0);
                return;
            }
            curServiceCode = item;
            logger.info('>>>>> SERVICE CODE : ' + curServiceCode.service_code);
            logger.info('>>>>> SERVICE HASH : ' + curServiceCode.service_hashkey);
            logger.info('>>>>> EXPIRED DATE : ' + moment(curServiceCode.expired_date).format('YYYY/MM/DD HH:mm:ssZZ'));

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            amqp.connect(amqpUrl).then(function (conn) {
                return conn.createChannel().then(function (amqp_ch) {

                    amqp_ch.on("error", function (err) {
                        logger.info("[AMQP] channel error", err.message);
                        process.exit();
                    });
                    amqp_ch.on("close", function () {
                        logger.info("[AMQP] channel closed");
                        process.exit();
                    });

                    var ok = amqp_ch.assertQueue(system_Queue, { autoDelete: true, durable: false });
                    var ok = amqp_ch.bindQueue(system_Queue, amqp_exchange, system_rpc_key);
                    var ok = ok.then(function () {
                        return amqp_ch.consume(system_Queue, system_daemon_consumer, { noAck: true });
                    });
                    return ok.then(function () {
                        logger.info("Ready to do service!!");
                    });

                    function system_daemon_consumer(msg) {
                        try {
                            var msgobj = JSON.parse(msg.content.toString());

                            logger.debug("system_daemon_consumer : [" + JSON.stringify(msgobj) + "]");
                            if (msgobj.header != null && msgobj.body != null) {
                                if (SERVICE_CODE != msgobj.header.groupID) {
                                    logger.info("Invalid Service Code!!");
                                }
                                else {
                                    // check msg validation
                                    var newmsg_hash = waviotmsg.make_waviot_hashcode(msgobj.body, "base64", curServiceCode.service_hashkey);
                                    if (newmsg_hash == msgobj.header.hashcode) {
                                        switch (msgobj.header.msgID) {
                                            case "reqHeartbeat":
                                                logger.info("reqHeartbeat from " + msgobj.header.deviceID)
                                                reqHeartbeatHandler(amqp_ch, msg, msgobj, curServiceCode.service_hashkey);
                                                break;
                                            case "cloudSaveValue":
                                                logger.info("cloudSaveValue from " + msgobj.header.deviceID)
                                                cloudSaveValueHandler(amqp_ch, msg, msgobj, curServiceCode.service_hashkey);
                                                break;
                                            case "cloudGetValue":
                                                logger.info("cloudGetValue from " + msgobj.header.deviceID)
                                                cloudGetValueHandler(amqp_ch, msg, msgobj, curServiceCode.service_hashkey);
                                                break;
                                            case "activeDevice":
                                                logger.info("activeDevice from " + msgobj.header.deviceID)
                                                activeDeviceHandler(amqp_ch, msg, msgobj, curServiceCode.service_hashkey);
                                                break;
                                            case "pushEvent":
                                                logger.info("pushEvent from " + msgobj.header.deviceID)
                                                pushEventHandler(amqp_ch, msg, msgobj, curServiceCode.service_hashkey);
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    else {
                                        logger.info("HASH CODE ERROR!!" + "[" + newmsg_hash + "]" + "[" + msgobj.header.hashcode + "]");
                                    }
                                }
                            }
                            else {
                                logger.info("MSG FORMAT ERROR!!");
                            }


                        } catch (error) {
                            logger.info("system_daemon_consumer:" + error);
                        }
                    }

                });
            }).catch(err)
            {
                if (err != null) {
                    console.log(err);
                    logger.info("AMQP Error: " + err)
                    process.exit(1);
                }
            }

        });

}

connect_mongodb();