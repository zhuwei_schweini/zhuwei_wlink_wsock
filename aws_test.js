var AWS = require("aws-sdk");
var path = require('path');
var config = require(path.join(__dirname,'config'));
var waviotUtil = require("./modules/waviotUtil");

global.config = config.real;

//process.env.AWS_ACCESS_KEY_ID="AKIAJVKIWJ4PDTXPMDWA"
//process.env.AWS_SECRET_ACCESS_KEY="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";
//var aws_region = "";
// SES는 Oregon us-west-2

//var config = new AWS.Config({
//  accessKeyId: 'AKIAJVKIWJ4PDTXPMDWA', secretAccessKey: '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', region: 'us-west-2'
//});
//var creds = new AWS.Credentials('AKIAJVKIWJ4PDTXPMDWA', 'yXat6d8/UHeBIu/Yofb9ORAjMi/QhNcuueDQ/Rhq');
//var ses = new AWS.SES({region:'us-west-2'});
//var ses = new AWS.SES(config);

/*
AWS.config.apiVersions = {
  ses: '2010-12-01',
  // other service API versions
};

// SES는 Oregon us-west-2
AWS.config.update({region: 'us-west-2'});
*/

function createTemplate()
{
  var verificationCode_en = {
    Template: { /* required */
      TemplateName: 'verificationCode_en', /* required */
      SubjectPart: 'Your {{service_name}} verification code',
      HtmlPart : '<p>Hello {{email}},<br>Here is the code you need to {{type}} for our service:<br><br><span style="font-size: 20px;font-weight: bold;">{{verification_code}}</span><br><br>This code will expire in {{expired_time}} minutes.<br>The {{service_name}} Support Team</p>',
      TextPart: 'Hello {{email}},\r\nHere is the code you need to {{type}} for our service:\r\n\r\n{{verification_code}}\r\n\r\nThis code will expire in {{expired_time}} minutes.\r\n\r\nThe {{service_name}} Support Team'
    }
  };

  var verificationCode_cn = {
    Template: { /* required */
      TemplateName: 'verificationCode_cn', /* required */
      SubjectPart: '您的 {{service_name}} 验证码',
      HtmlPart : '<p>您好, {{email}},<br>这是您用于{{type}}我们服务的验证码:<br><br><span style="font-size: 20px;font-weight: bold;">{{verification_code}}</span><br><br>此验证码有效期{{expired_time}}分钟.<br>{{service_name}}支持团队</p>',
      TextPart: '您好, {{email}},\r\n这是您用于{{type}}我们服务的验证码:\r\n\r\n{{verification_code}}\r\n\r\n此验证码有效期{{expired_time}}分钟.\r\n\r\n{{service_name}}支持团队'
    }
  };
  
  // Missing region in config ==> 
  ses.createTemplate(verificationCode_en, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });

  ses.createTemplate(verificationCode_cn, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });
}


function updateTemplate()
{
  var params = {
    Template: { /* required */
      TemplateName: 'SignUpVerificationCode', /* required */
      SubjectPart: 'Your {{service_name}} verification code',
      HtmlPart : '<p>Hello {{email}},<br>Here is the code you need to Sign up for our service:<br><br><span style="font-size: 20px;font-weight: bold;">{{verification_code}}</span><br><br>This code will expire in {{expired_time}} minutes.<br>The {{service_name}} Support Team</p>',
      TextPart: 'Hello {{email}},\nHere is the code you need to Sign up  for our service:\n\n{{verification_code}}\n\nThis code will expire in {{expired_time}} minutes.\n\nThe {{service_name}} Support Team'
    }
  };
  
  // Missing region in config ==> 
  ses.updateTemplate(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });
}


function deleteTemplate()
{
  var params1 = {
    Template: { /* required */
      TemplateName: 'SignUpVerificationCode', /* required */
      SubjectPart: 'Your {{service_name}} verification code',
      HtmlPart : '<p>Hello {{email}},<br>Here is the code you need to Sign up for our service:<br><br><span style="font-size: 20px;font-weight: bold;">{{verification_code}}</span><br><br>This code will expire in {{expired_time}} minutes.<br>The {{service_name}} Support Team</p>',
      TextPart: 'Hello {{email}},\nHere is the code you need to Sign up  for our service:\n\n{{verification_code}}\n\nThis code will expire in {{expired_time}} minutes.\n\nThe {{service_name}} Support Team'
    }
  };

  var params = {
    TemplateName: 'SignUpVerificationCode' /* required */
  };
  
  // Missing region in config ==> 
  ses.deleteTemplate(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });
}


function sendEmail_en(service_code, mail_type)
{
  try {
    var service_info = waviotUtil.get_global_service(service_code);
    var ses = new AWS.SES(service_info.aws_config);
    var TemplateData  = { 
      service_name: service_info.smtp.service_name, 
      email: "alex@wavlink.com",
      type : mail_type, 
      verification_code: waviotUtil.gen_verification_code(), 
      expired_time: "10"  
    };

    var sendParams =
    {
      "Source": "Do Not Reply <"+service_info.smtp.sender+">",
      "Template": "verificationCode_en",
      "Destination": {
        "ToAddresses": [ "alex@wavlink.com"
        ]
      },
      "TemplateData": JSON.stringify(TemplateData)
    }

    ses.sendTemplatedEmail(sendParams, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
    });
  } catch (error) {
     console.log("sendEmail_en:"+error)   
  }
}

function sendEmail_cn(service_code, mail_type)
{
  try {
    var service_info = waviotUtil.get_global_service(service_code);
    var ses = new AWS.SES(service_info.aws_config);
    // success@simulator.amazonses.com

    var TemplateData  = { 
      service_name: service_info.smtp.service_name, 
      email: "alex@wavlink.com",
      type : mail_type, 
      verification_code: "123456", 
      expired_time: "10"  
    };

    var sendParams =
    {
      "Source": "Do Not Reply <"+service_info.smtp.sender+">",
      "Template": "verificationCode_cn",
      "Destination": {
        "ToAddresses": [ "alex@wavlink.com"
        ]
      },
      "TemplateData": JSON.stringify(TemplateData)
    }

    ses.sendTemplatedEmail(sendParams, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
    });
  } catch (error) {
    console.log("sendEmail_en:"+error)   
  }
}


//createTemplate();
//deleteTemplate()
//updateTemplate();
//sendEmail();
//sendEmail_en();

function test1()
{
  var map = new Map(); //Empty Map
  var map = new Map([['W1',2],["W2",3]]); // map = {1=>2, 2=>3}
  map.set("W3", 200)
  console.log(map);
  //console.log(map[W1]);
  console.log(map.get("W2"));
  console.log(map.get("W3"));
}

waviotUtil.init_global_service();
//init_global_service();
sendEmail_en("WLINK_A0000", "Sign In");
sendEmail_cn("WLINK_A0001", "注册");