var amqp = require('amqplib');
var moment = require('moment-timezone');
var mongoose = require('mongoose');
var crypto = require('crypto');
var uuid = require('node-uuid');
var path = require('path');
var util = require('util');
var waviotmsg = require("./modules/waviotmsg");
var wavcam_activation = require('./wavcam_db/wavcam_activation');
var wavcam_push = require('./wavcam_db/wavcam_push');
var wavcam_push_badge = require('./wavcam_db/wavcam_push_badge');
var wavcam_push_list = require('./wavcam_db/wavcam_push_list');
var config = require(path.join(__dirname,'config_wavcam'));
var event_config = require(path.join(__dirname,'event_config_wavcam'));

var baidu_config;
var getui_config;
var apns_real_config;
var apns_debug_config;
var apns_topic;

// apn npm install -S apn
var apn = require('apn');
// baidu pushing npm install -S bpush-nodejs
var bpush = require('bpush-nodejs');

// getui push system  
// getui는 수정한 모듈을 직접 모듈 디렉토리에 복사 후 아래 패키지 설치 필요 
// npm install request --save
// npm install protobufjs@3.8.2 --save
var GeTui = require('getui/GT.push');
var GeTui_Target = require('getui/Target');
var GeTui_NotificationTemplate = require('getui/template/NotificationTemplate');
var GeTui_TransmissionTemplate = require('getui/template/TransmissionTemplate');

var GeTui_SingleMessage = require('getui/message/SingleMessage');

var SERVICE_CODE = "A0000";
var system_UID = "SYSTEM_DAEMON_" + SERVICE_CODE;
var system_Queue = "system_queue_" + SERVICE_CODE;
var system_rpc_key = "rpc_" + SERVICE_CODE;
var service_hashkey = "wavcam2015";
var curServiceCode;
var amqpUrl;
var amqp_exchange;
var badDeveiceToken = [];
var mainDB = "WAVCAM_"+SERVICE_CODE;
var mongoUrl;  

///////////////////////////////////////////////////////////////////////////////
// load configuration
var service_type = process.env.WLINK_SVC;
global.config = config.real;

if(service_type!=null && service_type!="")
{
    console.log("daemon_wavcam >>> WLINK SYSTEM :", service_type);
    global.config = config[service_type];

    if(global.config==null)
    {
        console.log("Cannot find configuration!!!", service_type);
        process.exit();
    }
}
else 
    console.log(">>> use REAL configuration!!!");
///////////////////////////////////////////////////////////////////////////////

global.config.path = path.join(__dirname);

if(process.argv[2]!=null)
{
    SERVICE_CODE = process.argv[2];
    system_UID = "SYSTEM_DAEMON_" + SERVICE_CODE;
    system_Queue = "system_queue_" + SERVICE_CODE;
    system_rpc_key = "rpc_" + SERVICE_CODE;
    mainDB = "WAVCAM_"+SERVICE_CODE;
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
process.on('uncaughtException', function (err) {
	logger.info('uncaughtException 발생 : ' + err);
    //if(err.toString().indexOf("ETIMEDOUT")!=-1)
    // 그냥 재시작
    process.exit();
});

var Logger = require('./modules/logger');

var logger = new Logger('wavcam_daemon_'+SERVICE_CODE);
logger.level = global.config.log_level; //'debug';
global.logger = logger;

for(var i=0; i<global.config.io_service.length; i++)
{
    console.log(">>> [%s] [%s]", global.config.io_service[i].service_code, global.config.io_service[i].amqp)
    if(global.config.io_service[i].service_code==SERVICE_CODE)
    {
        baidu_config = global.config.io_service[i].baidu;
        getui_config = global.config.io_service[i].getui;
        apns_debug_config = global.config.io_service[i].apn_debug;
        apns_real_config = global.config.io_service[i].apn_release;
        apns_topic = global.config.io_service[i].apn_topic;
        service_hashkey = global.config.io_service[i].hash_key;
        logger.info("FOUND push information. baidu[%s]", JSON.stringify(baidu_config));
        logger.info("FOUND push information. getui[%s]", JSON.stringify(getui_config));
        logger.info("FOUND push information. apn_debug[%s]", JSON.stringify(apns_debug_config));
        logger.info("FOUND push information. apn_release[%s]", JSON.stringify(apns_real_config));        
        logger.info("FOUND push information. apns_topic[%s]", apns_topic);  

        amqpUrl = global.config.io_service[i].amqp;
        mongoUrl = process.env.DB;  
        amqp_exchange = global.config.io_service[i].exchange;
        logger.info("FOUND amqp information. exhg[%s]", amqp_exchange);
    }

}

if(amqpUrl==false || mongoUrl==false)
{
    logger.info("cannot found a amqp or mongo information!!");
    process.exit(1);
}

if((baidu_config==null && getui_config==null) || apns_debug_config==null || apns_real_config==null)
{
    logger.info("cannot found push informations!!");
    process.exit(1);
}



///////////////////////////////////////////////////////////////////////////////
function insert_db_push_list(gid, platform, uid, token_hash, eventAction, eventTime, lang, full_msg)
{
    var new_push_msg= new wavcam_push_list({
        groupid : gid,
        platform : platform, 
        uid  : uid,
        token  : token_hash,
        eventAction  : eventAction,
        eventTime  : eventTime,
        lang  : lang,
        msg  : full_msg,
        create_date : moment().format('YYYYMMDDHHmmss'),
    });

    new_push_msg.save(function(err, newitem){
        if(err)
        {
            logger.debug("insert_db_push_list new:", err);
            return -1;            
        }
        else
        {
            logger.debug("insert_db_push_list new: OK");
            return 1;            
        }
    });
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
//apns_test = APNs(use_sandbox=True, cert_file='/home/wavlink/service/cer_debug.pem', key_file='/home/wavlink/service/cer_debug.pem' )
//apns_real = APNs(use_sandbox=False, cert_file='/home/wavlink/service/cer_release.pem', key_file='/home/wavlink/service/cer_release.pem' )
//var apns_real = new apn.Provider({  cert: "./cer_release.pem",  key: "./cer_release.pem",});
//var apns_test = new apn.Provider({  cert: "./cer_release.pem",  key: "./cer_release.pem",});
//var apns_real = new apn.Provider(global.config.apn_debug);
//var apns_test = new apn.Provider(global.config.apn_release);
var apns_real = new apn.Provider(apns_real_config);
var apns_test = new apn.Provider(apns_real_config);

function send_ios_push(token_hex, mesg, badge_count,test_flag)
{
    try
    {
        var wavcam_client = [token_hex];
        let note = new apn.Notification({
            alert: mesg, badge: badge_count, sound:"default"
        });
        note.topic= apns_topic;
        logger.debug("Sending: [%s] to [%s]", note.compile(), token_hex);

        if(test_flag==1)
            apns_test.send(note, wavcam_client).then( result => {
                logger.debug("IOS sent:", result.sent.length);
                logger.debug("IOS failed:", result.failed.length);
                // BadDeviceToken 에러에 대한 처리 필요. 
                if(result.failed.length)
                {
                  if(JSON.stringify(result.failed).indexOf("BadDeviceToken")!=-1)
                  {
                      if(badDeveiceToken.indexOf(token_hex)==-1)
                      {
                          badDeveiceToken.push(token_hex);
                          logger.info("Found IOS BAD DEVICE TOKEN", token_hex);
                      }                
                  }
                }
            });
        else
            apns_real.send(note, wavcam_client).then( result => {
                logger.debug("IOS sent:", result.sent.length);
                logger.debug("IOS failed:", result.failed.length);
                logger.debug(result.failed);
                // BadDeviceToken 에러에 대한 처리 필요. 
                if(result.failed.length)
                {
                  if(JSON.stringify(result.failed).indexOf("BadDeviceToken")!=-1)
                  {
                      if(badDeveiceToken.indexOf(token_hex)==-1)
                      {
                          badDeveiceToken.push(token_hex);
                          logger.info("Found IOS BAD DEVICE TOKEN", token_hex);
                      }                
                  }
                }
            });
    }  catch (error)
    {
        logger.info("send_ios_push ERROR:", error)        
    } 
}
        
function send_baidu_push(token_hex, title, mesg, badge_count, test_flag)
{
    var tconfig = baidu_config; //{ak:"vG6c7iLiujK451rHc4BitsZO", sk:"Hags4aQF4z7Hz2wj1ugFtuAamQZQjaUc"};
    //mesg = mesg.replace('\n', '\n\n');

    //opts = {msg_type:0, expires:300} 
    var data = {
        msg: JSON.stringify({
            title:title,
            description:mesg, 
            notification_builder_id:10
        }),
        channel_id: token_hex,
        msg_type: bpush.constant.MSG_TYPE.NOTIFICATION,
        //deploy_status: bpush.constant.DEPLOY_STATUS.DEVELOPMENT,
        device_type: bpush.constant.DEVICE_TYPE.ANDROID,
        expires:300,
        tag:baidu_config.tag

    };

    //new_push_msg = unicode(str(push_msg), "utf-8")
    //new_token_hex = unicode(str(token_hex), "utf-8")

    bpush.sendRequest(bpush.apis.pushMsgToSingleDevice, data, null, null, tconfig).then(function (data) {
        data = JSON.parse(data);
        logger.debug("send_baidu_push:", data);
    }).catch(function(e){
        logger.info("send_baidu_push ERROR:",e);
    });
  
}

//////////////////////////////////////////////////////////////////////////////////////
function send_getui_push(token_hex, title, mesg, badge_count, test_flag)
{
    var tconfig = getui_config; 
    // {"HOST": "'http://sdk.open.api.igexin.com/apiex.htm'", "APPID": "p8xdRj7MgTAylkdrte48D8", "APPKEY":"N0gxmBURcaAsc3sSzcgqE1", "MASTERSECRET":"Ab7j71gwtq7o1kCzLpSaU2"},
    var gt = new GeTui(tconfig["HOST"], tconfig["APPKEY"], tconfig["MASTERSECRET"]);

    var template =  new GeTui_TransmissionTemplate({
        appId: tconfig["APPID"],
        appKey: tconfig["APPKEY"],
        transmissionType: 2,
        transmissionContent: mesg
    });
/*
    var template = new GeTui_NotificationTemplate({
        appId: tconfig["APPID"],
        appKey: tconfig["APPKEY"],
        title: title,
        text: mesg,
        //logo: 'http://www.igetui.com/logo.png',
        isRing: true,
        isVibrate: true,
        isClearable: true,
        transmissionType: 1,
        transmissionContent: mesg
    });    
*/
    var message = new GeTui_SingleMessage({
        isOffline: true,                        //是否离线
        offlineExpireTime: 3600 * 12 * 1000,    //离线时间
        data: template,                          //设置推送消息类型
        pushNetWorkType:0                     //是否wifi ，0不限，1wifi
    });

    var target = new GeTui_Target({
        appId: tconfig["APPID"],
        clientId:token_hex
    });    

    gt.pushMessageToSingle(message, target, function(err, res){
        logger.debug("send_getui_push:", res);
        if(err != null && err.exception != null && err.exception instanceof  RequestError){
            var requestId = err.exception.requestId;
            logger.info("send_getui_push ERROR:", err.exception.requestId);
            gt.pushMessageToSingle(message,target,requestId,function(err, res){
                logger.info("send_getui_push RETRY:",err);
                logger.info("send_getui_push RETRY:",res);
            });
        }
    });
}



function send_wavcam_push(platform, token_hex, title, mesg, badge_count, test_flag)
{
    if(platform=="ios")
    {
        logger.debug ( "Send Push[ios] to ["+token_hex+"]!!")                    
        send_ios_push(token_hex, mesg, badge_count, test_flag);
    }
    else if(platform=="baidu")
    {
        logger.debug ( "Send Push[baidu] to ["+token_hex+"]!!")                    
        send_baidu_push(token_hex, title, mesg, badge_count, test_flag)
        //send_baidu_push("4108108582277042053", "test", "wavvam mesg", test_flag)
    }    
    else if(platform=="getui")
    {
        logger.debug ( "Send Push[getui] to ["+token_hex+"]!!")                    
        send_getui_push(token_hex, title, mesg, badge_count, test_flag)
        //send_getui_push("2ee67c41966edadd397392b7932b081d", title, mesg, badge_count, test_flag)
    }    
    else if(platform=="android")
    {

    }
    else if(platform=="qq")
    {

    }else 
    {

    }
}

///////////////////////////////////////////////////////////////////////////////
function make_motion_detection_message(lang, uid, name, time)
{
    /*
    var mapLangMotionDetection = {
        "0":"WavCam Event!!\nTime:%s\nCamera:%s\na Motion was detected.",
        "1":"WavCam 通知!!\n时间:%s\n摄像机:%s\n移动侦测报警.",
        "2":"WavCam 通知!!\n時間:%s\n攝像機:%s\n移動偵測報警.",  
        "3":"WavCam 알림!!\n시간:%s\n카메라:%s\n모션이 감지되었습니다.",
        "4":"WavCam Event!!\nTime:%s\nCamera:%s\na Motion was detected.",
        "5":"WavCam Event!!\nTime:%s\nCamera:%s\na Motion was detected.",
    };
*/
    var mapLangMotionDetection = event_config[SERVICE_CODE]["motion"];

    try {
        return util.format(mapLangMotionDetection[lang], name, time);     
    } catch (error) {
        return "";
    }
}

function make_audio_detection_message(lang, uid, name, time) 
{  
    /*
    var mapLangAudioDetection = {
        "0":"WavCam Event!!\nTime:%s\nCamera:%s\nan Audio was detected.",
        "1":"WavCam 通知!!\n时间:%s\n摄像机:%s\n音频侦测报警.",
        "2":"WavCam 通知!!\n時間:%s\n攝像機:%s\n音頻偵測報警.",
        "3":"WavCam 알림!!\n시간:%s\n카메라:%s\n소리가 감지되었습니다.",
        "4":"WavCam Event!!\nTime:%s\nCamera:%s\nan Audio was detected.",
        "5":"WavCam Event!!\nTime:%s\nCamera:%s\nan Audio was detected.",
    }*/

    var mapLangAudioDetection = event_config[SERVICE_CODE]["audio"];
    try {
        return util.format(mapLangAudioDetection[lang], name, time);    
    } catch (error) {
        return "";
    }
}

function make_inactivate_message(lang, uid, name, time)
{
    /*
    var mapLangInactivateDetection = {
        "0":"WavCam Alert!!\nTime:%s\nCamera:%s\nNetwork error was detected. Check your camera.",
        "1":"WavCam 警报!!\n时间:%s\n摄像机:%s\n网络错误. 检查摄像机的状态.",
        "2":"WavCam 警報!!\n時間:%s\n攝像機:%s\n網絡錯誤. 檢查攝像機的狀態.",
        "3":"WavCam 경고!!\n시간:%s\n카메라:%s\n통신 오류 발생. 카메라 상태를 확인하세요.",
        "4":"WavCam Alert!!\nTime:%s\nCamera:%s\nNetwork error was detected. Check your camera.",
        "5":"WavCam Alert!!\nTime:%s\nCamera:%s\nNetwork error was detected. Check your camera.",
    } 
    */   
    var mapLangInactivateDetection = event_config[SERVICE_CODE]["inactivate"];
    try {
        return util.format(mapLangInactivateDetection[lang], name, time);  
    } catch (error) {
        return "";
    }
} 
function make_diskfull_message(lang, uid, name, time)
{
    /*
    var mapDiskFullDetection = {
        "0":"WavCam Alert!!\nTime:%s\nCamera:%s\nSD Card is full now.",
        "1":"WavCam 警报!!\n时间:%s\n摄像机:%s\nSD卡空间不够.",
        "2":"WavCam 警報!!\n時間:%s\n攝像機:%s\nSD卡空間不夠.",
        "3":"WavCam 경고!!\n시간:%s\n카메라:%s\nSD Card의 여유공간이 부족합니다.",
        "4":"WavCam Alert!!\nTime:%s\nCamera:%s\nSD Card is full now.",
        "5":"WavCam Alert!!\nTime:%s\nCamera:%s\nSD Card is full now.",
    } 
    */   
    var mapDiskFullDetection = event_config[SERVICE_CODE]["full"];
    try {
        return util.format(mapDiskFullDetection[lang], name, time);     
    } catch (error) {
        return "";
    }
}   


function send_push_notification(gid, platform, targetUID, token, eventTime, eventAction, lang, name, timestr, push_test_mode, new_badge_count)
{
    var mapEventType = {
        "detection":"1",
        "system":"0",
    }
    var mapEventAction = {
        "systemReboot":"0",
        "nighttime":"1",
        "daytime":"2",
        "motion":"3", 
        "audio":"4",
        "temperature":"5",
        "humidity":"6",
        "newFirmware":"7",
        "disk":"8"
    }

    var mapEventMessage = {
        "motion":make_motion_detection_message,    
        "audio":make_audio_detection_message,    
        "inactivate":make_inactivate_message,    
        "full":make_diskfull_message    
    }


    logger.debug("send_push_notification: gid [%s], platform [%s], targetUID [%s], token [%s], eventTime [%s], eventAction [%s], lang [%s], name [%s], timestr [%s], push_test_mode [%d], new_badge_count [%d]", 
        gid, platform, targetUID, token, eventTime, eventAction, lang, name, timestr, push_test_mode, new_badge_count);


    try 
    {
        push_value = mapEventMessage[eventAction](lang, targetUID, name, timestr );
        if(push_value!="")
        {
            insert_db_push_list(gid, platform, targetUID, token, eventAction, eventTime, lang, push_value);  
            
            if(push_test_mode==1)
            {
                logger.debug ( "ipcamEvent : send event(T) platform:" + platform + ", token:" + token + ", value: " + push_value  + ", badge: " + new_badge_count);
                logger.info ( "ipcamEvent : send event(T) platform:%s, token:%d, UID:%s", platform, token, targetUID);
                send_wavcam_push(platform, token, name, push_value, new_badge_count, 1);

            }else{
                logger.debug ( "ipcamEvent : send event(R) platform:" + platform + ", token:" + token + ", value: " + push_value  + ", badge: " + new_badge_count);
                logger.info ( "ipcamEvent : send event(R) platform:%s, token:%d, UID:%s", platform, token, targetUID);
                send_wavcam_push(platform, token, name, push_value, new_badge_count, 0);
            }    
        }

    }catch(error)
    {
        logger.info("send_push_notification error", error);
    }
}

// 비동기가 발생하는 부분은 함수로 분리가 필요..  
function do_push_notification(idx, gid, platform, targetUID, token, eventTime, eventAction, lang, name, timestr, push_test_mode)
{
    logger.debug("do_push_notification: ", idx, gid, platform, targetUID, token, eventTime, eventAction, lang, name, timestr, push_test_mode);

    if(token==null || token=="" || token=="(null)" || token=="NaN")
    {
        logger.info(">>>> token is null.", gid, platform, targetUID )
        return false;        
    }

    wavcam_push_badge.findOne({groupid: gid, platform: platform, token : token})
    .exec(function(err, badgeitem) {
        if(err)
        {
            logger.info("update_db_push_badge_count:", err);
            contine;            
        }
        if(badgeitem==null)
        {
            // app를 통해서 한번도 등록이 안된 장비임
            // make new one
            var new_push_badge= new wavcam_push_badge({
                groupid: gid,
                platform: platform, 
                token : token,
                create_date : moment().format('YYYYMMDDHHmmss'),
                last_update_time: moment().format('YYYYMMDDHHmmss'),
                total_message_count: 1,
                last_badge_count: 1
            });

            wavcam_push_badge.findOneAndUpdate(
                {groupid: gid, platform: platform, token : token}, 
                new_push_badge, 
                {upsert: true, new: true, runValidators: true}, // options
                function (err, doc) { // callback
                    if(err)
                    {
                        logger.info("update_db_push_badge_count new:", err);
                    }
                    else
                    {
                        logger.debug("update_db_push_badge_count new: OK", idx,  gid,  platform, token);
                        send_push_notification(gid, platform, targetUID, token,
                                    eventTime, eventAction, 
                                    lang, name, timestr,
                                    push_test_mode, 1);
                    }
                }
            );            
        }
        else{
            // update
            badgeitem.last_update_time = moment().format('YYYYMMDDHHmmss');
            badgeitem.total_message_count++;
            badgeitem.last_badge_count++;
            badgeitem.save(function(err,  badgeupdateitem) {
                if(err) {
                    logger.info("update_db_push_message update:", err)
                }
                logger.debug("update_db_push_badge_count update: OK", idx,  gid,  platform, token);
                send_push_notification(gid, platform, targetUID, token,
                            eventTime, eventAction, 
                            lang, name, timestr,
                            push_test_mode, badgeitem.last_badge_count);
            });
            

        }

    });

}

/*
#  return 0 : always do not send
#         1 : always send
#         -1 : do not send because of push time limit
#         2 : send before the time limit
#         3 : send over the threshold of message
#         -2 : no activation from client
#         -3 : user_key error
#         -4 : bad device token
*/
function check_push_information(gid, uid, token, msg_time, user_key, client)
{
    // check push_enabled
    if(client.push_enabled==null || client.push_enabled<=0)
        return 0;

    if(badDeveiceToken.indexOf(token)!=-1)
        return -4; // bad device token

    // clients session check 
    var session_days = 6
    var expired = moment();
    expired.add(-session_days, "day");
    session_time_str = expired.format('YYYYMMDDHHmmss');
    logger.debug(" TIME before 6days: ", session_time_str);
    if(client.last_calling_time!=null && client.last_calling_time<session_time_str)
        return -2;  // 6일 동안 클라이언트 접속 기록이 없으면 메시지를 보내지 않는다. 

    client_user_key = client.userkey==null ? '' : client.userkey;
    if(user_key!='' && user_key!=client_user_key) 
    {
        logger.debug(">>>>> userkey:", user_key, "client_user_key:", client_user_key);
        return -3; // 암호가 변경된 경우 보내지 않는다. 
    }
        
    // 알림 발송 갯수 제한이 없을 경우에는 항상 전송한다.  
    push_interval = client.push_interval==null ? 0 :client.push_interval;
    push_threshold = client.push_threshold==null ? 0 :client.push_threshold;    
    if( push_interval==0 &&  push_threshold==0)
        return 1;
            
    // check client last_push_time. is it passed over than push_interval? then return true
    last_push_time = '20160101000000';
    if(client.last_push_time!=null)
        last_push_time = client.last_push_time;
    last_msg_time = moment(last_push_time, 'YYYYMMDDHHmmss');
    date_msg_time = moment(msg_time, 'YYYYMMDDHHmmss');
        
    diff = date_msg_time.diff(last_msg_time, 'minutes');
    //logger.info("last_msg_time:", last_msg_time.format('YYYYMMDDHHmmss'));
    //logger.info("date_msg_time:", date_msg_time.format('YYYYMMDDHHmmss'));
    //logger.info("date_msg_time-last_msg_time:", diff);
    
    // 특정시간안에 너무 많은 이벤트가 발생할 경우,20을 지정했다면 매 20번째 이벤트 마다 실제 푸시는 보내는다. 
    if(diff<push_interval)
    {
        last_unsent_count = client.last_unsent_count==null ? 1 : client.last_unsent_count+1;
        if(last_unsent_count==push_threshold)
            return 3;  // 메시지가 임계치에 도달해서 메시지를 발송함
        return -1; // 아직 발송하지 않음

    }
    return 2; // 시간이 지났으므로 메시지를 발솧함.
}


function ipcamEventHandler(amqp_ch, amqp_msg, msgobj, hashkey)
{
    gid = msgobj.header.groupID
    uid = msgobj.body.targetUID
    push_value = ""
    user_key = msgobj.body.userKey

    timestr = msgobj.body.eventTime.substr(0, 4) + "/" + msgobj.body.eventTime.substr(4,2) + "/" + msgobj.body.eventTime.substr(6, 2) + " " + msgobj.body.eventTime.substr(8, 2) + ":" + msgobj.body.eventTime.substr(10, 2) + ":" + msgobj.body.eventTime.substr(12, 2);
    logger.debug("ipcamEventHandler :", gid, uid);
    
    wavcam_push.findOne({groupid: gid, uid: uid})
    .exec(function(err, item) {
        if(err)
        {
            logger.info("ipcamEventHandler ERROR:", err);
            return false;            
        }
        if(item==null)
        {
            // app를 통해서 한번도 등록이 안된 장비임
            // 신규로 등록 처리하면, 클라이언트 정보가 없는 것이므로 아무것도 하지 않음. 
            var new_push= new wavcam_push({
                groupid: gid,
                uid: uid,
                last_msg_time: msgobj.header.msgTime,
                last_update_time: moment().format('YYYYMMDDHHmmss'),
                total_message_count: 1
            });


            wavcam_push.findOneAndUpdate(
                {groupid: gid, uid: uid}, 
                new_push, 
                {upsert: true, new: true, runValidators: true}, // options
                function (err, doc) { // callback
                    if(err)
                    {
                        logger.info("ipcamEventHandler new err:", err);
                        return false;            
                    }
                    else
                    {
                        logger.debug("ipcamEventHandler new ok: ", gid, uid);
                        return true;            
                    }
                }
            );            
            return true;
        }
        else
        {
            // update
            item.last_msg_time = msgobj.header.msgTime;
            item.last_update_time = moment().format('YYYYMMDDHHmmss');
            item.total_message_count++;

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            // push메시지 발송 처리 
            for (var idx = 0, len = item.clients.length; idx < len; idx++) {
                logger.debug("CLIENT", idx, JSON.stringify(item.clients[idx]));
                total_message_count = item.clients[idx].total_message_count==null ? 0 : item.clients[idx].total_message_count;
                total_push_count = item.clients[idx].total_push_count==null ? 0 : item.clients[idx].total_push_count;
                last_unsent_count = item.clients[idx].last_unsent_count==null ? 0 : item.clients[idx].last_unsent_count;
                last_push_time = item.clients[idx].last_push_time==null ? '' : item.clients[idx].last_push_time;

                //logger.debug ( ">>>> ", client.name, total_message_count, total_push_count, last_unsent_count, last_push_time);
                push_state = check_push_information(gid, uid, item.clients[idx].token, msgobj.body.eventTime, user_key, item.clients[idx]);
                
                logger.debug (">>>> check_push_information = ", "idx:", idx, "len:", len, "state:", push_state)

                if(push_state==0)
                {
                    //do not send message but update 
                    //increase total message count only
                    total_message_count += 1

/*
                "clients.$.total_message_count": total_message_count,  #  it include unsent message
                "clients.$.total_push_count": total_push_count,
                "clients.$.last_unsent_count": last_unsent_count,
                "clients.$.last_push_time": last_push_time,
                "clients.$.last_status": last_status,
                "clients.$.last_msg_time": msg_time   
                #  it include unsent message  

                    update_db_push_count(gid, uid, client['token'], mesg['body']['eventTime'], total_message_count, total_push_count, last_unsent_count, last_push_time, push_state)                    
*/

                }else if(push_state==1) {
                    // always send 
                    total_message_count += 1;
                    total_push_count += 1;
                    last_unsent_count = 0;
                    last_push_time = msgobj.body.eventTime;
                    
                    //update_db_push_count(gid, uid, client['token'], mesg['body']['eventTime'], total_message_count, total_push_count, last_unsent_count, last_push_time, push_state)                    

                }else if(push_state==-1) {
                    // -1 : do not send because of push time limit
                    total_message_count += 1
                    last_unsent_count += 1
                    
                    //update_db_push_count(gid, uid, client['token'], mesg['body']['eventTime'], total_message_count, total_push_count, last_unsent_count, last_push_time, push_state)                    

                }else if(push_state==-2) {
                    // -2 : do not send because no feedback from clients
                    total_message_count += 1
                    last_unsent_count += 1
                    //update_db_push_count(gid, uid, client['token'], mesg['body']['eventTime'], total_message_count, total_push_count, last_unsent_count, last_push_time, push_state)                    

                }else if(push_state==-3) {
                    // -3 : password error
                    total_message_count += 1
                    last_unsent_count += 1
                    //update_db_push_count(gid, uid, client['token'], mesg['body']['eventTime'], total_message_count, total_push_count, last_unsent_count, last_push_time, push_state)                    

                }else if(push_state==2) {
                    // 2 : send before the time limit
                    total_message_count += 1
                    total_push_count += 1
                    last_unsent_count = 0
                    last_push_time = msgobj.body.eventTime;
                    //update_db_push_count(gid, uid, client['token'], mesg['body']['eventTime'], total_message_count, total_push_count, last_unsent_count, last_push_time, push_state)                    
                
                }else if(push_state==3) {
                    // 3 : send over the threshold of message
                    // but now do nothing. 
                    total_message_count += 1
                    last_unsent_count += 1
                    //update_db_push_count(gid, uid, client['token'], mesg['body']['eventTime'], total_message_count, total_push_count, last_unsent_count, last_push_time, push_state)                    
                }else if(push_state==-4) {
                    item.clients[idx].push_enabled = -1; 
                    var tokenidx = badDeveiceToken.indexOf(item.clients[idx].token);  
                    if (tokenidx > -1) {
                        badDeveiceToken.splice(tokenidx, 1);
                        logger.debug("removed token from badDeveiceToken:", tokenidx, item.clients[idx].token);
                    }   
                    else 
                        logger.debug("Failed to removed token from badDeveiceToken:", tokenidx, item.clients[idx].token);

                }else {
                    // do nothing
                }

                // client clients information
                item.clients[idx].total_message_count = total_message_count;
                item.clients[idx].total_push_count = total_push_count;
                item.clients[idx].last_unsent_count = last_unsent_count;
                item.clients[idx].last_push_time = last_push_time;
                item.clients[idx].last_status = push_state;
                item.clients[idx].last_msg_time = msgobj.body.eventTime;

                // 1 or 2일 경우만 실제 메시지를 발송한다
                if(push_state==1 || push_state==2  || push_state==3)
                {
                    if (item.clients[idx].name == null || item.clients[idx].name=="")
                        name = msgobj.body.targetUID;
                    else
                        name = item.clients[idx].name;   

                    // update badge list
                    do_push_notification(idx, gid, item.clients[idx].platform, msgobj.body.targetUID, item.clients[idx].token,
                                                msgobj.body.eventTime, msgobj.body.eventAction, 
                                                item.clients[idx].lang, name, timestr,
                                                item.clients[idx].test);
                                                
                }
                else
                {
                    logger.info ( "ipcamEvent : Push is offed. token:", item.clients[idx].name, item.clients[idx].token);
                    if(item.clients[idx].token=="(null)" || item.clients[idx].token=="NaN")
                    {
                        item.clients[idx].push_enabled = -1; 
                    }
                }    
            } // the end of for loop


            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            // 최종 저장은 여기서 처리 
            item.save(function(err, updateitem) {
                if(err) {
                    logger.info("ipcamEventHandler update:", err)
                    return false;
                }
                else
                {
                    logger.debug("ipcamEventHandler update: ", gid, uid);
                    return true;
                }
            });
            

        }

    });
    
    return true;
}



///////////////////////////////////////////////////////////////////////////////
function print_activated_ipcam_count(gid)
{
    var session_time = 30
    var expired = moment();
    expired.add(-session_time, "minute");

    logger.info("=== " + moment().format('YYYY/MM/DD HH:mm:ss') + ": DB Checking for ["+gid+"]") 
    logger.info(">>> Bad Device Token : %d pcs", badDeveiceToken.length);
    wavcam_activation.find({"groupid": gid}).count(function(err, total_count){
        logger.info(">>> Current Activated Total ipcam : %d pcs", total_count) 

        wavcam_activation.find({"groupid": gid, "last_update_time" : { "$gte":expired.format("YYYYMMDDHHmmssZZ")}}).count(function(err, active_count){
            logger.info(">>> Current Activated Total ipcam before %d mins: %d pcs", session_time, active_count); 

            wavcam_push_list.find({"groupid": gid}).count(function(err, push_count){
                logger.info(">>> Current push_list Total counts : %d ea", push_count) 
            });
        });
    });
}


function update_ipcam_inactive(gid, uid)
{
    wavcam_activation.findOne({groupid: gid, uid: uid})
    .exec(function(err, item) {
        if(err)
        {
            logger.debug("update_ipcam_inactive:", err)
            return false;            
        }
        if(item!=null)
        {
            // update
            item.inactivate_flag = 1;
            item.last_update_time = moment().format('YYYYMMDDHHmmss');
            item.save(function(err, updateitem) {
                if(err) {
                    logger.info("update_ipcam_inactive update:", err);
                    return false;
                }
                logger.debug("update_ipcam_inactive update: OK", err);
                return true;
            });
        }
        else{
            logger.debug("update_ipcam_inactive : NO DATA!!");
        }

    });
    
    return true;
}

function send_inactivated_push_message(gid, uid, timestr)
{
    if(uid==null || uid=="") return;

    wavcam_push.findOne({groupid: gid, uid: uid})
    .exec(function(err, item) {
        if(err)
        {
            logger.debug("send_inactivated_push_message ERROR:", err);
            return false;            
        }
        if(item!=null)
        {
            // update
            item.last_msg_time = moment().format('YYYYMMDDHHmmssZZ');
            item.last_update_time = moment().format('YYYYMMDDHHmmss');
            item.total_message_count++;

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            // push메시지 발송 처리 
            for (var idx = 0, len = item.clients.length; idx < len; idx++) {
                logger.debug("INACTIVATED CLIENT", idx, JSON.stringify(item.clients[idx]));

                if(item.clients[idx].push_enabled==null || item.clients[idx].push_enabled==0)
                {
                    logger.debug ( " - send_inactivated_push_message : push is offed(1) :" + item.clients[idx].platform + ", token:" + item.clients[idx].token);
                    continue;
                }

                // 일정사간(6일) 동안 클라이언트 접속 기록이 없으면 메세지를 보내지 않음.
                var session_days = 6
                var expired = moment();
                expired.add(-session_days, "day");
                session_time_str = expired.format('YYYYMMDDHHmmss');
                logger.debug(" TIME before 6days: ", session_time_str);
                if(item.clients[idx].last_calling_time!=null && item.clients[idx].last_calling_time<session_time_str)
                    continue;

                total_message_count = item.clients[idx].total_message_count==null ? 0 : item.clients[idx].total_message_count;
                total_push_count = item.clients[idx].total_push_count==null ? 0 : item.clients[idx].total_push_count;
                last_unsent_count = item.clients[idx].last_unsent_count==null ? 0 : item.clients[idx].last_unsent_count;
                last_push_time = item.clients[idx].last_push_time==null ? '' : item.clients[idx].last_push_time;

                // always send 
                total_message_count += 1;
                total_push_count += 1;
                last_unsent_count = 0;
                last_push_time = moment().format('YYYYMMDDHHmmss');
                push_state = 1;
                eventTime = moment().format('YYYYMMDDHHmmssZZ');
                // client clients information
                item.clients[idx].total_message_count = total_message_count;
                item.clients[idx].total_push_count = total_push_count;
                item.clients[idx].last_unsent_count = last_unsent_count;
                item.clients[idx].last_push_time = last_push_time;
                item.clients[idx].last_status = push_state;
                item.clients[idx].last_msg_time = eventTime;

                if (item.clients[idx].name == null || item.clients[idx].name=="")
                    name = uid;
                else
                    name = item.clients[idx].name;   

                // update badge list
                do_push_notification(idx, gid, item.clients[idx].platform, uid, item.clients[idx].token,
                                            eventTime, "inactivate", 
                                            item.clients[idx].lang, name, timestr,
                                            item.clients[idx].test);
                                                
            } // the end of for loop


            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            // 최종 저장은 여기서 처리 
            item.save(function(err, updateitem) {
                if(err) {
                    logger.info("send_inactivated_push_message update:", err)
                    return false;
                }
                else
                {
                    logger.debug("send_inactivated_push_message update: ", gid, uid);
                    return true;
                }
            });
            

        }

    });
    
    return true;




}

function check_inactivated_ipcam(gid)
{
    var session_time = 6
    var expired = moment();
    expired.add(-session_time, "hour");
    search_time_str = expired.format('YYYYMMDDHHmmss');
    logger.info(" TIME before 6 hour: ", search_time_str);
    timestr = search_time_str.substr(0, 4) + "/" + search_time_str.substr(4,2) + "/" + search_time_str.substr(6, 2) + " " + search_time_str.substr(8, 2) + ":" + search_time_str.substr(10, 2) + ":" + search_time_str.substr(12, 2);
    logger.info("check_inactivated_ipcam :", gid);

    wavcam_activation.count({"groupid": gid, "inactivate_flag":{ "$ne" : 1 },  "last_update_time" : { "$lt":search_time_str} },
    function(err, inactive_count) 
    {
        logger.info(">>> Current Inactivated Total ipcam during %d hours is %d pcs [%s]", session_time, inactive_count, timestr);

        if(inactive_count>0)
        {
            wavcam_activation.find({"groupid": gid, "inactivate_flag":{ "$ne" : 1 },  "last_update_time" : { "$lt":search_time_str} },
            function(err, cameras) 
            {
                for(var idx=0, len=cameras.length; idx<len; idx++)
                {
                    logger.debug ( ">>> Inactivated UID : [%s]", cameras[idx].uid);
                    send_inactivated_push_message(gid, cameras[idx].uid, timestr);
                    update_ipcam_inactive(gid, cameras[idx].uid);
                }

            });
        }
    });
}


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

function make_resHeartbeat_body(targetUID, response)
{
  var resHeartbeat_msg = {targetUID: targetUID, systemTime: moment().format('YYYYMMDDHHmmssZZ')  };
  if(response!=null && response!="")
    resHeartbeat_msg.response = response;
  return resHeartbeat_msg
}

function update_device_activation(msgobj)
{
    logger.info("update_device_activation:", msgobj.header.groupID, msgobj.header.deviceID,msgobj.header.msgTime)

    wavcam_activation.findOne({groupid: msgobj.header.groupID, uid: msgobj.header.deviceID})
    .exec(function(err, item) {
        if(err)
        {
            logger.debug("update_device_activation:", err)
            return false;            
        }
        if(item==null)
        {
            // make new one
            var activation= new wavcam_activation({
                groupid: msgobj.header.groupID,
                uid: msgobj.header.deviceID,
                last_msg_time: msgobj.header.msgTime,
                inactivate_flag: 0
            });

            wavcam_activation.findOneAndUpdate(
                {groupid: msgobj.header.groupID, uid: msgobj.header.deviceID}, 
                activation, 
                {upsert: true, new: true, runValidators: true}, // options
                function (err, doc) { // callback
                    if(err)
                    {
                        logger.info("update_device_activation new err:", err, msgobj.header.groupID, msgobj.header.deviceID);
                        return false;            
                    }
                    else
                    {
                        logger.info("update_device_activation new ok:", err, msgobj.header.groupID, msgobj.header.deviceID);
                        return true;            
                    }
                }
            );            
        }
        else{
            // update
            item.inactivate_flag = 0;
            item.last_msg_time = msgobj.header.msgTime;
            item.last_update_time = moment().format('YYYYMMDDHHmmss');
            item.save(function(err, updateitem) {
                if(err) {
                    logger.info("update_device_activation update:", err)
                    return false;
                }
            });
            

        }

    });
    
    return true;
}

function reqHeartbeatHandler(amqp_ch, amqp_msg, msgobj, hashkey)
{
    var challenge = msgobj.body.challenge;
    var resBody = make_resHeartbeat_body(msgobj.header.deviceID, challenge+1);

    // device activation 관리 
    update_device_activation(msgobj);

    var resHeartbeat =  waviotmsg.make_waviot_msg("resHeartbeat", SERVICE_CODE, system_UID, "", "", hashkey, resBody); 

    logger.debug("resHeartbeat:" + JSON.stringify(resHeartbeat));
    amqp_ch.publish(amqp_exchange, amqp_msg.properties.replyTo, new Buffer(JSON.stringify(resHeartbeat)), 
        {contentType: "application/json", deliveryMode:2, correlationId: amqp_msg.properties.correlationId});
}

function sendErrorMsg(amqp_ch, msg, deviceID, errno, hashkey)
{
    var errmsg = waviotmsg.make_error_msg(errno, SERVICE_CODE, system_UID, "", "", hashkey);
    //var errBody = {errno: errno}; //waviotmsg.make_error_body(errno);
    //var errmsg =  waviotmsg.make_waviot_msg("error", SERVICE_CODE, system_UID, "", "", hashkey, errBody); 

    logger.info("SEND ERROR:",deviceID, errno);
    logger.debug("errmsg:" + JSON.stringify(errmsg));
    amqp_ch.publish(amqp_exchange, msg.properties.replyTo, new Buffer(JSON.stringify(errmsg)), 
        {contentType: "application/json", deliveryMode:2, correlationId: msg.properties.correlationId});
}

//////////////////////////////////////////////////////////////////////////////////////////
// DB Connection
var db = mongoose.connection;

db.on("error", function(err) {
    logger.info("DB Error: " + err)
})

mongoose.connect(mongoUrl);
db.once("open", function() {
    logger.info("DB is connected.", mongoUrl);
    system_daemon();
});

function display_status()
{
    print_activated_ipcam_count(SERVICE_CODE);
    check_inactivated_ipcam(SERVICE_CODE);
}

function system_daemon()
{
    logger.info('>>>>> CONFIGURATION: ' + global.config.name);
    // check service CODE
    curServiceCode = {service_code:SERVICE_CODE, service_hashkey:service_hashkey, expired_date:moment("20201230").format("YYYYMMDD")};
    logger.info('>>>>> SERVICE CODE : ' + curServiceCode.service_code);      
    logger.info('>>>>> SERVICE HASH : ' + curServiceCode.service_hashkey);
    logger.info('>>>>> EXPIRED DATE : ' +  moment(curServiceCode.expired_date).format('YYYY/MM/DD HH:mm:ssZZ'));

    display_status();
        
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    amqp.connect(amqpUrl).then(function(conn) {
    return conn.createChannel().then(function(amqp_ch) {

        amqp_ch.on("error", function(err) {
            logger.info("[AMQP] channel error", err.message);
            process.exit();
        });
        amqp_ch.on("close", function() {
            logger.info("[AMQP] channel closed");
            process.exit();
        });

        var ok = amqp_ch.assertQueue(system_Queue, {autoDelete:true, durable: false});
        var ok = amqp_ch.bindQueue(system_Queue, amqp_exchange, system_rpc_key);
        var ok = ok.then(function() {
        return amqp_ch.consume(system_Queue, system_daemon_consumer, {noAck: true});
        });
        return ok.then(function() {
            setInterval(display_status, 1000*60*5);  // every 5 mins 
            logger.info("Ready to do service!!");
        });



        function system_daemon_consumer(msg) {
            try {
                var msgobj = JSON.parse(msg.content.toString());

                logger.debug("system_daemon_consumer : ["+JSON.stringify(msgobj)+"]" );
                if(msgobj.header!=null && msgobj.body!=null){
                    
                    // check msg validation
                    var newmsg_hash = waviotmsg.make_waviot_hashcode(msgobj.body, "base64", curServiceCode.service_hashkey);
                    if(newmsg_hash==msgobj.header.hashcode)
                    {   
                        var res = null;


                        switch(msgobj.header.msgID)
                        {
                            case "reqHeartbeat":
                                reqHeartbeatHandler(amqp_ch, msg, msgobj, curServiceCode.service_hashkey);
                                break;
                            case "ipcamEvent":
                                ipcamEventHandler(amqp_ch, msg, msgobj, curServiceCode.service_hashkey);
                                break;
                            default:
                                logger.info("Invalid Msg [%s]", msgobj.header.msgID);
                                sendErrorMsg(amqp_ch, msg, msgobj.header.deviceID, 1003, curServiceCode.service_hashkey);
                                break;
                        }
                    }    
                    else
                    {
                        logger.debug("HASH CODE ERROR!!" + "["+newmsg_hash+"]" + "["+msgobj.header.hashcode+"]");
                        sendErrorMsg(amqp_ch, msg, msgobj.header.deviceID, 1000, curServiceCode.service_hashkey);
                    }

                }
                else
                {
                    logger.info("MSG FORMAT ERROR!!");
                    sendErrorMsg(amqp_ch, msg, msgobj.header.deviceID, 1002, curServiceCode.service_hashkey);
                }


            } catch (error) {
                logger.info("system_daemon_consumer:" + error);
                logger.info("Generally, it is a critical error. RESTART!!");
                process.exit();              
            }
        }
        
    });
}).catch(
function(err) {
//onsole.warn;
//console.warn("TEST", err);
logger.info(err);
if(err.toString().indexOf("ENETUNREACH")!=-1 || err.toString().indexOf("ECONNREFUSED")!=-1)
        process.exit();
    
});

}
