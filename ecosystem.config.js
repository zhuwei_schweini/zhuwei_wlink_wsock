module.exports = {
  apps : [{
    name: 'http_server',
    script: 'http_server.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      WLINK_SVC: 'localtest',
      DB: "mongodb://192.168.16.188/testdb",
      REDIS: [{"port": 7000, "host": "localhost"}, {"port": 7001, "host": "localhost"},{"port": 7002, "host": "localhost"}],
      JWT_SECRET: "15567b09-88d2-45ba-9ddc-00314dde41a9",
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    },
    env_testing: {
      NODE_ENV: 'testing',
      WLINK_SVC: 'localtest',
      DB: "mongodb://127.0.0.1:27017/wlink",
      REDIS: [{"port": 7000, "host": "localhost"}, {"port": 7001, "host": "localhost"},{"port": 7002, "host": "localhost"}],
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    },
    env_production: {
      NODE_ENV: 'production',
      WLINK_SVC: 'localtest',
      DB: "mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/wlink",
      REDIS: [{"port": 7379, "host": "172.26.9.73"}, {"port": 7379, "host": "172.26.3.26"},{"port": 7379, "host": "172.26.40.122"}],
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    }
  },{
    name: 'sock_server',
    script: 'sock_server.js',
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      WLINK_SVC: 'sandbox',
      DB: "mongodb://192.168.16.188/testdb",
      REDIS: [{"port": 7000, "host": "localhost"}, {"port": 7001, "host": "localhost"},{"port": 7002, "host": "localhost"}],
      JWT_SECRET: "16667b09-88d2-45ba-9ddc-00314dde41a9",
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    },
    env_testing: {
      NODE_ENV: 'testing',
      WLINK_SVC: 'sandbox',
      DB: "mongodb://127.0.0.1:27017/wlink",
      REDIS: [{"port": 7000, "host": "localhost"}, {"port": 7001, "host": "localhost"},{"port": 7002, "host": "localhost"}],
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    },
    env_production: {
      NODE_ENV: 'production',
      WLINK_SVC: 'sandbox',
      DB: "mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/wlink",
      REDIS: [{"port": 7379, "host": "172.26.9.73"}, {"port": 7379, "host": "172.26.3.26"},{"port": 7379, "host": "172.26.40.122"}],
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    }
  },{
    name: 'api_server',
    script: 'api_server.js',
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      WLINK_SVC: 'sandbox',
      DB: "mongodb://192.168.16.188/testdb",
      REDIS: [{"port": 7000, "host": "localhost"}, {"port": 7001, "host": "localhost"},{"port": 7002, "host": "localhost"}],
      JWT_SECRET: "17767b09-88d2-45ba-9ddc-00314dde41a9",
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    },
    env_testing: {
      NODE_ENV: 'testing',
      WLINK_SVC: 'sandbox',
      DB: "mongodb://127.0.0.1:27017/wlink",
      REDIS: [{"port": 7000, "host": "localhost"}, {"port": 7001, "host": "localhost"},{"port": 7002, "host": "localhost"}],
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    },
    env_production: {
      NODE_ENV: 'production',
      WLINK_SVC: 'sandbox',
      DB: "mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/wlink",
      REDIS: [{"port": 7379, "host": "172.26.9.73"}, {"port": 7379, "host": "172.26.3.26"},{"port": 7379, "host": "172.26.40.122"}],
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    }
  },{
    name: 'system_daemon',
    script: 'system_daemon.js',
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      WLINK_SVC: 'sandbox',
      SERVICE_CODE: 'WLINK_A0001',
      DB: "mongodb://192.168.16.188/testdb",
      REDIS: [{"port": 7000, "host": "localhost"}, {"port": 7001, "host": "localhost"},{"port": 7002, "host": "localhost"}],
      JWT_SECRET: "18867b09-88d2-45ba-9ddc-00314dde41a9",
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    },
    env_testing: {
      NODE_ENV: 'testing',
      WLINK_SVC: 'sandbox',
      SERVICE_CODE: 'WLINK_A0001',
      DB: "mongodb://127.0.0.1:27017/wlink",
      REDIS: [{"port": 7000, "host": "localhost"}, {"port": 7001, "host": "localhost"},{"port": 7002, "host": "localhost"}],
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    },
    env_production: {
      NODE_ENV: 'production',
      WLINK_SVC: 'sandbox',
      SERVICE_CODE: 'WLINK_A0001',
      DB: "mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/wlink",
      REDIS: [{"port": 7379, "host": "172.26.9.73"}, {"port": 7379, "host": "172.26.3.26"},{"port": 7379, "host": "172.26.40.122"}],
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug"
    }
  }],

  deploy : {
    production : {
      user : 'ubuntu',
      host : ['api1.wavlink.xyz', 'api2.wavlink.xyz'],
      ref  : 'origin/master',
      repo : 'https://zhuwei_schweini@bitbucket.org/zhuwei_schweini/zhuwei_wlink_wsock.git',
      path : '/home/ubuntu/zhuwei/wlink_wsock',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env production'
    },
    testing : {
      key  : "/home/kobe/WAVLINK-southeast-1-2.pem",
      user : 'ubuntu',
      host : '18.136.3.87',
      ref  : 'origin/master',
      repo : 'https://zhuwei_schweini@bitbucket.org/zhuwei_schweini/zhuwei_wlink_wsock.git',
      path : '/home/ubuntu/service/zhuwei/wlink_wsock',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env testing'
    },
    staging: {
      user : 'kobe',
      host : '192.168.16.188',
      ref  : 'origin/master',
      repo : 'https://zhuwei_schweini@bitbucket.org/zhuwei_schweini/zhuwei_wlink_wsock.git',
      path : '/home/kobe/zhuwei_test/wlink_wsock',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env development'
    },
    dev: {}
  } 
};
